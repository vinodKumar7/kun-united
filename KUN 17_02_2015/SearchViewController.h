//
//  SearchViewController.h
//  GrabItNow
//
//  Created by MyRewards on 11/25/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "CategoryXMLParser.h"
#import "ProductListParser.h"

@class HomeViewController;

@interface SearchViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, ASIHTTPRequestDelegate, CategoryXMLParserDelegate, ProductListXMLParserDelegate>

@property (nonatomic, strong) IBOutlet UILabel *locationLabel;
@property (nonatomic, strong) IBOutlet UILabel *keywordLabel;
@property(nonatomic, strong)UITextField *currentTextField;

@property (nonatomic, strong) IBOutlet UITextField *locationEditTextField;
@property (nonatomic, strong) IBOutlet UITextField *keywordEditTextField;
@property (nonatomic, strong) UITextField *currentTextfield;
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet UIView *toolBar;
@property (nonatomic, strong) IBOutlet UIView *settingsView;
@property (nonatomic, strong) IBOutlet UIView *infoView;
@property (nonatomic, strong) IBOutlet UIView *activityView;

@property (nonatomic, strong) IBOutlet UIButton *infoButton;
@property (strong, nonatomic) IBOutlet UIButton *resetButton;

@property (nonatomic, strong) HomeViewController *mainController;



- (IBAction)infoButtonTapped:(id)sender;

- (IBAction)settingsButtonTapped:(id)sender;
- (IBAction)searchButtonTapped:(id)sender;
- (IBAction)resetButtonTapped:(id)sender;

@end
