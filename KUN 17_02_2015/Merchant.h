//
//  Merchant.h
//  GrabItNow
//
//  Created by Monish Kumar on 14/01/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
@interface Merchant : NSObject
@property(nonatomic, strong) NSString *mId;
@property(nonatomic, strong) NSString *mname;
@property(nonatomic, strong) NSString *mContactFirstName;
@property(nonatomic, strong) NSString *mContactLastName;
@property(nonatomic, strong) NSString *mContactTitle;
@property(nonatomic, strong) NSString *mContactPosition;
@property(nonatomic, strong) NSString *mMailAddress1;
@property(nonatomic, strong) NSString *mMailAddress2;
@property(nonatomic, strong) NSString *mMailSuburb;
@property(nonatomic, strong) NSString *mState;
@property(nonatomic, strong) NSString *mPostCode;
@property(nonatomic, strong) NSString *mCountry;
@property(nonatomic, strong) NSString *mCity;
@property(nonatomic, strong) NSString *mEmail;
@property(nonatomic, strong) NSString *mPhone;
@property(nonatomic, strong) NSString *mMobile;
@property(nonatomic, strong) NSString *mFax;
@property(nonatomic, strong) NSString *mLatitude;
@property(nonatomic, strong) NSString *mLongitude;
@property (nonatomic, readwrite) CLLocationCoordinate2D coordinate;
@end
