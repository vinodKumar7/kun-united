//
//  ProductViewController.h
//  GrabItNow
//
//  Created by MyRewards on 12/25/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import "Product.h"
#import "ASIHTTPRequest.h"
#import "MerchantListParser.h"
#import "Merchant.h"


@protocol ProductViewDelegate;

@interface ProductViewController : UIViewController <ASIHTTPRequestDelegate, UIWebViewDelegate, UIAlertViewDelegate, UIGestureRecognizerDelegate,MerchantXMLParserDelegate,UITableViewDataSource,UITableViewDelegate>
{
    __unsafe_unretained id <ProductViewDelegate> delegate;
    NSMutableArray *merchantList;
    BOOL fromNearbyMe;
    
    CLLocationCoordinate2D Location;
    CLLocationCoordinate2D userLocation;
    CLLocationManager *locationManager;
    
    
    int phnoCount;
    int routeCheck;
    NSArray *phArray;
    NSMutableArray *phMutableArray;
}
@property(nonatomic,readwrite)CLLocationCoordinate2D Location;
@property(nonatomic,readwrite)CLLocationCoordinate2D userLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, nonatomic) UITextField *membership_TextField;

@property (nonatomic,readwrite) BOOL fromNearbyMe;
@property (nonatomic,readwrite) BOOL fromDailyDeals;
@property (nonatomic,readwrite) int routeCheck;

@property (unsafe_unretained) id <ProductViewDelegate> delegate;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andProduct:(Product *) pro;

@property (nonatomic, strong) IBOutlet UIView *containerView;

@property (nonatomic,strong) IBOutlet UIScrollView * scroller;
@property (strong, nonatomic) IBOutlet UIImageView *couponbackImageView;
@property (strong, nonatomic) IBOutlet UIButton *redeemnow;

@property (nonatomic,strong) IBOutlet UIView * imgContainerView;
@property (nonatomic,strong) IBOutlet UIImageView * imageView;
@property (nonatomic,strong) IBOutlet UIButton *favButton;
@property (nonatomic,strong) IBOutlet UIButton *redeemButton;
@property (nonatomic,strong) IBOutlet UIButton *couponButton;
@property (strong, nonatomic) IBOutlet UIImageView *couponBgImageView;

@property (nonatomic,strong) IBOutlet UIView * mainContentHolder;
@property (nonatomic,strong) IBOutlet UIButton *offerButton;
@property (nonatomic,strong) IBOutlet UIButton *contactButton;
@property (nonatomic,strong) IBOutlet UIButton *addressButton;

@property (nonatomic,strong) IBOutlet UILabel *noImageLabel;
@property (nonatomic, strong) IBOutlet UIView *couponView;
@property (nonatomic, strong) IBOutlet UILabel *discountLabel;
@property (nonatomic, strong) IBOutlet UILabel *memNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *memNumberLabel;
@property (nonatomic, strong) IBOutlet UILabel *clientLabel;
@property (nonatomic, strong) IBOutlet UILabel *productNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *productOfferLabel;
@property (nonatomic,strong) IBOutlet UIActivityIndicatorView *imageActivityIndicator;
@property(strong,nonatomic) IBOutlet UIImageView *coupounbackgrndImageView;
@property (nonatomic, strong) UITableView *merchantTableView;
@property (nonatomic, strong) UIView *mailComposeView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *redeemActivityIndicator;
// Coupon views
@property (nonatomic,strong) IBOutlet UIImageView * couponCardBackBg_ImageView;
@property (nonatomic,strong) IBOutlet UIImageView * couponMerchantImage;
@property (nonatomic,strong) IBOutlet UIImageView * couponImageView;
@property (nonatomic, strong) IBOutlet UILabel *TandCLabel;

@property (strong, nonatomic) IBOutlet UIButton *frontredeemButton;

@property (nonatomic, strong) IBOutlet UIButton *TandCButton;
@property (nonatomic, strong) IBOutlet UIView *TandCView;
@property (nonatomic, strong) IBOutlet UIView *couponFrontView;
@property (nonatomic, strong) IBOutlet UIView *couponBackView;
@property (nonatomic, strong) IBOutlet UITextView *couponTandCTextView;
@property (nonatomic, strong) IBOutlet UIButton *TandCBackButton;
@property (nonatomic, strong) IBOutlet UIView *couponFlipView;
@property (nonatomic, strong) UIImage *myCouponImage;
@property (nonatomic,strong) IBOutlet UIImageView * couponCardBg_ImageView;


-(IBAction)handleCouponTap:(id)sender;
- (IBAction) termsAndCondButtonPressed:(id)sender;
- (IBAction) termsAndCondBackButtonPressed:(id)sender;

- (IBAction) favoritesButtonPressed:(id) sender;
- (IBAction) redeemButtonPressed:(id) sender;
- (IBAction) couponButtonPressed:(id) sender;

- (IBAction) offerButtonPressed:(id) sender;
- (IBAction) contactButtonPressed:(id) sender;
- (IBAction) addressButtonPressed:(id) sender;
- (IBAction) mapButtonPressed:(id) sender;

- (void) updateUIWithProductDetails:(Product *) aProduct;

- (IBAction)couponreedmButtonPressed:(id)sender;

@end


@protocol ProductViewDelegate <NSObject>

- (void) productAddedToFavorites:(Product *) pro;
- (void) productremovedFromFavorites:(Product *) pro;
//- (void) showMailComposer:(NSArray *)recepients withBody:(NSString *)text;
- (void) showProductCouponForProduct:(Product *) pro couponImage:image;

@end


