//
//  MerchantListParser.m
//  GrabItNow
//
//  Created by Monish Kumar on 14/01/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "MerchantListParser.h"
#import "Merchant.h"

#import <CoreLocation/CoreLocation.h>

@interface MerchantListParser()
{
    NSMutableArray *merchantsList;
    NSMutableString *charString;
    Merchant *currMerchant;
}

@property (nonatomic, strong) NSMutableArray *merchantsList;

@end

@implementation MerchantListParser
@synthesize delegate;
@synthesize merchantsList;


- (void)parserDidStartDocument:(NSXMLParser *)parser {
    merchantsList = [[NSMutableArray alloc] init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    charString = nil;
    
    if ([elementName isEqualToString:@"merchant"]) {
        currMerchant = [[Merchant alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"id"]) {
        currMerchant.mId = finalString;
    }
    else if ([elementName isEqualToString:@"name"]) {
        currMerchant.mname = finalString;
    }
    else if ([elementName isEqualToString:@"address1"]) {
        currMerchant.mMailAddress1 = finalString;
    }
    else if ([elementName isEqualToString:@"address2"]) {
        currMerchant.mMailAddress2 = finalString;
    }
    else if ([elementName isEqualToString:@"suburb"]) {
        currMerchant.mMailSuburb = finalString;
    }
    else if ([elementName isEqualToString:@"state"]) {
        currMerchant.mState = finalString;
    }
    else if ([elementName isEqualToString:@"city"]) {
        currMerchant.mCity = finalString;
    }
    else if ([elementName isEqualToString:@"country"]) {
        currMerchant.mCountry = finalString;
    }
    else if ([elementName isEqualToString:@"postcode"]) {
        currMerchant.mPostCode = finalString;
    }
    else if ([elementName isEqualToString:@"phone"]) {
        currMerchant.mPhone = finalString;
    }
       else if ([elementName isEqualToString:@"latitude"]) {
        
        CLLocationCoordinate2D localCoo = currMerchant.coordinate;
        localCoo.latitude = [finalString doubleValue];
        currMerchant.coordinate = localCoo;
    }
    else if ([elementName isEqualToString:@"longitude"]) {
        
        CLLocationCoordinate2D localCoo = currMerchant.coordinate;
        localCoo.longitude = [finalString doubleValue];
        currMerchant.coordinate = localCoo;
    }
   
    else if ([elementName isEqualToString:@"merchant"]) {
        
        [merchantsList addObject:currMerchant];
        currMerchant = nil;
        
        
    }
    else if([elementName isEqualToString:@"merchants"]) {
        [parser abortParsing];
        NSLog(@"\n Mechants list in Parser = %@", merchantsList);
        [delegate parsingMerchantListFinished:merchantsList];
        
    }
    
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    [delegate parsingMerchantListXMLFailed];
}


@end
