//
//  LikeUsViewController.h
//  KUN
//
//  Created by vairat on 10/11/14.
//  Copyright (c) 2014 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LikeUsViewController : UIViewController
- (IBAction)socialNetworkButtonTapped:(id)sender;

@end
