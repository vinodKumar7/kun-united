//
//  GNoticeBoard.m
//  TWU
//
//  Created by vairat on 11/07/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "GNoticeBoard.h"

@implementation GNoticeBoard

@synthesize  notice_Id;
@synthesize  user_Id;
@synthesize  client_Id;
@synthesize  subject;
@synthesize  details;
@synthesize  created_Date;
@synthesize  start;
@synthesize  end;
@synthesize  sort;
@synthesize  active;
@synthesize index;

@end
