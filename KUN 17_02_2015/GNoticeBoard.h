//
//  GNoticeBoard.h
//  TWU
//
//  Created by vairat on 11/07/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GNoticeBoard : NSObject
{
    NSString  *notice_Id;
    NSString  *user_Id;
    NSString  *client_Id;
    NSString  *subject;
    NSString  *details;
    NSString  *created_Date;
    NSString  *start;
    NSString  *end;
    NSString  *sort;
    NSString  *active;
    NSInteger  index;
    
}

@property(nonatomic, strong)NSString  *notice_Id;
@property(nonatomic, strong)NSString  *user_Id;
@property(nonatomic, strong)NSString  *client_Id;
@property(nonatomic, strong)NSString  *subject;
@property(nonatomic, strong)NSString  *details;
@property(nonatomic, strong)NSString  *created_Date;
@property(nonatomic, strong)NSString  *start;
@property(nonatomic, strong)NSString  *end;
@property(nonatomic, strong)NSString  *sort;
@property(nonatomic, strong)NSString  *active;
@property(nonatomic, readwrite)NSInteger index;

@end
