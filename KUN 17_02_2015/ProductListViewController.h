//
//  ProductListViewController.h
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IconDownloader.h"
#import "Category.h"
#import "ASIHTTPRequest.h"
#import "ProductListParser.h"
#import "ProductDataParser.h"
#import "ProductViewController.h"
//#import <MessageUI/MessageUI.h>
//#import <MessageUI/MFMailComposeViewController.h>

typedef enum{
    ProductListTypeNone,
    ProductListTypeSearch,
    ProductListTypeNearbyMe,
    ProductListTypeOffers,
    ProductListTypeMyFavorites
} ProductListType;



@interface ProductListViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, IconDownloaderDelegate, UIScrollViewDelegate, ASIHTTPRequestDelegate, ProductListXMLParserDelegate,ProductXMLParserDelegate, ProductViewDelegate,UIAlertViewDelegate,CLLocationManagerDelegate>
{

    NSMutableArray *productsList;
    ProductListType productListType;
    IBOutlet UILabel *noProductsFoundLabel;
    
    CLLocationCoordinate2D userLocation;
    CLLocationManager *locationManager;

}
@property (nonatomic, strong) IBOutlet UILabel *noProductsFoundLabel;
@property (nonatomic, readwrite) ProductListType productListType;
@property (nonatomic, strong) NSMutableArray *productsList;
@property (strong, nonatomic) IBOutlet UIImageView *couponbackImageView;
@property (nonatomic, strong) IBOutlet UITableView *tblView;
@property (nonatomic, strong) IBOutlet UIView *couponView;
@property (strong, nonatomic) IBOutlet UIImageView *couponCardBg_ImageView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView *redeemActivityIndicator;
@property(nonatomic,readwrite)CLLocationCoordinate2D userLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (strong, nonatomic) IBOutlet UIImageView *coupounbgImageView;
@property (strong, nonatomic) IBOutlet UIImageView *couponCardBackBg_ImageView;
@property(strong,nonatomic) IBOutlet UITextField  *membership_TextField;
// Coupon views
@property (nonatomic,strong) IBOutlet UIImageView * couponMerchantImage;
@property (nonatomic,strong) IBOutlet UIImageView * couponImageView;
@property (nonatomic, strong) IBOutlet UILabel *TandCLabel;

@property (nonatomic, strong) IBOutlet UIButton *TandCButton;
@property (nonatomic, strong) IBOutlet UIView *TandCView;
@property (nonatomic, strong) IBOutlet UIView *couponFrontView;
@property (nonatomic, strong) IBOutlet UIView *couponBackView;
@property (nonatomic, strong) IBOutlet UITextView *couponTandCTextView;
@property (nonatomic, strong) IBOutlet UIButton *TandCBackButton;
@property (nonatomic, strong) IBOutlet UIView *couponFlipView;
@property (nonatomic,strong) IBOutlet UIButton *redeemButton;
@property (strong, nonatomic) IBOutlet UIButton *frontredeemButton;
@property (nonatomic, strong) IBOutlet UILabel *discountLabel;
@property (nonatomic, strong) IBOutlet UILabel *memNameLabel;
@property (nonatomic, strong) IBOutlet UILabel *memNumberLabel;
@property (nonatomic, strong) IBOutlet UILabel *clientLabel;
@property (nonatomic, strong) IBOutlet UILabel *productNameLabel;
@property (nonatomic, strong) UIImage *myImage;

@property (nonatomic,readwrite) BOOL fromSearch;


- (IBAction) termsAndCondButtonPressed:(id)sender;
- (IBAction) termsAndCondBackButtonPressed:(id)sender;
- (IBAction) redeemButtonPressed:(id) sender;
- (IBAction)couponredeemButtonPressed:(id)sender;



- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil productListType:(ProductListType) productListType;

- (void) setInitialProductsList:(NSArray *) plist;
- (void) setSearchCategoryID:(NSString *) catId keyword:(NSString *) key location:(NSString *) location;

@end
