//
//  MyCardViewController.h
//  GrabItNow
//
//  Created by Monish Kumar on 06/01/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface MyCardViewControllerGrab : UIViewController<ASIHTTPRequestDelegate>
{
    
}
@property(nonatomic,strong)IBOutlet UIImageView *background;
@property(nonatomic,strong)IBOutlet UIImageView *myCardImageView;
@property(nonatomic,strong)IBOutlet UILabel *userNameLabel;
@property(nonatomic,strong)IBOutlet UILabel *clientIdLabel;
@property(nonatomic,strong)IBOutlet UILabel *userNameLabel1;
@end
