//
//  ZSPinAnnotation.h
//  
//
//  Created by MyRewards on 3/6/13.
//
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>

@interface ZSPinAnnotation : NSObject
+ (UIImage *)pinAnnotationWithColor:(UIColor *)color;
+ (UIImage *)pinAnnotationWithRed:(int)red green:(int)green blue:(int)blue;

@end
