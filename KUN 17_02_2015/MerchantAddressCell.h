//
//  MerchantAddressCell.h
//  GrabItNow
//
//  Created by MyRewards on 3/14/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MerchantAddressCell : UITableViewCell
{
    UILabel *merchantAddress;
    UIButton *mapButton;
}
@property(nonatomic,strong) IBOutlet UILabel *maplabel;
@property(nonatomic,strong)IBOutlet UILabel *merchantAddress;
@property(nonatomic,strong)IBOutlet UIButton *mapButton;
@end
