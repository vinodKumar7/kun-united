//
//  MyUnion.h
//  TWU2
//
//  Created by MyRewards on 2/23/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface MyUnion : NSManagedObject

@property (nonatomic, strong) NSString * htmlContent;

@end
