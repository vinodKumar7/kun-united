//
//  NearByViewController.h
//  GrabItNow
//
//  Created by Monish Kumar on 06/01/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import <MapKit/MKMapview.h>
#import <CoreLocation/CoreLocation.h>
#import "ProductListParser.h"
#import "ProductViewController.h"
#import "ProductDataParser.h"
#import "Product.h"
@interface NearByViewController : UIViewController<ASIHTTPRequestDelegate, MKMapViewDelegate, CLLocationManagerDelegate,ProductListXMLParserDelegate,ProductViewDelegate,ProductXMLParserDelegate>
{
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentLocation;
    ProductViewController *productController;
    
}

@property(nonatomic,strong)IBOutlet MKMapView *mkView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, readwrite) CLLocationCoordinate2D currentLocation;
@property (nonatomic, strong) ProductViewController *productController;
@property (nonatomic, strong) Product *prodDetail;

@property (nonatomic, strong) IBOutlet UIView *activityView;




@end
