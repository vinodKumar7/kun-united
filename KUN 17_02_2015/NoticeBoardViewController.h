//
//  NoticeBoardViewController.h
//  GrabItNow
//
//  Created by MyRewards on 3/25/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "NoticeBoardDataParser.h"
#import "NoticeBoard.h"
#import "NSString_stripHtml.h"

@class NoticeBoard;
@interface NoticeBoardViewController : UIViewController<ASIHTTPRequestDelegate,NoticeBoardXMLParserDelegate,UITableViewDataSource, UITableViewDelegate>
{
    NoticeBoard *notice;
    NSMutableArray *noticesListArray;
    NSMutableArray *readMsgs_Array;
    NSMutableArray *readMsgInd_Array;
}
@property (nonatomic, strong)IBOutlet UIWebView *noticeWebView;
@property (nonatomic, strong) NoticeBoard *notice;
@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (nonatomic, strong) NSMutableArray *noticesListArray;
@property (nonatomic,strong)NSMutableArray *readMsgs_Array;
@property (nonatomic,strong)NSMutableArray *readMsgInd_Array;
@property (nonatomic, strong)IBOutlet UITableView *noticesTable;
@property (nonatomic, strong) IBOutlet UIView *noticeContentView;
@property (nonatomic, strong) IBOutlet UITextView *noticeContentTextView;
@end
