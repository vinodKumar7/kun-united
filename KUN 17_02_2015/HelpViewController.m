//
//  HelpViewController.m
//  GrabItNow
//
//  Created by Venkat Sasi Allamraju on 05/03/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "HelpViewController.h"
#import "DMLazyScrollView.h"
#import "AppDelegate.h"
#import "GrabItBaseViewController.h"
#import "SwitchTabBarController.h"

@interface HelpViewController ()<DMLazyScrollViewDelegate>{
    
    AppDelegate *appDelegate;
    DMLazyScrollView* lazyScrollView;
    NSMutableArray*    viewControllerArray;
    NSArray *pageImages_Array;
    NSArray *pageContent_Array;
    
}
@property(nonatomic,strong)NSArray *pageContent_Array;
@property(nonatomic,strong)NSArray *pageImages_Array;
//- (void) dismissActivityView;

@end

@implementation HelpViewController

@synthesize pageContent_Array;
@synthesize coarouselPage_Control;
@synthesize screens_ImageView;
@synthesize pageImages_Array;
@synthesize baseVC;
//=========================DELEGATE METHODS=============================//

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    
//    [self.navigationController setNavigationBarHidden:YES];
    
    if (appDelegate.reader.view.tag == 182) {
        self.switchTabBarController.tabBar.hidden = NO;
        appDelegate.reader.view.tag = nil;
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    
    UIImage *myImage2 = [UIImage imageNamed:@"scan1.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(scaningBarCode) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    
    TitleLabel.text = @"Help";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    TitleLabel.textColor=[UIColor  whiteColor];
    
    self.navigationItem.titleView = TitleLabel;
    
    
}

-(void)back
{
    [appDelegate showhomeScreen];
    //    [self.navigationController popViewControllerAnimated:NO];
}

-(void)scaningBarCode
{
    
    [appDelegate scanQRcode];
    // present and release the controller
    //    [self presentViewController:appDelegate.reader animated:YES completion:nil];
    [self.view.window.rootViewController presentViewController:appDelegate.reader animated:YES completion:nil];
    self.switchTabBarController.tabBar.hidden = YES;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    
    if(![appDelegate isIphone5])
    {
        NSLog(@"Iphone4...");
        CGRect Frame = self.screens_ImageView.frame;
        Frame.origin.x = 0;
        //  Frame.size.height = Frame.size.height-150;
        self.screens_ImageView.frame = Frame;
        CGRect Frame1 = self.coarouselPage_Control.frame;
        NSLog(@"position is %f ",Frame1.origin.y);
        Frame1.origin.y=340;
        self.coarouselPage_Control.frame = Frame1;

        
    }
    
    //configuring pages
    NSString *page1 =@"<h4> Welcome to your Rewards program... &nbsp&nbsp </h4> &nbsp&nbspEnjoy ongoing, uninterrupted acess to thousands of discounts on your phone. <br><br>&#9733; You can search by category, by keyword <br>&nbsp&nbsp&nbsp&nbsp and also search local with \"What around <br>&nbsp&nbsp&nbsp&nbsp me\".<br><br>&#9733; Select a merchant offer you like and <br>&nbsp&nbsp&nbsp&nbsp present the coupon on your phone at the <br>&nbsp&nbsp&nbsp&nbsp time of settling your bill. Always check the <br>&nbsp&nbsp&nbsp&nbsp terms of the offer.<br><br>&#9733; Some merchants only offer their services <br>&nbsp&nbsp&nbsp&nbsp online and in most cases should be able <br>&nbsp&nbsp&nbsp&nbsp to click through to their online offerings.<br><br><br><br><br><br><br><br><br><br><br><br>";
    
    NSString *page2 = @"<h4> Searching  for discounts is EASY! </h4> &#9733; Go to <b>Search</b> by category  in the menu... <br><br>&#9733;  Select an <b>offer category</b> that interests <br>&nbsp&nbsp&nbsp&nbsp you... <br><br>&#9733;  Include the <b>suburb or city</b> you  are <br>&nbsp&nbsp&nbsp&nbsp searching... <br><br>&#9733;  Add a <b>keyword</b> to help refine your  <br>&nbsp&nbsp&nbsp&nbsp  search... <br><br><br><br><br><br><br><br><br>";
    
    
    NSString *page3 = @"<h4> What's Around Me! </h4> <br>This local search function show offers that are around you wherever you are. <br><br>&#9733; You can expand or reduce the search area &nbsp&nbsp&nbsp&nbsp on screen.<br><br>&#9733;  The offer drop pins have an icon <br>&nbsp&nbsp&nbsp&nbsp representing type of offer.<br><br>&#9733; For example dining has a knife and fork <br>&nbsp&nbsp&nbsp&nbsp icon pin. <br><br>&#9733; The icon range are shown in the main <br>&nbsp&nbsp&nbsp&nbsp search function. <br><br><br><br><br><br><br><br><br>";
    
    
    NSString *page4 = @"<h4> Search Results wherever you are... </h4> <br> &#9733; Your search results will be retrieved.<br> <br>&#9733; If you don't get a result , widen your <br>&nbsp&nbsp&nbsp&nbsp search parameters.<br> <br>&#9733; You will receive a list of offers that <br>&nbsp&nbsp&nbsp&nbsp   meet the search criteria <br><br>&#9733; Click on your preferred supplier in the <br>&nbsp&nbsp&nbsp&nbsp  search results <br> <br>&#9733; This will take type the offer page <br>&nbsp&nbsp&nbsp&nbsp  of that merchant <br><br><br><br><br><br><br><br><br><br><br>";
    
    NSString *page5 = @"<h4> Your selected offer...  </h4>&#9733; You will  now see the offer  details, <br> &nbsp&nbsp&nbsp&nbsp including the address, contact details <br>&nbsp&nbsp&nbsp&nbsp  of the merchant, and all terms and <br> &nbsp&nbsp&nbsp&nbsp conditions.<br> <br>&#9733;   If you like the offer add it to \"your <br>&nbsp&nbsp&nbsp&nbsp favourites\" by clicking the heart. <br><br><br><br><br><br><br><br><br><br><br>";
    
    NSString *page6 = @"<h4> Your Coupon  </h4>&#9733; You now have the offer coupon on your <br> &nbsp&nbsp&nbsp&nbsp phone. How cool's that ! <br><br>&#9733;   Click the T&C button to see the terms and <br> &nbsp&nbsp&nbsp&nbsp conditions of use that apply to that <br> &nbsp&nbsp&nbsp&nbsp merchant.<br><br>&#9733; Read all offer terms and conditions <br> &nbsp&nbsp&nbsp&nbsp carefully before presentation to make sure <br> &nbsp&nbsp&nbsp&nbsp that you use the correct redemption option <br> &nbsp&nbsp&nbsp&nbsp as <b>some offers can only be redeemed <br> &nbsp&nbsp&nbsp&nbsp online</b>.<br><br>&#9733; Each coupon offer  has a <b> \"Merchant <br>&nbsp&nbsp&nbsp&nbsp Redeem \" </b> button. <b>THIS &nbsp&nbspIS FOR <br>&nbsp&nbsp&nbsp&nbsp MERCHANT USE ONLY </b>. Some offers <br>&nbsp&nbsp&nbsp&nbsp only allow a SINGLE USE, so don't waste <br>&nbsp&nbsp&nbsp&nbsp it by pressing the button. As once it's <br>&nbsp&nbsp&nbsp&nbsp gone... it's gone!<br><br> Happy saving... but if all else fails send an email to: <br> <a href=\"mailto:support@therewardsteam.com\">support@therewardsteam.com</a>.   <br><br><br><br><br><br><br><br><br><br>";
    
    
    self.pageContent_Array = [[NSArray alloc]initWithObjects:page1,page2,page3,page4,page5,page6,nil];
    
    NSLog(@" page array count ::%d",[self.pageContent_Array count]);
    
    //configuring Images
    
    self.pageImages_Array = [[NSArray alloc]initWithObjects:@"hImage1",@"hImage2",@"hImage3",@"hImage41",@"hImage5",@"hImage6", nil];
    
    
    
    // PREPARE PAGES
    NSUInteger numberOfPages = 6;
    viewControllerArray = [[NSMutableArray alloc] initWithCapacity:numberOfPages];
    for (NSUInteger k = 0; k < numberOfPages; k++)
    {
        [viewControllerArray addObject:[NSNull null]];
    }
    
    // PREPARE LAZY VIEW
    CGRect rect = CGRectMake(0, 0, self.view.frame.size.width, 548.0f);
    lazyScrollView = [[DMLazyScrollView alloc] initWithFrame:rect];
    
    
    
    lazyScrollView.dataSource = ^(NSUInteger index)
    {
        return [self controllerAtIndex:index];
    };
    lazyScrollView.numberOfPages = numberOfPages;
    lazyScrollView.controlDelegate = self;
    [self.view addSubview:lazyScrollView];
    
    
    [self.view bringSubviewToFront:self.coarouselPage_Control];
    // if(![appDelegate isIphone5])
    //self.screens_ImageView.frame=CGRectMake(0,-20 , 320, 174);
    // self.screens_ImageView.image = [self.pageImages_Array objectAtIndex:0];
    
}
//===================================================//
//==================METHOD TO LOAD HELP PAGES  ============================//

- (UIViewController *) controllerAtIndex:(NSInteger) index
{
    
    UIWebView   *helpWebView;
    
    if (index > viewControllerArray.count || index < 0) return nil;
    
    id res = [viewControllerArray objectAtIndex:index];
    if (res == [NSNull null])
    {
        UIViewController *contr = [[UIViewController alloc] init];
        contr.view.backgroundColor = [UIColor clearColor];
        
        if ([appDelegate isIphone5])
            helpWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0f,0.0f, 320.0f, 566.0f)];
        else
            helpWebView = [[UIWebView alloc] initWithFrame:CGRectMake(0.0f,0.0f, 320.0f, 440.0f)];
        helpWebView.backgroundColor = [UIColor whiteColor];
        helpWebView.delegate=self;
        NSString * helpText = [self.pageContent_Array objectAtIndex:index];
        // NSLog(@"helpText :%@ at index: %d",helpText,index);
        
        helpText = [NSString stringWithFormat:@"<html> \n"
                    "<head> \n"
                    "<style type=\"text/css\"> \n"
                    "body {font-family: \"%@\"; font-size: %@; text-align: %@; font-color: \"%@\"}\n"
                    "</style> \n"
                    "</head> \n"
                    "<body>%@</body> \n"
                    "</html>", @"Helvetica", [NSNumber numberWithInt:15],@"left", @"#D5D3D3", helpText];
        
        
        //  NSString *imagePath = [[NSBundle mainBundle] pathForResource:@"GIN" ofType:@"png"];
        
        
        
        //NSString *htmlString = [NSString stringWithFormat:@"<html><body<br><b>%@</b></body></html>",  helpText];
        //<html><body p style='color:red' text=\"#FFFFFF\" face=\"Bookman Old Style, Book Antiqua, Garamond\" size=\"5\">%@</body></html>"
        NSString *imageName = [self.pageImages_Array objectAtIndex:index];
        NSString *imagePath = [[NSBundle mainBundle] pathForResource:imageName ofType:@"png"];
        NSLog(@"imagePath::%@",imagePath);
        
        NSString *htmlString = [NSString stringWithFormat:@"<html><body p style='color:#000000'><img src=\"file://%@\"><style type='text/css'>img { max-width: 304%; width: auto; height: auto; }</style>%@</body></html>",imagePath,helpText];
        
        
        [helpWebView loadHTMLString:htmlString  baseURL:nil];
        
        //[helpWebView setOpaque:NO];
        [contr.view addSubview:helpWebView];
        
        [viewControllerArray replaceObjectAtIndex:index withObject:contr];
        return contr;
    }
    return res;
}
//===================================================//
//=============METHOD TO PASS CURRENT PAGE TO PAGE CONTROL=================//

- (void)lazyScrollView:(DMLazyScrollView *)pagingView currentPageChanged:(NSInteger)currentPageIndex
{
    NSLog(@"currentPageIndex:: %d",currentPageIndex);
    
    
    self.coarouselPage_Control.currentPage = currentPageIndex;
    
    //    if(currentPageIndex % 2 == 0)
    //        self.screens_ImageView.image = [UIImage imageNamed:@"image2.png"];
    //    else
    //        self.screens_ImageView.image = [UIImage imageNamed:@"image1.png"];
    // self.screens_ImageView.image = [self.pageImages_Array objectAtIndex:currentPageIndex];
    
}
//===================================================//

- (BOOL)webView:(UIWebView *)webView1 shouldStartLoadWithRequest:(NSURLRequest *)request  navigationType:(UIWebViewNavigationType)navigationType{
    
    NSLog(@"shouldStartLoadWithRequest");
    
    NSURL *requestURL = [request URL];
    NSString *str_url = [requestURL absoluteString];
    NSLog(@"%@",str_url);
    //if([[[request URL] scheme] isEqual:@"mailto"]){
    if (UIWebViewNavigationTypeLinkClicked == navigationType)
    {
        
        GrabItBaseViewController *ref=[appDelegate.refArray objectAtIndex:0];
        
        [ref showMailComposer];
    }
    return YES;
}

-(void) showMailComposer
{
    
    if ([MFMailComposeViewController canSendMail]) {
        NSArray *receipntsArray = [[NSArray alloc]initWithObjects:@"support@therewardsteam.com",nil];
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setToRecipients:receipntsArray];
        [mailViewController setSubject:@"Help"];
        [mailViewController setMessageBody:@"Your message goes here." isHTML:NO];
        
        /*  [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:mailViewController
         animated:YES
         completion:nil];
         
         UINavigationController *passNVc=[[UINavigationController alloc] initWithRootViewController:passcodeVc];
         
         UINavigationController *rootVc=(UINavigationController *) self.window.rootViewController;
         rootVC.visibleViewController presentViewController:passcodeNavigationVC animated:YES completion:^{}];
         
         
         */
        // mailViewController.view.superview.frame = CGRectMake(0,44,320,440);
        [self.navigationController presentModalViewController:mailViewController animated:YES];
        
        // [[baseVC navigationController] setNavigationBarHidden:YES animated:YES];
        
    }
    
    else {
        
        NSLog(@"\nDevice is unable to send email in its current state.");
        UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                             message:@"You will need to setup a mail account on your device before you can send mail!"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [alert_view show];
        
        
    }
    
    
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self dismissModalViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload
{
    [self setCoarouselPage_Control:nil];
    [super viewDidUnload];
}
@end
