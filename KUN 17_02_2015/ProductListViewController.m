//
//  ProductListViewController.m
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "ProductListViewController.h"
#import "ProductCell.h"
#import "Product.h"
#import "AppDelegate.h"
#import "ASIFormDataRequest.h"
#import "NSString_stripHtml.h"
#import "SwitchTabBarController.h"
#import <QuartzCore/QuartzCore.h>


#define ProductCellHeight 64.0;

@interface ProductListViewController ()
{
    NSMutableDictionary * imageDownloadsInProgress;
    int currPageNo;
    int pageSize;
    NSString *searchCategoryId;
    NSString *searchKeyword;
    NSString *searchLocation;
    NSString *redeemProductId;
    NSString *redeemProName;
    AppDelegate *appDelegate;
    ASIFormDataRequest * categorySearchRequest;
    ASIFormDataRequest * productFetchRequest;
    ASIFormDataRequest *redeemRequest;
    ProductListParser *productsXMLParser;
    ProductDataParser *productDataXMLParser;
    BOOL allProductsLoaded;
    NSIndexPath * openProductIndex;
    int indexpathRow;
    
    ProductViewController *productController;
    BOOL search;
    BOOL image;
    BOOL requestCancel;
    NSString *serviceName;
}

@property (nonatomic, strong) NSIndexPath * openProductIndex;
@property (nonatomic, strong) NSMutableDictionary * imageDownloadsInProgress;
@property (nonatomic, strong) ProductViewController *productController;
@property(nonatomic,assign)BOOL image;
- (void)startIconDownload:(Product *)aProduct forIndexPath:(NSIndexPath *)indexPath;
- (void) searchProductsOnKeywordBasis;
- (void) loadMoreProducts;
@end

@implementation ProductListViewController


@synthesize noProductsFoundLabel;
@synthesize productsList;
@synthesize imageDownloadsInProgress;
@synthesize openProductIndex;
@synthesize productController;
@synthesize tblView;
@synthesize productListType;
@synthesize redeemActivityIndicator;
@synthesize locationManager;
@synthesize userLocation;
@synthesize couponCardBackBg_ImageView;
@synthesize couponCardBg_ImageView;
// Coupon related parameters
@synthesize couponView;
@synthesize couponMerchantImage;
@synthesize couponImageView;
@synthesize TandCLabel;
@synthesize TandCButton;
@synthesize myImage;
@synthesize TandCView;
@synthesize couponFrontView;
@synthesize couponBackView;
@synthesize couponTandCTextView;
@synthesize TandCBackButton;
@synthesize couponFlipView;
@synthesize redeemButton;
@synthesize discountLabel;
@synthesize memNameLabel,memNumberLabel,clientLabel;
@synthesize productNameLabel;
@synthesize coupounbgImageView;
@synthesize couponbackImageView;
@synthesize frontredeemButton;
@synthesize image;
@synthesize membership_TextField,fromSearch;
//=========================DELEGATE METHODS=============================//

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil productListType:(ProductListType) productListTypeLocal
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        imageDownloadsInProgress = [[NSMutableDictionary alloc] init];
        productsList = [[NSMutableArray alloc] init];
        allProductsLoaded = NO;
        pageSize = 10;
        indexpathRow=0;
        openProductIndex = nil;
        self.productListType = productListTypeLocal;
        noProductsFoundLabel.hidden = YES;
    }
    return self;
}

- (void)dealloc
{
       if (imageDownloadsInProgress)
       {
           
        NSArray *iconDownloaders = [imageDownloadsInProgress allValues];
        [iconDownloaders makeObjectsPerformSelector:@selector(cancelDownload)];
        [imageDownloadsInProgress removeAllObjects];
           
       }
}
//======================================================//



- (void) configureController
{
    
    switch (productListType)
    {
        case ProductListTypeSearch:
            serviceName = @"search.php";
            search=YES;
            break;
        case ProductListTypeOffers:
            serviceName = @"get_hot_offer.php";
            break;
        default:
            break;
    }
     NSLog(@" configure controller.....");
}

- (void)fetchMyFavoritesAndUpdateList
{
    
    if (productsList.count > 0)
    {
        [productsList removeAllObjects];
    }
    
    [productsList addObjectsFromArray:[appDelegate myFavoriteProducts]];
    
    // If No products, hide table
    if (productsList.count == 0)
    {
        [self noProductsFound:YES];
    }
    else
    {
        [self noProductsFound:NO];
    }
    
    [self.tblView reloadData];
}

- (void) noProductsFound:(BOOL) notFound
{
    
    if (notFound)
    {
        self.tblView.hidden = YES;
        self.noProductsFoundLabel.hidden = NO;
        self.noProductsFoundLabel.layer.cornerRadius = 7.5;
        self.noProductsFoundLabel.layer.borderColor = [UIColor whiteColor].CGColor;
        self.noProductsFoundLabel.layer.borderWidth = 2.0;
    }
    else
    {
        self.tblView.hidden = NO;
        self.noProductsFoundLabel.hidden = YES;
    }
    
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    //userlocation tracking
    couponMerchantImage.layer.cornerRadius = 5.0;
    couponMerchantImage.layer.borderColor = [UIColor colorWithRed:69/255.0 green:141/255.0 blue:230/255.0 alpha:1.0].CGColor;
    couponMerchantImage.layer.borderWidth = 2.0;
    
    image=NO;
    search=NO;
    
    [self configureController];
    NSLog(@"fetching...");
    if (IOS_VERSION<=7)
    {
        CGRect tableframe=self.tblView.frame;
        tableframe.size.height = tableframe.size.height-20;
        self.tblView.frame=tableframe;
    }
    

    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    
    tblView.delegate = self;
    tblView.dataSource = self;
    
//For Title...
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 200, 44)];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.textAlignment = UITextAlignmentCenter;
    TitleLabel.textColor=[UIColor  whiteColor];

    
    
    if (appDelegate.reader.view.tag == 182)
    {
        appDelegate.reader.view.tag = nil;
        
        [self performSelector:@selector(unHideSwitchBar) withObject:nil afterDelay:0.003];
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
    UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [myButton1 setImage:myImage1 forState:UIControlStateNormal];
    myButton1.showsTouchWhenHighlighted = YES;
    myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
    [myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    
    if ((self.productListType == ProductListTypeMyFavorites) && (productsList.count == 0))
    {
        [self fetchMyFavoritesAndUpdateList];
//        [tblView reloadData];
        
        TitleLabel.backgroundColor = [UIColor clearColor];
        //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    
        TitleLabel.text = @"My Favorites";
        self.navigationItem.titleView = TitleLabel;
        
    }
    
    
    // Load Initial products if showing Hot-Offers
    else if((productListType == ProductListTypeOffers) && (productsList.count == 0))
    {
        
        NSLog(@"ViewWill appear.....");
        [self searchProductsOnKeywordBasis];
        
        
        TitleLabel.backgroundColor = [UIColor clearColor];
        //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        
        TitleLabel.text = @"Hot Offers";
        self.navigationItem.titleView = TitleLabel;
    }
    else
    {
        if (self.productListType == ProductListTypeSearch)
        {

            TitleLabel.backgroundColor = [UIColor clearColor];
            //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
            //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
            
            TitleLabel.text = @"My Rewards";
            self.navigationItem.titleView = TitleLabel;
        }
        if (self.productListType == ProductListTypeSearch)
        {

            TitleLabel.backgroundColor = [UIColor clearColor];
            //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
            //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
            
            TitleLabel.text = @"My Rewards";
            self.navigationItem.titleView = TitleLabel;
        }

        NSLog(@"ViewWill appear outside.....");
    }
    
    UIImage *myImage2 = [UIImage imageNamed:@"scan1.png"];
    UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(scaningBarCode) forControlEvents:UIControlEventTouchUpInside];
    myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
    self.navigationItem.rightBarButtonItem = rightButton;
    
    
}

-(void)back
{
    if([categorySearchRequest isExecuting])
    {
        requestCancel = YES;
        [categorySearchRequest cancel];
    }
    else if ([productFetchRequest isExecuting])
    {
        requestCancel = YES;
        [productFetchRequest cancel];
    }
    
    if (self.productListType == ProductListTypeSearch)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
        [appDelegate showhomeScreen];
}

-(void)scaningBarCode
{
    
    [appDelegate scanQRcode];
    // present and release the controller
    //    [self presentViewController:appDelegate.reader animated:YES completion:nil];
    [self.view.window.rootViewController presentViewController:appDelegate.reader animated:YES completion:nil];
    self.switchTabBarController.tabBar.hidden = YES;
}

-(void)unHideSwitchBar
{
    self.switchTabBarController.tabBar.hidden = NO;
}


-(void)getCurrentLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager startUpdatingLocation];
}

- (void) setInitialProductsList:(NSArray *) plist
{
    
    if (!productsList)
    {
        productsList = [[NSMutableArray alloc] initWithArray:plist];
    }
    else
    {
        [productsList removeAllObjects];
        [productsList addObjectsFromArray:plist];
    }

    if([plist count]< 30)
    {
        NSLog(@"Products count less than table size");
        allProductsLoaded = YES;
    }

    
    [tblView reloadData];
}

- (void) setSearchCategoryID:(NSString *) catId keyword:(NSString *) key location:(NSString *) location
{
    searchCategoryId = catId;
    searchKeyword = key;
    searchLocation = location;
}

#pragma mark -- UITableView Datasource & Delegate methods

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    if (categorySearchRequest && (self.productListType != ProductListTypeMyFavorites))
    {
        return (productsList.count + 1);
    }
    NSLog(@"tableView numberOfRowsInSection");
    return productsList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    static NSString *identifier = @"cell";
    static NSString *loadingCellIdentifier = @"loadingCell";
    
    if (indexPath.row == productsList.count)
    {
        
        UITableViewCell *loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
        
        if (!loadingCell)
        {
            loadingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadingCellIdentifier];
        }
        
        loadingCell.textLabel.text = @"Loading ....";
        loadingCell.textLabel.textColor = [UIColor blackColor];
        
        loadingCell.backgroundColor = [UIColor whiteColor];
        
        UIActivityIndicatorView *loadingActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        loadingActivityView.backgroundColor = [UIColor clearColor];
        [loadingActivityView startAnimating];
        loadingCell.accessoryView = loadingActivityView;
        
        loadingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return loadingCell;
        
    }
    
    
    ProductCell *cell = (ProductCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:nil options:nil];
        
        for (UIView *cellview in views)
        {
            if([cellview isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProductCell*)cellview;
            }
        }
        
    }
    
    Product *aPro = [productsList objectAtIndex:indexPath.row];
    
    // Set title and offer text here
    cell.productNameLabel.text = aPro.productName;
    cell.productOfferLabel.text = aPro.productOffer;
    cell.imgLoadingIndicator.hidesWhenStopped = YES;
    cell.accesoryImageView.hidden = NO;
    cell.accesoryImageView.image = [UIImage imageNamed:@"DownAccessory.png"];
    
    // Set different background colors for the cells.
    int rowModuleNo = indexPath.row % 2;
    
    if (rowModuleNo == 0)
    {
        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:0/255.0 green:130/255.0 blue:214/255.0 alpha:1.0];
        cell.productNameLabel.textColor=[UIColor whiteColor];
    }
    else if (rowModuleNo == 1)
    {
        cell.headerContainerView.backgroundColor = [UIColor whiteColor ];
        cell.productNameLabel.textColor=[UIColor colorWithRed:0/255.0 green:130/255.0 blue:214/255.0 alpha:1.0];
    }
    
    
    // Fetch image here
    if (aPro.productImage)
    {
        cell.productImg.image = aPro.productImage;
    }
    else {
        
        if (!aPro.imageDoesntExist)
        {
            // start loading the image
            [self startIconDownload:aPro forIndexPath:indexPath];
            
            // animate indicator
            [cell.imgLoadingIndicator startAnimating];
        }
        else
        {
            // Display NO IMAGE text
            cell.productImg.hidden = YES;
            cell.noImageLabel.hidden = NO;
        }
        
    }
    
    // If valid, make a loadMoreProducts call
    if ([self isLoadMoreValid] && (indexPath.row == (productsList.count - 1)))
    {
        // since last cell reached, load more products
        [self loadMoreProducts];
    }
    
    
    // Show Product controller if indexpath == openProductIndex
    if (openProductIndex && (indexPath.row == openProductIndex.row) )
    {
        
       cell.headerContainerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
        cell.headerContainerView.layer.shadowOpacity = 1.0;
        cell.headerContainerView.layer.shadowOffset = CGSizeMake(0.0, 5.0);
        cell.headerContainerView.layer.shadowRadius = 5.0;
        cell.accesoryImageView.image = [UIImage imageNamed:@"UpAccessory.png"];
        
        if (productController)
        {
            if ([productController.view superview])
            {
                [productController.view removeFromSuperview];
            };
            productController = nil;
        }
        
        ProductViewController *prVC = [[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil andProduct:aPro];
        prVC.delegate = self;
        
        
        CGRect frame = prVC.view.frame;
        float headerHeight = ProductCellHeight;
        
        frame.origin.y = headerHeight;
        frame.size.height = self.tblView.frame.size.height - headerHeight;
        prVC.view.frame = frame;
        
        NSLog(@"** Calculated view frame: %@",NSStringFromCGRect(frame));
        
        [cell.contentView addSubview:prVC.view];
        [cell.contentView bringSubviewToFront:cell.headerContainerView];
        
        productController = prVC;
        
    }
    
    // If favorites list, show remove favorite button.
    if (self.productListType == ProductListTypeMyFavorites)
    {
        cell.removeFavoritebutton.tag = indexPath.row;
        cell.removeFavoritebutton.hidden = NO;
        cell.accesoryImageView.hidden = YES;
        [cell.removeFavoritebutton addTarget:self action:@selector(removeFavoriteAction:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void) removeFavoriteAction:(id) sender
{
    
    int index = [sender tag];
    Product *pro = [productsList objectAtIndex:index];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Remove favorite?" message:[NSString stringWithFormat:@"Do you want to remove %@ from favorites",pro.productName] delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    alert.tag = 22 + index;
    
    [alert show];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (openProductIndex && (indexPath.row == openProductIndex.row) )
    {
        NSLog(@"ROW=%d toBeHeight=%f",indexPath.row,self.tblView.frame.size.height);
        return self.tblView.frame.size.height;
    }
    
    if (indexPath.row == productsList.count)
    {
        return 44.0;
    }
    
    return ProductCellHeight;
}

/** Delegate methods **/
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == [productsList count])
    {
        // Return since this is Loading more cell.
        return;
    }
    
    
    // **** Close the Product Details **** //NSIndexPath *indexPath.row == [productsList count]
    if (openProductIndex)
    {
       
        openProductIndex = nil;
        
        int totalRows = [self.tblView numberOfRowsInSection:0];
        
        // Update tapped cell
        [self.tblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // update cells under it
        NSMutableArray *indexPathsArray = [NSMutableArray array];
        for (int i = (indexPath.row + 1); i < totalRows; i++)
        {
            NSIndexPath *indPath = [NSIndexPath indexPathForRow:i inSection:0];
            [indexPathsArray addObject:indPath];
        }
        
        if (indexPathsArray.count > 0)
        {
            [self.tblView reloadRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
        
        
        self.tblView.scrollEnabled = YES;
        return;
    }
    
    
    
    // **** Open the Product Details **** //
    openProductIndex = indexPath;
    indexpathRow = indexPath.row;
    float toScrollOffsetY = openProductIndex.row * ProductCellHeight;
    
    // update content size
    if ( (self.tblView.contentSize.height - toScrollOffsetY) < self.tblView.frame.size.height)
    {
        self.tblView.contentSize = CGSizeMake(self.tblView.contentSize.width, (self.tblView.frame.size.height + toScrollOffsetY));
        
    }
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.tblView setContentOffset:CGPointMake(self.tblView.contentOffset.x, toScrollOffsetY)];
    } completion:^(BOOL finished) {
        
        int totalRows = [self.tblView numberOfRowsInSection:0];
        
        // Update tapped cell
        [self.tblView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // update cells under it
        NSMutableArray *indexPathsArray = [NSMutableArray array];
        for (int i = (indexPath.row + 1); i < totalRows; i++)
        {
            NSIndexPath *indPath = [NSIndexPath indexPathForRow:i inSection:0];
            [indexPathsArray addObject:indPath];
        }
        
        if (indexPathsArray.count > 0)
        {
            [self.tblView reloadRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationBottom];
        }
        
    }];
    
    self.tblView.scrollEnabled = NO;
    
    Product *pro = [productsList objectAtIndex:indexPath.row];
    [self fetchProductWithProductID:pro.productId];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL) isLoadMoreValid
{
    if (!allProductsLoaded
        && (productListType != ProductListTypeMyFavorites)
        && !openProductIndex
        && !categorySearchRequest
        )
    {
        
        return YES;
    }
    
    // In all other cases cannot load more
    return NO;
}


- (void) loadMoreProducts
{
    NSLog(@"loadMoreProducts");
    
    if ([self isLoadMoreValid])
    {
        [self searchProductsOnKeywordBasis];
    }
}


- (void) fetchProductWithProductID:(NSString *) productId
{
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product.php?id=%@",URL_Prefix,productId];
    
    NSLog(@"Search URL: %@",urlString);
  
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    productFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [productFetchRequest setDelegate:self];
    [productFetchRequest startAsynchronous];
    
    
}


- (void) searchProductsOnKeywordBasis
{
    
    //[self showActivityView];
    
    if (categorySearchRequest)
    {
        // A request is already in progress, return
        return;
    }
    
    NSLog(@"fetching more products");
    
    // Bring the table to the bottom
    if (tblView.contentSize.height > tblView.frame.size.height)
    {
        [tblView setContentOffset:CGPointMake(tblView.contentOffset.x, (tblView.contentSize.height - tblView.frame.size.height + 44.0) ) animated:YES];
    }
    
    int startPageNumber = [productsList count];
    NSString *urlString;
    // Make server call for more products.
    if(self.productListType == ProductListTypeSearch)
    {
        if ([searchKeyword isEqualToString:@"" ]||[searchLocation isEqualToString:@""])
            
            urlString = [NSString stringWithFormat:@"%@%@?cat_id=%@&p=&q=&cid=%@&country=%@&start=%d&limit=%d",URL_Prefix,serviceName,searchCategoryId,appDelegate.sessionUser.client_id,appDelegate.sessionUser.country,startPageNumber,pageSize];
        
        
        else
            
            urlString = [NSString stringWithFormat:@"%@%@?cat_id=%@&p=%@&q=%@&cid=%@&country=%@&start=%d&limit=%d",URL_Prefix,serviceName,searchCategoryId,searchLocation,searchKeyword,appDelegate.sessionUser.client_id,appDelegate.sessionUser.country,startPageNumber,pageSize];
    }
    else if(self.productListType == ProductListTypeOffers)
    {
        urlString = [NSString stringWithFormat:@"%@%@?cid=%@",URL_Prefix,serviceName,appDelegate.sessionUser.client_id];
        
        NSLog(@"Hot Offers... URL: %@",urlString);
    }
    
    NSURL *url = [NSURL URLWithString:urlString];
    categorySearchRequest = [ASIFormDataRequest requestWithURL:url];
    [categorySearchRequest setDelegate:self];
    [categorySearchRequest startAsynchronous];
    [self.tblView reloadData];
    
}

#pragma mark -- AlertView Delegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.cancelButtonIndex == buttonIndex)
    {
        // Remove from fav case
        if(alertView.tag!=222)
        {
            
            int index = alertView.tag - 22;
            Product *removePro = [productsList objectAtIndex:index];
            BOOL success = [appDelegate removeProductFromfavorites:removePro];
            
            
            if (success)
            {
                [self productremovedFromFavorites:removePro];}
            }
    }
    if(alertView.tag == 222)
    {
        NSLog(@"Redeem alert view and button index::%d",buttonIndex);
        if(buttonIndex == 1)
        {
            [self getCurrentLocation];
            NSString *lattitude = @"0.00"; // [NSString stringWithFormat:@"%f",self.userLocation.latitude];
            NSString *longitude =@"0.00"; //[NSString stringWithFormat:@"%f",self.userLocation.longitude];
            NSLog(@"%@",lattitude);
            NSLog(@"%@",longitude);
            
            NSString *urlString = [NSString stringWithFormat:@"%@redeemed.php",URL_Prefix];
            NSURL *url = [NSURL URLWithString:urlString];
            NSLog(@"url is:==> %@",url);
            
            
            redeemRequest = [[ASIFormDataRequest alloc] initWithURL:url];
            [redeemRequest setPostValue:appDelegate.sessionUser.userId forKey:@"user_id"];
            [redeemRequest setPostValue:redeemProductId forKey:@"pid"];
            [redeemRequest setPostValue:appDelegate.sessionUser.client_id forKey:@"cid"];
            [redeemRequest setPostValue:lattitude forKey:@"lat"];
            [redeemRequest setPostValue:longitude forKey:@"lon"];
            NSLog(@"User ID %@",appDelegate.sessionUser.userId );
            
            NSLog(@"Pro Id %@",redeemProductId);
            [redeemRequest setDelegate:self];
            [redeemRequest startAsynchronous];
            [self.redeemActivityIndicator startAnimating];
            
            
        }
        
    }
    if(alertView.tag == 333)
    {
        NSLog(@"ThankYou alert view and button index::%d",buttonIndex);
        if(buttonIndex == 0)
            NSLog(@"TNQ");
        [self handleCouponTap:nil];
            
        
    }
    if (alertView.tag == 212)
    {
     if(buttonIndex == 0)
         NSLog(@"cancel");
        else
        {
        NSLog(@"OK");
        
         NSString *urlString = [NSString stringWithFormat:@"%@redeem_status.php",URL_Prefix];
             NSURL *url = [NSURL URLWithString:urlString];
              NSLog(@"url is:==> %@",url);
               ASIFormDataRequest *req = [[ASIFormDataRequest alloc] initWithURL:url];
            [req setPostValue:appDelegate.sessionUser.userId forKey:@"user_id"];
            [req setPostValue:redeemProductId forKey:@"pid"];
            
            [req setDelegate:self];
            [req startAsynchronous];
            [self.redeemActivityIndicator startAnimating];


        }
    }
    
}
#pragma mark -
#pragma mark Table cell image support

- (void)startIconDownload:(Product *)aProduct forIndexPath:(NSIndexPath *)indexPath
{
    IconDownloader *iconDownloader = [imageDownloadsInProgress objectForKey:indexPath];
    if (iconDownloader == nil)
    {
        iconDownloader = [[IconDownloader alloc] init];
        iconDownloader.aProduct = aProduct;
        iconDownloader.indexPathInTableView = indexPath;
        iconDownloader.delegate = self;
        [imageDownloadsInProgress setObject:iconDownloader forKey:indexPath];
        [iconDownloader startDownload];
    }
}

- (void)appImageDidLoad:(NSIndexPath *)indexPath
{
    IconDownloader *iconDownloader = [imageDownloadsInProgress objectForKey:indexPath];
    
    
    if (iconDownloader != nil)
    {
        
        ProductCell *cell = (ProductCell *)[self.tblView cellForRowAtIndexPath:iconDownloader.indexPathInTableView];
        
        if (! iconDownloader.aProduct.imageDoesntExist)
        {
            // Display the newly loaded image
            cell.productImg.image = iconDownloader.aProduct.productImage;
        }
        else
        {
            // Display Dummy image
            cell.productImg.hidden = YES;
            cell.noImageLabel.hidden = NO;
        }
        
        
        // Stop activity animation
        [cell.imgLoadingIndicator stopAnimating];
    }
    else
    {
        //NSLog(@"-- Icon downloader doesn't exists");
    }
    
    // Remove the IconDownloader from the in progress list.
    // This will result in it being deallocated.
    [imageDownloadsInProgress removeObjectForKey:indexPath];
}


#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSLog(@"ProdcutsListVC -- requestFinished:");
    
    if (request == categorySearchRequest)
    {
        
        
        if([[request responseString]isEqualToString:@""])
        {
            NSLog(@"Product List  RES: null %@",[request responseString]);
           [categorySearchRequest cancel];
        }
        else
        {
            
            categorySearchRequest = nil;
            
            NSLog(@"Product List RES: %@",[request responseString]);
            
            NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
            productsXMLParser = [[ProductListParser alloc] init];
            productsXMLParser.delegate = self;
            productsParser.delegate = productsXMLParser;
            [productsParser parse];
        }

        
    }
    if (request == productFetchRequest)
    {
        if ([categorySearchRequest isExecuting])
        {
             productFetchRequest = nil;
            [productFetchRequest cancel];
        }
        else
        {
        productFetchRequest = nil;
        NSLog(@"Product ** RES: %@",[request responseString]);
        
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
        }
    }
    
    else if(request == redeemRequest)
    {
        redeemRequest = nil;
        [self.redeemActivityIndicator stopAnimating];
        NSLog(@"REDEEM RESPONSE::: %@",[request responseString]);
        NSString *msg = [NSString stringWithFormat:@"For redeeming this offer %@",redeemProName];
        if([[request responseString] isEqualToString:@"success"])
        {
            
            UIAlertView *thanksAlert = [[UIAlertView alloc]initWithTitle:@"Thank You!" message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
            thanksAlert.tag = 333;
            [thanksAlert show];
            
        }
        
    }
    else
    {
        NSLog(@"Coupon REDEEM RESPONSE:::%@",[request responseString]);
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    NSLog(@"ProdcutsListVC -- requestFailed:");
    
    if (requestCancel == YES) {
        requestCancel = NO;
    }
    else
    {
        if (request == categorySearchRequest)
        {
            categorySearchRequest = nil;
            [self.tblView reloadData];
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
        }
        else if(request == redeemRequest)
        {
            redeemRequest = nil;
            [self.redeemActivityIndicator stopAnimating];
            NSLog(@"REDEEM RESPONSE::: %@",[request responseString]);
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
        }
        else
        {
            productFetchRequest = nil;
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
        }

    }
    
    
}

#pragma mark -- Product Parser Delegate methods

- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal
{
    
    // Here update products list
    if (!productsList)
    {
        productsList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    }
    else
    {
        [productsList addObjectsFromArray:prodcutsListLocal];
    }
    
    
    
    // ** Update counter if there are no more products to load
    if ([prodcutsListLocal count] < pageSize || (self.productListType == ProductListTypeOffers))
    {
        allProductsLoaded = YES;
    }
    
    
    
    // Hide table if products list empty
    if (productsList.count == 0)
    {
        [self noProductsFound:YES];
    }
    else
    {
        [self noProductsFound:NO];
    }
    [self.tblView reloadData];
}

- (void)parsingProductListXMLFailed
{
    
}

#pragma mark -- Product Data Parser Delegate methods

- (void)parsingProductDataFinished:(Product *)product
{
    
    [productsList replaceObjectAtIndex:indexpathRow withObject:product];
    
    if (productController)
    {
        [productController updateUIWithProductDetails:product];
    }
    
    
}

- (void)parsingProductDataXMLFailed
{
    
}

#pragma mark -- Product view delegate methods

- (void)productAddedToFavorites:(Product *)pro
{
    
    if (self.productListType == ProductListTypeMyFavorites)
    {
        
        [self.tblView reloadData];
    }
    
}

- (void)productremovedFromFavorites:(Product *)pro
{
    if (self.productListType == ProductListTypeMyFavorites)
    {
        [self.productsList removeObject:pro];
        openProductIndex = nil;
        [self.tblView reloadData];
    }
    
    if (productsList.count == 0)
    {
        [self noProductsFound:YES];
    }
    else
    {
        [self noProductsFound:NO];
    }
    
}

- (void) showProductCouponForProduct:(Product *) pro couponImage:imagee
{   if ([pro.mobilecoupon isEqualToString:@"0"])
{
    UIAlertView *CouponAlert=[[UIAlertView alloc]initWithTitle:@"This is NOT a coupon offer" message:@"  Refer to offer terms for redemption instructions" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    CouponAlert.tag=123;
    [CouponAlert show];
    
}
else
{
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(couponGestureSelector:)];
    //recognizer.delegate = self;
    
    self.myImage = imagee;
    [couponView addGestureRecognizer:recognizer];
    couponView.frame = self.view.bounds;
    
    // Assign text and image
    redeemProductId = pro.productId;
    if (pro.productOffer.length == 0)  //If ProductOffer is Null.....
    {
        pro.productOffer = @"";
    }
    discountLabel.text = pro.productOffer;
    memNameLabel.text =[NSString stringWithFormat:@"Name: %@ %@",appDelegate.sessionUser.first_name, appDelegate.sessionUser.last_name];
    clientLabel.text = [NSString stringWithFormat:@"Client: %@", appDelegate.sessionUser.client_name];
    memNumberLabel.text = [NSString stringWithFormat:@"Membership: %@", appDelegate.sessionUser.username];
    couponMerchantImage.image =self.myImage;// image;//pro.productImage;
    productNameLabel.text = pro.productName;
    redeemProName = pro.productName;
    
    //===========
    NSLog(@"T&C: %@",pro.productTermsAndConditions);
    
    NSString *htmlString = pro.productTermsAndConditions;
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&"
                                                       withString:@"and"];
    
    couponTandCTextView.text = [htmlString stripHtml];
//    couponTandCTextView.text = [pro.productTermsAndConditions stripHtml];
    
    couponView.hidden = NO;
    couponView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.9];
    // couponView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:couponView];
    
    
    
   
    
    
    
    
     //aliging couponfrontview size                                        
    CGRect Cframe = couponFrontView.frame; 
    Cframe.origin.y= 0;
    if([appDelegate isIphone5])
        Cframe.size.height = 470;
    else
        Cframe.size.height = 400;
    couponFrontView.frame = Cframe;
    //=============
    //aliging background for couponfront side
    CGRect Cbgframe = couponCardBg_ImageView.frame;
    Cbgframe.origin.y= 0;
    if([appDelegate isIphone5])
        Cbgframe.size.height = 510;
    else
        Cbgframe.size.height = 420;

    couponCardBg_ImageView.frame = Cbgframe;
    //=============
    //aliging couponbackview size
    CGRect Cbframe = couponBackView.frame; 

    Cbframe.origin.y= 0;
    if([appDelegate isIphone5])
        Cbframe.size.height = 470;
    else
        Cbframe.size.height = 400;

    couponBackView.frame = Cbframe;
    //=============
    //aliging background for couponback side

    CGRect Cbbgframe = couponCardBackBg_ImageView.frame;
    Cbbgframe.origin.y= 0;
    if([appDelegate isIphone5])
        Cbbgframe.size.height = 510;
    else
        Cbbgframe.size.height = 420;
    couponCardBackBg_ImageView.frame = Cbbgframe;  
    //=============

    
    //======== configuring couponcard buttons and labels
    
    
    //buttons alignment
    CGRect TandCButtonFrame = TandCButton.frame;
    TandCButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - TandCButtonFrame.size.width-22;
    if ([appDelegate isIphone5])
        TandCButtonFrame.origin.y = couponFrontView.frame.size.height-70;
    else
        TandCButtonFrame.origin.y = couponFrontView.frame.size.height-90;

    TandCButton.frame = TandCButtonFrame;
    
    
    CGRect TandCBackButtonFrame = TandCBackButton.frame;
    TandCBackButtonFrame.origin.x = 40.0;
     if ([appDelegate isIphone5])
    TandCBackButtonFrame.origin.y = couponBackView.frame.size.height-72;
    else
    TandCBackButtonFrame.origin.y = couponBackView.frame.size.height-90;

    TandCBackButton.frame = TandCBackButtonFrame;
    
    
    CGRect fredeemButtonFrame = frontredeemButton.frame;
    fredeemButtonFrame.origin.x = 40.0;
    if ([appDelegate isIphone5])
    
       fredeemButtonFrame.origin.y = couponBackView.frame.size.height-70; 
    else
        fredeemButtonFrame.origin.y = couponBackView.frame.size.height-90;
 
    
    frontredeemButton.frame = fredeemButtonFrame;

    
    CGRect redeemButtonFrame = redeemButton.frame;
    redeemButtonFrame.origin.x =couponImageView.frame.origin.x + couponImageView.frame.size.width - redeemButtonFrame.size.width - 23.0;
    if ([appDelegate isIphone5])
    redeemButtonFrame.origin.y = couponBackView.frame.size.height-72;
    else
        redeemButtonFrame.origin.y = couponBackView.frame.size.height-90;
    redeemButton.frame = redeemButtonFrame;
    //=================
    
    //labels alignment
    
    CGRect discountLabelFrame = discountLabel.frame;
    discountLabelFrame.origin.y = self.productNameLabel.frame.origin.y + self.productNameLabel.frame.size.height+10;
    discountLabel.frame = discountLabelFrame;
    
    
    
    CGRect nameLabelFrame = memNameLabel.frame;
    
    if([appDelegate isIphone5])
        nameLabelFrame.origin.y = discountLabel.frame.origin.y+discountLabel.frame.size.height;
    else
        nameLabelFrame.origin.y = discountLabel.frame.origin.y+discountLabel.frame.size.height;
    memNameLabel.frame = nameLabelFrame;
    
    CGRect memnumLabelFrame = memNumberLabel.frame;
    memnumLabelFrame.origin.y = self.memNameLabel.frame.origin.y + self.memNameLabel.frame.size.height+10;
    memNumberLabel.frame = memnumLabelFrame;
    
    CGRect clientLabelFrame = clientLabel.frame;
    clientLabelFrame.origin.y = self.memNumberLabel.frame.origin.y + self.memNumberLabel.frame.size.height+10;
    clientLabel.frame = clientLabelFrame;
    
    CGRect TandCLabelFrame=self.TandCLabel.frame;
    TandCLabelFrame.origin.y=30;
    TandCLabel.frame=TandCLabelFrame;
    //==========
    //aligning terms and conditions
    CGRect couponTandCTextviewframe=self.couponTandCTextView.frame;
    if([appDelegate isIphone5])
        couponTandCTextviewframe.size.height=338;
    
    else
        couponTandCTextviewframe.size.height=258;
    
    couponTandCTextviewframe.origin.y=50;
    couponTandCTextView.frame=couponTandCTextviewframe;
    
    //==========
    //aligning background for labels
    CGRect couponBgFrame = coupounbgImageView.frame;
    couponBgFrame.origin.y=183;
    if([appDelegate isIphone5])
        couponBgFrame.size.height = 150;
    else
        couponBgFrame.size.height = 110;
        coupounbgImageView.frame = couponBgFrame;
    //===========
    
    CGRect couponFrame = couponImageView.frame;
    if ([appDelegate isIphone5])
    {}
    else
        couponFrame.size.height=360;
    couponImageView.frame= couponFrame;
    
    CGRect backcouponFrame = couponbackImageView.frame;
    if ([appDelegate isIphone5])
    {}
    else
        backcouponFrame.size.height=360;
    couponbackImageView.frame= backcouponFrame;


    
    
    //making coupon subview to couponflipview
    [couponFlipView addSubview:couponBackView];
    [couponFlipView addSubview:couponFrontView];
    couponBackView.alpha = 0.0;
    couponFrontView.alpha = 1.0;
    //===========
}

}



- (void) couponGestureSelector:(UIGestureRecognizer *) gesRec {
    
    CGPoint touchPoint = [gesRec locationOfTouch:0 inView:couponView];
    
    if (couponFrontView.alpha == 0.0) {
        
        NSLog(@"frontView is not visible, but Back view is");
        if( CGRectContainsPoint(TandCBackButton.frame, touchPoint) ) {
            [self termsAndCondBackButtonPressed:TandCBackButton];
        }
        else if( CGRectContainsPoint(redeemButton.frame, touchPoint) ) {
            [self redeemButtonPressed:redeemButton];
        }
        else {
            [self handleCouponTap:nil];
        }
    }
    else {
        // i.e Back view is not visible, but frontView is
        NSLog(@"Back view is not visible, but frontView is");
        if( CGRectContainsPoint(TandCButton.frame, touchPoint) ) {
            [self termsAndCondButtonPressed:TandCButton];
        }
        else if( CGRectContainsPoint(frontredeemButton.frame, touchPoint) ) {
            [self redeemButtonPressed:redeemButton];
        }
        else {
            [self handleCouponTap:nil];
        }
    }
}

-(IBAction)handleCouponTap:(id)sender
{
    NSLog(@"handleCouponTap:");
   
    
    // Remove recognizer
    for (UIGestureRecognizer *gesRec in couponView.gestureRecognizers)
    {
        [couponView removeGestureRecognizer:gesRec];
    }
    
    [couponView removeFromSuperview];
}



- (IBAction) termsAndCondButtonPressed:(id)sender
{
    NSLog(@"PLVC termsAndCondButtonPressed");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:couponFlipView cache:NO];
    couponFrontView.alpha = 0.0;
    couponBackView.alpha = 1.0;
    [UIView commitAnimations];
    
}

- (IBAction) termsAndCondBackButtonPressed:(id)sender
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:couponFlipView cache:NO];
    
    couponFrontView.alpha = 1.0;
    couponBackView.alpha = 0.0;
    [UIView commitAnimations];
}
- (IBAction) redeemButtonPressed:(id) sender
{
    
    UIAlertView *redemAlert = [[UIAlertView alloc]initWithTitle:@"The redeem button is for merchant use only." message:@"A merchant  will press this to record your redemption. Some offers/vouchers do not permit multiple use so don't waste a voucher.\n\n" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    
    
    UILabel *txtField = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 160.0, 260.0, 45.0)];
    [txtField setFont:[UIFont fontWithName:@"Helvetica-Bold" size:(15.0)]];
    txtField.textAlignment = UITextAlignmentCenter;
    txtField.numberOfLines = 2;
    txtField.textColor = [UIColor whiteColor];
    txtField.text = @"If you are not a merchant, Press No now to go back.";
    txtField.backgroundColor = [UIColor clearColor];
    [redemAlert addSubview:txtField];
    
    redemAlert.tag = 222;
    [redemAlert show];
}

- (IBAction)couponredeemButtonPressed:(id)sender {
    UIAlertView * alertView=[[UIAlertView alloc]initWithTitle:@"Redeem" message:@"Do you like Reedeem points.  Please press OK \n "  delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    membership_TextField = [[UITextField alloc] initWithFrame:CGRectMake(22.0, 90.0, 240.0, 25.0)];
    membership_TextField.placeholder = @"coupon code";
    membership_TextField.font = [UIFont systemFontOfSize:14];
    membership_TextField.borderStyle = UITextBorderStyleRoundedRect;
    [membership_TextField setBackgroundColor:[UIColor whiteColor]];
   // [alertView addSubview:membership_TextField];
    alertView.tag = 212;
    [alertView show];

}


#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"didFailWithError: %@", error);
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
    self.userLocation = [newLocation coordinate];
}



- (void)viewDidUnload {
    
    NSLog(@"Executed......");
    [self setCoupounbgImageView:nil];
    [self setCouponCardBg_ImageView:nil];
    [self setCouponCardBackBg_ImageView:nil];
    [self setCouponbackImageView:nil];
    [self setFrontredeemButton:nil];
    [super viewDidUnload];
}
@end
