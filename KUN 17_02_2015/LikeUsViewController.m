//
//  LikeUsViewController.m
//  KUN
//
//  Created by vairat on 10/11/14.
//  Copyright (c) 2014 MyRewards. All rights reserved.
//

#import "LikeUsViewController.h"

@interface LikeUsViewController ()

@end

@implementation LikeUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
    UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [myButton1 setImage:myImage1 forState:UIControlStateNormal];
    myButton1.showsTouchWhenHighlighted = YES;
    myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
    [myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"Like Us!";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor whiteColor]];
    self.navigationItem.titleView = TitleLabel;
    
}
-(void)back
{
    
    //[activityIndicatorView startAnimating];
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}
- (IBAction)socialNetworkButtonTapped:(id)sender
{
    switch ([sender tag ]) {
        case 6:
        {
            //NSURL *websiteUrl = [NSURL URLWithString:@"http://www.suncorpsocialclub.com/"];https://www.facebook.com/KunUnitedHyundai
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.facebook.com/KunUnitedHyundai"]];

        }
            break;
        case 16:
        {
           // NSURL *websiteUrl = [NSURL URLWithString:@"http://www.suncorpsocialclub.com/"];
            
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://twitter.com/Kununitedhyd/"]];
            
        }
            break;

            
        default:
            break;
    }
}
@end
