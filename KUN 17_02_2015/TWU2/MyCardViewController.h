//
//  MyCardViewController.h
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"

@interface MyCardViewController : UIViewController<ASIHTTPRequestDelegate,UIWebViewDelegate>
{
    IBOutlet UIView *containerView;
    IBOutlet UIImageView *background_ImageView;
    
}
@property(nonatomic,strong) IBOutlet UIImageView *background;
@property(nonatomic,strong)IBOutlet UIImageView *myCardImageView;
@property(nonatomic,strong)IBOutlet UILabel *userNameLabel;
@property(nonatomic,strong)IBOutlet UILabel *clientIdLabel;
@property(nonatomic,strong)IBOutlet UILabel *userNameLabel1;
@property(nonatomic,strong)IBOutlet UIView *containerView;
@property(nonatomic,strong) IBOutlet UIImageView *background_ImageView;
@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
-(void)doTheJob;
-(void)loadViews;
@end
