//
//  MyUnionParser.m
//  TWU2
//
//  Created by MyRewards on 2/20/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "MyUnionParser.h"


@interface MyUnionParser()
{
    
    NSMutableString *charString;
    Display  *dsplyContent;
    
}
@end
@implementation MyUnionParser
@synthesize List;
@synthesize delegate;



-(void)dealloc
{
  
    delegate = nil;
}

- (void)parserDidStartDocument:(NSXMLParser *)parser
{
    List = [[NSMutableArray alloc] init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    
    charString = nil;
    
    if ([elementName isEqualToString:@"page"])
    {
        NSLog(@"==Display object created==") ;
        dsplyContent = [[Display alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    
    if (!charString)
    {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else
    {
        [charString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"details"])
    {
        dsplyContent.displaymsg= finalString;
        NSLog(@"====>>>Parsed content is:::%@", dsplyContent.displaymsg);
    }
    else if([elementName isEqualToString:@"page"])
    {
        NSLog(@"parsing aboted");
        [parser abortParsing];
    
    //if (delegate) {
        //if ([delegate respondsToSelector:@selector(parsingMyUnionDetailsFinished:)]) {
            //NSLog(@"parsingMyUnionDetailsFinished method called");
            [delegate parsingMyUnionDetailsFinished:dsplyContent];
       // }
   //}
    }
    [List addObject:dsplyContent];
    NSLog(@"ARRAY COUNT IS::::::%d",[List count]);
}
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError
{
    NSLog(@"====ERROR IN PARSING======");
    if (delegate)
    {
        if ([delegate respondsToSelector:@selector(MyUnionDetailsXMLparsingFailed)])
        {
            [delegate MyUnionDetailsXMLparsingFailed];
        }
    }
}
@end