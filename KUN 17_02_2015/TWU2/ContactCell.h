//
//  ContactCell.h
//  TWU2
//
//  Created by vairat on 09/04/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactCell : UITableViewCell
{
    IBOutlet UIButton *removeContactbutton;
}
@property (nonatomic, strong) IBOutlet UIButton *removeContactbutton;
@property (nonatomic, strong) IBOutlet UILabel *nameLabel;
@property (nonatomic, strong) IBOutlet UILabel *numberlabel;
@property (strong, nonatomic) IBOutlet UIImageView *msgImage;
@property (nonatomic, strong) IBOutlet UIButton *callButton;
//
@property (nonatomic, strong) IBOutlet UIView *headerContainerView;
@property (nonatomic, strong) IBOutlet UILabel *noticeLabel;

@end
