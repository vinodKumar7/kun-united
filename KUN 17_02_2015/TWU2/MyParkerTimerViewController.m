//
//  MyParkerTimerViewController.m
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//
#import "AppDelegate.h"
#import "MyParkerTimerViewController.h"
#import <QuartzCore/QuartzCore.h>
#import <MapKit/MapKit.h>
#import "ProductAnnotation.h"
#import "ParkingTimer.h"
#import "LocationViewController.h"


@interface MyParkerTimerViewController ()
{
    
    
    AppDelegate *appDelegate;
    ParkingTimer *currentRemainder,*location_Details,*carLoc_Details;
    NSString *parkingRemainderCount;
    NSString *parking_Time;
    NSString *parking_Time_Hours;
    NSString *parking_Time_Minutes;
    NSString *carParked_Location;
    int currentButton;
    
    //BOOL isAddLocationSelected;
    BOOL isTimerSet;
    int badgeCount;
    int remainderCount;
    
    NSArray *hours_Array;
    NSArray *minutes_Array;
    NSMutableArray *labels_Array;
    CGFloat top;
    CGFloat height;
    UILabel *label;
    
    int hours, minutes, seconds;
    int secondsLeft;
    NSTimer *timer;
    
    UILabel *hours1;
    UILabel *hours2;
    
    UILabel *hour1;
    UILabel *hour2;
    int previousRow;
    
}
@property(nonatomic,strong)NSDate *endTimeIndicator;
@property(nonatomic,readwrite)int badgeCount;

@property(nonatomic,strong)NSArray *hours_Array;
@property(nonatomic,strong)NSArray *minutes_Array;
@property(nonatomic,strong)NSMutableArray *labels_Array;
@end

@implementation MyParkerTimerViewController

@synthesize badgeCount;
@synthesize endTimeIndicator;
@synthesize labels_Array;
@synthesize myCounterLabel;
@synthesize setremainderButton;
@synthesize settimerLabel;
@synthesize AfterthemeterLabel;
@synthesize locationManager;
@synthesize currentLocation;
@synthesize parkingMapView;
@synthesize addLocation_Button;
@synthesize gobackButton;
@synthesize currentTextField;
@synthesize setTimer_View;
@synthesize time_Picker;
@synthesize done_Button;
@synthesize location_TextField;
@synthesize hours_Array;
@synthesize minutes_Array;
@synthesize locdescLabel;

@synthesize timerFlipView;
@synthesize baseView;
//======================================DELEGATE METHODS==========================================//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
        hours_Array = [[NSArray alloc]initWithObjects:@"0",@"1",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"11",@"12",@"13",@"14",@"15",@"16",@"17",@"18",@"19",@"20",@"21",@"22",@"23", nil];
        
        minutes_Array = [[NSArray alloc]initWithObjects:@"0",@"5",@"10",@"15",@"20",@"25",@"30",@"35",@"40",@"45",@"50",@"55",nil];
        parking_Time = @"";
        parking_Time_Hours = @"";
        parking_Time_Minutes = @"";
        carParked_Location = @"";
        labels_Array = [[NSMutableArray alloc]init];
        
        //isAddLocationSelected = NO;
        isTimerSet = NO;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self getCurrentLocation];
    [self loadViews];
    
    parkingMapView.delegate=self;
    currentButton = 0;
    remainderCount = 0;
    self.badgeCount = 0;
    [timerFlipView addSubview:baseView];
    
    
}



- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"Parking Timer ViewWilAppear");
    //hour1.hidden = NO;
    // hour2.hidden = NO;
    
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
//    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
//    if ([[ver objectAtIndex:0] intValue] >= 7)
//    {
//        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];        self.navigationController.navigationBar.translucent = NO;
//    }
//    else
//    {
//        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
//    }
    
 /*   if([self.navigationController.navigationBar respondsToSelector:@selector(barTintColor)])
    {
        // iOS7
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
        self.navigationController.navigationBar.translucent = NO;
    }
    else
    {
        // older
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
    }*/
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    
    TitleLabel.text = @"My Parking Reminder";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    TitleLabel.textColor=[UIColor whiteColor];
    self.navigationItem.titleView = TitleLabel;
    
    
    
    parkingRemainderCount = [[NSUserDefaults standardUserDefaults] objectForKey:@"ParkingRemainderCount"];
    
    
    
    
    if([parkingRemainderCount intValue] == 1)
    {
        
        
        isTimerSet = YES;
        
        NSLog(@"parkingRemainder is set previous");
        currentRemainder = [appDelegate FetchRemainder];
        
        if (![currentRemainder isKindOfClass:[NSNull class]])
        {
            
            NSLog(@"currentRemainder object is available");
            [self.done_Button setImage:[UIImage imageNamed:@"bfCancelTimer1.png"] forState:UIControlStateNormal];
            self.location_TextField.text = currentRemainder.location;
            
            MKCoordinateRegion region;
            
            //center
            CLLocationCoordinate2D center;
            center.latitude = [currentRemainder.latitude floatValue];
            center.longitude = [currentRemainder.longitude floatValue];
            
            carLoc_Details = [[ParkingTimer alloc]init];
            carLoc_Details.latitude = currentRemainder.latitude;
            carLoc_Details.longitude = currentRemainder.longitude;
            carLoc_Details.parkedLocation = currentRemainder.parkedLocation;
            carLoc_Details.parkedTime = currentRemainder.parkedTime;
            
            self.currentLocation = center;
            //span
            MKCoordinateSpan span;
            span.latitudeDelta = 0.05f;
            span.longitudeDelta = 0.05f;
            
            region.center = center;
            region.span   = span;
            
            [parkingMapView setRegion:region animated:YES];
            [self addMyAnnotation];
            
            [appDelegate startCountDownTimerWithTime:appDelegate.x  andUILabel:myCounterLabel];
            
        }
        else{
            NSLog(@"currentRemainder object is NOT available");
        }
    }
    
    
    
    
    
}

-(void)back
{
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}


// =======================METHOD TO LOAD VIEWS IN IPHONE  ==================================//

-(void)loadViews
{
    if(![appDelegate isIphone5])
    {
        
        CGRect remainderbuttonframe=self.setremainderButton.frame;
        remainderbuttonframe.origin.y=363;
        self.setremainderButton.frame=remainderbuttonframe;
        
        CGRect locDescFrame=self.locdescLabel.frame;
        locDescFrame.origin.y=15;
        self.locdescLabel.frame=locDescFrame;
        
        CGRect locdescTextFieldFrame=self.location_TextField.frame;
        locdescTextFieldFrame.origin.y=44;
        self.location_TextField.frame=locdescTextFieldFrame;
        
        CGRect settimerLabelframe=self.settimerLabel.frame;
        settimerLabelframe.origin.y=85;
        self.settimerLabel.frame=settimerLabelframe;
        
        CGRect pickerframe=self.time_Picker.frame;
        pickerframe.origin.y=113;
        pickerframe.size.height=150;
        self.time_Picker.frame=pickerframe;
        
        CGRect afterthemeterLabelFrame=self.AfterthemeterLabel.frame;
        afterthemeterLabelFrame.origin.y=285;
        self.AfterthemeterLabel.frame=afterthemeterLabelFrame;
        
        CGRect starttimeframe=self.done_Button.frame;
        starttimeframe.origin.y=360;
        self.done_Button.frame=starttimeframe;
        
        CGRect flipframe=self.gobackButton.frame;
        flipframe.origin.y=382;
        flipframe.origin.x=6;
        self.gobackButton.frame=flipframe;
        
        CGRect mapframe=self.parkingMapView.frame;
        mapframe.origin.y=-60;
        self.parkingMapView.frame=mapframe;
        
        
    }
    
}

//==================================================================================================//
//===============================METHOD TO FIRE NOTIFICATIONS ======================================//

- (void)scheduleLocalNotificationWithDate: (NSDate *)fireDate
{
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    
    notification.fireDate = fireDate;
    notification.alertBody = @"Parking Time is about to  end";
    notification.soundName = @"old_phone.m4r";
    
    
//    self.badgeCount++;
//    notification.applicationIconBadgeNumber = self.badgeCount;
    
    
    [[UIApplication sharedApplication] scheduleLocalNotification:notification];
}



//=================================================================//

//====================== METHOD TO SET REMAINDER ==========================//
-(IBAction)setRemainderButton_clicked:(id)sender
{
    
    NSLog(@"setRemainderButton_clicked");
    
    if([[NSUserDefaults standardUserDefaults] objectForKey:@"ParkingRemainderCount"])
        [self.done_Button setImage:[UIImage imageNamed:@"bfCancelTimer1.png"] forState:UIControlStateNormal];
    else
        [self.done_Button setImage:[UIImage imageNamed:@"bfStartTimer.png"] forState:UIControlStateNormal];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft
						   forView:timerFlipView
							 cache:NO];
	
	[UIView commitAnimations];
    
    
    [timerFlipView addSubview:setTimer_View];
    
    parking_Time_Hours  = @"0";
    parking_Time_Minutes=@"5";
    [time_Picker selectRow:0 inComponent:0 animated:NO];
    [time_Picker selectRow:505 inComponent:1 animated:NO];
    
    CGRect screenRect = [[UIScreen mainScreen] applicationFrame];
#define toolbarHeight           185.0
    CGFloat pickerTop = screenRect.size.height - toolbarHeight - time_Picker.frame.size.height;
    
    // Add label on top of pickerView
    top = pickerTop + 2;
    height = time_Picker.frame.size.height - 2;
    
    [self addPickerLabel:@"hours" rightX:133.0 top:top height:height];
    [self addPickerLabel:@"mins" rightX:243.0 top:top height:height];
    [self addPickerLabel:@"hour" rightX:130.0 top:top height:height];
    
    hours1 =[labels_Array objectAtIndex:0];
    hours2 =[labels_Array objectAtIndex:1];
    
    hour1 =[labels_Array objectAtIndex:4];
    hour2 =[labels_Array objectAtIndex:5];
    hour1.hidden = YES;
    hour2.hidden = YES;
    
    
    
}
//=======================================================================//

//================ METHOD  ADDS LABELS TO PICKER VIEW =====================//

- (void)addPickerLabel:(NSString *)labelString rightX:(CGFloat)rightX top:(CGFloat)topp height:(CGFloat)heigt
{
    
#define PICKER_LABEL_FONT_SIZE 18
#define PICKER_LABEL_ALPHA 0.7
    
    UIFont *font = [UIFont boldSystemFontOfSize:PICKER_LABEL_FONT_SIZE];
    CGFloat x = rightX - [labelString sizeWithFont:font].width;
    
    // White label 1 pixel below, to simulate embossing.
    label = [[UILabel alloc] initWithFrame:CGRectMake(x, topp + 1, rightX, heigt)];
    label.text = labelString;
    label.font = font;
    label.textColor = [UIColor whiteColor];
    label.backgroundColor = [UIColor clearColor];
    label.opaque = NO;
    label.alpha = PICKER_LABEL_ALPHA;
    [labels_Array addObject:label];
    [self.setTimer_View addSubview:label];
    
    // Actual label.
    label = [[UILabel alloc] initWithFrame:CGRectMake(x, topp, rightX, heigt)];
    label.text = labelString;
    label.font = font;
    label.backgroundColor = [UIColor clearColor];
    label.opaque = NO;
    label.alpha = PICKER_LABEL_ALPHA;
    [labels_Array addObject:label];
    [self.setTimer_View addSubview:label];
}

//====================================================================//
//================ METHOD TO FLIP BUTTON ===============================//

- (IBAction)doneButton_Clicked:(id)sender {
    
    
    
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight
						   forView:timerFlipView
							 cache:YES];
    
    for(int i = 0; i < 6; i++){
        [[labels_Array objectAtIndex:i] removeFromSuperview];
    }
    
    [labels_Array removeAllObjects];
    
    
    [self.setTimer_View removeFromSuperview];
    
	[UIView commitAnimations];
}
//===================================================================//


//=================== METHOD TO START BUTTON =======================//
- (IBAction)startTimer_Clicked:(id)sender
{
    
    
    if([[[NSUserDefaults standardUserDefaults] objectForKey:@"ParkingRemainderCount"] intValue] == 1)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Timer"
                                                        message:@"Do you want to clear the previous Timer?"
                                                       delegate:self
                                              cancelButtonTitle:@"NO" otherButtonTitles:@"OK",nil];
        alert.tag = 333;
        [alert show];
        
        
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Timer"
                                                        message:@"Do you want to start Timer?"
                                                       delegate:self
                                              cancelButtonTitle:@"NO" otherButtonTitles:@"OK",nil];
        alert.tag = 444;
        [alert show];
    }
    
    
}

//==================================================================//


//================== METHOD TO SET TIMER ============================//


-(void)setTimer
{
    
    int totalSeconds = ([parking_Time_Hours intValue]*60*60) + ([parking_Time_Minutes intValue]*60);
    secondsLeft = totalSeconds;
    NSLog(@"secondsLeft::%d",secondsLeft);
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    
    NSDate* currentDate = [NSDate date];
    NSLog(@" current Time::  %@",[dateFormatter stringFromDate:currentDate]);
    NSDate *datePlusParkingTime = [currentDate dateByAddingTimeInterval:totalSeconds];
    
    NSLog(@" Alaram Time::  %@",[dateFormatter stringFromDate:datePlusParkingTime]);
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    [self scheduleLocalNotificationWithDate:datePlusParkingTime];
    remainderCount=1;
    
    NSString *ParkingRemainderCountString = [NSString stringWithFormat:@"%d",remainderCount];
    
    [[NSUserDefaults standardUserDefaults] setValue:ParkingRemainderCountString forKey:@"ParkingRemainderCount"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    currentRemainder = [[ParkingTimer alloc]init];
    
    currentRemainder.location = self.location_TextField.text;
    currentRemainder.parkedLocation = carParked_Location;
    // NSLog(@"%@",carParked_Location);
    currentRemainder.parkedTime = [NSString stringWithFormat:@"%@",[dateFormatter stringFromDate:currentDate]];
    currentRemainder.latitude =  [NSString stringWithFormat:@"%f",self.currentLocation.latitude];
    currentRemainder.longitude =  [NSString stringWithFormat:@"%f",self.currentLocation.longitude];
    
    BOOL success =    [appDelegate addRemainder:currentRemainder];
    [self countdownTimer];
    
    if(success)
        NSLog(@"remainder saved successfully");
    else
        NSLog(@"remainder not saved ");
    [self.done_Button setImage:[UIImage imageNamed:@"bfCancelTimer1.png"] forState:UIControlStateNormal];
    [self doneButton_Clicked:nil];
    
    
}
//++
-(void)countdownTimer
{
    NSLog(@"timerbbbbb");
    appDelegate.temp = 1;
    //int totalSeconds = ([parking_Time_Hours intValue]*60*60) + ([parking_Time_Minutes intValue]*60);
    [appDelegate startCountDownTimerWithTime:secondsLeft  andUILabel:myCounterLabel];
    NSLog(@"num %@",myCounterLabel.text);
}

//===========================================================//



//===================METHOD ADDS CIRCULAR PROPERTY========================//

-(void)pickerViewLoaded: (id)blah
{
	NSUInteger max = 1000;
	NSUInteger base10 = (max/2)-(max/2)%12;
	[time_Picker selectRow:[time_Picker selectedRowInComponent:1]%10+base10 inComponent:1 animated:false];
}


- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    NSInteger actualRow = row % 12;
    if(component == 0)
    {
        
        NSLog(@"%@",[self.hours_Array objectAtIndex:row]);
        parking_Time_Hours = [self.hours_Array objectAtIndex:row];
        
        if(row == 1)
        {
            
            
            hours1.hidden = YES;
            hours2.hidden = YES;
            
            hour1.hidden = NO;
            hour2.hidden = NO;
            
            
            
        }
        else
        {
            hour1.hidden = YES;
            hour2.hidden = YES;
            
            hours1.hidden = NO;
            hours2.hidden = NO;
            
        }
        
        
    }
    else
    {
        NSLog(@"%@",[self.minutes_Array objectAtIndex:actualRow]);
        parking_Time_Minutes = [self.minutes_Array objectAtIndex:actualRow];
        previousRow = row+1;
        
    }
    if([parking_Time_Hours isEqualToString:parking_Time_Minutes])
    {
        
        if(component == 1)
            [pickerView selectRow:row+1 inComponent:1 animated:YES];
        
        else
            [pickerView selectRow:previousRow inComponent:1 animated:YES];
        
        parking_Time_Minutes = @"5";
    }
    
    
}
//=======================================================//
//=============DELEGATE METHODS FOR PICKER VIEW===================//

-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (component==0)
    {
        
        return [self.hours_Array objectAtIndex:row];
    }
    else
    {
        NSInteger actualRow = row % 12;
        return [self.minutes_Array objectAtIndex:actualRow];
    }
    return 0;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
        
        return [self.hours_Array count];
    
    else
        return 1000;
    
    return 0;
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}



- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    
    return 120;
}



- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    if(component == 0)
    {
        
        UILabel *label1 = (UILabel*) view;
        if (label1 == nil)
        {
            label1 = [[UILabel alloc] init];
        }
        
        [label1 setText:[self.hours_Array objectAtIndex:row]];
        
        // This part just colorizes everything, since you asked about that.
        [label1 setTextColor:[UIColor blackColor]];
        [label1 setBackgroundColor:[UIColor clearColor]];
        CGSize rowSize = [pickerView rowSizeForComponent:component];
        CGRect labelRect = CGRectMake (0, 0, rowSize.width-15, rowSize.height);
        [label1 setFont:[UIFont fontWithName:@"digital-7" size:25]];
        [label1 setFrame:labelRect];
        
        return label1;
    }
    else
    {
        UILabel *label1 = (UILabel*) view;
        if (label1 == nil)
        {
            label1 = [[UILabel alloc] init];
        }
        
        [label1 setText:[self.minutes_Array objectAtIndex:(row%12)]];
        
        // This part just colorizes everything, since you asked about that.
        [label1 setTextColor:[UIColor blackColor]];
        [label1 setBackgroundColor:[UIColor clearColor]];
        CGSize rowSize = [pickerView rowSizeForComponent:component];
        CGRect labelRect = CGRectMake (0, 0, rowSize.width-15, rowSize.height);
        [label1 setFont:[UIFont fontWithName:@"digital-7" size:25]];
        [label1 setFrame:labelRect];
        return label1;
        
    }
    
    
    return nil;
}

//=================================================================================================//


//==============================  DELEGATE METHODS TO ALERTVIEW ===================================//
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.cancelButtonIndex == buttonIndex)
    {
        
        
        
    }
    
    else{
        if(alertView.tag == 333)
        {
            [[UIApplication sharedApplication] cancelAllLocalNotifications];
            [appDelegate clearRemainder];
            [appDelegate invalidateCurrentCountDownTimer];
            [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ParkingRemainderCount"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            remainderCount=0;
            
            
            
            [self.done_Button setImage:[UIImage imageNamed:@"bfStartTimer.png"] forState:UIControlStateNormal];
            
            
        }
        if(alertView.tag == 444)
        {
            
            
            [self setTimer];
            
        }
        
        
    }
    
    
    
}
//=================================================================================================//
//======================UITEXTFIELD DELEGATE METHODS  ===================//


- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return YES;
    
}

//=================================================================================================//





//======================  MAPVIEW DELEGATE METHODS  ===================//


#pragma mark CoreLocation Delegate Methods.........

- (void)updateCurrentLocation {
    
    if ([CLLocationManager locationServicesEnabled]) {
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager performSelector:@selector(requestWhenInUseAuthorization)];
        }
        // .. do something here ..//
    }
    
    [self.locationManager startUpdatingLocation];
}

-(void)getCurrentLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
//    [locationManager startUpdatingLocation];
    
    //Vinod Added
    [self updateCurrentLocation];
    
}


- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"==========locationManager===========");
    CLLocationCoordinate2D coordinate = [newLocation coordinate];
    
    
    self.currentLocation = coordinate;
    
    if(!isTimerSet)
    {
        NSLog(@"timer not set");
        MKCoordinateRegion region = parkingMapView.region;
        region.center.latitude = self.currentLocation.latitude;
        region.center.longitude = self.currentLocation.longitude;
        region.span.longitudeDelta = 0.05f;
        region.span.latitudeDelta = 0.05f;
        [parkingMapView setRegion:region animated:YES];
        
    }
    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
    [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
        for (CLPlacemark * placemark in placemarks)
        {
            NSString *addressTxt = [NSString stringWithFormat:@" %@,%@ %@",
                                    [placemark thoroughfare],
                                    [placemark locality], [placemark administrativeArea]];
            NSLog(@"Address::::%@",addressTxt);
            
            
            location_Details = [[ParkingTimer alloc]init];
            location_Details.location = addressTxt;
            carParked_Location = addressTxt;
            location_Details.latitude = [NSString stringWithFormat:@"%f",self.currentLocation.latitude];
            location_Details.longitude = [NSString stringWithFormat:@"%f",self.currentLocation.longitude];
            
            
            
        }
    }];
    
    
    [parkingMapView showsUserLocation];
    
    [parkingMapView setShowsUserLocation:YES];
    
    //map settings
    [parkingMapView setMapType:MKMapTypeStandard];
    [parkingMapView setZoomEnabled:YES];
    [parkingMapView setScrollEnabled:YES];
    
    
    if(!([parkingRemainderCount intValue] == 1))
        [self addMyAnnotation];
    
    [self stopupdatingMap];
}


-(void) stopupdatingMap
{
    [locationManager stopUpdatingLocation];
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
    NSLog(@"didUpdateUserLocation = '%@'", userLocation);
}

- (void) addMyAnnotation
{
    NSLog(@"==========addNewAnnotations===========");
    
    
    
    ProductAnnotation *ann = [[ProductAnnotation alloc]init];
    ann.coordinate = self.currentLocation;
    ann.title = @"My Car Parking Location";
    
    ProductAnnotation *ann1 = [[ProductAnnotation alloc]init];
    //ann1.coordinate = self.currentLocation;
    //ann1.title = @"Current Location";
    
    [parkingMapView addAnnotation:ann];
    [parkingMapView addAnnotation:ann1];
    
    [parkingMapView reloadInputViews];
}

- (MKOverlayView *)mapView:(MKMapView *)map viewForOverlay:(id <MKOverlay>)overlay
{
    NSLog(@"viewForOverlay");
    
    MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
    circleView.strokeColor = [UIColor redColor];
    circleView.fillColor = [[UIColor redColor] colorWithAlphaComponent:0.4];
    return circleView;
}

- (void)mapView:(MKMapView *)mapView didAddAnnotationViews:(NSArray *)views
{
    
    for (MKAnnotationView *pin in views)
    {
        
        
        pin.canShowCallout = YES;
        //pin.rightCalloutAccessoryView = detailsButton;
        CGRect endFrame = pin.frame;
        pin.frame = CGRectOffset(pin.frame, 0, -230);
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.45f];
        [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
        
        pin.frame = endFrame;
        
        [UIView commitAnimations];
    }
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
    static NSString *annotationIdentifier = @"AnnotationIdentifier";
    
    if ([[(ProductAnnotation*)annotation title] isEqualToString:@"Current Location"])
    {
        
        return nil;
    }
    
    MKAnnotationView *pinView = (MKAnnotationView *) [parkingMapView
                                                      dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    
    if (!pinView)
    {
        pinView = [[MKAnnotationView alloc]
                   initWithAnnotation:annotation
                   reuseIdentifier:annotationIdentifier];
        
        //  pinView.animatesDrop = YES;
        pinView.canShowCallout = YES;
        pinView.userInteractionEnabled = YES;
        //  pinView.pinColor = MKPinAnnotationColorGreen;
        pinView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        pinView.image = [UIImage imageNamed:@"car1.png"];
        return pinView;
    }
    else
    {
        pinView.annotation = annotation;
        //pinView.animatesDrop = YES;
        // pinView.image = [UIImage imageNamed:@"car.png"];
    }
    
    return pinView;
    
    
    
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    NSLog(@"calloutAccessoryControlTapped");
    
    LocationViewController *locVC;
    
    if(isTimerSet)
        locVC = [[LocationViewController alloc]initWithNibName:@"LocationViewController" bundle:nil userLoc:location_Details carLoc:carLoc_Details];
    else
        locVC = [[LocationViewController alloc]initWithNibName:@"LocationViewController" bundle:nil userLoc:location_Details carLoc:location_Details];
    
    
    
    
    locVC.modalTransitionStyle = UIModalTransitionStylePartialCurl;
    [self presentModalViewController:locVC animated:YES];
    
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (error.code ==  kCLErrorDenied)
    {
        NSLog(@"Location manager denied access - kCLErrorDenied");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location issue"
                                                        message:@"Your location cannot be determined."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if(textField.tag == 0)
        return (newLength > 25) ? NO : YES;
    else
        return (newLength > 10) ? NO : YES;
}


//===================================================================================================//








- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    
    [self setSetremainderButton:nil];
    [self setLocdescLabel:nil];
    [self setSettimerLabel:nil];
    [self setAfterthemeterLabel:nil];
    [self setGobackButton:nil];
    [self setBaseView:nil];
    [self setTimerFlipView:nil];
    [self setTimerFlipView:nil];
    [super viewDidUnload];
}
@end
