//
//  MenuViewController.h
//  TWU
//
//  Created by MyRewards on 2/15/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "MyUnionViewController.h"

#import "MyDelegateViewController.h"
#import "MyUnionMainViewController.h"
#import "MyCardViewController.h"
#import "MyCarViewController.h"
#import "SendAFriendViewController.h"
#import "MyParkerTimerViewController.h"
#import "MyNoticeBoardViewController.h"
#import "MyIssueViewController.h"
#import "MyAccountViewController.h"
#import "ProductListViewController.h"
#import "SearchViewController.h"
#import "myroadsideViewController.h"
#import "DailyDealsViewController.h"
#import "NoticeBoardViewController.h"
#import "HelpViewController.h"
#import "SwitchTabBar.h"
#import "SwitchTabBarController.h"



//@class MyUnionViewController;
@class MyNoticeBoardViewController;
@class SendAFriendViewController;
@class MyIssueViewController;
@class MyAccountViewController;
@class ProductListViewController;


@interface MenuViewController : UIViewController<NoticeBoardXMLParserDelegate>
{
    
    //MyUnionViewController *myunionViewController;
        
    
    MyUnionMainViewController           *myunionmainview;
    MyDelegateViewController            *mydelegateview;
    MyCardViewController                *mycardview;
    MyCarViewController                 *mycarviewcontroller;
    SendAFriendViewController           *sendafriendview;
    MyParkerTimerViewController         *myparkertimerview;
    MyNoticeBoardViewController         *mynoticeboard;
    MyIssueViewController               *myissueview;
    MyAccountViewController             *myaccountview;
    ProductListViewController           *myunionrewardsview;
    ProductListViewController           *favoriteViewController;
    SearchViewController                *search;
    myroadsideViewController            *roadsideview;
    
    
    UIButton *MyUnion_Button;
    UIButton *MyDelegates_Button;
    UIButton *MyCard_Button;
    
    UINavigationController *SearchNav;
    NSArray *imgArr;
    
   // IBOutlet UIView *bannerView;
    IBOutlet UIImageView *badgeCount_image;
    IBOutlet UILabel *badgeCountLabel;
    IBOutlet UIView *badge;
    
}
@property (nonatomic,strong) IBOutlet UIView *badge;
@property(nonatomic,strong) UIView *contentView;
@property(nonatomic,strong) IBOutlet UIImageView *badgeCount_image;
@property(nonatomic,strong) IBOutlet UILabel *badgeCountLabel;

@property (strong, nonatomic) IBOutlet UIButton *fourOne;
@property (strong, nonatomic) IBOutlet UIButton *fourTwo;
@property (strong, nonatomic) IBOutlet UIButton *fourThree;
@property (strong, nonatomic) IBOutlet UIButton *fourFour;
@property (strong, nonatomic) IBOutlet UIButton *fourFive;
@property (strong, nonatomic) IBOutlet UIButton *fourSix;
@property (strong, nonatomic) IBOutlet UIButton *fourSeven;
@property (strong, nonatomic) IBOutlet UIButton *fourEight;
@property (strong, nonatomic) IBOutlet UIButton *fourNine;
@property (strong, nonatomic) IBOutlet UIButton *fourTen;
@property (strong, nonatomic) IBOutlet UIButton *fourEleven;
@property (strong, nonatomic) IBOutlet UIButton *fourTwelve;
@property (strong, nonatomic) IBOutlet UIImageView *MenuBgView;

@property (strong, nonatomic) IBOutlet UIButton *fiveOne;
@property (strong, nonatomic) IBOutlet UIButton *fiveTwo;
@property (strong, nonatomic) IBOutlet UIButton *fiveThree;
@property (strong, nonatomic) IBOutlet UIButton *fiveFour;
@property (strong, nonatomic) IBOutlet UIButton *fiveSix;

@property (strong, nonatomic) IBOutlet UIButton *fiveFive;
@property (strong, nonatomic) IBOutlet UIButton *fiveSeven;
@property (strong, nonatomic) IBOutlet UIButton *fiveEight;
@property (strong, nonatomic) IBOutlet UIButton *fiveNine;
@property (strong, nonatomic) IBOutlet UIButton *fiveTen;
@property (strong, nonatomic) IBOutlet UIButton *fiveEleven;
@property (strong, nonatomic) IBOutlet UIButton *fiveTwelve;
@property (strong, nonatomic) IBOutlet UIImageView *nwBgMenu;


@property(nonatomic, strong)UIButton *MyUnion_Button;
@property(nonatomic, strong)UIButton *MyDelegates_Button;
@property(nonatomic, strong)UIButton *MyCard_Button;






- (IBAction)MenuViewButtonTapped:(id)sender;
-(void)loadViews;
-(void)tabBarController;
@end
