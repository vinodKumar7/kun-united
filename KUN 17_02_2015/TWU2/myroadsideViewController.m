//
//  myroadsideViewController.m
//  TWU2
//
//  Created by vairat on 22/04/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "myroadsideViewController.h"
#import "ProductAnnotation.h"
#import "RoadSideMap.h"

#define SPAN 0.10f;
#define HYD_LAT 17.3667;
#define HYD_LON 78.4667;

#define kBgQueue dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0)
#define kBaseUrl @"http://maps.googleapis.com/maps/api/directions/json?"

@interface myroadsideViewController ()
{
    NSString *callNumber;
}

@end

@implementation myroadsideViewController
@synthesize currentlocation_Label;
@synthesize currentLocation;
@synthesize roadMapView,locationTableView;
@synthesize wayPoints,locationLabel;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

#pragma mark- View LifeCycle Methods
- (void)viewDidLoad
{
   // [self StartUpdating];
    [super viewDidLoad];
    
    roadMapView.delegate = self;
    
//    TableView for Address
    CGRect frame = CGRectMake(20, -7, 290, 200);
    
    locationTableView = [[UITableView alloc]initWithFrame:frame];
    locationTableView.rowHeight = 80;
    
    locationTableView.dataSource = self;
    locationTableView.delegate = self;
    
    //table Desigin
    locationTableView.layer.cornerRadius = 10;
    locationTableView.layer.borderWidth = 2.0f;
    locationTableView.layer.masksToBounds = YES;
    locationTableView.layer.shadowColor = [[UIColor blackColor] CGColor];
    locationTableView.layer.shadowOffset = CGSizeMake(0.0f,0.5f);
    locationTableView.layer.shadowOpacity = .5f;
    locationTableView.layer.shadowRadius = 0.5f;
    
    locationTableView.hidden = YES;
    
    [self.roadMapView addSubview:locationTableView];

    self.roadMapView.showsUserLocation = YES;
    addressArray = [[NSMutableArray alloc]initWithObjects:@"#1-8-870, Azamabad, Near VST, RTC X Roads, Hyderabad.",@"Plot No: 8, Mini Industrial Estate, Hafeezpet, Miyapur, Hyderabad.",@"Plot No:285, Beside Lakshmi Gardens,ECIL X Road, RR Dist. - 500062.",@"B-4 Block 3, Beside Alkali Metal LTD, IDA, Uppal, Ranga Reddy - 500039",@"Door no 10-1, New gayatri nagar, Near SBH, Jillelaguda, Karmanghat, Hyderabad 500079",@"H.No:1-72/2/1/1, Plot No:2 & 13,Survey No:50, Gachi Bowli Main Road, Serilingam Pally, Hyderabad - 500032.",@" All Locations", nil];
   
    subTitleArray = [[NSMutableArray alloc]initWithObjects:@"RTC X Roads, Hyderabad",@"Hafeezpet, Hyderabad",@"ECIL X Roads, Hyderabad",@"IDA, Uppal",@"Jillelaguda, Karmanghat, Hyderabad",@"GachiBowli, Hyderabad ", nil];
    
    phnoArray = [[NSMutableArray alloc]initWithObjects:@"09246-537791",@"09246-537792",@"09246-537793",@"09246-537795",@"09246-537796",@"09246-537790", nil];
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
    UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
    [myButton1 setImage:myImage1 forState:UIControlStateNormal];
    myButton1.showsTouchWhenHighlighted = YES;
    myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
    [myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
    self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"My Roadside Assist";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor whiteColor]];
    self.navigationItem.titleView = TitleLabel;
    
    if (IOS_VERSION >= 7)
    {
        locationLabel.textColor=[UIColor whiteColor];
    }else
    {
        locationLabel.textColor=[UIColor blackColor];
    }
    
}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    //Your Code here
    [self StartUpdating];
}


- (void)viewDidUnload {
    NSLog(@"View did it Method Executed.............");
    
    
    currentlocation_Label = nil;
    [self setRoadMapView:nil];
    [self setCurrentlocation_Label:nil];
    [super viewDidUnload];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark- MapView Delegate Methods

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>) annotation{
   
//    if (annotation == mapView.userLocation)
//        return nil;
    
    if ([[(RoadSideMap *)annotation title] isEqualToString:@"Current Location"])
    {
        return nil;
    }
    
    static NSString *annotationIdentifier = @"AnnotationIdentifier";
    pin = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    
    if (!pin) {
        pin = [[MKPinAnnotationView alloc]initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
        
        pin.canShowCallout = YES;
        pin.image = [UIImage imageNamed:@"automobile_map.png"];
        
    
        callButton = [UIButton buttonWithType:UIButtonTypeCustom];
        callButton.frame = CGRectMake(0, 0, 30, 30);
        callButton.contentVerticalAlignment = UIControlContentVerticalAlignmentCenter;
        callButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        
        [callButton setImage:[UIImage imageNamed:@"call.png"] forState:UIControlStateNormal];
      
        pin.leftCalloutAccessoryView = callButton;
        pin.userInteractionEnabled = YES;
        

    }
    return pin;
   
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view
{
    //RoadSideMap *annotation = (RoadSideMap *)mapView.annotations;
//    NSInteger *yourIndex = annotation.title;
    NSLog(@"annotation string %ld",(long)[mapView tag]);
}


- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    RoadSideMap *ann =(RoadSideMap *)view.annotation;
    callNumber = ann.phoneNo;
    
    UIAlertView *call_Alert = [[UIAlertView alloc]initWithTitle:@"Call" message:callNumber delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel" , nil];
    [call_Alert show];
    
}


-(void)StartUpdating
{
    NSLog(@"Startupdating Method Executed.............");
    locManager = [[CLLocationManager alloc] init];
    locManager.delegate = self;
    locManager.distanceFilter = kCLDistanceFilterNone; // whenever we move
    locManager.desiredAccuracy = kCLLocationAccuracyHundredMeters; // 100 m
    [locManager startUpdatingLocation];
}

#pragma mark
#pragma mark locationManager delegate methods


- (void)locationManager: (CLLocationManager *)manager didUpdateToLocation: (CLLocation *)newLocation
           fromLocation: (CLLocation *)oldLocation
{
    
    
    _currentLocationCordinates = [newLocation coordinate];
     self.currentLocation = _currentLocationCordinates;
    MKCoordinateRegion region = roadMapView.region;
    
    region.center.latitude = self.currentLocation.latitude;
    region.center.longitude = self.currentLocation.longitude;
    
    
    
    
    NSString *req = [NSString stringWithFormat:@"http://maps.googleapis.com/maps/api/geocode/json?latlng=%f,%f&sensor=false",_currentLocationCordinates.latitude,_currentLocationCordinates.longitude];
    NSLog(@"reqest is %@",req);
    NSDictionary *googleResponse = [[NSString stringWithContentsOfURL: [NSURL URLWithString: req] encoding: NSUTF8StringEncoding error: NULL] JSONValue];
    
    NSDictionary *resultsDict = [googleResponse valueForKey:  @"results"];   // get the results dictionary
    NSArray  *currentLoctionAddress = [resultsDict valueForKey: @"formatted_address"];   // geometry dictionary within the  results dictionary

    NSLog(@"Addres Array is %@",currentLoctionAddress);
    addressTxt = [currentLoctionAddress objectAtIndex:1];
    NSLog(@"Addres Text is %@",addressTxt);
    
    if ([ addressTxt rangeOfString:@"India"].location==NSNotFound)
    {
        NSLog(@"Substring Not Found");
        
        _chooseBtnOutlet.userInteractionEnabled = NO;
        _selectBtnOutlet.userInteractionEnabled = NO;
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No services available " delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
        
    }
    else
    {
        NSLog(@"Substring Found Successfully");
    }

    
//    NSArray *lngArray = [locationDict valueForKey: @"lng"];
//    NSString *lngString = [lngArray lastObject];     // (one element) array entries provided by the json parser

   
//    float latitude = newLocation.coordinate.latitude;
//    strLatitude = [NSString stringWithFormat:@"%f",latitude];
//    float longitude = newLocation.coordinate.longitude;
//    strLongitude = [NSString stringWithFormat:@"%f", longitude];
//    NSLog(@"lat is %@ long is %@ ",strLatitude,strLongitude);
//    [self returnLatLongString];
//    CLGeocoder * geoCoder = [[CLGeocoder alloc] init];
//        [geoCoder reverseGeocodeLocation:newLocation completionHandler:^(NSArray *placemarks, NSError *error) {
//        for (CLPlacemark * placemark in placemarks)
//        {
//            addressTxt = [NSString stringWithFormat:@"%@,%@,%@",[placemark subLocality],[placemark locality],@"Hyderabad"];
//            NSLog(@"Address::::%@",addressTxt);
//        }
//    }];
    
    
    MKCoordinateSpan span;
    span.latitudeDelta  = SPAN;
    span.longitudeDelta = SPAN;
    //region.center =center;
    region.span=span;

    [roadMapView setRegion:region animated:YES];
    [roadMapView showsUserLocation];
    [self returnLatLongString];
    
}


-(NSString*)returnLatLongString
{
    NSLog(@"Return Method Executed.............");
    NSString *str = [NSString stringWithFormat: @"lat=%@  long=%@", strLatitude, strLongitude];
    NSLog(@"%@",str);
    [locManager stopUpdatingLocation];
    return str;
    
}


#pragma mark- Button Action Methods:
- (IBAction)kunServiceLinkButton_Tapped:(id)sender {
    
    //launch safari and open KunUnited Service site...
     [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.kununited.com/service.htm"]];
}

- (IBAction)chooseLocationButton_Tapped:(id)sender {
    
    self.roadMapView.showsUserLocation = NO;
    tableViewFlag++;

    if (tableViewFlag == 2) {
        tableViewFlag = 0;
        locationTableView.hidden = YES;
    }
    else
        locationTableView.hidden = NO;
    
}

-(void)back
{
    
    //[activityIndicatorView startAnimating];
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}

#pragma mark- TableView DataSource Methods

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [addressArray count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CellIdentifier";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    
    cell.textLabel.numberOfLines = 3;
    cell.textLabel.text = [addressArray objectAtIndex:indexPath.row];
    return cell;
}

#pragma mark- TableView Delegate Methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
   
    currentlocation_Label.text = [addressArray objectAtIndex:indexPath.row];
    
    locationTableView.hidden = YES;
    tableViewFlag = 0;
    
    
    if ([[addressArray objectAtIndex:indexPath.row] isEqualToString:@" All Locations"]) {
        for (int i = 0; i < [addressArray count]-1; i++) {
            if([[addressArray objectAtIndex:i] length] >0)
            {
                
                dispatch_async(kBgQueue, ^{
                    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                    NSString *strUrl;
                    
                    strUrl=[NSString stringWithFormat:@"%@origin=%@&destination=%@&sensor=true",kBaseUrl,addressTxt,[subTitleArray objectAtIndex:i]];
                    NSLog(@"String url is %@",strUrl);
                    
                    strUrl=[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                    NSData *data =[NSData dataWithContentsOfURL:[NSURL URLWithString:strUrl]];
                    
                    [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
                    
                    roadSideMapObj.title = [subTitleArray objectAtIndex:i];
                    roadSideMapObj.phoneNo = [phnoArray objectAtIndex:i];//For Taking Phone Number...

                });
                
            }
        }
    }
    else
    {
        [roadMapView removeAnnotations:roadMapView.annotations];
        distanceCompareValue = 0.0;
        timeCompareValue = 0;
        
        if([[addressArray objectAtIndex:indexPath.row] length] >0)
        {
            
            [roadMapView removeOverlays: roadMapView.overlays]; //To Remove overlayView on the map on every time...
            
            
            dispatch_async(kBgQueue, ^{
                [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
                NSString *strUrl;
                
               
                strUrl=[NSString stringWithFormat:@"%@origin=%@&destination=%@&sensor=true",kBaseUrl,addressTxt,[subTitleArray objectAtIndex:indexPath.row]];
                
                
                NSLog(@"String url is %@",strUrl);
                strUrl=[strUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
                NSData *data =[NSData dataWithContentsOfURL:[NSURL URLWithString:strUrl]];
                
                [self performSelectorOnMainThread:@selector(fetchedData:) withObject:data waitUntilDone:YES];
                
                roadSideMapObj.title = [subTitleArray objectAtIndex:indexPath.row];
                roadSideMapObj.phoneNo = [phnoArray objectAtIndex:indexPath.row];  //For Taking Phone Number...
            });
            
            
        }

    }

}


#pragma mark - json parser

- (void)fetchedData:(NSData *)responseData {
    NSError* error;

    NSDictionary* json = [NSJSONSerialization
                          JSONObjectWithData:responseData //1
                          
                          options:kNilOptions
                          error:&error];
    NSArray *arrRouts=[json objectForKey:@"routes"];
    
    if ([arrRouts isKindOfClass:[NSArray class]]&&arrRouts.count==0) {
        UIAlertView *alrt=[[UIAlertView alloc]initWithTitle:@"Alert" message:@"didn't find direction" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alrt show];
        return;
    }
    NSArray *arrDistance =[[[json valueForKeyPath:@"routes.legs.steps.distance.text"] objectAtIndex:0]objectAtIndex:0];
    NSString *totalDuration = [[[json valueForKeyPath:@"routes.legs.duration.text"] objectAtIndex:0]objectAtIndex:0];
    NSString *totalDistance = [[[json valueForKeyPath:@"routes.legs.distance.text"] objectAtIndex:0]objectAtIndex:0];
    NSArray *arrDescription =[[[json valueForKeyPath:@"routes.legs.steps.html_instructions"] objectAtIndex:0] objectAtIndex:0];
    dictRouteInfo=[NSDictionary dictionaryWithObjectsAndKeys:totalDistance,@"totalDistance",totalDuration,@"totalDuration",arrDistance ,@"distance",arrDescription,@"description", nil];
    
    if (distanceCompareValue == 0.0 && timeCompareValue == 0) {
        distanceCompareValue = [totalDistance doubleValue];
        timeCompareValue = [totalDuration intValue];
        
        _distanceLabel.text = [NSString stringWithFormat:@"%@ %@", @"Distance:", totalDistance];
        _timeLabel.text = [NSString stringWithFormat:@"%@ %@", @"Time:", totalDuration];
    }
    else
    {
        if (distanceCompareValue >= [totalDistance doubleValue] && timeCompareValue >= [totalDuration intValue]) {
            distanceCompareValue = [totalDistance doubleValue];
            timeCompareValue = [totalDuration intValue];
        }
        
        _distanceLabel.text = [NSString stringWithFormat:@"%@ %0.1f km", @"Shortest Dist:", distanceCompareValue];
        _timeLabel.text = [NSString stringWithFormat:@"%@ %d mins", @"Time:", timeCompareValue];
    }
    
    
    
    distanceAndTimeString = [NSString stringWithFormat:@"%@, %@", totalDistance, totalDuration];
    
//    NSLog(@"total duration %@",totalDuration);
//    NSLog(@"total distance %@",totalDistance);
    
    NSArray* arrpolyline = [[[json valueForKeyPath:@"routes.legs.steps.polyline.points"] objectAtIndex:0] objectAtIndex:0]; //2
    double srcLat=[[[[json valueForKeyPath:@"routes.legs.start_location.lat"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    double srcLong=[[[[json valueForKeyPath:@"routes.legs.start_location.lng"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    double destLat=[[[[json valueForKeyPath:@"routes.legs.end_location.lat"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    double destLong=[[[[json valueForKeyPath:@"routes.legs.end_location.lng"] objectAtIndex:0] objectAtIndex:0] doubleValue];
    
    NSLog(@"Source latitude and longitude is %f,%f",srcLat,srcLong);
    NSLog(@"Dest latitude and longitude is %f,%f",destLat,destLong);
    
    CLLocationCoordinate2D sourceCordinate = CLLocationCoordinate2DMake(srcLat, srcLong);
    CLLocationCoordinate2D destCordinate = CLLocationCoordinate2DMake(destLat, destLong);
    
	
	[self addAnnotationSrcAndDestination:sourceCordinate :destCordinate];
    
    NSMutableArray *polyLinesArray =[[NSMutableArray alloc]initWithCapacity:0];
    
    for (int i = 0; i < [arrpolyline count]; i++)
    {
        NSString* encodedPoints = [arrpolyline objectAtIndex:i] ;
        MKPolyline *route = [self polylineWithEncodedString:encodedPoints];
        [polyLinesArray addObject:route];
    }
    
    
    
    [roadMapView addOverlays:polyLinesArray];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    

}


#pragma mark - add annotation on source and destination

-(void)addAnnotationSrcAndDestination :(CLLocationCoordinate2D )srcCord :(CLLocationCoordinate2D)destCord
{
    roadSideMapObj = [[RoadSideMap alloc]init];
    
    MKPointAnnotation *sourceAnnotation = [[MKPointAnnotation alloc]init];
//    MKPointAnnotation *destAnnotation = [[MKPointAnnotation alloc]init];
    sourceAnnotation.coordinate=srcCord;
    roadSideMapObj.coordinate = destCord;
    
//    destAnnotation.coordinate=destCord;
    
    sourceAnnotation.title = @"Current Location";
    roadSideMapObj.subtitle = distanceAndTimeString;

    
    
    [roadMapView addAnnotation:sourceAnnotation];
    [roadMapView addAnnotation:roadSideMapObj];
    
    MKCoordinateRegion region;
    
    MKCoordinateSpan span;
    span.latitudeDelta=2;
    span.latitudeDelta=2;
    region.center=srcCord;
    region.span=span;
    CLGeocoder *geocoder= [[CLGeocoder alloc]init];
    for (NSString *strVia in wayPoints) {
        [geocoder geocodeAddressString:strVia completionHandler:^(NSArray *placemarks, NSError *error) {
            if ([placemarks count] > 0) {
                CLPlacemark *placemark = [placemarks objectAtIndex:0];
                CLLocation *location = placemark.location;
                //                CLLocationCoordinate2D coordinate = location.coordinate;
                MKPointAnnotation *viaAnnotation = [[MKPointAnnotation alloc]init];
                viaAnnotation.coordinate=location.coordinate;
                [roadMapView addAnnotation:viaAnnotation];
                //                NSLog(@"%@",placemarks);
            }
            
        }];
    }
    
    roadMapView.region=region;
}

#pragma mark - decode map polyline

- (MKPolyline *)polylineWithEncodedString:(NSString *)encodedString {
    const char *bytes = [encodedString UTF8String];
    NSUInteger length = [encodedString lengthOfBytesUsingEncoding:NSUTF8StringEncoding];
    NSUInteger idx = 0;
    
    NSUInteger count = length / 4;
    CLLocationCoordinate2D *coords = calloc(count, sizeof(CLLocationCoordinate2D));
    NSUInteger coordIdx = 0;
    
    float latitude = 0;
    float longitude = 0;
    while (idx < length) {
        char byte = 0;
        int res = 0;
        char shift = 0;
        
        do {
            byte = bytes[idx++] - 63;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLat = ((res & 1) ? ~(res >> 1) : (res >> 1));
        latitude += deltaLat;
        
        shift = 0;
        res = 0;
        
        do {
            byte = bytes[idx++] - 0x3F;
            res |= (byte & 0x1F) << shift;
            shift += 5;
        } while (byte >= 0x20);
        
        float deltaLon = ((res & 1) ? ~(res >> 1) : (res >> 1));
        longitude += deltaLon;
        
        float finalLat = latitude * 1E-5;
        float finalLon = longitude * 1E-5;
        
        CLLocationCoordinate2D coord = CLLocationCoordinate2DMake(finalLat, finalLon);
        coords[coordIdx++] = coord;
        
        if (coordIdx == count) {
            NSUInteger newCount = count + 10;
            coords = realloc(coords, newCount * sizeof(CLLocationCoordinate2D));
            count = newCount;
        }
    }
    
    MKPolyline *polyline = [MKPolyline polylineWithCoordinates:coords count:coordIdx];
    free(coords);
    
    return polyline;
}

#pragma mark - map overlay
- (MKOverlayView *)mapView:(MKMapView *)mapView
            viewForOverlay:(id<MKOverlay>)overlay {
    
    
    
    MKPolylineView *overlayView = [[MKPolylineView alloc] initWithOverlay:overlay];
    overlayView.lineWidth = 7;
    overlayView.strokeColor = [UIColor purpleColor];
    overlayView.fillColor = [[UIColor purpleColor] colorWithAlphaComponent:0.1f];
    return overlayView;
    
}


#pragma mark- AlertView Delegate Methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == 0) {
        
        NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@",callNumber];
        NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
        [[UIApplication sharedApplication] openURL:phoneURL];
    }
    

}



@end
