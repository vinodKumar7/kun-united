//
//  MyIssueViewController.m
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "MyUnionContact.h"
#import "MyIssueViewController.h"
#import "AppDelegate.h"
#define kOFFSET_FOR_KEYBOARD 135


@interface MyIssueViewController ()
{
    AppDelegate *appDelegate;
    BOOL stayup;
    UILabel *mylabel;
    NSString *textView_Text;
     NSString *ContactNumber;
    UIActionSheet *asheet;
     UIActionSheet *datesheet;
    UIActionSheet *servicesheet;
    NSArray *pickerContentList;
    NSArray *pickerServiceContents;
    NSDateFormatter *dateFormatter;
    UITextField *activeField;
    UIDatePicker *myDatepicker;
    UIView *ServicePickerContainerView;
    UIView *datePickerContainerView;
    UIView *locationPickerContainerView;
   
    int index;
    
    int currentRow;
   
    ParkingTimer *currentRemainder;
    NSArray *contactsArray;
   
    UIPickerView *mypickerView;
    UIPickerView *servicePicker;
    UIPickerView *datePicker;
    NSDate *endTimeIndicator;
    int currentMenuItemTag;
   
    UIScreen *mainScreen;
    CGFloat scale;
    CGFloat pixelHeight;
        
    NSString *selected_Date;


}@property(nonatomic,strong)NSArray *contactsArray;
@property(nonatomic,strong)NSArray *pickerContentList;
@property(nonatomic,strong)NSArray *pickerServiceContents;
@property(nonatomic,strong)UIPickerView *mypickerView;
@property(nonatomic,strong)UIActionSheet *asheet;
@property(nonatomic,strong)UIActionSheet *servicesheet;
@property(nonatomic,strong)UIPickerView *servicePicker;
@property(nonatomic,strong)NSDateFormatter *dateFormatter;


-(void)loadMyUnionContacts;

@property(nonatomic,strong)UILabel *mylabel;
@end

@implementation MyIssueViewController

@synthesize myTextView;
@synthesize textViewBackground;
@synthesize containerView;
@synthesize background_ImageView;
@synthesize accessoryView;
@synthesize mylabel;
@synthesize nameLabel;
@synthesize membershipLabel;
@synthesize send_Button;
@synthesize add_Button;
@synthesize delete_Button,current_Textfield;

@synthesize topImageView;
@synthesize namebackimageView;

@synthesize memnobackImageView;

@synthesize registration_TextField;
@synthesize vechicle_TextField;
@synthesize date_TextField;
@synthesize location_Textfield;
@synthesize service_TextField;

@synthesize mypickerView;
@synthesize servicePicker;
@synthesize servicesheet;

@synthesize contactsArray;

@synthesize pickerContentList;
@synthesize pickerServiceContents;

@synthesize asheet;

@synthesize dateFormatter;
@synthesize contactsList;


//@synthesize unionViewController;
//=========================DELEGATE METHODS=============================//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        textView_Text = @"";
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
     
    
    [self loadViews];
     index = 0;
    currentRow=0;
    
    pickerContentList=[[NSArray alloc]initWithObjects:@"Camberwell VW",@"City Peugeot",@"Coburg Ford",@"CoburgFIA",@"Epping VW",@"Epping Ford",@"South Yarra Peugeot",nil];
    //Free Service, Paid Service, Running Repair, Accident Repair, Accessories
    pickerServiceContents=[[NSArray alloc]initWithObjects:@"Free Service",@"Paid Service",@"Running Repair",@"Other",nil];
    locationArray = [[NSMutableArray alloc]initWithObjects:@"RTC X Roads",@"Hafeezpet",@"ECIL X Road",@"IDA, Uppal",@"Karmanghat",@"Gachi Bowli Main Road", nil];
    
    self.nameLabel.text = [NSString stringWithFormat:@"%@ %@",appDelegate.sessionUser.first_name, appDelegate.sessionUser.last_name];
    self.membershipLabel.text = appDelegate.sessionUser.username;
    
    //setting container corner curve shape
    
    myTextView.layer.cornerRadius = 10.0;
    myTextView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    myTextView.layer.shadowOpacity = 1.0;

    //[webView setClipsToBounds:YES];
   
    
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self.vechicle_TextField     setEnabled:YES];
    [self.registration_TextField setEnabled:YES];
    self.date_TextField.tag=50;
    //[self.date_TextField     setEnabled:NO];
   // [self.service_TextField setEnabled:NO];
    //[self.location_Textfield setEnabled:NO];
    
    vechicle_TextField.text=@"";
    registration_TextField.text=@"";
    date_TextField.text=@"";
    service_TextField.text=@"";
    location_Textfield.text=@"";
   // myTextView.text=@"";

    [self addServiceAction:nil];
    [self addDateAction:nil];
    [self addLocationAction:nil];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"Book A Service";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor whiteColor]];;
    self.navigationItem.titleView = TitleLabel;
   
    [self loadMyUnionContacts];
    
}

-(void)back
{
    [self doneButton_Pressed:0];

    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}

//=================================================================//
-(void)loadMyUnionContacts
{
    NSLog(@"[self loadMyUnionContacts];");
      NSLog(@"contact list3:: %d",[appDelegate mysoscontacts].count);
    if (contactsList.count > 0)
    {
        //[nocontactsavailableLabel setHidden:YES];
        [contactsList removeAllObjects];
        NSLog(@"contact list2:: %d",contactsList.count);
    }
    
    [contactsList addObjectsFromArray:[appDelegate mysoscontacts]];
    NSLog(@"contact list4:: %d",contactsList.count);
    if ([appDelegate mysoscontacts] > 0)
    {
        int last=[[appDelegate mysoscontacts] count]-1 ;
        if (last==-1)
        {
            NSLog(@"Omit");
            
        }
        else
        {
            MyUnionContact *newContact = [[appDelegate mysoscontacts] objectAtIndex:last];
            
            
            NSString *text=[NSString stringWithFormat:@"%@/%@",newContact.name,newContact.carModel];
            self.vechicle_TextField.text=text  ;
            self.registration_TextField.text=newContact.carRegistration  ;
//            [self.registration_TextField setEnabled:NO];
//            [self.vechicle_TextField setEnabled:NO];
           
            NSLog(@"Details is.... %@",newContact);
        }
    }
    if (contactsList.count == 0)
    {
        
        
        NSLog(@"contact list3:: %d",[appDelegate mysoscontacts].count);
    }
    
}


//==============DELEGATE METHODS TO LOCK ORIENTATION==============//


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}

- (BOOL) shouldAutorotate
{
    return NO;
}

//==================================================//

// ===============METHOD TO LOAD VIEWS IN IPHONE  =================//

-(void)loadViews
{
    if([appDelegate isIphone5])
    {
        NSLog(@"Iphone5");
        
        CGRect frame =self.background_ImageView.frame;
        frame.size.height = 435;
        self.background_ImageView.frame = frame;
        
        
        
        CGRect textViewframe = self.myTextView.frame;
        textViewframe.size.height = 95;
        self.myTextView.frame = textViewframe;
        
        CGRect textViewBackgroundframe = self.textViewBackground.frame;
        textViewBackgroundframe.size.height = 230;
        textViewBackgroundframe.size.width=260;
        textViewBackgroundframe.origin.x=30;
        self.textViewBackground.frame = textViewBackgroundframe;
        
        CGRect containerframe2 = self.topImageView.frame;
        containerframe2.origin.x=30;
        containerframe2.size.width=260;
        containerframe2.origin.y = 25;
        self.topImageView.frame = containerframe2;
        
               CGRect namelabelframe = self.nameLabel.frame;
        namelabelframe.origin.y = 33;
        self.nameLabel.frame = namelabelframe;
        
        CGRect nameimageframe=self.namebackimageView.frame;
        nameimageframe.origin.y=33;
        self.namebackimageView.frame=nameimageframe;
        
        CGRect memnolabelframe = self.membershipLabel.frame;
        memnolabelframe.origin.y = 75;
        self.membershipLabel.frame = memnolabelframe;
        
        CGRect memimageframe=self.memnobackImageView.frame;
        memimageframe.origin.y=73;
        self.memnobackImageView.frame=memimageframe;
        
        
        CGRect sendButtonframe =self.send_Button.frame;
        sendButtonframe.origin.y = 458;
        self.send_Button.frame = sendButtonframe;
        
        
        CGRect deleteButtonframe =self.delete_Button.frame;
        deleteButtonframe.origin.y = 468;
        self.delete_Button.frame = deleteButtonframe;
        
        mylabel = [[UILabel alloc]initWithFrame:CGRectMake(14, -85, 280, 250)];
        mylabel.text = @"please add your name and a contact number so someone from the team can contact you and book you in for your service.";
        mylabel.backgroundColor = [UIColor clearColor];
        mylabel.font = [UIFont systemFontOfSize:14];
        mylabel.numberOfLines=4;
        mylabel.textColor = [UIColor lightGrayColor];
        [self.myTextView addSubview:mylabel];
        
        
        
    }
    else
    {
        NSLog(@"Iphone4");
        
        CGRect textViewframe = self.myTextView.frame;
        textViewframe.size.height = 104;
        self.myTextView.frame = textViewframe;
        
        CGRect textViewBackgroundframe = self.textViewBackground.frame;
        textViewBackgroundframe.size.height = 148;
        textViewBackgroundframe.size.width=260;
        textViewBackgroundframe.origin.x=30;
        self.textViewBackground.frame = textViewBackgroundframe;
        
        CGRect sendButtonframe =self.send_Button.frame;
        sendButtonframe.origin.y = 380;
        self.send_Button.frame = sendButtonframe;
        
        
        CGRect deleteButtonframe =self.delete_Button.frame;
        deleteButtonframe.origin.y = 380;
        self.delete_Button.frame = deleteButtonframe;
        mylabel = [[UILabel alloc]initWithFrame:CGRectMake(14, -80, 280, 250)];
        mylabel.text = @"please add your name and a contact number so someone from the team can contact you and book you in for your service.";
        mylabel.backgroundColor = [UIColor clearColor];
        mylabel.numberOfLines=4;
        mylabel.font = [UIFont systemFontOfSize:14];
        mylabel.textColor = [UIColor lightGrayColor];
       [self.myTextView addSubview:mylabel];
        
        
    }
    
}
//======================================================//

//=======================METHOD TO DONE BUTTON ====================//

-(IBAction)doneButton_Pressed:(id)sender
{
    if(myTextView.text.length == 0)
    {
        mylabel.hidden = NO;
        textView_Text = @"";
        
        
    }
    [myTextView resignFirstResponder];
     [current_Textfield resignFirstResponder];
    
}

//===================================================================//

//====================METHOD TO SEND BUTTON ==========================//

-(IBAction)sendButton_Pressed:(id)sender
{
    
    if ([MFMailComposeViewController canSendMail])
    {
        // Create and show composer
        if (vechicle_TextField.text.length ==0 || registration_TextField.text.length == 0 || location_Textfield.text.length == 0 || date_TextField.text.length == 0 || service_TextField.text.length == 0) {
            
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"All fields are mandatory" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
        }
        else
        {
            MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
            controller.mailComposeDelegate = self;
            
            NSArray *array=[[NSArray alloc] initWithObjects:@"kunited_hyd@yahoo.com", nil];
            [controller setToRecipients:array];
            [controller setSubject:@"Book A Service"];
            [controller setMessageBody:[NSString stringWithFormat:@" Vechicle name:: %@</br>Registration::  %@</br>Location::%@</br> Date and Time :: %@</br>Type of Service::%@</br>Comments::%@",vechicle_TextField.text,registration_TextField.text,location_Textfield.text,date_TextField.text,service_TextField.text,myTextView.text]  isHTML:YES];
            [self presentModalViewController:controller animated:YES];
            

        }
        
        
    }
    else
    {
        // Show some error message here
        UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                             message:@"You will need to setup a mail account on your device before you can send mail!"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [alert_view show];
        return;
    }
    
    
}
//==============================================================//

//===========DELEGATE METHOD TO CANCEL MAIL COMPOSER=================//

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
//===========================================================//

//==========================METHOD TO DELETE BUTTON =====================//
-(IBAction)deleteButton_Pressed:(id)sender
{
    UIActionSheet *deletesheet = [[UIActionSheet alloc] initWithTitle:@"" delegate:self cancelButtonTitle:nil destructiveButtonTitle:@"Cancel" otherButtonTitles:@"Delete", nil];
    [deletesheet showInView:[self.view superview]];
    
    
}

- (IBAction)addDateAction:(id)sender
{
    UIPickerView  *myPickerView = [[ UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator=YES;
    myPickerView.backgroundColor=[UIColor whiteColor];
    
    
    
    [myPickerView setFrame:CGRectMake(0, 20, 320, 250)];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(250, 10, 50, 30)];
    [button setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneButton_Pressed:) forControlEvents:UIControlEventTouchUpInside];
    
    
    datePickerContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, 250, 320, 200)];
    datePickerContainerView.backgroundColor = [UIColor whiteColor];
    // Add the picker
    myDatepicker = [[UIDatePicker alloc] initWithFrame:CGRectMake(0,40, 320, 216)];
    [myDatepicker addTarget:self action:@selector(dateIsChanged:) forControlEvents:UIControlEventValueChanged];
    [datePickerContainerView addSubview:myDatepicker];
    
}

- (void)dateIsChanged:(id)sender
{
    NSLog(@"Date changed:%@",myDatepicker.date);
    NSString *dateString;
    NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
    [dateFormatter1 setDateFormat:@"dd-MM-yyyy hh:mm "];//EEEE MMMM d,yyyy hh:mm a
    dateString = [dateFormatter1 stringFromDate:myDatepicker.date];
    self.date_TextField.text=dateString;
    NSLog(@"%@",dateString);
    
}

- (IBAction) btnActinDoneClicked:(id)sender
{
    
    [asheet dismissWithClickedButtonIndex:0 animated:YES];
    
    NSLog(@"btn action %d",[sender tag]);
    // NSLog(@"Selected date:%@",dateString);
    // NSLog(@"btnAction done:%@",dateString);
    
    
    // NSLog(@"btnAction done:%@",dateString);
    switch ([sender tag])
    {
        case 1:
            if (self.date_TextField.text.length==0)
            {
                NSString *dateString;
                // NSDate *date;
                NSDateFormatter *dateFormatter1 = [[NSDateFormatter alloc]init];
                [dateFormatter1 setDateFormat:@"dd-MM-yyyy hh:mm"];//EEEE MMMM d,yyyy hh:mm a

                dateString = [dateFormatter1 stringFromDate:myDatepicker.date];
                self.date_TextField.text=dateString;
                NSLog(@"%@",dateString);
                
                [servicesheet dismissWithClickedButtonIndex:0 animated:YES];
            }
            else
            {
                [servicesheet dismissWithClickedButtonIndex:0 animated:YES];

            }

            break;
        case 2:
        {
            if (self.service_TextField.text.length==0)
            {
                self.service_TextField.text=[self.pickerServiceContents objectAtIndex:0];
            }
            [servicesheet dismissWithClickedButtonIndex:0 animated:YES];
        }
            break;
        case 3:
        {
            if (self.location_Textfield.text.length==0)
            {
                self.location_Textfield.text=[locationArray objectAtIndex:0];
            }
        }

            break;
            
        default:
            break;
    }
        
}





-(void)btnActinCancelClicked
{
    [asheet dismissWithClickedButtonIndex:0 animated:YES];
    
    NSLog(@"tag value is----------- %d",mypickerView.tag);
}

- (IBAction)addServiceAction:(id)sender
{
    
    
    
    
        
    UIPickerView  *myPickerView = [[ UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator=YES;
    myPickerView.backgroundColor=[UIColor whiteColor];
    
    
    
    [myPickerView setFrame:CGRectMake(0, 20, 320, 250)];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(250, 10, 50, 30)];
    [button setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
   ServicePickerContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, 250, 320, 200)];
   ServicePickerContainerView.backgroundColor = [UIColor whiteColor];
   
    // Add the picker
    servicePicker = [[UIPickerView alloc]  initWithFrame:CGRectMake(0,0, 320, 216)];
    servicePicker.tag=2;
    servicePicker.delegate=self;
    servicePicker.showsSelectionIndicator = YES;
    
    
    [ServicePickerContainerView addSubview:servicePicker];

    
}

//-(void)dismissActionSheet {
//    [servicesheet dismissWithClickedButtonIndex:0 animated:YES];
//}

- (IBAction)addLocationAction:(id)sender {
   /* asheet = [[UIActionSheet alloc] initWithTitle:@"Select your location" delegate:self     cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:nil];
    asheet.actionSheetStyle = UIActionSheetStyleBlackOpaque;
    
    // Add the picker
    mypickerView = [[UIPickerView alloc]  initWithFrame:CGRectMake(0,40, 320, 216)];
    mypickerView.tag=1;
    mypickerView.delegate=self;
    mypickerView.showsSelectionIndicator = YES;
    [asheet addSubview:mypickerView];
    
    
    UIToolbar *tools=[[UIToolbar alloc]initWithFrame:CGRectMake(0, 0,320,40)];
    tools.barStyle=UIBarStyleBlackOpaque;
    [asheet addSubview:tools];
       
    UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btnActinDoneClicked:)];
    doneButton.imageInsets=UIEdgeInsetsMake(200, 6, 50, 25);
    doneButton.tag=3;
    UILabel *lblPickerTitle=[[UILabel alloc]initWithFrame:CGRectMake(60,8, 200, 25)];
    lblPickerTitle.text=@"Select Your Location";
    lblPickerTitle.backgroundColor=[UIColor clearColor];
    lblPickerTitle.textColor=[UIColor whiteColor];
    lblPickerTitle.textAlignment = NSTextAlignmentCenter;
    lblPickerTitle.font=[UIFont boldSystemFontOfSize:15];
    [tools addSubview:lblPickerTitle];

    UIBarButtonItem *flexSpace= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    /*UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(btnActinDoneClicked)];
     doneButton.imageInsets=UIEdgeInsetsMake(200, 6, 50, 25);*/
  //  NSArray *array = [[NSArray alloc]initWithObjects:flexSpace,flexSpace,doneButton,nil];
    
    /*UIBarButtonItem *doneButton=[[UIBarButtonItem alloc]initWithTitle:@"Done" style:UIBarButtonItemStyleBordered target:self action:@selector(dismissActionSheet) ];
     doneButton.imageInsets=UIEdgeInsetsMake(200, 6, 50, 25);
     doneButton.imageInsets=UIEdgeInsetsMake(200, 6, 50, 25);
     NSArray *array = [[NSArray alloc]initWithObjects:doneButton,nil];*/
    
    //[tools setItems:array];
   // [asheet showInView:self.view];
    
    
    
  //  [asheet showFromRect:CGRectMake(0,480, 320,215) inView:self.view animated:YES];
   // [asheet setBounds:CGRectMake(0,0, 320, 475)];
    
 //******************************************************************
    
    
    UIPickerView  *myPickerView = [[ UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator=YES;
    myPickerView.backgroundColor=[UIColor whiteColor];
    
    
    
    [myPickerView setFrame:CGRectMake(0, 20, 320, 250)];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(250, 10, 50, 30)];
    [button setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
    locationPickerContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, 250, 320, 200)];
    locationPickerContainerView.backgroundColor = [UIColor whiteColor];
    
  
    // Add the picker
    mypickerView = [[UIPickerView alloc]  initWithFrame:CGRectMake(0,40, 320, 216)];
    mypickerView.tag=1;
    mypickerView.delegate=self;
    mypickerView.showsSelectionIndicator = YES;
    [asheet addSubview:mypickerView];
    
    
    [locationPickerContainerView addSubview:mypickerView];

   
    
}

#pragma mark- UIPicker Delegate and DataSource Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    
    if (pickerView == mypickerView)
        return [locationArray count];
    else if (pickerView == servicePicker)
        return [self.pickerServiceContents count];
//    else if (pickerView == )
    else
        return 0;
    
}
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 300, 37)];
    if (pickerView == mypickerView)
    label.text = [NSString stringWithFormat:@"%@",[locationArray objectAtIndex:row]];
     if (pickerView == servicePicker)
         label.text = [NSString stringWithFormat:@"%@",[self.pickerServiceContents objectAtIndex:row]];
    
    label.textAlignment = NSTextAlignmentCenter;
    label.font=[UIFont fontWithName:@"Helvetica-Bold" size:16];
    label.backgroundColor = [UIColor clearColor];
    
    return label;
}
/*- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
    if (pickerView == mypickerView)
        return [self.pickerContentList objectAtIndex:row];
    
    if (pickerView == servicePicker)
        return [self.pickerServiceContents objectAtIndex:row];
    
    return 0;
    
}*/
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (pickerView == mypickerView)
    {
        self.location_Textfield.text= [locationArray objectAtIndex:row];
     NSLog(@"Row no is %d",row);
        currentRow=row;
    }
    
    if (pickerView == servicePicker)
        
        self.service_TextField.text= [self.pickerServiceContents objectAtIndex:row];
   
    
}


//===============================================================//

//=========METHOD TO ADJUST SCREEN WHEN KEYBOARD ENTERS SCREEN ==========//
-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD+60;
        
        
        
    }
    else
    {
        
        rect.origin.y += kOFFSET_FOR_KEYBOARD+60;
        
    }
    self.view.frame = rect;
    [UIView commitAnimations];
    
}

//===============================================================//

//=============== DELEGATE METHOD TO DELETE TEXTVIEW ===============//


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    if(buttonIndex == 0) //cancel button
    {
        
        NSLog(@"%d",buttonIndex);
    }
    else if(buttonIndex == 1) //delete button
    {
        NSLog(@"%d",buttonIndex);
        myTextView.text = @"";
        
    }
}




#pragma mark- UITEXTVIEW DELEGATE METHODS

- (BOOL)textViewShouldBeginEditing:(UITextView *)aTextView
{
    NSLog(@"textfieldbegin");
    [self setViewMovedUp:YES];
       mylabel.hidden = YES;
    
    [myTextView setInputAccessoryView:accessoryView];
    
    return YES;
    
}

-(void) textViewDidChange:(UITextView *)textView
{
    if ([appDelegate isIphone5]) {
        
   
    if(myTextView.text.length == 0){
        mylabel.hidden = NO;
        [myTextView resignFirstResponder];
    }
    textView_Text = textView.text;
    }else
    {
     NSLog(@"textfieldchange");
        if(myTextView.text.length == 0){
            mylabel.hidden = NO;
            [myTextView resignFirstResponder];
        }
        textView_Text = textView.text;
    
    }
    
    
}

-(void)textViewDidEndEditing:(UITextView *)textView
{  NSLog(@"textfieldend");
    [self setViewMovedUp:NO];
}


#pragma mark- UITEXTFIELD DELEGATE METHODS

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"textFieldShouldReturn:");
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    current_Textfield = textField;
    [textField setInputAccessoryView:accessoryView];
    
    if(![appDelegate isIphone5])
    {
      //  [self setViewMovedUp:YES];
    }
    
    if (textField == date_TextField)
    {
        
        textField.inputView = datePickerContainerView;

    }
    else if (textField == service_TextField)
    {
        //[self addServiceAction:nil];
        
        textField.inputView = ServicePickerContainerView;
        
        
    }

    else if (textField == location_Textfield)
    {
        textField.inputView = locationPickerContainerView;
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(![appDelegate isIphone5])
    {
       // [self setViewMovedUp:NO];
    }
}
//================================================================================================//




//===================================================================//
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



- (void)viewDidUnload
{
    
    [self setTopImageView:nil];
    [self setNameLabel:nil];
        [self setNamebackimageView:nil];
    [self setMemnobackImageView:nil];
       // [self setRegistration_TextField:nil];
    //[self setVechicle_TextField:nil];
    [self setDate_TextField:nil];
    [self setService_TextField:nil];
    [self setLocation_Textfield:nil];
   
    [super viewDidUnload];
}
@end
