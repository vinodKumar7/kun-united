//
//  MyNoticeBoardViewController.h
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"
#import "NoticeBoardDataParser.h"
#import "NoticeBoard.h"
#import "NSString_stripHtml.h"
//#import "MenuViewController.h"

@interface MyNoticeBoardViewController : UIViewController<ASIHTTPRequestDelegate,NoticeBoardXMLParserDelegate,UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *noticesListArray;
    NSMutableArray *readMsgs_Array;
    NSMutableArray *readMsgInd_Array;
}



@property (nonatomic,strong) IBOutlet UIWebView *webView;
@property (nonatomic, strong)IBOutlet UIWebView *noticeWebView;
@property (nonatomic, strong) NoticeBoard *notice;
@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (nonatomic, strong) NSMutableArray *noticesListArray;
@property (nonatomic, strong)IBOutlet UITableView *noticesTable;
@property (nonatomic, strong) IBOutlet UIView *noticeContentView;
@property (nonatomic, strong) IBOutlet UITextView *noticeContentTextView;
@property (strong, nonatomic) IBOutlet UIImageView *containerImageView;
@property (nonatomic,strong)NSMutableArray *readMsgs_Array;
@property (nonatomic,strong)NSMutableArray *readMsgInd_Array;
@property (strong, nonatomic) IBOutlet UIView *loadingView;
@property(nonatomic,strong)IBOutlet UIImageView *background_ImageView;
@property(nonatomic,strong)IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *acivityIndicator;
@end
