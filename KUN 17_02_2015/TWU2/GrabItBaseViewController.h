//
//  GrabItBaseViewController.h
//  TWU2
//
//  Created by vairat on 03/04/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MenuViewController;
#import "ZBarSDK.h"

@interface GrabItBaseViewController : UIViewController< ZBarReaderDelegate >
{
   int previousMenuButtonTag; 
}
@property (strong, nonatomic) IBOutlet UIButton *backButton;
@property(nonatomic,strong)IBOutlet UIView *customTabView;
@property(nonatomic,strong)IBOutlet UIScrollView *tabScrollView;
@property(nonatomic,strong)IBOutlet UILabel *header_Label;
@property(nonatomic,strong)IBOutlet UIView *header_view;
@property(nonatomic,readwrite)int previousMenuButtonTag;
@property (strong, nonatomic) IBOutlet UIButton *noticeButton;
@property (strong, nonatomic) IBOutlet UIView *badgecontainerView;
@property (strong, nonatomic) IBOutlet UIButton *GsearchButton;
@property (strong, nonatomic) IBOutlet UIButton *GnearbymeButton;
@property (strong, nonatomic) IBOutlet UIButton *GdailydealsButton;
- (IBAction)menuButton_Action:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *GnoticeboardButton;
@property (strong, nonatomic) IBOutlet UIButton *GhelpButton;

-(IBAction)backButton_Pressed:(id)sender;
- (IBAction)scrollViewButtonTapped:(id)sender;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil title:(NSString *)title;
- (void)showMailComposer;
@end
