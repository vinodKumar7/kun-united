//
//  UserDataXMLParser.h
//  GrabItNow
//
//  Created by MyRewards on 12/2/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"



@protocol UserDataXMLParser;

@interface UserDataXMLParser : NSObject <NSXMLParserDelegate>

@property (unsafe_unretained) id <UserDataXMLParser> delegate;

@end


@protocol UserDataXMLParser <NSObject>
- (void) parsingUserDetailsFinished:(User *) userDetails;
- (void) userDetailXMLparsingFailed;
@end