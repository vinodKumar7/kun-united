//
//  GrabItBaseViewController.m
//  TWU2
//
//  Created by vairat on 03/04/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "GrabItBaseViewController.h"
#import "AppDelegate.h"
#import <QuartzCore/QuartzCore.h>
#import "SearchViewController.h"
#import "NearByViewController.h"
#import "ProductListViewController.h"
#import "DailyDealsViewController.h"
#import "NoticeBoardViewController.h"
#import "HelpViewController.h"
#import "MyCardViewControllerGrab.h"
#import "MenuViewController.h"


#define SearchButtonTag 1
#define AroundMeButtonTag 2
#define HotOffersButtonTag 3
#define MyFavouriteButtonTag 4
#define DailyDealsButtonTag 5
#define NoticeBoardButtonTag 6
#define HelpButtonTag 7
#define MyMembershipCardButtonTag 8
#define LogOutButtonTag 9
#define TIME_FOR_SHRINKING 0.61f // Has to be different from SPEED_OF_EXPANDING and has to end in 'f'
#define TIME_FOR_EXPANDING 0.60f // Has to be different from SPEED_OF_SHRINKING and has to end in 'f'
#define SCALED_DOWN_AMOUNT 0.01  // For example, 0.01 is one hundredth of the normal size

@interface GrabItBaseViewController ()
{
    int currentMenuItemTag ;
    AppDelegate *appDelegate;
     BOOL menuViewUp;
    SearchViewController *searchViewController;
    NearByViewController *aroundMeViewController;
    ProductListViewController *hotOffersViewController;
    ProductListViewController *favoriteViewController;
    DailyDealsViewController *dailyDealsViewController;
    NoticeBoardViewController *noticeBoardViewController;
    HelpViewController *helpViewController;
    MyCardViewControllerGrab *cardViewController;
    MenuViewController *menuVC;
    UINavigationController *navController;
    NSString *title_String;
    UILabel *TitleLabel;
}
@property(nonatomic,strong)UILabel *TitleLabel;
@end

@implementation GrabItBaseViewController
@synthesize customTabView;
@synthesize tabScrollView;
@synthesize header_Label;
@synthesize header_view;
@synthesize previousMenuButtonTag;
@synthesize noticeButton;
@synthesize badgecontainerView;
@synthesize GsearchButton;
@synthesize GnearbymeButton;
@synthesize GdailydealsButton;
@synthesize GnoticeboardButton;
@synthesize GhelpButton;
@synthesize TitleLabel;
//===================DELEGATE METHODS=========================//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil title:(NSString *)title
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        title_String=title;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    menuViewUp=NO;
    self.header_Label.text=title_String;
    [self loadViews];
    [self.badgecontainerView addSubview:menuVC.badge];
    self.tabScrollView.contentSize = CGSizeMake(600,10);
    self.tabScrollView.scrollEnabled = YES;
    self.tabScrollView.showsHorizontalScrollIndicator = NO;
    
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
//    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
//    if ([[ver objectAtIndex:0] intValue] >= 7)
//    {
//        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];        self.navigationController.navigationBar.translucent = NO;
//    }
//    else
//    {
//    self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
//    }
    
  /*  if([self.navigationController.navigationBar respondsToSelector:@selector(barTintColor)])
    {
        // iOS7
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
        self.navigationController.navigationBar.translucent = NO;
    }
    else
    {
        // older
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
    }*/
    
    if([self.navigationController.navigationBar respondsToSelector:@selector(barTintColor)])
    {
        // iOS7
        self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
        [self.navigationController.navigationBar setTranslucent:NO];
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    }
    else
    {
        // older
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:7/255.f green:110/255.f blue:180/255.f alpha:1.0f];
    }

    
   
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(backaction) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    UIImage *myImage2 = [UIImage imageNamed:@"scan1.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(scanQRcode:) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
    self.navigationItem.leftBarButtonItem=leftButton;

    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = title_String;
    TitleLabel.textAlignment = UITextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor whiteColor]];
    self.navigationItem.titleView = TitleLabel;
    
    
}

-(void)backaction
{
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}

//=============================================================//

-(void)scanQRcode:(id)sender
{
    // ADD: present a barcode reader that scans from the camera feed
    ZBarReaderViewController *reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    
    UIImageView *overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlaygraphic.png"]];
    [overlayImageView setFrame:CGRectMake(30, 100, 260, 200)];
    [reader.view addSubview:overlayImageView];
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    activityIndicator.center = CGPointMake(160, 200);
    activityIndicator.hidesWhenStopped = NO;
    [reader.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
    // present and release the controller
    [self presentModalViewController: reader
                            animated: YES];
    
    
    
}

- (void) imagePickerController: (UIImagePickerController*) reader
 didFinishPickingMediaWithInfo: (NSDictionary*) info
{
    // ADD: get the decode results
    id<NSFastEnumeration> results =
    [info objectForKey: ZBarReaderControllerResults];
    ZBarSymbol *symbol = nil;
    for(symbol in results)
        // EXAMPLE: just grab the first barcode
        break;
    
    // EXAMPLE: do something useful with the barcode data
    //resultText.text = symbol.data;
    
    // EXAMPLE: do something useful with the barcode image
    //resultImage.image =
    //[info objectForKey: UIImagePickerControllerOriginalImage];
    
    // ADD: dismiss the controller (NB dismiss from the *reader*!)
    if([symbol.typeName isEqualToString:@"QR-Code"])
    {
        
      
    
                      
        NSString *testString = symbol.data;
        NSDataDetector *detect = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypeLink error:nil];
        NSArray *matches = [detect matchesInString:testString options:0 range:NSMakeRange(0, [testString length])];
    
        NSLog(@"Result is %@", matches);
        NSURL *url = [NSURL URLWithString:symbol.data];
        
        [[UIApplication sharedApplication] openURL:url];

        
        NSString *testString1 = symbol.data;
        NSDataDetector *detect1 = [[NSDataDetector alloc] initWithTypes:NSTextCheckingTypePhoneNumber error:nil];
        NSArray *matches1 = [detect1 matchesInString:testString1 options:0 range:NSMakeRange(0, [testString1 length])];
        
        NSLog(@"Result is %@", matches1);
        
         NSLog(@"Result is %@",symbol.data);
        
        NSString *searchString=symbol.data;
        NSString *urlAddress = [NSString stringWithFormat:@"http://www.google.com/search?q=%@",searchString];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlAddress]];
     
       


    
    }
    [reader dismissModalViewControllerAnimated: YES];
}
//=============DELEGATE METHODS TO LOCK ORIENTATION====================//


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}

- (BOOL) shouldAutorotate
{
    return NO;
}

//=====================================================//
// ================METHOD TO LOAD VIEWS IN IPHONE  ======================//

-(void)loadViews
{
    if([appDelegate isIphone5])
    {
    }
    else
    {
        if (IOS_VERSION>=7)
            self.customTabView.frame = CGRectMake(0,520, 320, 73);
        else
            self.customTabView.frame = CGRectMake(0,500, 320, 73);
        [self.view addSubview:self.customTabView];
    }
    
            
}
//=================================================================//
// ================METHOD TO LOAD VIEWS IN BOTTOM TAB  ===================//
- (IBAction)scrollViewButtonTapped:(id)sender
{
    
    currentMenuItemTag = [sender tag];
    NSLog(@"%d",currentMenuItemTag);
    
    switch ([sender tag])
    {
        case SearchButtonTag:                                          //Loading Search
            [self presentHotOffersController];
             [self.GsearchButton setImage:[UIImage imageNamed:@"Specials.png"] forState:UIControlStateNormal];
             NSLog(@"search tag %d",currentMenuItemTag);
            break;
        case AroundMeButtonTag:                                         //Loading Nearby Me
            [self presentWhatsAroundMeController];
            break;
        case DailyDealsButtonTag:                                       //Loading Daily Deals
            [self presentDailyDealsController];
            break;
        case NoticeBoardButtonTag:                                      //Loading Notice Board
            [self presentNoticeBoardController];
            break;
        case HelpButtonTag:                                            //Loading Help
            [self presentHelpController];
            NSLog(@"help tag %d",currentMenuItemTag);
            [self.GhelpButton setImage:[UIImage imageNamed:@"help.png"] forState:UIControlStateNormal];

            break;
        default:
            break;
    }
    
    
    
}
//==============================================================================================//

//==================================METHOD TO LOAD SEARCH VIEW====================================//

-(void)presentHotOffersController
{
    NSLog(@"========Hot Offers Controller========");
    if (!hotOffersViewController)
    {
        hotOffersViewController = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil productListType:ProductListTypeOffers];
    }
    self.GsearchButton.userInteractionEnabled  = NO;
    TitleLabel.text=@"Hot Offers";
    //self.header_Label.text = @"Search";
    [self.GsearchButton setImage:[UIImage imageNamed:@"Specials.png"] forState:UIControlStateNormal];
    [self presentNewViewController:hotOffersViewController];
    
}
//searchViewController==============================================================================================//
//===========================METHOD TO LOAD NEARBY ME VIEW====================================//

-(void)presentWhatsAroundMeController
{
    NSLog(@"========WhatsAroundMe Controller========");
   // if (!favoriteViewController)
   // {
        favoriteViewController = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil productListType:ProductListTypeMyFavorites];
   // }
    TitleLabel.text=@"My Favourites";
    self.header_Label.text = @"My Favourites";
    self.GnearbymeButton.userInteractionEnabled  = NO;
    [self.GnearbymeButton setImage:[UIImage imageNamed:@"favourites.png"] forState:UIControlStateNormal];
    [self presentNewViewController:favoriteViewController];
    
}
//==============================================================================================//


-(void) showMailComposer
{
    
    if ([MFMailComposeViewController canSendMail]) {
        NSArray *receipntsArray = [[NSArray alloc]initWithObjects:@"support@therewardsteam.com",nil];
        MFMailComposeViewController *mailViewController = [[MFMailComposeViewController alloc] init];
        mailViewController.mailComposeDelegate = self;
        [mailViewController setToRecipients:receipntsArray];
        [mailViewController setSubject:@"Help"];
        [mailViewController setMessageBody:@"Your message goes here." isHTML:NO];
        
        /*  [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:mailViewController
         animated:YES
         completion:nil];
         
         UINavigationController *passNVc=[[UINavigationController alloc] initWithRootViewController:passcodeVc];
         
         UINavigationController *rootVc=(UINavigationController *) self.window.rootViewController;
         rootVC.visibleViewController presentViewController:passcodeNavigationVC animated:YES completion:^{}];
         
         
         */
        // mailViewController.view.superview.frame = CGRectMake(0,44,320,440);
        [self.navigationController presentModalViewController:mailViewController animated:YES];
        
        // [[baseVC navigationController] setNavigationBarHidden:YES animated:YES];
        
    }
    
    else {
        
        NSLog(@"\nDevice is unable to send email in its current state.");
        UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                             message:@"You will need to setup a mail account on your device before you can send mail!"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [alert_view show];
        
        
    }
    
    
}

-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self dismissModalViewControllerAnimated:YES];
    
}

//===========================METHOD TO LOAD DAILY DEALS VIEW====================================//

-(void)presentDailyDealsController
{
    NSLog(@"========DailyDeals Controller========");
    if (!dailyDealsViewController)
    {
        dailyDealsViewController = [[DailyDealsViewController alloc] initWithNibName:@"DailyDealsViewController" bundle:nil];
    }
    self.GdailydealsButton.userInteractionEnabled  = NO;
    [self.GdailydealsButton setImage:[UIImage imageNamed:@"Deals.png"] forState:UIControlStateNormal];
    TitleLabel.text= @"Daily Deals";
    
    [self presentNewViewController:dailyDealsViewController];
}
//==============================================================================================//

//===========================METHOD TO LOAD NOTICE BOARD VIEW====================================//

-(void)presentNoticeBoardController
{
    NSLog(@"========NoticeBoard Controller========");
    if (!noticeBoardViewController)
    {
        noticeBoardViewController = [[NoticeBoardViewController alloc] initWithNibName:@"NoticeBoardViewController" bundle:nil];
    }
    self.GnoticeboardButton.userInteractionEnabled  = NO;
    [self.GnoticeboardButton setImage:[UIImage imageNamed:@"Notice.png"] forState:UIControlStateNormal];
   //TitleLabel.text = @"About Us";
   TitleLabel.text = @"Notice Board";
  
    [self presentNewViewController:noticeBoardViewController];
    
}
//==============================================================================================//


//===========================METHOD TO LOAD HELP VIEW====================================//

-(void)presentHelpController
{
    NSLog(@"========Help Controller========");
    if (!helpViewController)
    {
        helpViewController = [[HelpViewController alloc] initWithNibName:@"HelpViewController" bundle:nil];
    }
    [self.GhelpButton setImage:[UIImage imageNamed:@"help.png"] forState:UIControlStateNormal];

    self.GhelpButton.userInteractionEnabled  = NO;
    
    TitleLabel.text=@"Help";
    
    [self presentNewViewController:helpViewController];
    
    
}
//==============================================================================================//


//======================METHOD TO CHANGE IMAGES IN BOTTOM TAB===================================//

- (void) presentNewViewController:(UIViewController *) controller
{
    
 
    
   // CGRect frame = controller.view.frame;
   // frame.origin.x =24;
    //frame.size.height = (appDelegate.window.frame.size.height);
   // controller.view.frame =frame;
    
    //==================== changing previous button image ======================//
   switch (previousMenuButtonTag)
    {
            
            NSLog(@"previous Button tag::%d",previousMenuButtonTag);
            
        case SearchButtonTag:
            
              [self.GsearchButton setImage:[UIImage imageNamed:@"specialsHLs.png"] forState:UIControlStateNormal];
              self.GsearchButton .userInteractionEnabled  = YES;
              break;
        case AroundMeButtonTag:
              [self.GnearbymeButton setImage:[UIImage imageNamed:@"mfHLss.png"] forState:UIControlStateNormal];
              self.GnearbymeButton.userInteractionEnabled = YES;
              break;
        case DailyDealsButtonTag:
              [self.GdailydealsButton setImage:[UIImage imageNamed:@"dealsHLs.png"] forState:UIControlStateNormal];
               self.GdailydealsButton.userInteractionEnabled = YES;
              break;
        case NoticeBoardButtonTag:
              [self.GnoticeboardButton setImage:[UIImage imageNamed:@"noticeHLs.png"] forState:UIControlStateNormal];

              self.GnoticeboardButton.userInteractionEnabled = YES;
              break;
        case HelpButtonTag:
              [self.GhelpButton setImage:[UIImage imageNamed:@"helpHLs.png"] forState:UIControlStateNormal];
              self.GhelpButton.userInteractionEnabled = YES;
              break;
                   
        default:
              break;
    }
    
        
    self.previousMenuButtonTag = currentMenuItemTag;
    
    navController = [[UINavigationController alloc]initWithRootViewController:controller];
    
    
    [self performSelector:@selector(animateTransition:) withObject:[NSNumber numberWithFloat: TIME_FOR_EXPANDING]];
    
}
//==============================================================================================//
//======================METHODS TO SHRINK AND EXPAND VIEWS IN BOTTOM TAB==========================//

-(void)animateTransition:(NSNumber *)duration
{
    NSLog(@"animateTransition");
    
	self.view.userInteractionEnabled=NO;
       
    
    
	[[self view] addSubview:navController.view];
    [self.view bringSubviewToFront:self.customTabView];
    
    if ((navController.view.hidden==false) && ([duration floatValue]==TIME_FOR_EXPANDING))
    {
        NSLog(@"IF");
        navController.view.frame=[[UIScreen mainScreen] bounds];
        navController.view.transform=CGAffineTransformMakeScale(SCALED_DOWN_AMOUNT, SCALED_DOWN_AMOUNT);
    }
	navController.view.hidden=false;
	if ([duration floatValue]==TIME_FOR_SHRINKING)
    {
        NSLog(@"SHRINKING");
		[UIView beginAnimations:@"animationShrink" context:NULL];
		[UIView setAnimationDuration:[duration floatValue]];
		navController.view.transform=CGAffineTransformMakeScale(SCALED_DOWN_AMOUNT, SCALED_DOWN_AMOUNT);
	}
	else
    {
        NSLog(@"EXPAND");
		[UIView beginAnimations:@"animationExpand" context:NULL];
		[UIView setAnimationDuration:[duration floatValue]];
		navController.view.transform=CGAffineTransformMakeScale(1.01, 1.01);
	}
	[UIView setAnimationDelegate:self];
	[UIView setAnimationDidStopSelector:@selector(animationDidStop:finished:context:)];
	[UIView commitAnimations];
}

-(void)animationDidStop:(NSString *)animationID finished:(BOOL)finished context:(void *)context
{
    
	self.view.userInteractionEnabled=YES;
	if ([animationID isEqualToString:@"animationExpand"])
    {
		//[[self navigationController] pushViewController:viewController animated:NO];
	}
	else {
		navController.view.hidden=true;
	}
}
//====================================================================================//


//================METHOD TO ADDTRANSITION EFFECT IN VIEWS ==========================//

- (void)transitionToViewController:(UIViewController *)viewController
                    withTransition:(UIViewAnimationOptions)transition
{
    
    [UIView transitionFromView:self.view
                        toView:viewController.view
                      duration:0.55f
                       options:transition
                    completion:^(BOOL finished){
                    } ];
}
//==============================================================================================//
//================METHOD TO MENU BUTTON ACTION IN BOTTOM TAB ==========================//

- (IBAction)menuButton_Action:(id)sender
{
    
    if(menuViewUp)
    {
        
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect ContainerFrame = self.customTabView.frame;
            ContainerFrame.origin.y = self.customTabView.frame.origin.y + 40.0;
            self.customTabView.frame = ContainerFrame;}
         
                         completion:^(BOOL finished) {
                             menuViewUp = NO;
                             
                         } ];
    
    }
    else
    {
        [UIView animateWithDuration:0.3 animations:^{
            
            CGRect ContainerFrame = self.customTabView.frame;
            ContainerFrame.origin.y = self.customTabView.frame.origin.y - 40.0;
            self.customTabView.frame = ContainerFrame;}
         
                         completion:^(BOOL finished) {
                             menuViewUp = YES;
                             
                         } ];

    
    
  
    }
}
//==============================================================================================//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload
{
    [self setNoticeButton:nil];
    [self setBadgecontainerView:nil];
    [self setGsearchButton:nil];
    [self setGnearbymeButton:nil];
    [self setGdailydealsButton:nil];
    //[self setGnoticeboard:nil];
    [self setGnoticeboardButton:nil];
    [self setGhelpButton:nil];
    [self setBackButton:nil];
    [super viewDidUnload];
}

@end
