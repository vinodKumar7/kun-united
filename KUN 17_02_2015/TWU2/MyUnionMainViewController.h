//
//  MyUnionMainViewController.h
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"
#import "MyUnionParser.h"
#import "AppDelegate.h"

@interface MyUnionMainViewController : UIViewController <ASIHTTPRequestDelegate, NSXMLParserDelegate, MyUnionParser>
{
    IBOutlet UIWebView *myUnionDisplayView;
    IBOutlet UIActivityIndicatorView *myUnionActivityIndicator;
    IBOutlet UIView *containerView;
    IBOutlet UIImageView *background_ImageView;
    NSMutableArray *productsList;
    
    //AppDelegate *appDelegate;
    
}

//@property(nonatomic,retain)AppDelegate *appDelegate;
@property (strong, nonatomic) IBOutlet UIImageView *containerImageView;
@property(nonatomic,strong)IBOutlet UIWebView *myUnionDisplayView;
@property(nonatomic,strong)IBOutlet UIActivityIndicatorView *myUnionActivityIndicator;
@property(nonatomic,strong)IBOutlet UIView *containerView;
@property(nonatomic,strong) IBOutlet UIImageView *background_ImageView;
@property (nonatomic, strong) NSMutableArray *productsList;
- (IBAction)websiteButtonClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIImageView *webbgImageView;
@property (strong, nonatomic) IBOutlet UIView *activityView;

@property (strong, nonatomic) IBOutlet UIButton *websiteButton;
@property (strong, nonatomic) IBOutlet UIButton *fbButton;
@property (strong, nonatomic) IBOutlet UIButton *tweetButton;

-(void)loadViews;
- (void)lotsOfComputation ;
@end
