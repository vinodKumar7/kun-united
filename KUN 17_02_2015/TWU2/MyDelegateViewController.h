//
//  MyDelegateViewController.h
//  TWU2
//
//  Created by MyRewards on 2/15/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ASIFormDataRequest.h"
#import "MyUnionParser.h"
#import "AppDelegate.h"

@interface MyDelegateViewController : UIViewController<ASIHTTPRequestDelegate ,NSXMLParserDelegate, MyUnionParser>
{
   
    IBOutlet UIImageView *background_ImageView;
    NSMutableArray *productsList;
}
@property(nonatomic,strong)IBOutlet UIWebView *myUnionDisplayView;
@property(nonatomic,strong)IBOutlet UIActivityIndicatorView *myUnionActivityIndicator;
@property(nonatomic,strong)IBOutlet UIView *containerView;
@property (nonatomic, strong) NSMutableArray *productsList;
@property(nonatomic,strong)IBOutlet UIImageView *background_ImageView;
@property (strong, nonatomic) IBOutlet UIImageView *containerImageView;

-(void)loadViews;
@end
