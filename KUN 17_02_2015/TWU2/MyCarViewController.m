//
//  MyCarViewController.m
//  KUN
//
//  Created by vairat on 05/12/14.
//  Copyright (c) 2014 MyRewards. All rights reserved.
//

#import "MyCarViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "MyUnionContact.h"


#define ProductCellHeight 44.0;
#define kOFFSET_FOR_KEYBOARD 135


@interface MyCarViewController ()
{
    AppDelegate *appDelegate;
    int index;
    BOOL stayup;
//    NSArray *contactsArray;
//    NSString *ContactNumber;
    //  int currentTag;
}
@property(nonatomic,strong)NSArray *contactsArray;
-(void)loadMyUnionContacts;

@end

@implementation MyCarViewController

@synthesize contactsList;
@synthesize contactsArray;
@synthesize background_ImageView;
@synthesize nameTextField;
@synthesize numberTextField;
@synthesize  accessoryView;
@synthesize currentTextField;
@synthesize carModelTextField;
@synthesize carRegsTextField;
@synthesize carYearTextField;
@synthesize saveButton;
@synthesize carMake_label;
@synthesize carColor_label;
@synthesize carRegs_Label;
@synthesize carModel_Label;
@synthesize carYear_Label;
@synthesize doneButton;

//=========================DELEGATE METHODS=============================//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        contactsList = [[NSMutableArray alloc] init];

    }
    return self;
}

#pragma mark- ViewLife Cycle Methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    nameTextField.clearButtonMode=YES;
    nameTextField.clearButtonMode=UITextFieldViewModeWhileEditing;
    index = 0;
    
    
}



-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];

    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"My Car";
    TitleLabel.textAlignment = NSTextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor whiteColor]];
    self.navigationItem.titleView = TitleLabel;
    
    [self loadMyUnionContacts];
}


#pragma mark- DELEGATE METHODS TO LOCK ORIENTATION

-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}
//-(BOOL)s

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

- (BOOL) shouldAutorotate
{
    return NO;
}

//=====================================================================//
// =======================METHOD TO LOAD CONTACTS ===================//
-(void)loadMyUnionContacts
{
    //NSLog(@"contact list1:: %d",contactsList.count);
    if (contactsList.count > 0)
    {
        //[nocontactsavailableLabel setHidden:YES];
        [contactsList removeAllObjects];
        NSLog(@"contact list2:: %d",contactsList.count);
    }
    
    [contactsList addObjectsFromArray:[appDelegate mysoscontacts]];
    NSLog(@"contact list4:: %d",contactsList.count);
    if (contactsList.count > 0)
    {
        int last=[contactsList count]-1 ;
        if (last==-1)
        {
            NSLog(@"Omit");
            
        }
        else
        {
            MyUnionContact *newContact = [contactsList objectAtIndex:last];
//            
//            self.nameTextField.text=newContact.name   ;//VIN
//            self.numberTextField.text=newContact.number ;//Color
//            self.carRegsTextField.text=newContact.carRegistration ;//Regs
//            self.carModelTextField.text =newContact.carModel;//model
//            self.carYearTextField.text =newContact.carYear ;//year
            
            self.nameTextField.text=newContact.name   ;//VIN
            self.numberTextField.text=newContact.number ;//Color
            self.carRegsTextField.text=newContact.carRegistration ;//Regs
            self.carModelTextField.text =newContact.carModel;//model
            self.carYearTextField.text =newContact.carYear ;//year
        }
    }
    
    
}

#pragma mark- Method to save the data "saveButton_Clicked:"

- (IBAction)saveButton_Clicked:(id)sender {
    
    MyUnionContact *newContact = [[MyUnionContact alloc]init];
    newContact.name   = self.nameTextField.text;
    newContact.number = self.numberTextField.text;
    newContact.carRegistration = self.carRegsTextField.text;
    newContact.carModel = self.carModelTextField.text;
    newContact.carYear = self.carYearTextField.text;

    
    
    if([self.nameTextField.text length] <=0 || [self.carRegsTextField .text length] <=0)
    {
        UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"Fields required" message:@"Car Make and Car Registration  are required" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [myAlert show];
        
    }
    else
    {
        
        
        BOOL success = [appDelegate addContact:newContact];
        
        if(success)
        {
            NSLog(@"Added Sucessfully");
            
            UIAlertView *myAlert = [[UIAlertView alloc]initWithTitle:@"Added" message:@"Car Details Saved" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [myAlert show];
            
            self.nameTextField.text = @"";
            self.numberTextField.text = @"";
            
            [self loadMyUnionContacts];
        }
    }
    [self.currentTextField resignFirstResponder];

}

#pragma mark- Method For Done Button

-(IBAction)doneButton_Pressed:(id)sender
{
    [self.currentTextField resignFirstResponder];
}

#pragma mark- Method For Back to MainView

-(void)back
{
    
    [currentTextField resignFirstResponder];
    [self doneButton_Pressed:doneButton];
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
    
}


#pragma mark- Method to ADJUST SCREEN WHEN KEYBOARD ENTERS SCREEN

-(void)setViewMovedUp:(BOOL)movedUp
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        NSLog(@"INSIDE MOVEDUP AND SCROLL VIEW Y IS::%f",rect.origin.y);
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD+5;
    
        
    }
    else
    {
        
        rect.origin.y += kOFFSET_FOR_KEYBOARD+5;
    }
    self.view.frame = rect;
    [UIView commitAnimations];
    
}


#pragma mark- UITEXTFIELD DELEGATE METHODS

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 25) ? NO : YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"textFieldShouldReturn:");
    
    [textField resignFirstResponder];
    
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    
    [textField setInputAccessoryView:accessoryView];
    
    if(![appDelegate isIphone5])
    {
        if( textField.tag<=4  )
        {
            
            [self setViewMovedUp:YES];
        }
    }
    else
    {
        
        if( textField.tag<=3   )
        {
            
            [self setViewMovedUp:YES];
        }
        
    }
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    if(![appDelegate isIphone5])
    {
        if( textField.tag<=4  )
        {
            [self setViewMovedUp:NO];
            
        }
        
    } else
    {
        
        if( textField.tag<=3 )
        {
            
            [self setViewMovedUp:NO];
        }
        
    }
    
}


//================================================================================================//

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload
{
    [self setCarRegsTextField:nil];
    [self setCarModelTextField:nil];
    [self setCarYearTextField:nil];
    [self setSaveButton:nil];
    
    [self setCarMake_label:nil];
    [self setCarColor_label:nil];
    [self setCarRegs_Label:nil];
    [self setCarModel_Label:nil];
    [self setCarYear_Label:nil];
    [self setDoneButton:nil];
    [super viewDidUnload];
}

@end
