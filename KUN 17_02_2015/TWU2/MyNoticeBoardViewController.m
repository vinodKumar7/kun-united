//
//  MyNoticeBoardViewController.m
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "MyNoticeBoardViewController.h"
#import "AppDelegate.h"
#import "ContactCell.h"
#import <QuartzCore/QuartzCore.h>
#import "ProductCell.h"

#define ProductCellHeight 64.0;

@interface MyNoticeBoardViewController ()
{
    AppDelegate *appDelegate;
    ASIFormDataRequest *noticeBoardRequest;
    NoticeBoardDataParser *NoticeBoardXMLParser;
    int select;
    NSIndexPath * openProductIndex;
    BOOL isRead;
    BOOL requestCancel;
    
}
@property (nonatomic, strong) NSIndexPath * openProductIndex;
-(void)fetchNotices;
@end

@implementation MyNoticeBoardViewController
@synthesize notice;
@synthesize noticesTable;
@synthesize noticesListArray;
@synthesize openProductIndex;
@synthesize activityView;
@synthesize noticeContentView;
@synthesize noticeContentTextView;
@synthesize noticeWebView;
@synthesize containerView;
@synthesize background_ImageView;
@synthesize containerImageView;
@synthesize readMsgs_Array;
@synthesize readMsgInd_Array;
@synthesize loadingView;
@synthesize acivityIndicator;
//=========================DELEGATE METHODS=============================//


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        openProductIndex = nil;
        select = 0;
        self.readMsgs_Array = [[NSMutableArray alloc]init];
        self.readMsgInd_Array = [[NSMutableArray alloc]init];
        
    }
    return self;
}

- (void)viewDidLoad
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    // Do any additional setup after loading the view from its nib.
    
    [self loadViews];
    
    //setting container corner curve shape
    
    [self.background_ImageView setHidden:YES];
    [self.containerImageView setHidden:YES];
    
     self.noticesListArray = [[NSMutableArray alloc]init];
    [self showActivityView];
    [self performSelector:@selector(showActivityView) withObject:nil afterDelay:0.03];

    [self fetchNotices];
    [super viewDidLoad];
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
//    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
//    if ([[ver objectAtIndex:0] intValue] >= 7)
//    {
//        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];        self.navigationController.navigationBar.translucent = NO;
//    }
//    else
//    {
//        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
//    }
    
/*    if([self.navigationController.navigationBar respondsToSelector:@selector(barTintColor)])
    {
        // iOS7
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
        self.navigationController.navigationBar.translucent = NO;
    }
    else
    {
        // older
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
    }*/
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"My Notice Board";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor whiteColor]];
    self.navigationItem.titleView = TitleLabel;
    
    
    //Assigning webView height
    CGRect webframe=self.noticeWebView.frame;
    if ([appDelegate isIphone5])
    webframe.size.height=480;
    else
    webframe.size.height=360;
    noticeWebView.frame=webframe;
    NSLog(@"height is %f",webframe.size.height);
    //Assigning  Table height
    CGRect tableframe=self.noticesTable.frame;
    if ([appDelegate isIphone5])
    tableframe.size.height=548;
    else
    tableframe.size.height=460;
    noticesTable.frame=tableframe;

    
}

-(void)back
{
    
    if([noticeBoardRequest isExecuting])
    {
        requestCancel = YES;
        [noticeBoardRequest cancel];
    }
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}
//=========================================================//
-(BOOL)webView:(UIWebView *)descriptionTextView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (UIWebViewNavigationTypeLinkClicked == navigationType) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}
//===============DELEGATE METHODS TO LOCK ORIENTATION===================//


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}

- (BOOL) shouldAutorotate
{
    return NO;
}

//==============================================================//


// ==============METHOD TO LOAD VIEWS IN IPHONE  ==============//

-(void)loadViews
{
    if([appDelegate isIphone5])
    {
        NSLog(@"Iphone5");
        
        CGRect frame =self.background_ImageView.frame;
        frame.size.height = 435;
        self.background_ImageView.frame = frame;
        
               
        
        
    }
    else
    {
        NSLog(@"Iphone4");
        CGRect containerframe = self.containerImageView.frame;
        containerframe.origin.y=10;
        containerframe.size.height = 395;
        self.containerImageView.frame = containerframe;
        
    }
    
}

//==========================================================//


//============================METHOD TO SHOW ACTIVITY WINDOW  =============================//
- (void) showActivityView
{
   
    
    CGRect frame =activityView.frame;
    self.activityView.frame=frame;
     
    [self.view addSubview:activityView];
}

- (void) dismissActivityView
{
    [activityView removeFromSuperview];
}
//==================================================================================//

//==============================================================================//
-(void)fetchNotices
{
    
    
    
    NSString *urlString;
   urlString = [NSString stringWithFormat:@"%@",DailysaversNoticeBoardURL];
      
    NSURL *url = [NSURL URLWithString:urlString];
    NSLog(@"url is:==> %@",url);
    
    
    noticeBoardRequest = [[ASIFormDataRequest alloc] initWithURL:url];
    [noticeBoardRequest setDelegate:self];
    [noticeBoardRequest startAsynchronous];
    
    
    
}


//============================ ASIHTTP REQUEST DELEGATE METHODS ====================//
#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSLog(@"NoticeBoardVC -- requestFinished:");
    
    if (request == noticeBoardRequest)
    {
        
        NSLog(@"**getImageNPidRequest  RES: %@",[request responseString]);
        NSString *result=[NSString stringWithFormat:@"%@", [request responseString]];
        NSXMLParser *noticeParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        NoticeBoardXMLParser = [[NoticeBoardDataParser alloc] init];
        NoticeBoardXMLParser.delegate = self;
        noticeParser.delegate = NoticeBoardXMLParser;
        [noticeParser parse];
        if (result.length==0)
        {
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Notices available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
            [alert show];
            
        }
        [self dismissActivityView];
    }
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    NSLog(@"NoticeBoardVC -- requestFailed:");
    
    if (requestCancel == YES) {
        
    }
    else
    {
        if (request == noticeBoardRequest)
        {
            noticeBoardRequest = nil;
            [self dismissActivityView];
            
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
            [self dismissActivityView];
        }
        [self dismissActivityView];
    }
    
}
//===================================================

#pragma mark -- UITableView Datasource & Delegate methods

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
    return  [self.noticesListArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (openProductIndex && (indexPath.row == openProductIndex.row) )
    {
        return self.noticesTable.frame.size.height;
    }
    
    
    if (indexPath.row == self.noticesListArray.count)
    {
        return 44.0;
    }
    
    return ProductCellHeight;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    NSLog(@"cellForRowAtIndexPath");
    static NSString *identifier = @"cell";
    static NSString *loadingCellIdentifier = @"loadingCell";
    
    if (indexPath.row == self.noticesListArray.count)
    {
        
        UITableViewCell *loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
        
        if (!loadingCell)
        {
            loadingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadingCellIdentifier];
        }
        
        loadingCell.textLabel.text = @"Loading ....";
        loadingCell.textLabel.textColor = [UIColor blackColor];//[UIColor colorWithRed:255/255.0 green:97/255.0 blue:55/255.0 alpha:1.0];
        
        loadingCell.backgroundColor = [UIColor whiteColor];
        
        UIActivityIndicatorView *loadingActivityView = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
        loadingActivityView.backgroundColor = [UIColor clearColor];
        [loadingActivityView startAnimating];
        loadingCell.accessoryView = loadingActivityView;
        
        loadingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return loadingCell;
        
    }
    
    ProductCell *cell = (ProductCell *)[tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell)
    {
        
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:nil options:nil];
        
        for (UIView *cellview in views)
        {
            if([cellview isKindOfClass:[UITableViewCell class]])
            {
                cell = (ProductCell*)cellview;
            }
        }
        
    }
    
    
    NoticeBoard *currentnotice = [self.noticesListArray objectAtIndex:indexPath.row];
    
    
    
    cell.productImg.hidden=NO;
    if(select == 0)
    {
        
        isRead = [self noticeObjectExistsInDb:currentnotice];
        
        if(isRead)
        {
            NSLog(@"mesg read");
            cell.productImg.image = [UIImage imageNamed:@"read.png"];
            [self.readMsgInd_Array replaceObjectAtIndex:indexPath.row withObject:@"1"];
        }
        
        else
            cell.productImg.image = [UIImage imageNamed:@"Unread.png"];
        
        
    }
    
    NSLog(@"self.readMsgInd_Array %@",self.readMsgInd_Array);
    
    if(select == 1)
    {
        NSLog(@"Configuring cells after DidSelect row...");
        if([[self.readMsgInd_Array objectAtIndex:indexPath.row]  isEqualToString:@"1"])
            
            cell.productImg.image = [UIImage imageNamed:@"read.png"];
        
        else
           cell.productImg.image = [UIImage imageNamed:@"Unread.png"];
    }
    
    // Set title and offer text here
    cell.noImageLabel.text = currentnotice.subject;
    cell.productNameLabel.hidden=NO;
    cell.productNameLabel.text = currentnotice.subject;
    cell.productOfferLabel.text = currentnotice.created_Date;
    
    CGRect noticelabelframe= cell.noImageLabel.frame;
    noticelabelframe.origin.x=60;
    cell.noImageLabel.frame=noticelabelframe;
    
    CGRect imageFrame=cell.productImg.frame;
    imageFrame.size.width=35;
    imageFrame.size.height=35;
    cell.productImg.frame=imageFrame;
    
    CGRect noticelabelframez= cell.productNameLabel.frame;
    noticelabelframez.origin.x=60;
    cell.productNameLabel.frame=noticelabelframez;
    NSLog(@"cell.noImageLabel.text::%@",cell.noImageLabel.text);
    CGRect noticelabelframe1= cell.productOfferLabel.frame;
    noticelabelframe1.origin.x=60;
    noticelabelframe1.origin.y=30;
    cell.productOfferLabel.frame=noticelabelframe1;
    
    // Set different background colors for the cells.
    cell.productNameLabel.textColor = [UIColor colorWithRed:255/255.0 green:168/255.0 blue:71/255.0 alpha:1.0];
    
    int rowModuleNo = indexPath.row % 2;
    
    if (rowModuleNo == 0)
    {
        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:0/255.0 green:130/255.0 blue:214/255.0 alpha:1.0];
        cell.productNameLabel.textColor = [UIColor whiteColor];
    }
    else
    {
        cell.headerContainerView.backgroundColor = [UIColor whiteColor];
        cell.productNameLabel.textColor = [UIColor colorWithRed:0/255.0 green:130/255.0 blue:214/255.0 alpha:1.0];
    }
    
    
    
    
    
    // Show Product controller if indexpath == openProductIndex
    if (openProductIndex && (indexPath.row == openProductIndex.row) )
    {
        NSLog(@"Show Product controller if indexpath == openProductIndex");
        
        cell.headerContainerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
        cell.headerContainerView.layer.shadowOpacity = 1.0;
        cell.headerContainerView.layer.shadowOffset = CGSizeMake(0.0, 5.0);
        
        cell.headerContainerView.layer.shadowRadius = 5.0;
        CGRect frame1 =cell.headerContainerView.frame;
        
        // NSLog(@"headercontainer height is ... %f",frame1.size.height);
        cell.headerContainerView.frame=frame1;
        
        
        
        
        CGRect frame = noticeContentView.frame;
        float headerHeight = ProductCellHeight;
        
        frame.origin.y = headerHeight;
        frame.origin.x = 0;
        frame.size.width = 320;
        // NSLog(@"noticeContentView :%f",(self.noticesTable.frame.size.height - headerHeight));
        frame.size.height = self.noticesTable.frame.size.height - headerHeight;
        noticeContentView.frame = frame;
        // [noticeIDArray count]
        
        NoticeBoard *currrentNotice = [self.noticesListArray objectAtIndex:indexPath.row];
        //self.noticeContentTextView.text = [currrentNotice.details stripHtml];
        
        
        //NSLog(@" html data is...........%@",myDescriptionHTML);
        
        [noticeWebView loadHTMLString:currrentNotice.details baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
        /* CGRect noticewebframe=self.noticeWebView.frame;
         noticewebframe.size.width=320;
         self.noticeWebView.frame=noticelabelframe;*/
        
        // NSLog(@"** Calculated view frame: %@",NSStringFromCGRect(frame));
        
        
        [cell.contentView addSubview:self.noticeContentView];
        [cell.contentView bringSubviewToFront:cell.headerContainerView];
        
        
        
    }
    
    
    
    
    //========
    //cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (BOOL)noticeObjectExistsInDb:(NoticeBoard *)currNotice

{
    NoticeBoard *dBnb1;
    NSLog(@"dBnb %@ ",dBnb1);
    
    for (int j=0; j<[self.readMsgs_Array count]; j++) {
        
        NoticeBoard *dBnb = [self.readMsgs_Array objectAtIndex:j];
        NSLog(@"dBnb %@ ",dBnb);
        
        if([currNotice.notice_Id isEqualToString:dBnb.notice_Id])
            return YES;
    }
    
    return  NO;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    NSLog(@"didSelectRowAtIndexPath");
    NoticeBoard *currentnotice = [self.noticesListArray objectAtIndex:indexPath.row];
    NSLog(@" current index:: %d",currentnotice.index);
    
    if (indexPath.row == [self.noticesListArray count])
    {
        // Return since this is Loading more cell.
        return;
    }
    
    
    // **** Close the Product Details **** //
    if (openProductIndex)
    {
        
        openProductIndex = nil;
        
        
        int totalRows = [self.noticesTable numberOfRowsInSection:0];
        
        // Update tapped cell
        [self.noticesTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // update cells under it
        NSMutableArray *indexPathsArray = [NSMutableArray array];
        for (int i = (indexPath.row + 1); i < totalRows; i++)
        {
            NSIndexPath *indPath = [NSIndexPath indexPathForRow:i inSection:0];
            [indexPathsArray addObject:indPath];
        }
        
        if (indexPathsArray.count > 0)
        {
            [self.noticesTable reloadRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationAutomatic];
        }
        
        
        
        self.noticesTable.scrollEnabled = YES;
        return;
    }
    
    
    
    
    // **** Open the Product Details **** //
    openProductIndex = indexPath;
    
    float toScrollOffsetY = openProductIndex.row * ProductCellHeight;
    
    // update content size
    if ( (self.noticesTable.contentSize.height - toScrollOffsetY) < self.noticesTable.frame.size.height)
    {
        self.noticesTable.contentSize = CGSizeMake(self.noticesTable.contentSize.width, (self.noticesTable.frame.size.height + toScrollOffsetY));
    }
    
    //Adding  NB id to coredata
    // NSLog(@"notice board Vc Prashanth1010");
    ProductCell *cell = (ProductCell *)[tableView cellForRowAtIndexPath:indexPath];
   
    cell.productImg.image = [UIImage imageNamed:@"read.png"];
    select = 1;
    if([appDelegate nbExistsInCoredata:[self.noticesListArray objectAtIndex:indexPath.row]])
    {
        NSLog(@"NB already available in Coredata notice board VC ");
    }
    
    else
    {
        NSLog(@"NB Not available in Coredata notice board VC");
        [appDelegate addNoticeBoardID:[self.noticesListArray objectAtIndex:indexPath.row]];
        
        //[self.readMsgs_Array replaceObjectAtIndex:indexPath.row withObject:[self.noticesListArray objectAtIndex:indexPath.row]];
        [self.readMsgInd_Array replaceObjectAtIndex:indexPath.row withObject:@"1"];
        
    }
    
    
    
    
    [UIView animateWithDuration:0.5 animations:^{
        [self.noticesTable setContentOffset:CGPointMake(self.noticesTable.contentOffset.x, toScrollOffsetY)];
    } completion:^(BOOL finished) {
        
        int totalRows = [self.noticesTable numberOfRowsInSection:0];
        
        // Update tapped cell
        [self.noticesTable reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        
        // update cells under it
        NSMutableArray *indexPathsArray = [NSMutableArray array];
        for (int i = (indexPath.row + 1); i < totalRows; i++)
        {
            NSIndexPath *indPath = [NSIndexPath indexPathForRow:i inSection:0];
            [indexPathsArray addObject:indPath];
        }
        
        if (indexPathsArray.count > 0)
        {
            [self.noticesTable reloadRowsAtIndexPaths:indexPathsArray withRowAnimation:UITableViewRowAnimationBottom];
        }
        
    }];
    
    self.noticesTable.scrollEnabled = NO;
    
    
    
}



- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.row % 2 == 0)
    {
        
        UIColor *color = [UIColor colorWithRed:0/255.0 green:130/255.0 blue:214/255.0 alpha:1.0];
               cell.backgroundColor = color;
        
    }
    else
    {
        UIColor *color = [UIColor whiteColor];

        cell.backgroundColor = color;
    }
    
}




//============================ ***********************   ==========================//

#pragma mark -- Product Data Parser Delegate methods

- (void) parsingNoticeBoardDataFinished:(NSArray *) noticesList
{
    if (!self.noticesListArray)
        self.noticesListArray = [[NSMutableArray alloc] initWithArray:noticesList];
    
    else
        [self.noticesListArray addObjectsFromArray:noticesList];
    
    [self.readMsgInd_Array removeAllObjects];
    [self.readMsgs_Array removeAllObjects];
    
    for (int i = 0; i < [self.noticesListArray count]; i++)
        [self.readMsgInd_Array addObject:@"0"];
    
    
    self.readMsgs_Array = [[NSMutableArray alloc]initWithArray:[appDelegate noticeBoardIds]];
    
    if([self.noticesListArray count] > 0)
    {
        for (int i=0;i< [self.noticesListArray count];i++)
        {
            
            NoticeBoard *nb = [self.noticesListArray objectAtIndex:i];
            NSLog(@"Webservice notice Id ::%@",nb.notice_Id);
            NSLog(@"Webservice notice Index: %d",nb.index);
            
            for (int j=0; j<[self.readMsgs_Array count]; j++)
            {
                
                NoticeBoard *dBnb = [self.readMsgs_Array objectAtIndex:j];
                
                if([nb.notice_Id isEqualToString:dBnb.notice_Id])
                    [self.readMsgInd_Array replaceObjectAtIndex:nb.index withObject:@"1"];
            }
            
            
        }
    }
    
    
    
    
    
    
    [self.noticesTable reloadData];
    [self dismissActivityView];
    
    
}



- (void) parsingNoticeBoardDataXMLFailed
{
    
    
    
}

- (void)viewDidDisappear:(BOOL)animated

{
    NSLog(@"viewDidDisappear");
    
    [self.readMsgs_Array removeAllObjects];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)viewDidUnload
{
    [self setNoticeWebView:nil];
    [self setLoadingView:nil];
    [self setAcivityIndicator:nil];
    [super viewDidUnload];
}

@end
