//
//  User.h
//  GrabItNow
//
//  Created by MyRewards on 12/2/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject
{
    NSString *userId;
    NSString *client_id;
    NSString *domain_id;
    NSString *type;
    NSString *username;
    NSString *email;
    NSString *first_name;
    NSString *last_name;
    NSString *state;
    NSString *country;
    NSString *mobile;
    NSString *card_ext;
    NSString *client_name;
    NSString *newsletter;
    NSString *password;
    NSString *clientDomainName;
}

@property (nonatomic, strong) NSString *clientDomainName;
@property (nonatomic, strong) NSString *password;
@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *client_id;
@property (nonatomic, strong) NSString *domain_id;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *username;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *first_name;
@property (nonatomic, strong) NSString *last_name;
@property (nonatomic, strong) NSString *state;
@property (nonatomic, strong) NSString *country;
@property (nonatomic, strong) NSString *mobile;
@property (nonatomic, strong) NSString *card_ext;
@property (nonatomic, strong) NSString *client_name;
@property (nonatomic, strong) NSString *newsletter;

@end
