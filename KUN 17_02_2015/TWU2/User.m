//
//  User.m
//  GrabItNow
//
//  Created by MyRewards on 12/2/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "User.h"

@implementation User

@synthesize userId;
@synthesize client_id;
@synthesize domain_id;
@synthesize type;
@synthesize username;
@synthesize email;
@synthesize first_name;
@synthesize last_name;
@synthesize state;
@synthesize country;
@synthesize mobile;
@synthesize card_ext;
@synthesize client_name;
@synthesize newsletter;
@synthesize password;
@synthesize clientDomainName;

@end
