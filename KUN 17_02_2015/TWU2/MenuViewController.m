//
//  MenuViewController.m
//  TWU
//
//  Created by MyRewards on 2/15/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "MenuViewController.h"
#import "GrabItBaseViewController.h"
#import "NearByViewController.h"

#import "NoticeBoardViewController.h"
#import "CustomBadge.h"
#import "NoticeBoardDataParser.h"
#import "LikeUsViewController.h"

@class CustomBadge;

@interface MenuViewController ()
{
    int currentMenuItemTag;
    AppDelegate *appDelegate;
    UIViewController *baseViewController;
    int NoticeCount;
    ASIFormDataRequest *noticeBoardIDsRequest;
    GrabItBaseViewController *ginbaseView;
    NoticeBoardDataParser *NoticeBoardXMLParser;
    NSMutableArray *noticeIDArray;
    NSString *noticeBoardCount;
    UINavigationController *navController;
    NearByViewController *aroundMeViewController;

    CustomBadge *customBadge;
   // UIView *badgeView;
    NSString *title;
    LikeUsViewController *likeUs;
    
}
@property (nonatomic, strong) NoticeBoardViewController *noticeViewController;

-(void) calculatingUnreadNotices;

@end

@implementation MenuViewController

//@synthesize custom_TabBar;
@synthesize MyCard_Button,MyUnion_Button,MyDelegates_Button;
@synthesize badgeCount_image;
@synthesize badgeCountLabel;
@synthesize noticeViewController;
@synthesize contentView;
@synthesize badge;
@synthesize fourOne;
@synthesize fourTwo;
@synthesize fourThree;
@synthesize fourFour;
@synthesize fourFive;
@synthesize fourSix;
@synthesize fourSeven;
@synthesize fourEight;
@synthesize fourNine;
@synthesize fourTen;
@synthesize fourEleven;
@synthesize fourTwelve;
@synthesize MenuBgView;
@synthesize fiveOne,fiveTwo,fiveThree,fiveFour,fiveFive,fiveSix,fiveSeven,fiveEight,fiveNine,fiveTen,fiveEleven,fiveTwelve;
@synthesize nwBgMenu;



//@synthesize newView;
//=========================DELEGATE METHODS=============================//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        noticeBoardCount = @"";
        NoticeCount = 0;
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // [[self navigationController] setNavigationBarHidden:YES animated:NO];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegate.refArray = [[NSMutableArray alloc]init];
    [self loadViews];
    self.badge .tag = 222;
    appDelegate.refArray = [[NSMutableArray alloc]init];
    //--------------------- NoticeCount = -----------------------------------//
    
    
    
   
    
    //[self.badge addSubview:customBadge];
    //[ginbaseView.badgecontainerView addSubview:customBadge];
    
}
-(void)viewWillAppear:(BOOL)animated
{
    [[self navigationController] setNavigationBarHidden:YES animated:NO];
    [self fetchNoticeBoardItemsCount];
    
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7)
    {
        CGRect badgefr=self.badge.frame;
        badgefr.origin.y=232;
        self.badge.frame=badgefr;
    
        if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            CGRect frame10=self.fiveTen.frame;
            frame10.origin.y=442;
            self.fiveTen.frame=frame10;
            
            CGRect frame11=self.fiveEleven.frame;
            frame11.origin.y=442;
            self.fiveEleven.frame=frame11;
            
            CGRect frame12=self.fiveTwelve.frame;
            frame12.origin.y=442;
            self.fiveTwelve.frame=frame12;

            
            CGRect frame7=self.fiveSeven.frame;
            frame7.origin.y=316;
            self.fiveSeven.frame=frame7;
            
            CGRect frame8=self.fiveEight.frame;
            frame8.origin.y=316;
            self.fiveEight.frame=frame8;
            
            CGRect frame9=self.fiveNine.frame;
            frame9.origin.y=316;
            self.fiveNine.frame=frame9;

            
            CGRect frame4=self.fiveFour.frame;
            frame4.origin.y=190;
            self.fiveFour.frame=frame4;
            
            CGRect frame5=self.fiveFive.frame;
            frame5.origin.y=190;
            self.fiveFive.frame=frame5;
            
            CGRect frame6=self.fiveSix.frame;
            frame6.origin.y=190;
            self.fiveSix.frame=frame6;
            
            CGRect frame1=self.fiveOne.frame;
            frame1.origin.y=64;
            self.fiveOne.frame=frame1;
            
            CGRect frame2=self.fiveTwo.frame;
            frame2.origin.y=64;
            self.fiveTwo.frame=frame2;
            
            CGRect frame3=self.fiveThree.frame;
            frame3.origin.y=64;
            self.fiveThree.frame=frame3;

            
            
            CGRect bgframe=self.nwBgMenu.frame;
            bgframe.origin.y=0;
            bgframe.size.height=568;
            self.nwBgMenu.frame=bgframe;//Kun-Final-App-Skin
            self.nwBgMenu.image=[UIImage imageNamed:@"Kun_menu"];
            //self.nwBgMenu.image=[UIImage imageNamed:@"menu_iphone7.png"];
        }
        else
        {
        CGRect frame10=self.fourTen.frame;
        frame10.origin.y=376;
        self.fourTen.frame=frame10;
        
        CGRect frame11=self.fourEleven.frame;
        frame11.origin.y=376;
        self.fourEleven.frame=frame11;
        
        CGRect frame12=self.fourTwelve.frame;
        frame12.origin.y=376;
        self.fourTwelve.frame=frame12;
        
        CGRect frame7=self.fourSeven.frame;
        frame7.origin.y=272;
        self.fourSeven.frame=frame7;
        
        CGRect frame8=self.fourEight.frame;
        frame8.origin.y=272;
        self.fourEight.frame=frame8;
        
        CGRect frame9=self.fourNine.frame;
        frame9.origin.y=272;
        self.fourNine.frame=frame9;
        
        CGRect frame4=self.fourFour.frame;
        frame4.origin.y=168;
        self.fourFour.frame=frame4;
        
        CGRect frame5=self.fourFive.frame;
        frame5.origin.y=168;
        self.fourFive.frame=frame5;

        CGRect frame6=self.fourSix.frame;
        frame6.origin.y=168;
        self.fourSix.frame=frame6;

        
        CGRect frame1=self.fourOne.frame;
        frame1.origin.y=64;
        self.fourOne.frame=frame1;
        
        CGRect frame2=self.fourTwo.frame;
        frame2.origin.y=64;
        self.fourTwo.frame=frame2;
        
        CGRect frame3=self.fourThree.frame;
        frame3.origin.y=64;
        self.fourThree.frame=frame3;
        
        CGRect bgframe=self.MenuBgView.frame;
        bgframe.origin.y=0;
        bgframe.size.height=480;
        self.MenuBgView.frame=bgframe;
            self.MenuBgView.image=[UIImage imageNamed:@"Kun_menu"];
  
       // self.MenuBgView.image=[UIImage imageNamed:@"menu7.png"];
        }
    }
   
}
//==========================================================//

- (BOOL)prefersStatusBarHidden {
    return YES;
}
//===========DELEGATE METHODS TO LOCK ORIENTATION=======================//

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationPortrait];
    
}
//
//- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation{
//   
//    UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
//}

-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}
//-(BOOL)s



-(NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (BOOL) shouldAutorotate
{
    return NO;
}
//========================================================================//
//----------------------------Notice Board ID----------------------------------------------//

#pragma mark -- Notice Board Data Parser Delegate methods

- (void) parsingNoticeBoardDataFinished:(NSArray *) noticesList
{
    
    if (!noticeIDArray) {
        noticeIDArray = [[NSMutableArray alloc] initWithArray:noticesList];
    }
    else {
        [noticeIDArray removeAllObjects];
        [noticeIDArray addObjectsFromArray:noticesList];
    }
    
    
    
    NSLog(@"noticeIDArray Count: %d",[noticeIDArray count]);
    
    [self calculatingUnreadNotices];
    
    
    
}




-(void) fetchNoticeBoardItemsCount
{
    
    NSLog(@"... fetchNoticeBoardItemsCount ...");
    
    NSString *urlString = [NSString stringWithFormat:@"%@",DailysaversNoticeBoardURL];
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIFormDataRequest *req = [[ASIFormDataRequest alloc] initWithURL:url];
    NSLog(@"NoticeBoardItemsCount url:%@",urlString);
    [req setDelegate:self];
    [req startAsynchronous];
    noticeBoardIDsRequest = req;
}


-(void) calculatingUnreadNotices{
    
    NSLog(@"calculatingUnreadNotices");
    
    NoticeBoard *currrentNotice;
    NoticeBoard *cur;
    NoticeCount = 0;
    
    NSMutableArray *myArray = [[NSMutableArray alloc]init];
    [myArray addObjectsFromArray:[appDelegate noticeBoardIds]];
    
    if([myArray count] >0 )
    {
        
        for (int i = 0; i<[noticeIDArray count]; i++)
        {
            
            for (int j=0; j<[myArray count]; j++)
            {
                
                currrentNotice = [noticeIDArray objectAtIndex:i];
                cur = [myArray objectAtIndex:j];
                
                if([currrentNotice.notice_Id isEqualToString:cur.notice_Id])
                    NoticeCount++;
                
                
            }
            
        }
        NoticeCount  = [noticeIDArray count]- NoticeCount;
    }
    else
        NoticeCount  = [noticeIDArray count]- NoticeCount;
    
    
    noticeBoardCount = [NSString stringWithFormat:@"%d",NoticeCount];
    // NSLog(@"noticeBoardCount:%@",noticeBoardCount);
     appDelegate.badgecount=[noticeBoardCount intValue];
    NSLog(@"Unread count:: %d",NoticeCount);
    appDelegate.noticeBoardCount=NoticeCount;
    if (NoticeCount!= 0) {
        
        
        customBadge = [CustomBadge customBadgeWithString:noticeBoardCount
                                         withStringColor:[UIColor whiteColor]
                                          withInsetColor:[UIColor blueColor]
                                          withBadgeFrame:YES
                                     withBadgeFrameColor:[UIColor whiteColor]
                                               withScale:1.0
                                             withShining:YES];
        
        [self.view viewWithTag:222].hidden = NO;
        [self.badge addSubview:customBadge];
        
         NSLog(@"Unhiding Badge view");
        
    }
    else{
        NSLog(@"Hiding Badge view");
        self.badge = nil;
        //[[self.view viewWithTag:222] removeFromSuperview];
          [self.view viewWithTag:222].hidden = YES;
    }
    
    
    
    
    
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    if(request == noticeBoardIDsRequest){
        NSLog(@"NB IDs:%@",[request responseString]);
        noticeBoardIDsRequest = nil;
        NSXMLParser *noticeParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        NoticeBoardXMLParser = [[NoticeBoardDataParser alloc] init];
        NoticeBoardXMLParser.delegate = self;
        noticeParser.delegate = NoticeBoardXMLParser;
        [noticeParser parse];
    }
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request {
    
    if(request == noticeBoardIDsRequest){
        NSLog(@"NoticesIDFetchRequest FAILED: %@",request.error.description);
        noticeBoardIDsRequest = nil;
    }
    
}

//---------------------------noticeCount ended------------------------------------//


// =======================METHOD TO LOAD VIEWS IN IPHONE  ==================================//

-(void)loadViews
{
    
    
    
    
    if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        // IPhone 5
        NSLog(@"Iphone5 View Loaded");
        self.view = [[NSBundle mainBundle] loadNibNamed:@"MenuViewController_5" owner:self options:nil][0];
    }
    else
    {
        //Iphone 4
        NSLog(@"Iphone4 View Loaded");
        CGRect frame=self.view.frame;
        frame.size.height=436;
        self.view.frame=frame;
        self.view = [[NSBundle mainBundle] loadNibNamed:@"MenuViewController" owner:self options:nil][0];
        
    }
    
}

//==============================================================================================//



//=========METHOD ADDS TRANSITION EFFECT TO VIEW CONTROLLER==============================//




- (void)transitionToViewController:(UIViewController *)viewController
                    withTransition:(UIViewAnimationOptions)transition
{
    
    [UIView transitionFromView:self.view
                        toView:viewController.view
                      duration:0.15f
                       options:transition
                    completion:^(BOOL finished){
                    } ];
}

//==============================================================================================//

//========================METHOD TO LOAD VIEWS IN MENU VIEW CONTROLLER==============================//

- (IBAction)MenuViewButtonTapped:(id)sender
{
    
    currentMenuItemTag = [sender tag];
    NSLog(@"%d",currentMenuItemTag);
    
    
    
    switch ([sender tag])
    {
        case MyUnionButtonTag:
            [self presentMyUnionMainController];
            break;
        case MyDelegateButtonTag:
            [self presenMyCarViewController];
            break;
        case MyCardButtonTag:
            [self presentMyCardController];
            break;
        case MySosContactsButtonTag:
             [self presentMyIssuueController];
          

            break;
        case MyNoticeBoardButtonTag:
                [self presentMyParkingTimerController];
            break;
        case SendAFriendButtonTag:
             [self presentMyNoticeBoardController];
           
            break;
        case MyUnionRewardsButtonTag:
             [self presentSendAFriendController];
            
            break;
        case SearchMYRewardsButtonTag:
            [self presentMyDelegateController];

            break;
        case LogoutButtonTag:
            [self presentLogoutController];
            break;
        case MyParkingTimerButtonTag:
            [self presentMyAccountController];
            break;
        case MyIssueButtonTag:
         
             [self presentSearchMYRewardsController];
            break;
        case MyAccountButtonTag:
           [self presentMyUnionrewardsController];
            break;
            
            
        default:
            break;
    }
    
    
}

//==============================================================================================//



//=================METHODS TO VIEW RESPONDING VIEW CONTROLLER==============//
-(void)presentMyUnionMainController
{
    NSLog(@"========MyUnionController========");
    
    myunionmainview = [[MyUnionMainViewController alloc] initWithNibName:@"MyUnionMainViewController" bundle:nil];
    
    
    [self presentNewViewController:myunionmainview];
    
}
-(void)presentMyDelegateController
{
    NSLog(@"========MyDelegateController========");
    
    if (!mycardview)
    {
        mycardview = [[MyCardViewController alloc] initWithNibName:@"MyCardViewController" bundle:nil];
    }
    
    [self presentNewViewController:mycardview];
    
}
-(void)presentMyCardController
{
    NSLog(@"========MyCardController========");
    
    if (!roadsideview)
    {
        roadsideview = [[myroadsideViewController alloc] initWithNibName:@"myroadsideViewController" bundle:nil];
    }
    
    [self presentNewViewController:roadsideview];
}
-(void)presenMyCarViewController
{
    NSLog(@"========MyCarViewController========");
    
    if (!mycarviewcontroller)
    {
        mycarviewcontroller = [[MyCarViewController alloc]initWithNibName:@"MyCarViewController" bundle:nil];
    }
    
    [self presentNewViewController:mycarviewcontroller];
    
}
-(void)presentMyNoticeBoardController
{
    NSLog(@"========MyNoticeBoardController========");
    
    mynoticeboard = [[MyNoticeBoardViewController alloc] initWithNibName:@"MyNoticeBoardViewController" bundle:nil];
    
    [self presentNewViewController:mynoticeboard];
    
}

-(void)presentSendAFriendController
{
    NSLog(@"========SendAFriendController========");
    
    if (!sendafriendview)
    {
        sendafriendview = [[SendAFriendViewController alloc] initWithNibName:@"SendAFriendViewController" bundle:nil];
    }
    
    [self presentNewViewController:sendafriendview];
}

-(void)presentMyParkingTimerController
{
    NSLog(@"========MyParkingTimerController========");
    if (!myparkertimerview)
    {
    myparkertimerview = [[MyParkerTimerViewController alloc] initWithNibName:@"MyParkerTimerViewController" bundle:nil];
    }
    [self presentNewViewController:myparkertimerview];
    
    
}

-(void)presentMyIssuueController
{
    NSLog(@"========MyIssuueController========");
    
    if (!myissueview)
    {
        myissueview = [[MyIssueViewController alloc] initWithNibName:@"MyIssueViewController" bundle:nil];
    }
    [self presentNewViewController:myissueview];
    
    
}


-(void)presentMyAccountController
{
    NSLog(@"========MyAccountController========");
    
    if (!myaccountview)
    {
        myaccountview = [[MyAccountViewController alloc] initWithNibName:@"MyAccountViewController" bundle:nil];
    }
    [self presentNewViewController:myaccountview];
}
//=========================================================================//
//===============METHOD TO ADD NAVIGATION CONTROLLER=======================//
-(void)presentNewViewController:(UIViewController *) controller
{
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.01f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController pushViewController:controller animated:NO];}
     completion:NULL];
    
}
//=========================================================================//

//========================GRABITNOW VIEW CONTROLLERS=======================//
-(void)presentMyUnionrewardsController
{
    NSLog(@"========MyUnionrewardsController========");
    
    appDelegate.isFromMenuPresentMyUnionrewardsController = @"YES";
    [self tabBarController];
    
    
//    search = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil ];
//    
//   
//    appDelegate.path = 1;
//     
//    [self presentGrabItNowViewController:search];
    
}

-(void) presentSearchMYRewardsController
{
    NSLog(@"========SearchMyRewardsController========");
    
//     aroundMeViewController = [[NearByViewController alloc] initWithNibName:@"NearByViewController" bundle:nil];
//    //favoriteViewController = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil productListType:ProductListTypeMyFavorites];
//    
//    appDelegate.path = 1;//ProductListTypeMyFavorites
//    [self presentGrabItNowViewController:aroundMeViewController];
    
    appDelegate.isFromMenuPresentMyUnionrewardsController = @"NO";
    [self tabBarController];
}

- (void) presentGrabItNowViewController:(UIViewController *) controller{
    
    if (currentMenuItemTag==12)
        
        title=@"My Rewards";
    
    else
        
        title=@"Nearby Rewards";
    
    GrabItBaseViewController *base = [[GrabItBaseViewController alloc]initWithNibName:@"GrabItBaseViewController" bundle:nil title:title];
    [appDelegate.refArray removeAllObjects];
    [appDelegate.refArray addObject:base];
    
    if([appDelegate isIphone5])
    {
        
        CGRect frame = controller.view.frame;
        frame.origin.y = -5;
        frame.size.height = (appDelegate.window.frame.size.height)-20;
        controller.view.frame =frame;
    }
    else
    {
        CGRect frame = controller.view.frame;
        frame.origin.y = -5;
        frame.size.height = (appDelegate.window.frame.size.height)+80;
        controller.view.frame =frame;
        
    }
    
    
    [controller viewWillAppear:YES];
    
    [base.view addSubview:controller.view];
    
    
    [base.view bringSubviewToFront:base.customTabView];
   // [base.view bringSubviewToFront:base.header_view];
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController pushViewController:base animated:NO];}
     completion:NULL];
    
    
    //[self.navigationController pushViewController:myunionViewController animated:YES];
}

//=========================================================================//
//==============METHODS TO ADD NAVIGATION CONTROLLER=======================//

-(void) presentLogoutController
{
  /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logout?" message:@"Do you really want to logout?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    
    alert.tag = 111;
    
    [alert show]; likeUs */
    if (!likeUs)
    {
        likeUs = [[LikeUsViewController alloc] initWithNibName:@"LikeUsViewController" bundle:nil];
    }
    [self presentNewViewController:likeUs];
    
    
}




- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (alertView.tag == 111)
    {
        if (buttonIndex == alertView.cancelButtonIndex)
        {
            [appDelegate logoutUserSession];
        }
    }
    
}


//=========================================================================//

#pragma mark - SwitchTabBar Controller Method

-(void)tabBarController
{
    
    
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       [UIColor whiteColor], UITextAttributeTextColor,
                                                       nil] forState:UIControlStateNormal];
    UIColor *titleHighlightedColor = [UIColor colorWithRed:153/255.0 green:192/255.0 blue:48/255.0 alpha:1.0];
    [[UITabBarItem appearance] setTitleTextAttributes:[NSDictionary dictionaryWithObjectsAndKeys:
                                                       titleHighlightedColor, UITextAttributeTextColor,
                                                       nil] forState:UIControlStateHighlighted];
    
    // Change the tab bar background
    UIImage* tabBarBackground = [UIImage imageNamed:@"tabbar.png"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    [[UITabBar appearance] setSelectionIndicatorImage:[UIImage imageNamed:@"tabbar_selected.png"]];
    
    if ([appDelegate.isFromMenuPresentMyUnionrewardsController isEqualToString:@"YES"]) {
        
        search = [[SearchViewController alloc] initWithNibName:@"SearchViewController" bundle:nil ];
        SearchNav = [[UINavigationController alloc] initWithRootViewController:search];
        
        
        imgArr = [NSArray arrayWithObjects:[UIImage imageNamed:@"tab_bar_0"],[UIImage imageNamed:@"tab_bar_2"],[UIImage imageNamed:@"tab_bar_3"],[UIImage imageNamed:@"tab_bar_4"],[UIImage imageNamed:@"tab_bar_5"],[UIImage imageNamed:@"tab_bar_6"],[UIImage imageNamed:@"tab_bar_7"],[UIImage imageNamed:@"tab_bar_4"],nil];
    }
    else
    {
        aroundMeViewController = [[NearByViewController alloc] initWithNibName:@"NearByViewController" bundle:nil];
        SearchNav = [[UINavigationController alloc] initWithRootViewController:aroundMeViewController];
        
        imgArr = [NSArray arrayWithObjects:[UIImage imageNamed:@"tab_bar_1"],[UIImage imageNamed:@"tab_bar_2"],[UIImage imageNamed:@"tab_bar_3"],[UIImage imageNamed:@"tab_bar_4"],[UIImage imageNamed:@"tab_bar_5"],[UIImage imageNamed:@"tab_bar_6"],[UIImage imageNamed:@"tab_bar_7"],[UIImage imageNamed:@"tab_bar_4"],nil];
    }
    
    
    ProductListViewController *hotOffersViewController = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil productListType:ProductListTypeOffers];
    UINavigationController * hotOffersNav = [[UINavigationController alloc] initWithRootViewController:hotOffersViewController];
    
    favoriteViewController=[[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil productListType:ProductListTypeMyFavorites];
    UINavigationController * favoriteNav = [[UINavigationController alloc] initWithRootViewController:favoriteViewController];
    
    
    DailyDealsViewController *dailyDealsViewController = [[DailyDealsViewController alloc] initWithNibName:@"DailyDealsViewController" bundle:nil];
    UINavigationController * dailyDealsNav = [[UINavigationController alloc] initWithRootViewController:dailyDealsViewController];
    
    NoticeBoardViewController *noticeBoardViewController = [[NoticeBoardViewController alloc] initWithNibName:@"NoticeBoardViewController" bundle:nil];
    UINavigationController * NoticeNav = [[UINavigationController alloc] initWithRootViewController:noticeBoardViewController];
    
    
    HelpViewController *helpViewController = [[HelpViewController alloc] initWithNibName:@"HelpViewController" bundle:nil];
    UINavigationController *helpNav = [[UINavigationController alloc]initWithRootViewController:helpViewController];
    
    
    
    
    NSArray *ctrlArr = [NSArray arrayWithObjects:SearchNav,hotOffersNav,favoriteNav,dailyDealsNav,NoticeNav,dailyDealsNav,helpNav,nil];
    
    //    NSArray *imgArr = [NSArray arrayWithObjects:[UIImage imageNamed:@"tab_bar_1"],[UIImage imageNamed:@"tab_bar_2"],[UIImage imageNamed:@"tab_bar_3"],[UIImage imageNamed:@"tab_bar_4"],[UIImage imageNamed:@"tab_bar_5"],[UIImage imageNamed:@"tab_bar_6"],[UIImage imageNamed:@"tab_bar_7"],[UIImage imageNamed:@"tab_bar_4"],nil];
	
	SwitchTabBarController *switchTabBarCntrl = [[SwitchTabBarController alloc] initWithViewControllers:ctrlArr imageArray:imgArr];
	[switchTabBarCntrl.tabBar setBackgroundImage:[UIImage imageNamed:@"mainpage_bottombg"]];
	[switchTabBarCntrl setTabBarTransparent:YES];
    
    [appDelegate.navController pushViewController:switchTabBarCntrl animated:NO];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
