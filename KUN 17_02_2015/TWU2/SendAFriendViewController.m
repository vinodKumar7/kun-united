//
//  SendAFriendViewController.m
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "SendAFriendViewController.h"
#import "AppDelegate.h"
#import "ASIFormDataRequest.h"
@interface SendAFriendViewController ()
{
    AppDelegate *appDelegate;
    UIView *loadingView;
}


@end

@implementation SendAFriendViewController

@synthesize containerView;
@synthesize background_ImageView;
@synthesize containerImageView;
@synthesize safimageView;
@synthesize safButton;
@synthesize subView;
@synthesize friendName;
@synthesize friendMail;
@synthesize currentTextField;
@synthesize safScrollview;
@synthesize safpageView;
//=========================DELEGATE METHODS=============================//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
   
    [self.containerImageView setHidden:NO];
    [self.subView setHidden:YES];
    UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleSingleTap:)];
    
    [safpageView addGestureRecognizer:singleTap];
    
 NSLog(@"UID %@",appDelegate.sessionUser.userId );
    [self loadViews];
    safScrollview.contentSize = CGSizeMake(255, 695);
    safScrollview.scrollEnabled=YES;
    
   
    [safScrollview setCanCancelContentTouches:YES];
    [safpageView setUserInteractionEnabled:YES];
    // Do any additional setup after loading the view from its nib.
}
-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
        [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
//    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
//    if ([[ver objectAtIndex:0] intValue] >= 7)
//    {
//        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];        self.navigationController.navigationBar.translucent = NO;
//    }
//    else
//    {
//        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
//    }
    
 /*   if([self.navigationController.navigationBar respondsToSelector:@selector(barTintColor)])
    {
        // iOS7
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
        self.navigationController.navigationBar.translucent = NO;
    }
    else
    {
        // older
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
    }*/
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"Send A Friend";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor whiteColor]];
    self.navigationItem.titleView = TitleLabel;
    
    subView.layer.cornerRadius = 10.0;
    subView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    subView.layer.shadowOpacity = 1.0;
    
    safScrollview.layer.cornerRadius = 10.0;
    safScrollview.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    //safScrollview.shadowOpacity = 1.0;

    
    
    if (![appDelegate isIphone5])
    {
    [self  subViewframe];    
    }
    
    [self.safButton setEnabled:YES];
    
}

-(void)back
{
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    [self.subView setHidden:YES];
   
    
    
}



//==============================================================================================//
//=====================DELEGATE METHODS TO LOCK ORIENTATION=====================================//
-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}
//-(BOOL)s

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

- (BOOL) shouldAutorotate
{
    return NO;
}
//===============================================//
// ================METHOD TO LOAD VIEWS IN IPHONE======================//
-(void)loadViews
{
    if([appDelegate isIphone5])
    {
        NSLog(@"Iphone5");
        
        CGRect frame =self.background_ImageView.frame;
        frame.size.height = 435;
        self.background_ImageView.frame = frame;
        
        CGRect containerframe = self.containerImageView.frame;
        containerframe.size.height = 457;
        self.containerImageView.frame = containerframe;
        
        CGRect safcontainerframe = self.safimageView.frame;
        safcontainerframe.size.height = 457;
        self.safimageView.frame = safcontainerframe;
        
        CGRect safbuttonframe = self.safButton.frame;
        safbuttonframe.size.height = 457;
        self.safButton.frame = safbuttonframe;
                     
    }
    else
    {
        NSLog(@"Iphone4");
    }
    
}
//=========================================//
// =================VALIDATING USER SUBMITING DETAILS ================//
-(void) subViewframe
{

    CGRect frame=self.subView.frame;
    frame.origin.y=15;
    self.subView.frame=frame;


}

- (BOOL) validateLogin
{
    
    BOOL success = YES;
    
    if (self.friendMail.text.length == 0 || self.friendName.text.length == 0 )
    {
        success = NO;
    }
    
    return success;
}

//===================================================================//


// ===========METHOD  LOADS VIEW WHEN SCREEN TAPPED ===================//

- (IBAction)closeWindow:(id)sender
{
    [self.subView  setHidden:YES];
   // [self.safButton setEnabled:YES];
    [friendMail resignFirstResponder];
    [friendName resignFirstResponder];
}
//==== Method to handle user gesture
- (void)handleSingleTap:(UIGestureRecognizer *)gestureRecognizer
{
    // single tap handling
    
    [self.subView setHidden:NO];
    [self bounceOutAnimationStoped];
   // NSURL *websiteUrl = [NSURL URLWithString:@"http://www.sendafriend.com.au/app/webroot/saf/invitation.html"];
    
   // [[UIApplication sharedApplication] openURL:websiteUrl];
}

-(IBAction)sendAFriend_clicked:(id)sender
{
   /* if ([MFMailComposeViewController canSendMail])
    {
        // Create and show composer
       // NSArray *receipntsArray = [[NSArray alloc]initWithObjects:@"info@dailysavers.com.au",nil];
             
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        //[controller setToRecipients:receipntsArray];
        [controller setSubject:@"Join CEPU"];
       // [controller setMessageBody:@"<h4> &nbsp&nbsp&nbsp&nbsp  9 great reasons to join the union &nbsp&nbsp </h4>&#9733;  Every day advice and assistance.<br><br>&#9733; Answers and expertise.<br><br>&#9733;  Representation.<br><br>&#9733; Better pay and conditions.<br><br>&#9733;Have someone always on your side.<br><br>&#9733;It's your right – exercise it<br><br>&#9733;Union membership fees are tax deductible<br><br>&#9733;Save more than enough on shopping, banking, insurances and holidays – to more than cover the cost of your membership<br><br>&#9733;Visit  <a href=http://www.cwuvic.org.au/join/>Hyperlink Code</a>" isHTML:YES];
       // [self presentModalViewController:controller animated:YES];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"Click here to Join"]];

        [controller setMessageBody:@"Hi I’m in the CEPU and thought you may like to join to as they offer great benefits and work place security.To find out more http://www.ceputas.com.au/join/" isHTML:YES];
        [self presentViewController:controller animated:YES completion:nil];
        
    }
    else
    {
        // Show some error message here
        UIAlertView* alert_view = [[UIAlertView alloc] initWithTitle:@"Alert"
                                                             message:@"You will need to setup a mail account on your device before you can send mail!"
                                                            delegate:nil
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
        [alert_view show];
        return;
    }
    
}
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
-(BOOL)webView:(UIWebView *)descriptionTextView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (UIWebViewNavigationTypeLinkClicked == navigationType) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;*/
   
    [self.safButton setEnabled:NO];

}
-(void)mailComposeController:(MFMailComposeViewController*)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError*)error {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if(textField.tag == 0)
        return (newLength > 60) ? NO : YES;
    else
        return (newLength > 25) ? NO : YES;
}
- (IBAction)sendfriendDetails:(id)sender
{
    
    [friendMail resignFirstResponder];
    [friendName resignFirstResponder];
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    if (friendName.text.length == 0 || friendMail.text.length == 0 )
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fields Empty" message:@"All fields are mandatory. \nPlease fill all the fields." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        

    }
    else if ([emailTest evaluateWithObject:friendMail.text] == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Invalid EmailId" message:@"Email should be in proper format" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    
    
    }
    else
    {
        
        loadingView=[[UIView alloc]initWithFrame:self.subView.bounds];
        loadingView.layer.cornerRadius = 10.0;
        loadingView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
        loadingView.layer.shadowOpacity = 1.0;
        UILabel *activityLabel = [[UILabel alloc] init];
        activityLabel.text = @"Sending....";
        activityLabel.backgroundColor = [UIColor clearColor];
        activityLabel.textColor = [UIColor whiteColor];
        activityLabel.font = [UIFont systemFontOfSize:20];
        [loadingView addSubview:activityLabel];
        activityLabel.frame = CGRectMake(120, 80, 200, 25);
        UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite] ;
        activityIndicator.frame=CGRectMake(50, 80, 100, 25);
        [activityIndicator startAnimating];
        [loadingView addSubview:activityIndicator];
        
        
        //        CGRect loadingFrame=loadingView.frame;
        //        loadingFrame.size.width=287;
        //        loadingFrame.size.height=205;
        //        loadingView.frame=loadingView.frame;
        loadingView.backgroundColor=[UIColor lightTextColor];
        [self.subView addSubview:loadingView];

    
        NSString *urlString = [NSString stringWithFormat:@"%@saf.php",URL_Prefix];
        NSURL *url = [NSURL URLWithString:urlString];
        NSLog(@"URL is::%@",urlString);
        
        ASIFormDataRequest *req = [[ASIFormDataRequest alloc] initWithURL:url];
        [req setPostValue:appDelegate.sessionUser.userId forKey:@"uid"];
       
        //[req setPostValue:appDelegate.sessionUser.password forKey:@"pwd"];
        [req setPostValue:friendName.text forKey:@"fname"];
        [req setPostValue: friendMail.text forKey:@"email"];
        [req setDelegate:self];
        [req startAsynchronous];
    }
   
   [self.currentTextField resignFirstResponder]; 
}

- (void)bounceOutAnimationStoped
{
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self.subView.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.9, 0.9);
         self.subView.alpha = 0.8;
     }
                     completion:^(BOOL finished){
                         [self bounceInAnimationStoped];
                     }];
}

- (void)bounceInAnimationStoped
{
    [UIView animateWithDuration:0.1 animations:
     ^(void){
         self.subView.transform = CGAffineTransformScale(CGAffineTransformIdentity,1, 1);
         self.subView.alpha = 1.0;
     }
                     completion:^(BOOL finished){
                         ;
                     }];
}
//==============================================================================================//
//===========ASIHTTPREQUEST  DELEGATE METHODS =======================//

#pragma mark -- ASIHttpRequest Delegate methods


- (void)requestFinished:(ASIHTTPRequest *)request
{
    [loadingView removeFromSuperview];
    NSLog(@"REQUEST SUBMITTED:%@",[request responseString]);
    friendMail.text=@"";
    friendName.text=@"";
     NSString *result = [request responseString];
    if([result isEqualToString:@"success"])
    {
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Your invitation has been sent" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        //status "Not Login"
        UIAlertView *failedAlert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Your invitation has been sent" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [failedAlert show];
        
        
    }


}
- (void)requestFailed:(ASIHTTPRequest *)request

{
    [loadingView removeFromSuperview];
    UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [failureAlert show];

    NSLog(@"REQUEST Failed");

}
//==============================================================================================//
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"textFieldShouldReturn:");
    
    [textField resignFirstResponder];
    
    return YES;
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.currentTextField = textField;
    
    
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch = [touches anyObject];
    if (touch.tapCount == 2) {
       // UIView *view = [[[UIImageView alloc] initWithImage:[UIImage imageNamed:[NSString stringWithFormat:@"1.jpg"]]] autorelease];
        //view.frame = CGRectMake(0, 0, 320, 450);
        // [carousel addSubview:couponView];
        
        NSLog(@"ruk rock //This will cancel the doubleTap action");
        [NSObject cancelPreviousPerformRequestsWithTarget:self];
    }
    //NSLog(@"ruk rock");
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setContainerImageView:nil];
    [self setSafimageView:nil];
    [self setSafButton:nil];
    [self setSafScrollview:nil];
    [self setSafpageView:nil];
    [super viewDidUnload];
}
- (IBAction)doneButtonClicked:(id)sender {
}
@end
