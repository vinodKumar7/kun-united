//
//  MyUnionParser.h
//  TWU2
//
//  Created by MyRewards on 2/20/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Display.h"


@protocol MyUnionParser;

@interface MyUnionParser : NSObject<NSXMLParserDelegate>
{
    
    NSMutableArray *List;
}
@property (unsafe_unretained) id <MyUnionParser> delegate;
@property(nonatomic,strong)NSMutableArray *List;
@end

@protocol MyUnionParser <NSObject>
- (void) parsingMyUnionDetailsFinished:(Display *) myUnionDisplay;
- (void) MyUnionDetailsXMLparsingFailed;
@end