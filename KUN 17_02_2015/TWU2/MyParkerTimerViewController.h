//
//  MyParkerTimerViewController.h
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MKMapview.h>
#import <AVFoundation/AVFoundation.h>

@interface MyParkerTimerViewController : UIViewController<UIPickerViewDataSource,UIPickerViewDelegate,UIAlertViewDelegate,CLLocationManagerDelegate,MKMapViewDelegate,AVAudioRecorderDelegate, AVAudioPlayerDelegate>
{
    CLLocationManager *locationManager;
    CLLocationCoordinate2D currentLocation;
    AVAudioRecorder *recorder;
    AVAudioPlayer *player;
}


@property (strong, nonatomic) IBOutlet UIButton *gobackButton;
@property (strong, nonatomic) IBOutlet UIView *baseView;
@property (strong, nonatomic) IBOutlet UIView *timerFlipView;

@property (strong, nonatomic) IBOutlet UILabel *settimerLabel;
@property(nonatomic, strong)UITextField *currentTextField;
@property (strong, nonatomic) IBOutlet UILabel *locdescLabel;

@property(nonatomic,strong)IBOutlet MKMapView *parkingMapView;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, readwrite) CLLocationCoordinate2D currentLocation;
@property(nonatomic,strong)IBOutlet UIButton *addLocation_Button;
@property(nonatomic,strong)IBOutlet UILabel *myCounterLabel;
@property (strong, nonatomic) IBOutlet UIButton *remainderButton;
@property(nonatomic,strong)IBOutlet UIView *setTimer_View;
@property(nonatomic,strong)IBOutlet UITextField *location_TextField;
@property(nonatomic,strong)IBOutlet UIPickerView *time_Picker;
@property(nonatomic,strong)IBOutlet UIButton *done_Button;

@property (strong, nonatomic) IBOutlet UIButton *setremainderButton;
@property (strong, nonatomic) IBOutlet UILabel *AfterthemeterLabel;

-(IBAction)setRemainderButton_clicked:(id)sender;
- (IBAction)doneButton_Clicked:(id)sender;
- (IBAction)startTimer_Clicked:(id)sender;


- (void)scheduleLocalNotificationWithDate: (NSDate *)fireDate;

@end
