//
//  MyAccountViewController.h
//  TWU2
//
//  Created by MyRewards on 2/22/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyAccountViewController : UIViewController<UITextFieldDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
    IBOutlet UIScrollView *myScrollView;
    IBOutlet UIImageView *background_ImageView;
    IBOutlet UITextField *firstName_TextFeild;
    IBOutlet UITextField *lastName_TextFeild;
    IBOutlet UITextField *email_TextFeild;
    IBOutlet UITextField *state_TextFeild;
    IBOutlet UITextField *memberShip_TextFeild;
    
    IBOutlet UIButton *submit_Button;
   
}
@property (strong, nonatomic) IBOutlet UIImageView *logoView;
@property (strong, nonatomic) IBOutlet UILabel *firstnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *lastnameLabel;
@property (strong, nonatomic) IBOutlet UILabel *emailLabel;
@property (strong, nonatomic) IBOutlet UILabel *stateLabel;
@property (strong, nonatomic) IBOutlet UILabel *memnoLabel;
@property (strong, nonatomic) IBOutlet UILabel *noteLabel;
@property (strong, nonatomic) IBOutlet UITextField *firstnameTextField;
@property (strong, nonatomic) IBOutlet UITextField *lastnameTextField;
@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) IBOutlet UITextField *stateTextField;
@property (strong, nonatomic) IBOutlet UITextField *memnoTextField;

@property(nonatomic, strong)IBOutlet UIScrollView *myScrollView;
@property(nonatomic, strong)IBOutlet UIImageView *background_ImageView;
@property(nonatomic, strong)IBOutlet UITextField *firstName_TextFeild;
@property(nonatomic, strong)IBOutlet UITextField *lastName_TextFeild;
@property(nonatomic, strong)IBOutlet UITextField *email_TextFeild;
@property(nonatomic, strong)IBOutlet UITextField *state_TextFeild;
@property(nonatomic, strong)IBOutlet UITextField *memberShip_TextFeild;
@property(nonatomic, strong)UITextField *currentTextFeild;

@property(nonatomic, strong)IBOutlet UIButton *submit_Button;

-(IBAction)submitButton_Pressed:(id)sender;
-(void)loadViews;
@end
