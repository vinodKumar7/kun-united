//
//  AppDelegate.h
//  TWU2
//
//  Created by MyRewards on 2/15/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "MyUnion.h"
#import "MyUnionContact.h"
#import "User.h"
#import "Product.h"
#import "ParkingTimer.h"
#import <Parse/Parse.h>
#import "NoticeBoard.h"
#import "ZBarSDK.h"
#import <AVFoundation/AVFoundation.h>

@class LoginViewController;
@class MenuViewController;
//@class RegistrationViewController;
@interface AppDelegate : UIResponder <UIApplicationDelegate,AVAudioPlayerDelegate,ZBarReaderDelegate>
{
   
    NSManagedObjectContext *managedObjectContext;
    NSString *docPath;
    NSMutableArray *userDetailsArray;
    int path;
    int remndr;
    UINavigationController *navController;
    
    NSTimer *CountTimer;
    NSTimer *CountDownTimer;
    int countDownTime;
    int actualTime;
    UILabel *label;
    int temp;
    int x;
    
    NSDate *closedTime;
    AVAudioPlayer *player;
    NSMutableArray *refArray;
    int noticeBoardCount;
     NSInteger badgecount;

}

@property(nonatomic, readwrite)int temp;
@property(nonatomic, readwrite)int noticeBoardCount;
@property(nonatomic, readwrite)int x;
@property (readonly, strong, nonatomic)NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property(nonatomic,strong) NSString *docPath;
@property(nonatomic, readwrite)NSInteger badgecount;
@property (nonatomic, strong) ZBarReaderViewController *reader;

@property(nonatomic,strong) NSMutableArray *userDetailsArray;
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong) User *sessionUser;
//@property (strong, nonatomic) RegistrationViewController *registrationviewController;
@property (strong, nonatomic) LoginViewController *viewController;
@property (strong, nonatomic) MenuViewController *menuViewController;
@property (strong, nonatomic)UINavigationController *navController;
@property (nonatomic, readwrite) int path;
@property (nonatomic, readwrite) int remndr;
@property (nonatomic, strong) NSString *isFromMenuPresentMyUnionrewardsController;
//===
@property(nonatomic,strong) NSMutableArray *refArray;
//====
- (NSString *) sessionUsername;
- (NSString *) sessionPassword;
- (NSString *) clientDomainName;

-(void)startCountDownTimerWithTime:(int)time andUILabel:(UILabel *)currentLabel;
-(void)invalidateCurrentCountDownTimer;

// My Favorite Methods
- (BOOL) addProductToFavorites:(Product *) product;
- (BOOL) removeProductFromfavorites:(Product *) product;
- (BOOL) productExistsInFavorites:(Product *) product;

//My Contacts Methods
-(BOOL) addContact:(MyUnionContact *) contact;
- (BOOL) removeContact:(MyUnionContact *) contact;
- (NSArray *) mysoscontacts;
-(BOOL) contactExists:(MyUnionContact *) contact;
//My Parking remainder Methods
-(BOOL) addRemainder:(ParkingTimer *) remainder;
- (ParkingTimer *) FetchRemainder;
- (BOOL) clearRemainder;

//Notice Board methods
- (BOOL) addNoticeBoardID:(NoticeBoard *) nb;
- (BOOL) removeNoticeboard:(NoticeBoard *) nb;
- (BOOL) nbExistsInCoredata:(NoticeBoard *) nb;
- (NSArray *) noticeBoardIds;
//GINNotice Board methods
- (BOOL) addGNoticeBoardID:(NoticeBoard *) nb;
- (BOOL) removeGNoticeboard:(NoticeBoard *) nb;
- (BOOL) gnbExistsInCoredata:(NoticeBoard *) nb;
- (NSArray *) gnoticeBoardIds;

- (BOOL) isIphone5;

- (NSArray *) myFavoriteProducts;

- (void) logoutUserSession;
- (void) loginSuccessfulWithUserdetails:(User *) userDetails password:(NSString *) pWord;

- (NSString *) fetchRecord;
- (void) addRecord:(NSString *)htmlString;
-(void)scanQRcode;
- (void) showhomeScreen ;

@end
