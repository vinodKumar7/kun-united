//
//  ContactCell.m
//  TWU2
//
//  Created by vairat on 09/04/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "ContactCell.h"

@implementation ContactCell

@synthesize removeContactbutton;
@synthesize nameLabel;
@synthesize numberlabel;
@synthesize msgImage;
@synthesize headerContainerView;
@synthesize noticeLabel;
@synthesize callButton;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
