//
//  MyUnionMainViewController.m
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "MyUnionMainViewController.h"
#import <QuartzCore/QuartzCore.h>


#import "Display.h"
#import "AppDelegate.h"


@interface MyUnionMainViewController ()
{
    ASIFormDataRequest *SearchRequest;
    MyUnionParser *DisplayXMLParser;
    
    AppDelegate *appDelegate;
    NSString *searchCategoryId;
    NSString *searchKeyword;
    NSString *searchLocation;
    NSString *serviceName;
    
    BOOL requestCancel;
}
- (void)getmyunionData ;
@end


@implementation MyUnionMainViewController
@synthesize myUnionDisplayView;
@synthesize myUnionActivityIndicator;
@synthesize productsList;
@synthesize containerView;
@synthesize background_ImageView;
@synthesize containerImageView;
@synthesize websiteButton;
@synthesize fbButton;
@synthesize tweetButton;
@synthesize webbgImageView;
@synthesize activityView;
//=========================DELEGATE METHODS=============================//

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{      // [self.background_ImageView setHidden:YES];
    [self loadViews];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
        
    //setting container corner curve shape
    [self.view addSubview:activityView];
    
        [self performSelector:@selector(removeActivityview) withObject:nil afterDelay:0.9];
   

 
    [super viewDidLoad];
    
   

}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
    myUnionDisplayView.layer.cornerRadius = 10.0;
    myUnionDisplayView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    myUnionDisplayView.layer.shadowOpacity = 1.0;
    [ myUnionDisplayView setClipsToBounds:YES];
     [self performSelector:@selector(getmyunionData) withObject:nil afterDelay:0.1];
    
//    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
//    if ([[ver objectAtIndex:0] intValue] >= 7)
//    {
//        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];        self.navigationController.navigationBar.translucent = NO;
//    }
//    else
//    {
//        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
//    }
//
  /*  if([self.navigationController.navigationBar respondsToSelector:@selector(barTintColor)])
    {
        // iOS7
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
        self.navigationController.navigationBar.translucent = NO;
    }
    else
    {
        // older
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
    }*/
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"My Dealership";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor whiteColor]];
    self.navigationItem.titleView = TitleLabel;
    if ([appDelegate isIphone5])
    {
        CGRect frames=self.websiteButton.frame;
        
        frames.origin.y=395;
        self.websiteButton.frame=frames;
        NSLog(@"WSB y org  %f",frames.origin.y);
        
        CGRect frame1=self.fbButton.frame;
        
        frame1.origin.y=428;
        self.fbButton.frame=frame1;
        NSLog(@"fbB y org  %f",frame1.origin.y);
        
        CGRect frame2=self.tweetButton.frame;
        
        frame2.origin.y=428;
        self.tweetButton.frame=frame2;
        NSLog(@"TWTB y org  %f",frame2.origin.y);
        
        CGRect mywebview=self.myUnionDisplayView.frame;
        self.myUnionDisplayView.frame=mywebview;
        
        CGRect bgframe=self.webbgImageView.frame;
        bgframe.size.height=mywebview.size.height-15;
        self.webbgImageView.frame=bgframe;

    }
    
    
}

-(void)back
{
    if([SearchRequest isExecuting])
    {
        requestCancel = YES;
        [SearchRequest cancel];
    }
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}

//==============================================================================================//

//===============DELEGATE METHODS TO LOCK ORIENTATION===========================================//
-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

- (BOOL) shouldAutorotate
{
    return NO;
}
//==========================================================//
//============= LOADS XIBS FOR IPHONE-4 AND 5 ==================//
- (IBAction)websiteButtonClicked:(id)sender
{
    switch ([sender tag])
    {
        case 0:
        {
            NSLog(@"Website button");
            NSURL *websiteUrl = [NSURL URLWithString:@"http://www.bayford.com.au/"];
            
            [[UIApplication sharedApplication] openURL:websiteUrl];
        }
            break;
        case 1:
        {
            NSLog(@"FB Button");
            NSURL *websiteUrl = [NSURL URLWithString:@"https://m.facebook.com/bayfordgroup"];
            
            [[UIApplication sharedApplication] openURL:websiteUrl];
        }

           
            break;
        case 2:
        {
            NSLog(@"tweet Button");
            NSURL *websiteUrl = [NSURL URLWithString:@"https://mobile.twitter.com/bayfordgroup"];
            
            [[UIApplication sharedApplication] openURL:websiteUrl];
       }

        default:
            break;
    }
    
}

-(void)loadViews
{
    
   
        
     if ([appDelegate isIphone5])
     {
            
            NSLog(@"====UIDeviceResolution_iPhoneRetina5====");
            
            CGRect frame =self.background_ImageView.frame;
            frame.size.height = 510;//self.view.frame.size.height;
            self.background_ImageView.frame = frame;
            
            
            CGRect containerframe = self.containerImageView.frame;
            containerframe.origin.y=0;
            containerframe.origin.x=0;
            containerframe.size.height = 457;
            self.containerImageView.frame = containerframe;
            
            NSLog(@"height of container image view %f",containerframe.size.height);
            CGRect webviewframe = self.myUnionDisplayView.frame;
            webviewframe.origin.y=0;
            webviewframe.origin.x=0;
            webviewframe.size.width=320;
            webviewframe.size.height = 435;
            self.myUnionDisplayView.frame= webviewframe;
            
            //setting Activity Indicator Frame
            CGRect Activityframe = self.myUnionActivityIndicator.frame;
            NSLog(@"X=%f",self.myUnionActivityIndicator.frame.origin.x);
            NSLog(@"Y=%f",self.myUnionActivityIndicator.frame.origin.y);
            Activityframe.origin.y+=40;
            self.myUnionActivityIndicator.frame = Activityframe;
         
         
         
         CGRect frames=self.websiteButton.frame;
         
         frames.origin.y=395;
         self.websiteButton.frame=frames;
         NSLog(@"WSB y org  %f",frames.origin.y);
         
         CGRect frame1=self.fbButton.frame;
         
         frame1.origin.y=428;
         self.fbButton.frame=frame1;
         NSLog(@"fbB y org  %f",frame1.origin.y);
         
         CGRect frame2=self.tweetButton.frame;
         
         frame2.origin.y=428;
         self.tweetButton.frame=frame2;
         NSLog(@"TWTB y org  %f",frame2.origin.y);

            
        }
     else
     {
         CGRect frames=self.websiteButton.frame;
         
         frames.origin.y=324;
         self.websiteButton.frame=frames;
         NSLog(@"WSB y org  %f",frames.origin.y);
         
         CGRect frame1=self.fbButton.frame;
         
         frame1.origin.y=354;
         self.fbButton.frame=frame1;
         NSLog(@"fbB y org  %f",frame1.origin.y);
         
         CGRect frame2=self.tweetButton.frame;
         
         frame2.origin.y=354;
         self.tweetButton.frame=frame2;
         NSLog(@"TWTB y org  %f",frame2.origin.y);
     
     
     
     
     }
        
        
            
   

}
-(void)showActivityView
{

    [self.view addSubview:activityView];
}
//==============================================================//
-(BOOL)webView:(UIWebView *)descriptionTextView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (UIWebViewNavigationTypeLinkClicked == navigationType) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}
//==================== SENDS REQUEST TO DATABASE=========================//

- (void)getmyunionData
{
    
    NSString *urlString;
    //==============================================
    urlString = @"http://www.myrewards.com.au/newapp/get_page.php?cid=92&pid=1";
    //===============================================
    NSLog(@"URL: %@",urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    SearchRequest = [ASIFormDataRequest requestWithURL:url];
    [SearchRequest setDelegate:self];
    [SearchRequest startAsynchronous];
    
    [myUnionActivityIndicator startAnimating];
   
}
//=====================================================================//


//======== ASIHTTP REQUEST DELEGATE METHODS ================//

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSLog(@"DetailView -- requestFinished:");
    
       if (request == SearchRequest)
    { 
        
        SearchRequest = nil;
        NSLog(@"============>Response is ::: %@",[request responseString]);
       // NSString *responseString = [request responseString];
       // NSLog(@"RESPONSE STRING IS:: %@",responseString);
        
        //[appDelegate addRecord:responseString];
        
        NSXMLParser *myParser = [[NSXMLParser alloc] initWithData:[request responseData]];
       
        DisplayXMLParser = [[MyUnionParser alloc] init];
        DisplayXMLParser.delegate = self;
        myParser.delegate = DisplayXMLParser;
        [myParser parse];
  
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    NSLog(@"DetailView -- requestFailed:");
    if (requestCancel == YES) {
        requestCancel = NO;
    }
    else
    {
        UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [failureAlert show];
    }
    
 
    
    [self.myUnionActivityIndicator stopAnimating];
}
-(void)removeActivityview
{
    [activityView removeFromSuperview];
}
//====================================================//
//=========== USER DATA XML PARSER DELEGATE MATHODS =============//
#pragma mark -- UserDataXMLParser Delegate methods

- (void) parsingMyUnionDetailsFinished:(Display *) myUnionDisplay
{
    
    NSMutableString *html;
    html = [NSMutableString stringWithString:myUnionDisplay.displaymsg];
    
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"saveToCoreData"])
    {
        NSLog(@"===== HTMLPage NOT Available====");
        [appDelegate addRecord:html];
        [[NSUserDefaults standardUserDefaults] setValue:@"HTMLPageAvailable" forKey:@"saveToCoreData"];
    }
  
    
       
    NSString *myDescriptionHTML=[NSString stringWithFormat:@"<html> \n"
                                 "<head> \n"
                                 "<style type=\"text/css\"> \n"
                                 "body {font-family: \"%@\"; font-size: %@;line-height:1.5%;}\n"
                                 "</style> \n"
                                 "</head> \n"
                                 "</body>%@</body>\n"
                                 "</html>", @"Helvetica", [NSNumber numberWithInt:13], html];
    
    
    [self.myUnionDisplayView setBackgroundColor:[UIColor clearColor]];
    // self.displayView.scalesPageToFit = YES;
    [self.myUnionDisplayView loadHTMLString:myDescriptionHTML  baseURL:nil];
    [self.myUnionActivityIndicator stopAnimating];
    
    
}
- (void) MyUnionDetailsXMLparsingFailed
{
    
}

//==================================================================================//








- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setContainerImageView:nil];
    [self setWebsiteButton:nil];
    [self setFbButton:nil];
    [self setTweetButton:nil];
    [super viewDidUnload];
}
@end
