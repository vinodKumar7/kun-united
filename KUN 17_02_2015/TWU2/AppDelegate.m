//
//  AppDelegate.m
//  TWU2
//
//  Created by MyRewards on 2/15/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//
#import <Crashlytics/Crashlytics.h>
#import "AppDelegate.h"
#import <CoreData/CoreData.h>
#import <Parse/Parse.h>
#import "NoticeBoard.h"
#import "LoginViewController.h"
#import "MenuViewController.h"
#import "ParkingTimer.h"
#import <AudioToolbox/AudioServices.h>
#define Login_Token_Delimiter @""
#define Login_Token_Expiry_period (7*24*60*60)//1*60
#define Login_Token_Username_Key @"Login_Token_Username_Key"
#define Login_Token_Password_Key @"Login_Token_Password_Key"
#define Login_Token_Timestamp_Key @"Login_Token_Timestamp_Key"
#define User_object_Key @"User_object"

@interface AppDelegate()
{
    BOOL image;
    ParkingTimer *prk;
    int num;
}
- (User *) retrieveUserDetailsFromUserDefaults;
@end

@implementation AppDelegate

@synthesize userDetailsArray;
@synthesize sessionUser;
@synthesize temp;
//@synthesize registrationviewController;
@synthesize viewController;
@synthesize menuViewController;
@synthesize managedObjectContext;
@synthesize managedObjectModel;
@synthesize persistentStoreCoordinator;
@synthesize docPath;

@synthesize path;
@synthesize refArray;
@synthesize remndr;
@synthesize navController;
@synthesize x;
@synthesize noticeBoardCount;
@synthesize badgecount;
@synthesize isFromMenuPresentMyUnionrewardsController,reader;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [Crashlytics startWithAPIKey:@"7632893f759fed91f0ce8e726134fc9fbc72c5cf"];
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] ;
   
    remndr=0;
    badgecount=0;
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 7)
        [application setStatusBarStyle:UIStatusBarStyleLightContent];

    image=NO;
    self.path=0;
    refArray=[[NSMutableArray alloc]initWithCapacity:1];
    //=========== pushnotification code
    [Parse setApplicationId:@"gYXjMhtVy9eg2tumffBJUtb68ddR75y0EeT6Wguc"
                  clientKey:@"C09BQ8pgGePZDenY2pRPYafxAyQjjy0lk0e7eMyu"];
    [PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
    
    
    // Register for push notifications
    [application registerForRemoteNotificationTypes:
     UIRemoteNotificationTypeBadge |
     UIRemoteNotificationTypeAlert |
     UIRemoteNotificationTypeSound];
    
    
    //=============================
    
    
    self.userDetailsArray = [[NSMutableArray alloc]init];
    
    
    /* // Override point for customization after application launch.
     self.registrationviewController = [[[RegistrationViewController alloc] initWithNibName:@"RegistrationViewController" bundle:nil] autorelease];
     UINavigationController *navController = [[UINavigationController alloc]initWithRootViewController:self.registrationviewController];
     //self.window.rootViewController = navController.view;
     [self.window addSubview:navController.view];
     [self.window makeKeyAndVisible];
     return YES;*/
    
    // NSLog(@"window heght::%f",self.window.frame.size.height);
    
    if (![self isFirstTimeLogin])
    {
        
        // Fetch user details
        self.sessionUser = [self retrieveUserDetailsFromUserDefaults];
        
        // check for token validity.
        if ([self isLoginTokenValid])
        {
            [self showhomeScreen];
        }
        else
        {
            // If token expired, renew automatically.(i.e login user again automatically using users credentials previously stored)
            [self renewTheLoginToken];
        }
    }
    else
    {
        // Login user
        //[self showRegistrationScreen];
        [self renewTheLoginToken];
    }
    
    return YES;
    application.applicationIconBadgeNumber = 0;
    UILocalNotification *notification = [[UILocalNotification alloc]init];
    notification.applicationIconBadgeNumber=noticeBoardCount;
    NSLog(@"Noticeboard count is %d ",noticeBoardCount)
    ;
}

//====================================   LOCAL NOTIFICAIONS   =======================//
- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)notification
{
    
    
    NSLog(@"didReceiveLocalNotification");
    application.applicationIconBadgeNumber = 0;
    
    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Back To My Car" message:@"Parking is about to expire!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    NSError *error;
    NSURL *url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@/old_phone.m4r", [[NSBundle mainBundle] resourcePath]]];
    player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:&error];
    player.numberOfLoops = -1;
    [player play];
    
    [alert show];
    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    [self clearRemainder];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ParkingRemainderCount"];
    if (application.applicationIconBadgeNumber == 0) {
        [self clearRemainder];
        NSLog(@"clear....");
    }
    
}


- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    if (buttonIndex==0) {
        [player stop];
        
    }
    
}

//============================================================================================//
-(void)startCountDownTimerWithTime:(int)time andUILabel:(UILabel *)currentLabel
{
    num=time;
    countDownTime = time;
    actualTime = time;
    label = currentLabel;
    [self StartCountDownTimer];
}


-(void)invalidateCurrentCountDownTimer
{
    [self InvalidateCountDownTimer];
}

#pragma mark -
#pragma mark count Down Timer
-(void)InvalidateCountDownTimer
{
    NSLog(@"InvalidateCountDownTimer");
    countDownTime =actualTime;
    if (CountDownTimer!=nil)
    {
        if ([CountDownTimer isValid])
        {
            [CountDownTimer invalidate];
            
        }
        CountDownTimer=nil;
    }
}

-(void)StartCountDownTimer
{
    [self InvalidateCountDownTimer];
    CountDownTimer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(DecrementCounterValue) userInfo:nil repeats:YES];
    
    label.text=[NSString stringWithFormat:@"%02d:%02d:%02d", actualTime/3600, (actualTime % 3600) / 60, (actualTime %3600) % 60];
}

-(void)DecrementCounterValue
{
    
    NSLog(@"countDownTime %d",countDownTime);
    if (countDownTime>0)
	{
        countDownTime--;
        x=countDownTime;
        
        label.text = [NSString stringWithFormat:@"%02d:%02d:%02d", countDownTime/3600, (countDownTime % 3600) / 60, (countDownTime %3600) % 60];
        
        NSLog(@"%@",label.text);
    }
    else
	{
        [self InvalidateCountDownTimer];
        label.text = @"";
    }
    
    
    
    
}

//=============================================================
//============METHOD TO JUSFITYING IPHONE 5 OR NOT ===============//

- (BOOL) isIphone5
{
    if([ [ UIScreen mainScreen ] bounds ].size.height == 568)
        
        return YES;
    
    else
        return  NO;
}

//===================================================================================//

//============METHOD TO VERIFYING FIRST TIME LOGIN ===============//
- (BOOL) isFirstTimeLogin
{
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:Login_Token_Timestamp_Key])
    {
        return NO;
    }
    
    return YES;
}
//============================================================================================//
//============METHOD TO VERIFYING USER DETAILS IN DATABASE ===============//

- (BOOL) isLoginTokenValid {
    
    NSString *timeStampString = [[NSUserDefaults standardUserDefaults] objectForKey:Login_Token_Timestamp_Key];
    
    if(timeStampString)
    {
        
        NSDateFormatter *dateForm = [[NSDateFormatter alloc] init];
        [dateForm setDateFormat:@"yyyy-MM-dd HH:mm:ss Z"];
        NSDate *prevLoginDate = [dateForm dateFromString:timeStampString];
        
        
        
        NSTimeInterval elapsedTime = [prevLoginDate timeIntervalSinceNow];
        elapsedTime = (elapsedTime < 0) ? -elapsedTime : elapsedTime;
        
        if (elapsedTime < Login_Token_Expiry_period)
        {
            return YES;
        }
    }
    
    return NO;
}
//============================================================================================//
//============METHOD TO STORE USER DETAILS ===============//

- (void) loginSuccessfulWithUserdetails:(User *) userDetails password:(NSString *) pWord
{
    
    self.sessionUser = userDetails;
    
    NSDate *loginDate = [NSDate date];
    NSString *dateString = [loginDate description];
    
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.username forKey:Login_Token_Username_Key];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.password forKey:Login_Token_Password_Key];
    [[NSUserDefaults standardUserDefaults] setValue:dateString forKey:Login_Token_Timestamp_Key];
    
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.userId forKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_id forKey:@"client_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.domain_id forKey:@"domain_id"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.type forKey:@"type"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.email forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.first_name forKey:@"first_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.last_name forKey:@"last_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.state forKey:@"state"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.country forKey:@"country"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.mobile forKey:@"mobile"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.card_ext forKey:@"card_ext"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.client_name forKey:@"client_name"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.newsletter forKey:@"newsletter"];
    [[NSUserDefaults standardUserDefaults] setValue:userDetails.clientDomainName forKey:@"clientDomainName"];
    
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    NSLog(@"password in loginsuccesfuldtls ::%@",sessionUser.password);
    
    NSLog(@"====>>>>>%@ %@",userDetails.state,userDetails.country);
    
    [self showhomeScreen];
}
//============================================================================================//
//============METHOD TO REMOVE USER DETAILS ===============//

- (void) logoutUserSession
{ NSString *countDownTimeStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"parkingTime"];
    NSLog(@"countDownTimeStr... %@",countDownTimeStr);
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Login_Token_Timestamp_Key];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Login_Token_Username_Key];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Login_Token_Password_Key];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:Login_Token_Timestamp_Key];
    
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"userId"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"client_id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"domain_id"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"type"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"email"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"first_name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"last_name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"state"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"country"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"mobile"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"card_ext"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"client_name"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"newsletter"];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"clientDomainName"];
    
   // [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self renewTheLoginToken];
    
}
//============================================================================================//

//============METHOD TO LOADS LOGINVIEW WHILE RELOGIN ===============//
- (void) renewTheLoginToken
{
    
    
    self.viewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    
    
    [self.viewController setProcessRenwalOfToken:YES];
    
    self.window.rootViewController = self.viewController;
    [self.window makeKeyAndVisible];
    
}
//============================================================================================//


//============METHOD TO LOADS MENU VIEW ===============//
- (void) showhomeScreen {
    
    
    NSLog(@"Timer started");
    [self startCountDownTimerWithTime:actualTime andUILabel:label];
    NSLog(@"actualTime TimeStr... %d",actualTime);
    
    self.menuViewController = [[MenuViewController alloc] initWithNibName:@"MenuViewController" bundle:nil];
    
    self.navController = [[UINavigationController alloc]initWithRootViewController:self.menuViewController];
    
    //[self.window addSubview:self.navController.view];
     self.window.rootViewController =self.navController;
    [self.window makeKeyAndVisible];
    
    NSLog(@"password in loginsuccesfuldtls ::%@",sessionUser.password);
}

- (NSString *) sessionUsername
{
    NSString *uName = [[NSUserDefaults standardUserDefaults] objectForKey:Login_Token_Username_Key];
    return uName;
    NSLog(@"sessionUser details ");
}

- (NSString *) sessionPassword
{
    NSString *pWord = [[NSUserDefaults standardUserDefaults] objectForKey:Login_Token_Password_Key];
    return pWord;
    // NSLog(@" password::%@ ",pWord);
}

- (NSString *) clientDomainName
{
    if (sessionUser)
    {
        return sessionUser.clientDomainName;
    }
    return nil;
}


- (User *) retrieveUserDetailsFromUserDefaults
{
    User *userdetails = [[User alloc] init];
    
    userdetails.username = [[NSUserDefaults standardUserDefaults] objectForKey:Login_Token_Username_Key];
    userdetails.password= [[NSUserDefaults standardUserDefaults] objectForKey:@"pWord"];
    
    userdetails.userId = [[NSUserDefaults standardUserDefaults] objectForKey:@"userId"];
    userdetails.client_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"client_id"];
    userdetails.domain_id = [[NSUserDefaults standardUserDefaults] objectForKey:@"domain_id"];
    userdetails.type = [[NSUserDefaults standardUserDefaults] objectForKey:@"type"];
    userdetails.email = [[NSUserDefaults standardUserDefaults] objectForKey:@"email"];
    userdetails.first_name = [[NSUserDefaults standardUserDefaults] objectForKey:@"first_name"];
    userdetails.last_name = [[NSUserDefaults standardUserDefaults] objectForKey:@"last_name"];
    userdetails.state = [[NSUserDefaults standardUserDefaults] objectForKey:@"state"];
    userdetails.country = [[NSUserDefaults standardUserDefaults] objectForKey:@"country"];
    userdetails.mobile = [[NSUserDefaults standardUserDefaults] objectForKey:@"mobile"];
    userdetails.card_ext = [[NSUserDefaults standardUserDefaults] objectForKey:@"card_ext"];
    userdetails.client_name = [[NSUserDefaults standardUserDefaults] objectForKey:@"client_name"];
    userdetails.newsletter = [[NSUserDefaults standardUserDefaults] objectForKey:@"newsletter"];
    userdetails.clientDomainName = [[NSUserDefaults standardUserDefaults] objectForKey:@"clientDomainName"];
    //userdetails.password=[[NSUserDefaults standardUserDefaults] objectForKey:@"pWord"];
    NSLog(@"retrieve User details ");
    NSLog(@"password %@",sessionUser.password);
    return userdetails;
    
}

//=================================================================================================//
//************    QRCode METHODS   *************//

-(void)scanQRcode
{
    // ADD: present a barcode reader that scans from the camera feed
    reader = [ZBarReaderViewController new];
    reader.readerDelegate = self;
    reader.view.tag=182;
    reader.supportedOrientationsMask = ZBarOrientationMaskAll;
    
    ZBarImageScanner *scanner = reader.scanner;
    // TODO: (optional) additional reader configuration here
    
    // EXAMPLE: disable rarely used I2/5 to improve performance
    [scanner setSymbology: ZBAR_I25
                   config: ZBAR_CFG_ENABLE
                       to: 0];
    UIView *_containerView = [[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]];
    UIImageView *overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlaygraphic.png"]];
    [overlayImageView setFrame:CGRectMake(30, 100, 260, 200)];
    reader.view.frame=_containerView.bounds;
    [reader.view addSubview:overlayImageView];
    UIActivityIndicatorView *activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
    activityIndicator.alpha = 1.0;
    activityIndicator.center = CGPointMake(160, 200);
    activityIndicator.hidesWhenStopped = NO;
    [reader.view addSubview:activityIndicator];
    [activityIndicator startAnimating];
    
}


//=================================================================================================//
//************    CORE DATA METHODS   *************//


- (void) addRecord:(NSString *)htmlString
{
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *hdata = [NSEntityDescription insertNewObjectForEntityForName:@"MyUnion" inManagedObjectContext:context];
    
    [hdata setValue:htmlString forKey:@"htmlContent"];
    
    NSError *error;
    if(![context save:&error])
    {
        //This is a serious error saying the record
        //could not be saved. Advise the user to
        
        NSLog(@"=====RECORD NOT SAVE INTO COREDATA=========");
    }
    else
    {
        NSLog(@"====record saved===");
    }
    
    
}
- (NSString *) fetchRecord
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *content = [NSEntityDescription entityForName:@"MyUnion" inManagedObjectContext:context];
    [fetchRequest setEntity:content];
    
    
    NSError *error;
    NSArray *fetchedRecord = [context executeFetchRequest:fetchRequest error:&error];
    
    NSLog(@"======= FETCHED COUNT:::::%u",[fetchedRecord count]);
    NSManagedObject *record = [fetchedRecord objectAtIndex:0];
    NSLog(@"FETCHED DATA:::%@",[record valueForKey:@"htmlContent"]);
    return [record valueForKey:@"htmlContent"];
}

//=====================================REMAINDER CORE DATA METHODS =================================================//


-(BOOL) addRemainder:(ParkingTimer *) remainder
{
    
    
    //  NSLog(@"addContact: %@",contact.name);
    // NSLog(@"addContact: %@",contact.number);
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *newRemainder = [NSEntityDescription insertNewObjectForEntityForName:@"ParkingTimer" inManagedObjectContext:context];
    [newRemainder setValue:remainder.location forKey:@"location"];
    [newRemainder setValue:remainder.parkedLocation forKey:@"parkedLocation"];
    [newRemainder setValue:remainder.parkedTime forKey:@"parkedTime"];
    [newRemainder setValue:remainder.latitude forKey:@"latitude"];
    [newRemainder setValue:remainder.longitude forKey:@"longitude"];
    
    remndr++;
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding remainder failed %@", [error localizedDescription]);
        return NO;
        remndr--;
    }
    
    return YES;
    
}

- (BOOL) clearRemainder
{
    
    NSLog(@"clearRemainder");
    NSManagedObjectContext *context = [self managedObjectContext];
    remndr--;
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *delRemainder = [NSEntityDescription entityForName:@"ParkingTimer" inManagedObjectContext:context];
    [fetchRequest setEntity:delRemainder];
    // NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@ && number==%@",contact.name,contact.number];
    // [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        NSManagedObject *cont = [fetchedObjects objectAtIndex:0];
        
        [context deleteObject:cont];
        NSLog(@"Remainder Object removed");
        
        NSError *error;
        if (![context save:&error])
        {
            NSLog(@"Removing remainder failed %@", [error localizedDescription]);
            
            // Removing object failed.
            return NO;
        }
        
        // Success
        return YES;
    }
    
    // There is no remainder as such specified by input param in parkingTimer.
    
    // There is no remainder as such specified by input param in parkingTimer.
    return NO;
}


- (ParkingTimer *) FetchRemainder
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *contact = [NSEntityDescription entityForName:@"ParkingTimer" inManagedObjectContext:context];
    [fetchRequest setEntity:contact];
    
    
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *remaindersArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        
        
        ParkingTimer *currRemainder = [[ParkingTimer alloc] init];
        currRemainder.location = [[info valueForKey:@"location"] copy];
        currRemainder.parkedLocation = [[info valueForKey:@"parkedLocation"] copy];
        currRemainder.parkedTime = [[info valueForKey:@"parkedTime"] copy];
        currRemainder.latitude = [[info valueForKey:@"latitude"] copy];
        currRemainder.longitude = [[info valueForKey:@"longitude"] copy];
        
        [remaindersArr addObject:currRemainder];
    }
    if([remaindersArr count] > 0)
    {
        return [remaindersArr objectAtIndex:0];
        
    }
    else
        return NULL;
}


//============================  MYUNION CONTACTS COREDATA METHODS  =====================//

-(BOOL) addContact:(MyUnionContact *) contact
{
    
    
    NSLog(@"addContact: %@",contact.name);
    NSLog(@"addContact: %@",contact.number);
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *newContact = [NSEntityDescription insertNewObjectForEntityForName:@"MyUnionContact" inManagedObjectContext:context];
    
    [newContact setValue:contact.name forKey:@"name"];
    [newContact setValue:contact.number forKey:@"number"];
    [newContact setValue:contact.carRegistration forKey:@"carRegistration"];
    [newContact setValue:contact.carModel forKey:@"carModel"];
    [newContact setValue:contact.carYear forKey:@"carYear"];
    
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding contact failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
    
}

- (BOOL) removeContact:(MyUnionContact *) contact
{
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Fetch the said NSManagedObject(favoriteProduct)
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *delContact = [NSEntityDescription entityForName:@"MyUnionContact" inManagedObjectContext:context];
    [fetchRequest setEntity:delContact];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name == %@ && number==%@",contact.name,contact.number];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        NSManagedObject *cont = [fetchedObjects objectAtIndex:0];
        
        [context deleteObject:cont];
        
        NSError *error;
        if (![context save:&error])
        {
            NSLog(@"Removing contact failed %@", [error localizedDescription]);
            
            // Removing object failed.
            return NO;
        }
        
        // Success
        return YES;
    }
    
    // There is no contact as such specified by input param in Mycontacts.
    
    // There is no contact as such specified by input param in Mycontacts.
    return NO;
}



- (NSArray *) mysoscontacts
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *contact = [NSEntityDescription entityForName:@"MyUnionContact" inManagedObjectContext:context];
    [fetchRequest setEntity:contact];
    
    //NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId==%@",self.sessionUser.userId];
    //[fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *contactsArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        NSLog(@"Contact Name: %@", [info valueForKey:@"name"]);
        NSLog(@"Contact Number: %@", [info valueForKey:@"number"]);
        
        MyUnionContact *currContact = [[MyUnionContact alloc] init];
        currContact.name = [[info valueForKey:@"name"] copy];
        currContact.number = [[info valueForKey:@"number"] copy];
        currContact.carRegistration=[[info valueForKey:@"carRegistration"]copy];
        currContact.carModel=[[info valueForKey:@"carModel"]copy];
        currContact.carYear=[[info valueForKey:@"carYear"]copy];
        [contactsArr addObject:currContact];
    }
    
    return contactsArr;
}

-(BOOL) contactExists:(MyUnionContact *) contact
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *contact1 = [NSEntityDescription entityForName:@"MyUnionContact" inManagedObjectContext:context];
    [fetchRequest setEntity:contact1];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name==%@",contact.name];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        return YES;
    }
    return NO;
}

//============================


#pragma mark -- Favorite methods
- (BOOL) addProductToFavorites:(Product *) product
{
    
    if ([self productExistsInFavorites:product])
    {
        // Product already exists. return.
        return NO;
    }
    
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *favoriteProduct = [NSEntityDescription insertNewObjectForEntityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [favoriteProduct setValue:product.productId forKey:@"productId"];
    [favoriteProduct setValue:product.productName forKey:@"productName"];
    [favoriteProduct setValue:product.productOffer forKey:@"productHighlight"];
    [favoriteProduct setValue:self.sessionUser.userId forKey:@"userId"];
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding product failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}

- (BOOL) removeProductFromfavorites:(Product *) product
{
    
    if ([self productExistsInFavorites:product])
    {
        NSManagedObjectContext *context = [self managedObjectContext];
        
        // Fetch the said NSManagedObject(favoriteProduct)
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
        [fetchRequest setEntity:favProduct];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productId == %@ && userId==%@",product.productId,self.sessionUser.userId];
        [fetchRequest setPredicate:predicate];
        NSError *error;
        NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
        
        if (fetchedObjects.count > 0)
        {
            NSManagedObject *favoriteProduct = [fetchedObjects objectAtIndex:0];
            
            [context deleteObject:favoriteProduct];
            
            NSError *error;
            if (![context save:&error])
            {
                NSLog(@"Removing product failed %@", [error localizedDescription]);
                
                // Removing object failed.
                return NO;
            }
            
            // Success
            return YES;
        }
        
        // There is no Product as such specified by input param in favorites.
    }
    
    // There is no Product as such specified by input param in favorites.
    return NO;
}



- (BOOL) productExistsInFavorites:(Product *) product
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"productId==%@ && userId==%@",product.productId,self.sessionUser.userId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        return YES;
    }
    
    return NO;
}

- (NSArray *) myFavoriteProducts
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"FavoriteProduct" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"userId==%@",self.sessionUser.userId];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *productsArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        NSLog(@"Product Name: %@", [info valueForKey:@"productName"]);
        NSLog(@"Product Id: %@", [info valueForKey:@"productId"]);
        
        Product *pr = [[Product alloc] init];
        pr.productId = [[info valueForKey:@"productId"] copy];
        pr.productName = [[info valueForKey:@"productName"] copy];
        pr.productOffer = [[info valueForKey:@"productHighlight"] copy];
        [productsArr addObject:pr];
    }
    
    return productsArr;
}



//=================== NOTICEBOARD COREDATA METHODS =======================//

- (BOOL) addNoticeBoardID:(NoticeBoard *) nb
{
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *obj = [NSEntityDescription insertNewObjectForEntityForName:@"NoticeBoard" inManagedObjectContext:context];
    [obj setValue:nb.notice_Id forKey:@"noticeId"];
    [obj setValue:[NSString stringWithFormat:@"%d",nb.index] forKey:@"noticeIndex"];
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding NoticeBoard_ID failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
}
- (BOOL) addGNoticeBoardID:(NoticeBoard *) nb
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSManagedObject *obj = [NSEntityDescription insertNewObjectForEntityForName:@"GNoticeBoard" inManagedObjectContext:context];
    
    [obj setValue:nb.notice_Id forKey:@"gnoticeId"];
    [obj setValue:[NSString stringWithFormat:@"%d",nb.index] forKey:@"gnoticeIndex"];
    
    NSError *error;
    if (![context save:&error])
    {
        NSLog(@"Adding NoticeBoard_ID failed %@", [error localizedDescription]);
        return NO;
    }
    
    return YES;
    
    
}



- (NSArray *) noticeBoardIds
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"NoticeBoard" inManagedObjectContext:context];
    [fetchRequest setEntity:entityDescription];
    
    //  NSPredicate *predicate = [NSPredicate predicateWithFormat:@"Id==%@",self.sessionUser.userId];
    //  [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *noticeBoardArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    
    for (NSManagedObject *info in fetchedObjects)
    {
        
        //NSLog(@"NoticeBoard Id: %@", [info valueForKey:@"noticeId"]);
        
        NoticeBoard *nb = [[NoticeBoard alloc] init];
        nb.notice_Id = [[info valueForKey:@"noticeId"] copy];
        NSString *str = [[info valueForKey:@"noticeIndex"] copy];
        nb.index = [str intValue];
        
        [noticeBoardArr addObject:nb];
    }
    
    return noticeBoardArr;
}
- (NSArray *) gnoticeBoardIds
{
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entityDescription = [NSEntityDescription entityForName:@"GNoticeBoard" inManagedObjectContext:context];
    [fetchRequest setEntity:entityDescription];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    NSMutableArray *noticeBoardArr = [[NSMutableArray alloc] initWithCapacity:[fetchedObjects count]];
    for (NSManagedObject *info in fetchedObjects)
    {
        
        //NSLog(@"NoticeBoard Id: %@", [info valueForKey:@"noticeId"]);
        
        NoticeBoard *nb = [[NoticeBoard alloc] init];
        nb.notice_Id = [[info valueForKey:@"gnoticeId"] copy];
        NSString *str = [[info valueForKey:@"gnoticeIndex"] copy];
        nb.index = [str intValue];
        
        [noticeBoardArr addObject:nb];
    }
    
    return noticeBoardArr;
}


- (BOOL) removeNoticeboard:(NoticeBoard *) nb
{
    
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Fetch the said NSManagedObject(favoriteProduct)
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"NoticeBoard" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"noticeId == %@",nb.notice_Id];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        NSManagedObject *favoriteProduct = [fetchedObjects objectAtIndex:0];
        
        [context deleteObject:favoriteProduct];
        
        NSError *error;
        if (![context save:&error])
        {
            NSLog(@"Removing NB Id failed %@", [error localizedDescription]);
            
            // Removing object failed.
            return NO;
        }
        
        // Success
        return YES;
    }
    
    
    
    // There is no ids as such specified by input param in NB.
    return NO;
}
- (BOOL) removeGNoticeboard:(NoticeBoard *) nb
{
    NSManagedObjectContext *context = [self managedObjectContext];
    
    // Fetch the said NSManagedObject(favoriteProduct)
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *favProduct = [NSEntityDescription entityForName:@"GNoticeBoard" inManagedObjectContext:context];
    [fetchRequest setEntity:favProduct];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"gnoticeId == %@",nb.notice_Id];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    
    if (fetchedObjects.count > 0)
    {
        NSManagedObject *favoriteProduct = [fetchedObjects objectAtIndex:0];
        
        [context deleteObject:favoriteProduct];
        
        NSError *error;
        if (![context save:&error])
        {
            NSLog(@"Removing GNB Id failed %@", [error localizedDescription]);
            
            // Removing object failed.
            return NO;
        }
        
        // Success
        return YES;
    }
    
    
    
    // There is no ids as such specified by input param in NB.
    return NO;
    
    
    
    
    
}

- (BOOL) nbExistsInCoredata:(NoticeBoard *) nb
{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *obj = [NSEntityDescription entityForName:@"NoticeBoard" inManagedObjectContext:context];
    [fetchRequest setEntity:obj];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"noticeId==%@",nb.notice_Id];
    [fetchRequest setPredicate:predicate];
    
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        return YES;
    }
    
    return NO;
}
- (BOOL) gnbExistsInCoredata:(NoticeBoard *) nb{
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *obj = [NSEntityDescription entityForName:@"GNoticeBoard" inManagedObjectContext:context];
    [fetchRequest setEntity:obj];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"gnoticeId==%@",nb.notice_Id];
    [fetchRequest setPredicate:predicate];
    NSError *error;
    NSArray *fetchedObjects = [context executeFetchRequest:fetchRequest error:&error];
    
    if (fetchedObjects.count > 0)
    {
        return YES;
    }
    
    return NO;
    
    
}



//====================================================================================================//

- (NSManagedObjectContext *)managedObjectContext
{
    if (managedObjectContext != nil)
    {
        return managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil)
    {
        managedObjectContext = [[NSManagedObjectContext alloc] init];
        [managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (managedObjectModel != nil)
    {
        return managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"HtmlContent" withExtension:@"momd"];
    managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return managedObjectModel;
}

/*
 - (NSManagedObjectModel *)managedObjectModel {
 if (managedObjectModel != nil) {
 return managedObjectModel;
 }
 managedObjectModel = [NSManagedObjectModel mergedModelFromBundles:nil];
 return managedObjectModel;
 }
 */

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (persistentStoreCoordinator != nil)
    {
        return persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"HtmlContent.sqlite"];
    
    NSError *error = nil;
    persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error])
    {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
    
    return persistentStoreCoordinator;
}
//==============================================

//========================== PUSH NOTIFICATIONS METHODS==========================//

- (void)application:(UIApplication *)application
didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)newDeviceToken
{
    // Store the deviceToken in the current installation and save it to Parse.
    // Store the deviceToken in the current installation and save it to Parse.
 //   NSArray *channelsArray = [[NSArray alloc]initWithObjects:@"KunChannel", nil];
      PFInstallation *currentInstallation = [PFInstallation currentInstallation];
     [currentInstallation setDeviceTokenFromData:newDeviceToken];
     [currentInstallation saveInBackground];
       [currentInstallation addUniqueObject:@"KunChannel" forKey:@"channels"];
     [PFPush subscribeToChannelInBackground:@"KunChannel"];
     NSString *myString = [[NSString alloc] initWithData:newDeviceToken encoding:NSUTF8StringEncoding];
     NSLog(@"DEVICE TOKEN:::%@",myString );
    
/*#if !TARGET_IPHONE_SIMULATOR
    
	// Get Bundle Info for Remote Registration (handy if you have more than one app)
	NSString *appName = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleDisplayName"];
	NSString *appVersion = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleVersion"];
    
	// Check what Notifications the user has turned on.  We registered for all three, but they may have manually disabled some or all of them.
	NSUInteger rntypes = [[UIApplication sharedApplication] enabledRemoteNotificationTypes];
    
	// Set the defaults to disabled unless we find otherwise...
	NSString *pushBadge = @"disabled";
	NSString *pushAlert = @"disabled";
	NSString *pushSound = @"disabled";
    
	// Check what Registered Types are turned on. This is a bit tricky since if two are enabled, and one is off, it will return a number 2... not telling you which
	// one is actually disabled. So we are literally checking to see if rnTypes matches what is turned on, instead of by number. The "tricky" part is that the
	// single notification types will only match if they are the ONLY one enabled.  Likewise, when we are checking for a pair of notifications, it will only be
	// true if those two notifications are on.  This is why the code is written this way
	if(rntypes == UIRemoteNotificationTypeBadge){
		pushBadge = @"enabled";
	}
	else if(rntypes == UIRemoteNotificationTypeAlert){
		pushAlert = @"enabled";
	}
	else if(rntypes == UIRemoteNotificationTypeSound){
		pushSound = @"enabled";
	}
	else if(rntypes == ( UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert)){
		pushBadge = @"enabled";
		pushAlert = @"enabled";
	}
	else if(rntypes == ( UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound)){
		pushBadge = @"enabled";
		pushSound = @"enabled";
	}
	else if(rntypes == ( UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)){
		pushAlert = @"enabled";
		pushSound = @"enabled";
	}
	else if(rntypes == ( UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound)){
		pushBadge = @"enabled";
		pushAlert = @"enabled";
		pushSound = @"enabled";
	}
    
	// Get the users Device Model, Display Name, Unique ID, Token & Version Number
	UIDevice *dev = [UIDevice currentDevice];
	NSString *deviceUuid = [[[UIDevice currentDevice] identifierForVendor] UUIDString];;
    NSString *deviceName = dev.name;
	NSString *deviceModel = dev.model;
	NSString *deviceSystemVersion = dev.systemVersion;
    
	// Prepare the Device Token for Registration (remove spaces and < >)
	NSString *deviceToken = [[[[newDeviceToken description]
                               stringByReplacingOccurrencesOfString:@"<"withString:@""]
                              stringByReplacingOccurrencesOfString:@">" withString:@""]
                             stringByReplacingOccurrencesOfString: @" " withString: @""];
    
	// Build URL String for Registration
	// !!! CHANGE "www.mywebsite.com" TO YOUR WEBSITE. Leave out the http://
	// !!! SAMPLE: "secure.awesomeapp.com"
	NSString *host = @"184.107.135.82:8080";
    
	// !!! CHANGE "/apns.php?" TO THE PATH TO WHERE apns.php IS INSTALLED
	// !!! ( MUST START WITH / AND END WITH ? ).
	// !!! SAMPLE: "/path/to/apns.php?"
	NSString *urlString = [@"/apns.php?"stringByAppendingString:@"task=register"];
    
	urlString = [urlString stringByAppendingString:@"&appname="];
	urlString = [urlString stringByAppendingString:appName];
	urlString = [urlString stringByAppendingString:@"&appversion="];
	urlString = [urlString stringByAppendingString:appVersion];
	urlString = [urlString stringByAppendingString:@"&deviceuid="];
	urlString = [urlString stringByAppendingString:deviceUuid];
	urlString = [urlString stringByAppendingString:@"&devicetoken="];
	urlString = [urlString stringByAppendingString:deviceToken];
	urlString = [urlString stringByAppendingString:@"&devicename="];
	urlString = [urlString stringByAppendingString:deviceName];
	urlString = [urlString stringByAppendingString:@"&devicemodel="];
	urlString = [urlString stringByAppendingString:deviceModel];
	urlString = [urlString stringByAppendingString:@"&deviceversion="];
	urlString = [urlString stringByAppendingString:deviceSystemVersion];
	urlString = [urlString stringByAppendingString:@"&pushbadge="];
	urlString = [urlString stringByAppendingString:pushBadge];
	urlString = [urlString stringByAppendingString:@"&pushalert="];
	urlString = [urlString stringByAppendingString:pushAlert];
	urlString = [urlString stringByAppendingString:@"&pushsound="];
	urlString = [urlString stringByAppendingString:pushSound];
    
	// Register the Device Data
	// !!! CHANGE "http" TO "https" IF YOU ARE USING HTTPS PROTOCOL
	NSURL *url = [[NSURL alloc] initWithScheme:@"http" host:host path:urlString];
    NSURLRequest *request = [[NSURLRequest alloc] initWithURL:url];
	NSData *returnData = [NSURLConnection sendSynchronousRequest:request returningResponse:nil error:nil];
	NSLog(@"Register URL: %@", url);
	NSLog(@"Return Data: %@", returnData);
    
#endif */
    
    
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    [PFPush handlePush:userInfo];
    /*
#if !TARGET_IPHONE_SIMULATOR
    
	NSLog(@"remote notification: %@",[userInfo description]);
	NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
    
	NSString *alert = [apsInfo objectForKey:@"alert"];
	NSLog(@"Received Push Alert: %@", alert);
    
	NSString *sound = [apsInfo objectForKey:@"sound"];
	NSLog(@"Received Push Sound: %@", sound);
	AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    
	NSString *badge = [apsInfo objectForKey:@"badge"];
	NSLog(@"Received Push Badge: %@", badge);
	application.applicationIconBadgeNumber = [[apsInfo objectForKey:@"badge"] integerValue];
    
#endif*/
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
    if ([error code] == 3010)
    {
        NSLog(@"Push notifications don't work in the simulator!");
    }
    else
    {
        NSLog(@"didFailToRegisterForRemoteNotificationsWithError: %@", error);
    }
}




//==================== END OF PUSH NOTIFICATIONS METHODS===========//

//===================================================================================//







- (void)applicationWillResignActive:(UIApplication *)application
{
    
    NSString *str = [NSString stringWithFormat:@"%d",countDownTime];
    [[NSUserDefaults standardUserDefaults] setValue:str forKey:@"parkingTime"];
    
    // NSLog(@"applicationWillResignActive Closed countDownTime::%d",countDownTime);
    NSLog(@"applicationWillResignActive");
    [self InvalidateCountDownTimer];
    [[NSUserDefaults standardUserDefaults] setValue:[NSDate date] forKey:@"closedTime"];
    //closedTime = [NSDate date];
    
    
    
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
    NSLog(@"applicationDidBecomeActive");
    
    NSString *countDownTimeStr = [[NSUserDefaults standardUserDefaults] objectForKey:@"parkingTime"];
    NSDate *currentDate = [NSDate date];
    
    NSTimeInterval distanceBetweenDates = [currentDate timeIntervalSinceDate:[[NSUserDefaults standardUserDefaults] objectForKey:@"closedTime"]];
    
    // NSLog(@"time: %f", distanceBetweenDates);
      NSLog(@"countDownTimeStr... %@",countDownTimeStr);
    NSLog(@"istanceBetweenDates %f",distanceBetweenDates);
    
    if(self.temp == 1 && [countDownTimeStr intValue]-distanceBetweenDates > 0 && countDownTime > 0 && [[NSUserDefaults standardUserDefaults] objectForKey:@"ParkingRemainderCount"])
    {
        
        
        
        NSLog(@"Timer started");
        [self startCountDownTimerWithTime:[countDownTimeStr intValue]-distanceBetweenDates andUILabel:label];
    }
    else{
        NSLog(@"Timer & Parking Time Values cleared");
        [self InvalidateCountDownTimer];
        label.text = @"";
        countDownTime = 0;
        application.applicationIconBadgeNumber=0;
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"parkingTime"];
    }
    
    
}









- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSLog(@"badgecount:::::::%d",badgecount);
    application.applicationIconBadgeNumber = badgecount;
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    
    
    /* [[UIApplication sharedApplication] cancelAllLocalNotifications];
     [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
     
     UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"BackToMyCar" message:@"Parking is about to expire!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];*/
    
    
    //[alert show];
    //[alert release];
    
    
    //    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}



- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    
    
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}


@end
