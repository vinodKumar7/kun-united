//
//  DisplayMap.h
//  SampleTabApp
//
//  Created by MyRewards on 2/6/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapKit/MKAnnotation.h"

@interface DisplayMap : NSObject<MKAnnotation >
{
    CLLocationCoordinate2D coordinate;
    NSString *title;
    NSString *subtitle;
}
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *subtitle;

@end
