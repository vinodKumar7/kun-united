//
//  MyCarViewController.h
//  KUN
//
//  Created by vairat on 05/12/14.
//  Copyright (c) 2014 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCarViewController : UIViewController<UITextFieldDelegate,UIAlertViewDelegate>{
    
    
    NSMutableArray *contactsList;
    
}

@property (nonatomic, strong) NSMutableArray *contactsList;


@property(nonatomic,strong)IBOutlet UIView *accessoryView;
@property(nonatomic,strong)IBOutlet UIImageView *background_ImageView;
@property(nonatomic, strong)UITextField *currentTextField;

@property(nonatomic,strong)IBOutlet UITextField *nameTextField;
@property(nonatomic,strong)IBOutlet UITextField *numberTextField;
@property (strong, nonatomic) IBOutlet UITextField *carRegsTextField;
@property (strong, nonatomic) IBOutlet UITextField *carModelTextField;
@property (strong, nonatomic) IBOutlet UITextField *carYearTextField;

@property (strong, nonatomic) IBOutlet UILabel *carMake_label;
@property (strong, nonatomic) IBOutlet UILabel *carColor_label;
@property (strong, nonatomic) IBOutlet UILabel *carRegs_Label;
@property (strong, nonatomic) IBOutlet UILabel *carModel_Label;
@property (strong, nonatomic) IBOutlet UILabel *carYear_Label;
@property (strong, nonatomic) IBOutlet UIButton *saveButton;
@property (strong, nonatomic) IBOutlet UIButton *doneButton;

- (IBAction)saveButton_Clicked:(id)sender;
- (IBAction)doneButton_Pressed:(id)sender;


@end
