//
//  LoginViewController.m
//  GrabItNow
//
//  Created by MyRewards on 11/24/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "LoginViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "ASIFormDataRequest.h"
#import "User.h"
#import "MyCustomAlert.h"
#import <Parse/Parse.h>
#import "CustomIOS7AlertView.h"

#define Status_XML_tag @"status"
#define FirstLogin_XML_tag @"first_status"
#define FirstLoginDetailsSubmit_XML_tag @"first_response"
#define Status_success_message @"SUCCESS"
#define Status_failure_message @"FAILURE"
//#define Default_keyboard_height_iPhone 200

@interface LoginViewController ()
{
    UIView *loginProcessingView;
    ASIFormDataRequest *logingRequest;
    ASIFormDataRequest *getUserRequest;
    ASIFormDataRequest *firstTimeLoginRequest;
    ASIFormDataRequest *firstTimeLoginDetailsPostingRequest;
    NSXMLParser *parser;
    
    UserDataXMLParser *parserDelegate;
    
    
    
    
    NSString *userName;
    NSString *password;
    NSString *subDomainName;
    UITextField *currentTextField;
    
    BOOL iAgreechecked;
    BOOL isNewsLetter;
    UIImage *checkedImage;
    UIImage *uncheckedImage;
    NSString *firstloginUser_ID;
    MyCustomAlert *myAlert;
    UIActionSheet *asheet;
    
    
    NSArray *indian_StatesList;
    NSArray *australian_StatesList;
    NSArray *hongkong_StatesList;
    NSArray *philippines_StatesList;
    NSArray *newZealand_StatesList;
    NSArray *Countries_List;
    
    NSString *country;
    NSString *state;
    NSString *country_str;
    NSString *state_str;
    
    NSMutableArray *CountriesLabels_List;
    NSMutableArray *indian_StatesLabels_List;
    NSMutableArray *australian_StatesLabels_List;
    NSMutableArray *hongkong_StatesLabels_List;
    NSMutableArray *philippines_StatesLabels_List;
    NSMutableArray *newZealand_StatesLabels_List;
    
    //int comp2Row;
    int direction;
    int shakes;
    //int prevRow;
    UIView* pickerContainerView;
    CustomIOS7AlertView *alertView7;
    UIView *demoView;
    
    AppDelegate *appDelegate;
}
- (BOOL) validateLogin;
@end

@implementation LoginViewController

@synthesize usernameTextField;
@synthesize passwordTextField;
@synthesize submitButton;
@synthesize loginContainerView;
@synthesize processRenwalOfToken;
@synthesize loginContainer_Container;
@synthesize subDomainTextField;
@synthesize bottom_Banner;
@synthesize firsttimeloginButton;
@synthesize lastname_TextField;
@synthesize firstname_TextField;
@synthesize password_TextField;
@synthesize confirmpwd_TextField;
@synthesize email_TextField;
@synthesize state_TextField;
@synthesize checkButton;
@synthesize newsLetter_Button;
@synthesize firsttimeLogin_submitButton;
@synthesize tc_TextView;
@synthesize processing_View;
@synthesize firsttimelogin_ContainerView;
@synthesize membership_TextField;
@synthesize webAddress_TextField;
@synthesize referralno_TextField;
@synthesize bgView;
@synthesize current_TextField;
@synthesize alertTextField;

//=========================DELEGATE METHODS=============================//

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        processRenwalOfToken = NO;
        iAgreechecked = YES;
        isNewsLetter = YES;
        // comp2Row = 0;
        firstloginUser_ID = @"";
        state_str = @"";
        country_str = @"";
        country = @"Australia";
        state = @"";
        
        
        
        Countries_List = [[NSArray alloc]initWithObjects:@"Australia",@"Hongkong",@"India",@"New Zealand",@"Philippines", nil];
        
        indian_StatesList = [[NSArray alloc]initWithObjects:@"AndhraPradesh",@"Delhi",@"Gujarat",@"Haryana",@"Karnataka",@"Maharastra",@"TamilNadu",@"UttarPradesh",@"WestBengal",nil];
        
        australian_StatesList = [[NSArray alloc]initWithObjects:@"Australian Capital Territory",@"New South Wales",@"Northern Territory",@"Queensland",@"South Australia",@"Tasmania",@"Victoria",@"Western Australia", nil];
        
        hongkong_StatesList = [[NSArray alloc]initWithObjects:@"Hongkong", nil];
        
        philippines_StatesList = [[NSArray alloc]initWithObjects:@"Luzon",@"Mindanao",@"NCR",@"Visayas", nil];
        
        newZealand_StatesList = [[NSArray alloc]initWithObjects:@"Auckland",@"Bay of plenty",@"Canterbury",@"Fiordland",@"Gisborne",@"Hawkes Bay",@"Marlborough",@"Manawattu",@"Nelson & Bays",@"Northland",@"otago",@"Southland",@"Taranaki",@"Timaru",@"Tasman",@"wanganui",@"Willington",@"Waikato",@"Wairapapa",@"West Coast", nil];
        
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    [self loadViews];
    
    [self.firsttimelogin_ContainerView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"appBg.png"]]];
    [self.firsttimelogin_ContainerView setBackgroundColor:[UIColor whiteColor]];
    
    
   
     
     /*PFObject *testObject = [PFObject objectWithClassName:@"TestObject"];
     [testObject setObject:@"bar" forKey:@"foo"];
     [testObject save];*/
    
    
    /*  bgView.animationImages = [NSArray arrayWithObjects:[UIImage imageNamed:@"LoginImage11.png"],
     [UIImage imageNamed:@"LoginImage12.png"],[UIImage imageNamed:@"LoginImage13.png"],[UIImage imageNamed:@"LoginImage14.png"],[UIImage imageNamed:@"LoginImage15.png"],nil];
     
     bgView.animationDuration = 5.0;
     bgView.animationRepeatCount = 0;
     [bgView startAnimating];
     */
    
    
    // ** Initial frame changes to the login container
    CGRect loginContainerFrame = loginContainerView.frame;
    loginContainerFrame.size.width = loginContainerFrame.size.width - 40.0;
    loginContainerView.frame = loginContainerFrame;
    loginContainerView.center = self.view.center;
    loginContainerFrame.origin.y = self.view.frame.size.height - (loginContainerFrame.size.height + Default_keyboard_height_iPhone+15 + 5);
    
    // ** Apply round corners to the Container
    loginContainerView.layer.cornerRadius = 10.0;
    loginContainerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    loginContainerView.layer.shadowOpacity = 1.0;
    loginContainerView.layer.shadowOffset = CGSizeMake(10.0, 10.0);
    loginContainerView.layer.shadowRadius = 10.0;
    
    
    CGRect userNameFrame = usernameTextField.frame;
    userNameFrame.origin.x = (loginContainerView.frame.size.width - userNameFrame.size.width)/2.0;
    usernameTextField.frame = userNameFrame;
    
    CGRect pWordFrame = passwordTextField.frame;
    pWordFrame.origin.x = (loginContainerView.frame.size.width - pWordFrame.size.width)/2.0;
    passwordTextField.frame = pWordFrame;
    
    
    
    CGRect submitBtnFrame = submitButton.frame;
    submitBtnFrame.origin.x = (loginContainerView.frame.size.width - submitBtnFrame.size.width)/2.0;
    submitButton.frame = submitBtnFrame;
    CGRect firsttimeloginBtnFrame = firsttimeloginButton.frame;
    firsttimeloginBtnFrame.origin.x = (loginContainerView.frame.size.width - firsttimeloginBtnFrame.size.width)/2.0;
    firsttimeloginButton.frame = firsttimeloginBtnFrame;
    
    subDomainTextField.hidden = YES;
    
    [self configureCountryPicker];
    if (![appDelegate isIphone5]) {
        
        self.bgView.image=[UIImage imageNamed:@"LoginPage1"];
    }
    
}

- (void) viewDidAppear:(BOOL)animated
{
    
    [super viewDidAppear:animated];
    //[self alignViewsInLoginContainer];
    if (processRenwalOfToken)
    {
        [self renewTheToken];
    }
    else
    {
        [self showLoginContainer];
    }
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    if (![appDelegate isIphone5]) {
        
         self.bgView.image=[UIImage imageNamed:@"LoginPage1"];
    }
}
//=========================================================================//

//===========DELEGATE METHODS TO LOCK ORIENTATION=======================//
-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}
//-(BOOL)s

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}
- (BOOL) shouldAutorotate
{
    return NO;
}
//========================================================================//

// =============METHOD TO LOAD VIEWS IN IPHONE  ========================//

-(void)loadViews
{
    if([appDelegate isIphone5])
    {
        NSLog(@"Iphone5");
        
        
    }
    else
    {
        NSLog(@"Iphone4");
        CGRect frame = self.bottom_Banner.frame;
        frame.origin.y = 435.0;
        self.bottom_Banner.frame = frame;
    }
    
}

//========================================================================//
// =============METHOD TO ADD SHAKE EFFECT   ========================//

-(void)shake:(MyCustomAlert *)theOneYouWannaShake
{
    [UIView animateWithDuration:0.03 animations:^
     {
         theOneYouWannaShake.transform = CGAffineTransformMakeTranslation(5*direction, 0);
     }
                     completion:^(BOOL finished)
     {
         if(shakes >= 10)
         {
             theOneYouWannaShake.transform = CGAffineTransformIdentity;
             return;
         }
         shakes++;
         direction = direction * -1;
         [self shake:theOneYouWannaShake];
     }];
}














/*
 - (void)viewDidLayoutSubviews {
 [self alignViewsInLoginContainer];
 }
 */
//============METHOD TO ALIGN OBJECTS ===============//
- (void) alignViewsInLoginContainer
{
    
    float commonGap = ( loginContainerView.frame.size.height - (usernameTextField.frame.size.height + passwordTextField.frame.size.height + submitButton.frame.size.height) + subDomainTextField.frame.size.height)/ 5.0;
    
    CGRect usernameTextFieldFrame = usernameTextField.frame;
    usernameTextFieldFrame.origin.y = commonGap;
    usernameTextField.frame = usernameTextFieldFrame;
    
    CGRect passwordTextFieldFrame = passwordTextField.frame;
    passwordTextFieldFrame.origin.y = usernameTextFieldFrame.origin.y + commonGap + usernameTextFieldFrame.size.height;
    passwordTextField.frame = passwordTextFieldFrame;
    
    CGRect subdomainTextFieldFrame = subDomainTextField.frame;
    subdomainTextFieldFrame.origin.y = passwordTextFieldFrame.origin.y + commonGap + passwordTextFieldFrame.size.height;
    subDomainTextField.frame = subdomainTextFieldFrame;
    
    CGRect submitButtonFrame = submitButton.frame;
    submitButtonFrame.origin.y = subdomainTextFieldFrame.origin.y + commonGap + subdomainTextFieldFrame.size.height;
    submitButton.frame = submitButtonFrame;
}

//=========================================//


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}





//============ALERT VIEW DELEGATE  METHOD  ===============//


- (void)alertView:(MyCustomAlert *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.cancelButtonIndex == buttonIndex)
    {
        
        alertView.dontDisppear = NO;
        if (alertView.tag == 222)
        {
            usernameTextField.text = userName;
            subDomainTextField.text = subDomainName;
            [self closeButton_Tapped:nil];
        }
        //        else if(alertView.tag == 111)
        //            [self readjustloginContainer];
        
    }
    else
    {
        NSLog(@"Submit clicked");
        
        
        alertView.dontDisppear = YES;
        if ([self.membership_TextField.text length] == 0 )
        {
            
            //  [alertView setMessage:@"As this is your first time login, here we need you to complete the registration. \n Membership Number required \n \n"];
            
            
            //======
            direction = 1;
            shakes = 0;
            [self shake:alertView];
            //===========
            
            
            
            [alertView setTitle:@" \n \n"];
            UILabel *alertLabel = [ [UILabel alloc ] initWithFrame:CGRectMake(15, 20, 250.0, 40.0)];
            alertLabel.textAlignment =  UITextAlignmentCenter;
            alertLabel.textColor = [UIColor redColor];
            alertLabel.backgroundColor = [UIColor clearColor];
            alertLabel.font = [UIFont fontWithName:@"Arial Rounded MT Bold" size:(18.0)];
            alertLabel.numberOfLines = 2;
            alertLabel.text = @"Membership Number required";
            [alertView addSubview:alertLabel];
        }
        else
        {
            alertView.dontDisppear = NO;
            NSString *membership_Number = self.membership_TextField.text;
            // NSString *web_address = self.webAddress_TextField.text;
            NSLog(@"membership_Number :: %@",membership_Number);
            
            
            NSString *urlString = [NSString stringWithFormat:@"%@first_time_login.php?",URL_Prefix];
            NSLog(@"first_time_login URL: %@",urlString);
            NSURL *url = [NSURL URLWithString:urlString];
            
            firstTimeLoginRequest = [[ASIFormDataRequest alloc] initWithURL:url];
            [firstTimeLoginRequest setPostValue:membership_Number forKey:@"uname"];
            [firstTimeLoginRequest setPostValue:@"www.mycustomerprivileges.in" forKey:@"sub"];
            [firstTimeLoginRequest setDelegate:self];
            [firstTimeLoginRequest startAsynchronous];
        }
        
    }
    
}
//==================================================//
//============METHOD TO LOGIN BUTTON ===============//

- (IBAction)loginButtonTapped:(id)sender
{
    
    if ([usernameTextField isFirstResponder])
    {
        [usernameTextField resignFirstResponder];
    }
    
    if ([passwordTextField isFirstResponder])
    {
        [passwordTextField resignFirstResponder];
    }
    
    if ([subDomainTextField isFirstResponder])
    {
        [subDomainTextField resignFirstResponder];
    }
    
    
    [self readjustloginContainer];
    
    if (![self validateLogin])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fields Empty" message:@"All fields are mandatory. \nPlease fill all the fields." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [self performLoginWithUserName:usernameTextField.text password:passwordTextField.text domainName:@"www.mycustomerprivileges.in"];
    
    // menuViewController = [[MenuViewController alloc]initWithNibName:@"MenuViewController" bundle:nil];
    // UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:menuViewController];
    
    
    //[self transitionToViewController:menuViewController
    //  withTransition: UIViewAnimationOptionTransitionCurlUp];
}
//=============================================================//
//============METHOD TO FIRST TIME LOGIN BUTTON ===============//

- (IBAction)firsttimeLogin_ButtonTapped:(id)sender
{
    NSLog(@"firsttimeLogin_ButtonTapped");
    if (IOS_VERSION >= 7)
    {
        NSLog(@"firsttimeLogin_ButtonTapped ios 7");
        [self launchDialog:firsttimeLogin_submitButton];
    }
    else
    {
        
        
        myAlert = [[MyCustomAlert alloc] initWithTitle:@"Please enter your Membership number provided to complete the registration." message:@"\n" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Submit", nil];
        
        
        membership_TextField = [[UITextField alloc] initWithFrame:CGRectMake(22.0, 85.0, 240.0, 25.0)];
        membership_TextField.placeholder = @"Membership Number";
        membership_TextField.font = [UIFont systemFontOfSize:14];
        membership_TextField.borderStyle = UITextBorderStyleRoundedRect;
        membership_TextField.delegate=self;
        
        [membership_TextField setBackgroundColor:[UIColor whiteColor]];
        
        [myAlert addSubview:membership_TextField];
        /* webAddress_TextField = [[UITextField alloc] initWithFrame:CGRectMake(22.0, 115.0, 240.0, 25.0)];
         webAddress_TextField.placeholder = @"Web address on your card";
         webAddress_TextField.font = [UIFont systemFontOfSize:14];
         webAddress_TextField.borderStyle = UITextBorderStyleRoundedRect;
         [webAddress_TextField setBackgroundColor:[UIColor whiteColor]];
         [webAddress_TextField setKeyboardType:UIKeyboardTypeURL];
         // [myAlert addSubview:webAddress_TextField];*/
        
        [myAlert show];
        [currentTextField resignFirstResponder];//To resign keyboard
    }
    //[self settingPickerViewLabels];
}
- (IBAction)launchDialog:(id)sender
{
    // Here we need to pass a full frame
    alertView7 = [[CustomIOS7AlertView alloc] init];
    [currentTextField resignFirstResponder];//To resign keyboard
    // Add some custom content to the alert view
    [alertView7 setContainerView:[self createDemoView]];
    
    // Modify the parameters
    [alertView7 setButtonTitles:[NSMutableArray arrayWithObjects:@"Close", @"Submit", nil]];
    [alertView7 setDelegate:self];
    
    // You may use a Block, rather than a delegate.
    /*   [alertView setOnButtonTouchUpInside:^(CustomIOS7AlertView *alertView, int buttonIndex) {
     NSLog(@"Block: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView tag]);
     [alertView close];
     }];  */
    
    [alertView7 setUseMotionEffects:true];
    
    // And launch the dialog
    [alertView7 show];
}

- (void)customIOS7dialogButtonTouchUpInside: (CustomIOS7AlertView *)alertView clickedButtonAtIndex: (NSInteger)buttonIndex
{
    
    NSLog(@"Delegate: Button at position %d is clicked on alertView %d.", buttonIndex, [alertView7 tag]);
    
    if(buttonIndex == 0)
    {
        [alertView7 close];
        if (alertView7.tag == 222)
        {
            usernameTextField.text = userName;
            subDomainTextField.text = subDomainName;
            [self closeButton_Tapped:nil];
        }
        [self showLoginContainer];
    }
    else
    {
        // [alertView close];
        if ([self.alertTextField.text length] == 0 )
        {
            alertView7.direction = 1;
            alertView7.shakes = 0;
            [alertView7 shake];
        }
        else
        {
            NSString *urlString = [NSString stringWithFormat:@"%@first_time_login.php?",URL_Prefix];
            NSLog(@"first_time_login URL: %@",urlString);
            NSURL *url = [NSURL URLWithString:urlString];
            
            NSString *membership_Number = self.alertTextField.text;
            
            
            firstTimeLoginRequest = [[ASIFormDataRequest alloc] initWithURL:url];
            [firstTimeLoginRequest setPostValue:membership_Number forKey:@"uname"];
            [firstTimeLoginRequest setPostValue:@"www.mycustomerprivileges.in" forKey:@"sub"];
            [firstTimeLoginRequest setDelegate:self];
            [firstTimeLoginRequest startAsynchronous];
            
        }
        
    }
}



- (UIView *)createDemoView
{
    demoView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 275, 170)];
    
    //    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 270, 180)];
    //    [imageView setImage:[UIImage imageNamed:@"demo"]];
    //    [demoView addSubview:imageView];
    
    UILabel *alertlabel = [[UILabel alloc]initWithFrame:CGRectMake(10, 20, 240, 70)];
    alertlabel.numberOfLines = 3;
    //alertlabel.font = [UIFont fontNamesForFamilyName:@"System Bold"];
    alertlabel.font = [UIFont boldSystemFontOfSize:17];
    alertlabel.textAlignment = NSTextAlignmentCenter;
    alertlabel.text = @"Please enter your Membership number provided to complete the registration.";
    [demoView addSubview:alertlabel];
    
    alertTextField = [[UITextField alloc]initWithFrame:CGRectMake(25, 120, 230, 30)];
    alertTextField.delegate = self;
    alertTextField.borderStyle = UITextBorderStyleRoundedRect;
    alertTextField.placeholder = @"Membership Number";
    [demoView addSubview:alertTextField];
    
    return demoView;
}



/*
 -(void)settingPickerViewLabels{
 
 
 [CountriesLabels_List removeAllObjects];
 [indian_StatesLabels_List removeAllObjects];
 [australian_StatesLabels_List removeAllObjects];
 [hongkong_StatesLabels_List removeAllObjects];
 [philippines_StatesLabels_List removeAllObjects];
 [newZealand_StatesLabels_List removeAllObjects];
 
 UILabel *theview;
 for (int i=0;i<=4;i++) {
 theview = [[UILabel alloc] init];
 
 theview.text = [NSString stringWithFormat:@"%@",[Countries_List objectAtIndex:i]];
 //NSLog(@"%@",[NSString stringWithFormat:@"%@",[Countries_List objectAtIndex:i]]);
 theview.textColor = [UIColor blackColor];
 theview.frame = CGRectMake(0,0,200,200);
 theview.backgroundColor = [UIColor clearColor];
 theview.textAlignment = UITextAlignmentCenter;
 theview.shadowColor = [UIColor whiteColor];
 theview.shadowOffset = CGSizeMake(-1,-1);
 //theview.adjustsFontSizeToFitWidth = YES;
 
 UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:15];
 [theview setFont:myFont];
 
 [CountriesLabels_List addObject:theview];
 }
 
 UILabel *theview1;
 for (int i=0;i<9;i++) {
 theview1 = [[UILabel alloc] init];
 
 theview1.text = [NSString stringWithFormat:@"%@",[indian_StatesList objectAtIndex:i]];
 theview1.textColor = [UIColor blackColor];
 theview1.frame = CGRectMake(0,0,200,200);
 theview1.backgroundColor = [UIColor clearColor];
 theview1.textAlignment = UITextAlignmentCenter;
 theview1.shadowColor = [UIColor whiteColor];
 theview1.shadowOffset = CGSizeMake(-1,-1);
 //theview.adjustsFontSizeToFitWidth = YES;
 
 UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
 [theview1 setFont:myFont];
 
 [indian_StatesLabels_List addObject:theview1];
 }
 
 UILabel *theview2;
 for (int i=0;i<8;i++) {
 theview2 = [[UILabel alloc] init];
 
 theview2.text = [NSString stringWithFormat:@"%@",[australian_StatesList objectAtIndex:i]];
 theview2.textColor = [UIColor blackColor];
 theview2.frame = CGRectMake(0,0,200,200);
 theview2.backgroundColor = [UIColor clearColor];
 theview2.textAlignment = UITextAlignmentCenter;
 theview2.shadowColor = [UIColor whiteColor];
 theview2.shadowOffset = CGSizeMake(-1,-1);
 //theview.adjustsFontSizeToFitWidth = YES;
 
 UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
 [theview2 setFont:myFont];
 
 [australian_StatesLabels_List addObject:theview2];
 }
 
 UILabel *theview3;
 for (int i=0;i<4;i++) {
 theview3 = [[UILabel alloc] init];
 
 theview3.text = [NSString stringWithFormat:@"%@",[philippines_StatesList objectAtIndex:i]];
 theview3.textColor = [UIColor blackColor];
 theview3.frame = CGRectMake(0,0,200,200);
 theview3.backgroundColor = [UIColor clearColor];
 theview3.textAlignment = UITextAlignmentCenter;
 theview3.shadowColor = [UIColor whiteColor];
 theview3.shadowOffset = CGSizeMake(-1,-1);
 //theview.adjustsFontSizeToFitWidth = YES;
 
 UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
 [theview3 setFont:myFont];
 
 [philippines_StatesLabels_List addObject:theview3];
 }
 
 UILabel *theview4;
 for (int i=0;i<20;i++) {
 theview4 = [[UILabel alloc] init];
 
 theview4.text = [NSString stringWithFormat:@"%@",[newZealand_StatesList objectAtIndex:i]];
 theview4.textColor = [UIColor blackColor];
 theview4.frame = CGRectMake(0,0,200,200);
 theview4.backgroundColor = [UIColor clearColor];
 theview4.textAlignment = UITextAlignmentCenter;
 theview4.shadowColor = [UIColor whiteColor];
 theview4.shadowOffset = CGSizeMake(-1,-1);
 //theview.adjustsFontSizeToFitWidth = YES;
 
 UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
 [theview4 setFont:myFont];
 
 [newZealand_StatesLabels_List addObject:theview4];
 }
 
 UILabel *theview5;
 theview5 = [[UILabel alloc] init];
 
 theview5.text = [NSString stringWithFormat:@"%@",[hongkong_StatesList objectAtIndex:0]];
 theview5.textColor = [UIColor blackColor];
 theview5.frame = CGRectMake(0,0,200,200);
 theview5.backgroundColor = [UIColor clearColor];
 theview5.textAlignment = UITextAlignmentCenter;
 theview5.shadowColor = [UIColor whiteColor];
 theview5.shadowOffset = CGSizeMake(-1,-1);
 //theview.adjustsFontSizeToFitWidth = YES;
 
 UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
 [theview5 setFont:myFont];
 
 [hongkong_StatesLabels_List addObject:theview5];
 
 
 
 }
 */
//=============================================================//

//============METHOD TO SUBMIT BUTTON ===============//

- (IBAction)firsttimeLoginDetailsSubmit_ButtonTapped:(id)sender
{
    
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    if (firstname_TextField.text.length == 0 || lastname_TextField.text.length == 0 || password_TextField.text.length == 0 || confirmpwd_TextField.text.length == 0 || email_TextField.text.length == 0 || state_TextField.text.length == 0 || iAgreechecked == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fields Empty" message:@"All fields are mandatory. \nPlease fill all the fields." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        
        
        
    }
    
    else if(![password_TextField.text isEqualToString:confirmpwd_TextField.text])
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Password and Conform Password should be same" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
        
        
    }
    else  if ([emailTest evaluateWithObject:email_TextField.text] == NO)
    {
        
        
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Invalid EmailId" message:@"Email should be in proper format" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else
    {
        NSLog(@"COUNTRY :%@",country);
        NSLog(@"COUNTRY field is :%@",self.state_TextField.text);
        
        NSLog(@"STATE is :%@ STATE CODE is :%@",state,state_str);
        
        
        processing_View.hidden = NO;
        
        NSString *str = @"";
        if(isNewsLetter)
            str= @"1";  //YES
        else
            str = @"0";  //NO
        
        NSString *urlString = [NSString stringWithFormat:@"%@first_time_user.php",URL_Prefix];
        NSLog(@"first_time_user URL: %@",urlString);
        NSURL *url = [NSURL URLWithString:urlString];
        
     firstTimeLoginDetailsPostingRequest = [[ASIFormDataRequest alloc] initWithURL:url];
        [firstTimeLoginDetailsPostingRequest setPostValue:firstname_TextField.text forKey:@"fname"];
        [firstTimeLoginDetailsPostingRequest setPostValue:lastname_TextField.text forKey:@"lname"];
        [firstTimeLoginDetailsPostingRequest setPostValue:password_TextField.text forKey:@"pwd"];
        [firstTimeLoginDetailsPostingRequest setPostValue:country forKey:@"country"];
        [firstTimeLoginDetailsPostingRequest setPostValue:email_TextField.text forKey:@"email"];
        [firstTimeLoginDetailsPostingRequest setPostValue:state_str forKey:@"state"];
        [firstTimeLoginDetailsPostingRequest setPostValue:str forKey:@"newsletter"];
        [firstTimeLoginDetailsPostingRequest setPostValue:firstloginUser_ID forKey:@"id"];
        [firstTimeLoginDetailsPostingRequest setDelegate:self];
        [firstTimeLoginDetailsPostingRequest startAsynchronous];
        
        
    }
    
    
}
//=============================================================//
//======METHOD TO "RECIVE NEWS LETTER" AND "I AGREE" BUTTONS======//

- (IBAction)tandcButton_Tapped:(id)sender
{
    
    
    if ([sender tag] ==1)
    {
        
        
        if(!iAgreechecked)
        {
            [self.checkButton setImage:checkedImage forState:UIControlStateNormal];
            iAgreechecked = YES;
            
        }
        else
        {
            [self.checkButton setImage:uncheckedImage forState:UIControlStateNormal];
            iAgreechecked = NO;
        }
    }
    
    
    else{
        
        
        if(!isNewsLetter)
        {
            [self.newsLetter_Button setImage:checkedImage forState:UIControlStateNormal];
            isNewsLetter = YES;
        }
        else
        {
            [self.newsLetter_Button setImage:uncheckedImage forState:UIControlStateNormal];
            isNewsLetter = NO;
        }
        
        
    }
}

//=========================================//


//============METHOD TO ADD TRANISITION EFFECT ===============//
- (void)transitionToViewController:(UIViewController *)viewController
                    withTransition:(UIViewAnimationOptions)transition
{
    
    [UIView transitionFromView:self.view
                        toView:viewController.view
                      duration:0.55f
                       options:transition
                    completion:^(BOOL finished){
                    } ];
}
//=========================================//
//============METHOD TO VALIDATING USER DETAILS  ===============//
- (BOOL) validateLogin
{
    
    BOOL success = YES;
    
    if (usernameTextField.text.length == 0 || passwordTextField.text.length == 0 )
    {
        success = NO;
    }
    
    return success;
}

//=========================================//



//============TEXT FIELD DELEGATE METHODS ===============//

#pragma mark - TextfieldDelegate methods

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextField = textField;
    
    
    if (textField == usernameTextField)
    {
        [self usernameTextFieldDidBegin];
    }
    else if (textField == passwordTextField)
    {
        [self passwordTextFieldDidBegin];
    }
    else if (textField == subDomainTextField)
    {
        [self bringTheLoginViewUp];
    }
    else if (textField == email_TextField)
    {
        [self bringTheFirstTimeLoginViewUp];
    }
    else if (textField == referralno_TextField)
    {
        [self bringTheFirstTimeLoginViewUp];
    }
    else if (textField == state_TextField)
    {
        
        [self  bringTheFirstTimeLoginViewUp];
        textField.inputView = pickerContainerView;
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    [textField resignFirstResponder];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    if (textField == usernameTextField) {
        [passwordTextField becomeFirstResponder];
    }
    else if (textField == passwordTextField) {
        [self passwordTextFieldDidClose];
    }
    
    else if(textField == email_TextField){
        
        [textField resignFirstResponder];
        [self bringTheFirstTimeLoginViewDown];
    }
    else if(textField == referralno_TextField){
        
        [textField resignFirstResponder];
        [self bringTheFirstTimeLoginViewDown];
    }
    else
    {
        [textField resignFirstResponder];
    }
    return YES;
    
}
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    if (email_TextField)
        return (newLength > 80) ? NO : YES;
    else
        return (newLength > 50) ? NO : YES;
}
//=========================================//
//============METHODS TO ANIMATE FIRST TIME LOGI VIEW  ===============//

-(void) bringTheFirstTimeLoginViewUp
{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        
        
        CGRect firstTimeLoginContainerFrame = firsttimelogin_ContainerView.frame;
        if ([appDelegate isIphone5])
        {
            firstTimeLoginContainerFrame.origin.y = self.view.frame.size.height - (firstTimeLoginContainerFrame.size.height + Default_keyboard_height_iPhone);
        }
        else
        {
            firstTimeLoginContainerFrame.origin.y=self.view.frame.size.height - (firstTimeLoginContainerFrame.size.height + Default_keyboard_height_iPhone);;
            
        }
        
        firsttimelogin_ContainerView.frame = firstTimeLoginContainerFrame;
    } completion:^(BOOL finished) {
        
    }];
    
    
    
    
    
}


-(void) bringTheFirstTimeLoginViewDown
{
    
    [UIView animateWithDuration:0.5 animations:^{
        
        NSLog(@"login view y position is.");
        
        CGRect firstTimeLoginContainerFrame = firsttimelogin_ContainerView.frame;
        if ([appDelegate isIphone5])
        {
            
            
            firstTimeLoginContainerFrame.origin.y =  (firstTimeLoginContainerFrame.origin.y + Default_keyboard_height_iPhone -45);
            NSLog(@"login view y position is.");
        }
        else {
            firstTimeLoginContainerFrame.origin.y = 0;
            NSLog(@"login view y position is..%f",firstTimeLoginContainerFrame.origin.y);
        }
        
        firsttimelogin_ContainerView.frame = firstTimeLoginContainerFrame;
    } completion:^(BOOL finished) {
        
    }];
    
    
}
//=========================================//


#pragma mark -- Other Methods
//============METHODS  TO CALL KEYBOARD  ===============//

- (void)usernameTextFieldDidBegin
{
    [self bringTheLoginViewUp];
    
}
- (void)passwordTextFieldDidBegin
{
    [self bringTheLoginViewUp];
}

//=========================================//

//============METHOD TO MOVE LOGIN VIEW UP  ===============//

- (void) bringTheLoginViewUp
{
    
    
    [UIView animateWithDuration:0.5 animations:^{
        
        float bottomMargin = 5.0;
        
        CGRect loginContainerFrame = loginContainerView.frame;
        loginContainerFrame.origin.y = self.view.frame.size.height - (loginContainerFrame.size.height + Default_keyboard_height_iPhone+15 + bottomMargin);
        loginContainerView.frame = loginContainerFrame;
    } completion:^(BOOL finished) {
        
    }];
    
    
}
//=========================================//

//============METHOD TO VALIDATE FIRST TIME LOGIN  ===============//

- (void)usernameTextFieldDidCloseAndPassControlToPasswordTextField:(BOOL) passControl
{
    
    if (!passControl)
    {
        [self readjustloginContainer];
    }
    
}

- (void)passwordTextFieldDidClose
{
    
    [passwordTextField resignFirstResponder];
    [self readjustloginContainer];
    
}
//=========================================//
//============METHOD TO ALIGN "LOGIN CONTAINER"  ===============//

- (void) readjustloginContainer
{
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = loginContainerView.frame;
        if ([appDelegate isIphone5])
        {
            frame.origin.y = self.view.frame.size.height - frame.size.height-10;
        }
        else
            frame.origin.y = self.view.frame.size.height - frame.size.height-2;
        loginContainerView.frame = frame;
    } completion:^(BOOL finished) {
        
    }];
}
//=========================================//
//============METHOD TO RELOGIN   ===============//

- (void) renewTheToken
{
    //AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
    NSString *uName = [appDelegate sessionUsername];
    NSString *pWord = [appDelegate sessionPassword];
    NSString *domainName = [appDelegate clientDomainName];
    
    
    if (uName && uName.length!=0 && pWord && pWord.length!=0 && domainName && domainName.length!=0)
    {
        loginContainerView.hidden = YES;
        [self performLoginWithUserName:uName password:pWord domainName:domainName];
    }
    else
    {
        [self showLoginContainer];
    }
    
}
//=========================================//
//============METHOD TO LOAD "LOGIN CONTAINER"  ===============//

- (void) showLoginContainer
{
    
    CGRect loginContainerFrame = loginContainerView.frame;
    loginContainerFrame.origin.y = -loginContainerFrame.size.height;
    loginContainerView.frame = loginContainerFrame;
    
    //[self alignViewsInLoginContainer];
    /*
     [UIView animateWithDuration:0.4 animations:^{
     loginContainerView.center = self.view.center;
     
     } completion:^(BOOL finished) {
     
     }];*/
    [self readjustloginContainer];
}
//=========================================//
//============METHOD TO VALIDATE  LOGIN DETAILS ===============//

- (void) performLoginWithUserName:(NSString *) uName password:(NSString *) pWord domainName:(NSString *) domainName
{
    
    if ([usernameTextField isFirstResponder])
        
        [usernameTextField resignFirstResponder];
    
    if ([passwordTextField isFirstResponder])
        
        [passwordTextField resignFirstResponder];
    
    if ([subDomainTextField isFirstResponder])
        
        [subDomainTextField resignFirstResponder];
    
    
    userName = uName;
    password = pWord;
    subDomainName = domainName;
    
    
    //[self readjustloginContainer];
    
    [self showLoginProgressingView];
    
    
    /**** Here goes actual login checking code ****/
    
    NSString *urlString = [NSString stringWithFormat:@"%@login.php",URL_Prefix];
    NSURL *url = [NSURL URLWithString:urlString];
    
    ASIFormDataRequest *req = [[ASIFormDataRequest alloc] initWithURL:url];
    [req setPostValue:uName forKey:@"uname"]; // pwd, sub
    [req setPostValue:pWord forKey:@"pwd"];
    [req setPostValue:domainName forKey:@"sub"];
    [req setDelegate:self];
    [req startAsynchronous];
    logingRequest = req;
    
}
//=========================================//
//============METHOD CALLED AFTER SUCCESSFUL  LOGIN  ===============//

- (void) dismissLoginContainerWithUserDetails:(User *)uDetails password:(NSString *) pWord
{
    
    
    usernameTextField.hidden = passwordTextField.hidden = subDomainTextField.hidden = submitButton.hidden = YES;
    
    [UIView animateWithDuration:1.0 delay:0.0 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
        loginContainerView.layer.masksToBounds = YES;
        loginContainerView.frame = CGRectMake((self.view.frame.size.width/2.0 - 1), (self.view.frame.size.height/2.0 - 1), 2.0, 2.0);
    } completion:^(BOOL finished) {
        [loginContainerView removeFromSuperview];
        
        /*** These statements are called on successful login ***/
        //AppDelegate *appDelegate = (AppDelegate *) [[UIApplication sharedApplication] delegate];
        uDetails.clientDomainName = subDomainName;
        [appDelegate loginSuccessfulWithUserdetails:uDetails password:pWord];
        
    }];
    
}
//=========================================//
//============METHOD TO SHOW PROCESSING VIEW ===============//

- (void) showLoginProgressingView
{
    CGSize processingViewSize = CGSizeMake(self.view.frame.size.width*(3/4.0), self.view.frame.size.height*(1/4.0));
    
    CGPoint processingViewPoint = CGPointMake( (self.view.frame.size.width - processingViewSize.width)/2, (self.view.frame.size.height - processingViewSize.height)/2);
    CGRect processingViewrect = CGRectZero;
    processingViewrect.origin = processingViewPoint;
    processingViewrect.size = processingViewSize;
    
    if (!loginProcessingView)
    {
        loginProcessingView = [[UIView alloc] initWithFrame:processingViewrect];
        loginProcessingView.backgroundColor = [UIColor colorWithWhite:0 alpha:0.5];
        UILabel *loadingLabel = [[UILabel alloc] initWithFrame:loginProcessingView.bounds];
        loadingLabel.textColor = [UIColor whiteColor];
        loadingLabel.tag = 2020;
        loadingLabel.font = [UIFont systemFontOfSize:17.0];
        loadingLabel.textAlignment = UITextAlignmentCenter;
        loadingLabel.backgroundColor = [UIColor clearColor];
        loadingLabel.text = @"Processing ....";
        loadingLabel.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        
        [loginProcessingView addSubview:loadingLabel];
        
        loginProcessingView.layer.cornerRadius = 5.0;
        loginProcessingView.layer.shadowColor = [UIColor colorWithWhite:0.3 alpha:1.0].CGColor;
        
        
    }
    
    loginProcessingView.frame = processingViewrect;
    [self.view addSubview:loginProcessingView];
}

//=========================================//
//============METHOD TO REMOVE PROCESSING VIEW ===============//

- (void) removeLoginProgressView
{
    
    [loginProcessingView removeFromSuperview];
}
//=========================================//
//============METHOD TO GET USER DETAILS ===============//

- (void) getuserDetails
{
    
    if (loginProcessingView)
    {
        UILabel *messLabel = (UILabel *)[loginProcessingView viewWithTag:2020];
        messLabel.text = @"loading ....";
    }
    //need to send user ID here as parameter.
    ASIFormDataRequest *req = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@get_user.php?uname=%@",URL_Prefix,userName]]];
    req.delegate = self;
    [req startAsynchronous];
    getUserRequest = req;
    
}
//=========================================//
//===========ASIHTTPREQUEST  DELEGATE METHODS =======================//

#pragma mark -- ASIHttpRequest Delegate methods


- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    
    if (request == logingRequest)
    {
        logingRequest = nil;
        
        NSLog(@"LOGIN:%@",[request responseString]);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        xmlParser.delegate =self;
        [xmlParser parse];
        parser = xmlParser;
        
        
        //^^^ [self removeLoginProgressView];
    }
    else if(request == getUserRequest)
    {
        
        NSString *responseString = [request responseString];
        NSLog(@"USER DATA:: %@",responseString);
        NSXMLParser *userDetailsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        parserDelegate = [[UserDataXMLParser alloc] init];
        parserDelegate.delegate = self;
        userDetailsParser.delegate = parserDelegate;
        [userDetailsParser parse];
        
        
        [self removeLoginProgressView];
    }
    
    else if(request == firstTimeLoginRequest)
    {
        NSString *responseString = [request responseString];
        NSLog(@"GET USER RESPONSE IS RESPONSE STRING IS:: %@",responseString);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        xmlParser.delegate =self;
        [xmlParser parse];
        parser = xmlParser;
        [alertView7 close];
        
        
        
    }
    
    else if(request == firstTimeLoginDetailsPostingRequest)
    {
        [processing_View removeFromSuperview];
        NSString *responseString = [request responseString];
        NSLog(@"firstTimeLoginDetailsPostingRequest RESPONSE STRING IS:: %@",responseString);
        
        NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        xmlParser.delegate =self;
        [xmlParser parse];
        parser = xmlParser;
    }
    
    
    
    
}
- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    
    if (request == logingRequest || request == firstTimeLoginDetailsPostingRequest || request == firstTimeLoginRequest)
    {
        logingRequest = nil;
        
        UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
        [failureAlert show];
        
        [processing_View removeFromSuperview];
        [self removeLoginProgressView];
    }
    else
    {
        [self removeLoginProgressView];
    }
    
    
}
//=========================================//

//============METHOD TO CLOSE BUTTON ===============//
- (IBAction)closeButton_Tapped:(id)sender
{
    
    loginContainerView.hidden = NO;
    [currentTextField resignFirstResponder];
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = firsttimelogin_ContainerView.frame;
        
        frame.origin.y = firsttimelogin_ContainerView.frame.origin.y - frame.size.height-100;
        firsttimelogin_ContainerView.frame = frame;
        //loginContainerView.center = self.view.center;
    } completion:^(BOOL finished) {
        
    }];
    
    //  [firsttimelogin_ContainerView removeFromSuperview];
    //[self alignViewsInLoginContainer];
    
}
//=========================================//


//============METHOD TO SHOW PICKERVIEW ===============//

-(void)configureCountryPicker
{
    
    
    
    UIPickerView  *myPickerView = [[ UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator=YES;
    myPickerView.backgroundColor=[UIColor whiteColor];
    
    
    
    
    [myPickerView setFrame:CGRectMake(0, 50, 320, 250)];
    
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(250, 10, 50, 30)];
    [button setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
    pickerContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, 300, 320, 266)];
    pickerContainerView.backgroundColor = [UIColor whiteColor];
    [pickerContainerView addSubview:myPickerView];
    [pickerContainerView addSubview:button];
    
}


-(void)doneButtonPressed
{
    
    NSLog(@"doneButtonPressed");
    if (state_TextField.text.length==0)
    {
        state_TextField.text=@"Australia,Australia Capital Territory";
        state_str=@"ACT";
    }
    [self bringTheFirstTimeLoginViewDown];
    //[pickerContainerView removeFromSuperview];
    [currentTextField resignFirstResponder];
}



//=========================================//


//===========NSXMLPARSER DELEGATE METHODS =======================//


#pragma mark -- NSXMLParser Delegate methods

NSMutableString *parserElementString;



- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict
{
    if ([elementName isEqualToString:@"status"])
    {
        parserElementString = [[NSMutableString alloc] init];
    }
    else if ([elementName isEqualToString:@"first_status"])
    {
        parserElementString = [[NSMutableString alloc] init];
    }
    else if ([elementName isEqualToString:@"first_response"])
    {
        parserElementString = [[NSMutableString alloc] init];
    }
}


- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    [parserElementString appendString:string];
}


- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    
    if ([elementName isEqualToString:Status_XML_tag])
    {
        
        if ([parserElementString isEqualToString:Status_success_message])
        {
            //^^^ [self dismissLoginContainerForUsername:usernameTextField.text password:passwordTextField.text];
            [self getuserDetails];
        }
        else
        {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Login Failed" message:@"Please provide valid details" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
            [self removeLoginProgressView];
        }
        
    }
    
    // FirstTime login
    
    if ([elementName isEqualToString:FirstLogin_XML_tag])
    {
        
        if ([parserElementString isEqualToString:Status_failure_message])
        {
            
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Invalid Membership number " delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
            
            
            
        }
        else
        {
            
            firstloginUser_ID = parserElementString;
            userName = membership_TextField.text;
            subDomainName = webAddress_TextField.text;
            NSLog(@"%@",userName);
            NSLog(@"%@",subDomainName);
            
            NSLog(@"firstloginUser_ID= %@",firstloginUser_ID);
            loginContainerView.hidden = YES;
            [self readjustfirstloginContainer];
        }
        
    }
    // FirstTime Details submit
    if ([elementName isEqualToString:FirstLoginDetailsSubmit_XML_tag])
    {
        
        if ([parserElementString isEqualToString:@"success"])
        {
            
            //  [firsttimelogin_ContainerView removeFromSuperview];
            MyCustomAlert *alert = [[MyCustomAlert alloc] initWithTitle:@"Sucess" message:@"Your details has been submitted sucessfully, you can login now" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            alert.tag = 222;
            [alert show];
            
            
            //[self closeButton_Tapped:nil];
        }
        else
        {
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Details submission failed" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
        }
    }
    
    
}

//=========================================//
//===========PICKERVIEW  DELEGATE METHODS =======================//
- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    state = @"Australian Capital Territory";
    country = @"Australia";
    
    
    
    //Configure picker...
    
    
    //Add picker to action sheet
    // [actionSheet addSubview:myPickerView];
    
    
    //Gets an array af all of the subviews of our actionSheet
    NSArray *subviews = [actionSheet subviews];
    
    [[subviews objectAtIndex:0] setFrame:CGRectMake(20, 170, 280, 40)];
    // [[subviews objectAtIndex:1] setFrame:CGRectMake(20, 317, 280, 46)];
}
//=========================================//
//============METHOD TO ALIGN "FIRST TIME LOGIN CONTAINER"  ===============//

- (void) readjustfirstloginContainer
{
    
    // ** Initial frame changes to the firstlogin container
    
    // myScrollView.frame = CGRectMake(0, 0, 320, 460);
    //self.myScrollView.contentSize = CGSizeMake(0, 600);
    // self.myScrollView.scrollEnabled = NO;
    //self.myScrollView.showsVerticalScrollIndicator = NO;
    
    firsttimelogin_ContainerView.frame =  CGRectMake(0, 0, 320, 447);
    CGRect firsttimeloginContainerFrame = firsttimelogin_ContainerView.frame;
    firsttimeloginContainerFrame.size.width = firsttimeloginContainerFrame.size.width - 40.0;
    if ([appDelegate isIphone5])
    {
        firsttimeloginContainerFrame.origin.x = 20;
    }
    else
    {
        firsttimeloginContainerFrame.origin.x = 20;
        firsttimeloginContainerFrame.origin.y = 00;
    }
    
    firsttimelogin_ContainerView.frame = firsttimeloginContainerFrame;
    //  firsttimelogin_ContainerView.center = self.view.center;
    
    // ** Apply round corners to the Container
    firsttimelogin_ContainerView.layer.cornerRadius = 10.0;
    firsttimelogin_ContainerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    firsttimelogin_ContainerView.layer.shadowOpacity = 1.0;
    firsttimelogin_ContainerView.layer.shadowOffset = CGSizeMake(10.0, 10.0);
    firsttimelogin_ContainerView.layer.shadowRadius = 10.0;
    
    processing_View.layer.cornerRadius = 10.0;
    
    CGRect firstNameFrame = firstname_TextField.frame;
    firstNameFrame.origin.x = (firsttimelogin_ContainerView.frame.size.width - firstNameFrame.size.width)/2.0;
    firstname_TextField.frame = firstNameFrame;
    
    CGRect lastNameFrame = lastname_TextField.frame;
    lastNameFrame.origin.x = (firsttimelogin_ContainerView.frame.size.width - lastNameFrame.size.width)/2.0;
    lastname_TextField.frame = lastNameFrame;
    
    CGRect passwordFrame = password_TextField.frame;
    passwordFrame.origin.x = (firsttimelogin_ContainerView.frame.size.width - passwordFrame.size.width)/2.0;
    password_TextField.frame = passwordFrame;
    
    CGRect conformpwdFrame = confirmpwd_TextField.frame;
    conformpwdFrame.origin.x = (firsttimelogin_ContainerView.frame.size.width - conformpwdFrame.size.width)/2.0;
    confirmpwd_TextField.frame = conformpwdFrame;
    
    CGRect emailFrame = email_TextField.frame;
    emailFrame.origin.x = (firsttimelogin_ContainerView.frame.size.width - emailFrame.size.width)/2.0;
    email_TextField.frame = emailFrame;
    
    CGRect stateFrame = state_TextField.frame;
    stateFrame.origin.x = (firsttimelogin_ContainerView.frame.size.width - stateFrame.size.width)/2.0;
    state_TextField.frame = stateFrame;
    state_TextField.text = @"";
    
    CGRect tCFrame = tc_TextView.frame;
    tCFrame.origin.x = (firsttimelogin_ContainerView.frame.size.width - tCFrame.size.width)/2.0;
    tc_TextView.frame = tCFrame;
    
    
    CGRect submitBtnFrame = firsttimeLogin_submitButton.frame;
    submitBtnFrame.origin.x = (firsttimelogin_ContainerView.frame.size.width - submitBtnFrame.size.width)/2.0;
    firsttimeLogin_submitButton.frame = submitBtnFrame;
    
    
    
    processing_View.hidden = YES;
    
    checkedImage   = [UIImage imageNamed:@"checkbox_on.png"];
    uncheckedImage = [UIImage imageNamed:@"checkbox.png"];
    
    //  [self.checkButton setBackgroundImage:uncheckedImage forState:UIControlStateNormal];
    // [self.checkButton setImage:uncheckedImage forState:UIControlStateNormal];
    
    
    
    
    [self.view addSubview:firsttimelogin_ContainerView];
    
    CGRect ContainerFrame = firsttimelogin_ContainerView.frame;
    ContainerFrame.origin.y = -ContainerFrame.size.height;
    firsttimelogin_ContainerView.frame = ContainerFrame;
    
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = firsttimelogin_ContainerView.frame;
        if([appDelegate isIphone5])
        {
            frame.origin.y = self.view.frame.size.height - frame.size.height-45;
        }
        else
        {
            frame.origin.y=05;
        }
        firsttimelogin_ContainerView.frame = frame;
        //loginContainerView.center = self.view.center;
    }
                     completion:^(BOOL finished)
     {
         
     }];
}

//=========================================//



- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    NSLog(@"%d",buttonIndex);
    if(buttonIndex == 0)
    {
        
        
        
        NSLog(@"country::%@",country);
        NSLog(@"state::%@",state);
        self.state_TextField.text = [NSString stringWithFormat:@"%@,%@",country,state];
        
        
        if([country isEqualToString:@"Australia"])
        {
            country_str = @"Australia";
            
            
            if ([state isEqualToString:@"Australian Capital Territory"])
                state_str = @"ACT";
            else if ([state isEqualToString:@"New South Wales"])
                state_str = @"NSW";
            else  if ([state isEqualToString:@"Northern Territory"])
                state_str = @"NT";
            else if ([state isEqualToString:@"Queensland"])
                state_str = @"QLD";
            else if ([state isEqualToString:@"South Australia"])
                state_str = @"SA";
            else if ([state isEqualToString:@"Tasmania"])
                state_str = @"TAS";
            else if ([state isEqualToString:@"Victoria"])
                state_str = @"VIC";
            else
                state_str = @"WA";
            
        }
        
        
        else if([country isEqualToString:@"Hongkong"])
        {
            
            country_str = @"HK";
            state_str = @"HK";
        }
        
        
        
        else if([country isEqualToString:@"India"])
        {
            
            
            country_str = @"IN";
            
            if ([state isEqualToString:@"AndhraPradesh"])
                state_str = @"AP";
            else if ([state isEqualToString:@"Delhi"])
                state_str = @"DL";
            else if ([state isEqualToString:@"Gujarat"])
                state_str = @"GJ";
            else if ([state isEqualToString:@"Haryana"])
                state_str = @"HR";
            else if ([state isEqualToString:@"Karnataka"])
                state_str = @"KA";
            else if ([state isEqualToString:@"Maharastra"])
                state_str = @"MH";
            else if ([state isEqualToString:@"TamilNadu"])
                state_str = @"TN";
            else if ([state isEqualToString:@"UttarPradesh"])
                state_str = @"UP";
            else
                state_str = @"WB";
            
            
            
        }
        else if([country isEqualToString:@"New Zealand"])
        {
            
            country_str = @"NZ";
            
            if ([state isEqualToString:@"Auckland"])
                state_str = @"AUK";
            else  if ([state isEqualToString:@"Bay of plenty"])
                state_str = @"BOP";
            else  if ([state isEqualToString:@"Canterbury"])
                state_str = @"CAN";
            else  if ([state isEqualToString:@"Fiordland"])
                state_str = @"FIL";
            else  if ([state isEqualToString:@"Gisborne"])
                state_str = @"GIS";
            else if ([state isEqualToString:@"Hawkes Bay"])
                state_str = @"HKB";
            else  if ([state isEqualToString:@"Marlborough"])
                state_str = @"MBH";
            else  if ([state isEqualToString:@"Manawattu"])
                state_str = @"MWT";
            else  if ([state isEqualToString:@"Nelson & Bays"])
                state_str = @"NAB";
            else  if ([state isEqualToString:@"Northland"])
                state_str = @"NTL";
            else  if ([state isEqualToString:@"otago"])
                state_str = @"OTA";
            else  if ([state isEqualToString:@"Southland"])
                state_str = @"STL";
            else  if ([state isEqualToString:@"Taranaki"])
                state_str = @"TKI";
            else  if ([state isEqualToString:@"Timaru"])
                state_str = @"TMR";
            else  if ([state isEqualToString:@"Tasman"])
                state_str = @"TSM";
            else  if ([state isEqualToString:@"wanganui"])
                state_str = @"WGI";
            else if ([state isEqualToString:@"Willington"])
                state_str = @"WGN";
            else  if ([state isEqualToString:@"Waikato"])
                state_str = @"WKO";
            else  if ([state isEqualToString:@"Wairapapa"])
                state_str = @"WPP";
            
            else
                state_str = @"WTC";
            
            
            
        }
        else
        {                           //Philippines
            country_str = @"PH";
            
            if ([state isEqualToString:@"Luzon"])
                state_str = @"LUZ";
            else  if ([state isEqualToString:@"Mindanao"])
                state_str = @"MIN";
            else if ([state isEqualToString:@"NCR"])
                state_str = @"NCR";
            else
                state_str = @"VIS";
            
        }
        
        
        country = nil;
        state = nil;
        [asheet removeFromSuperview];
        [self bringTheFirstTimeLoginViewDown];
    }
    
    
    
}
//===========PICKERVIEW  DELEGATE METHODS =======================//
- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == 0)
        return 105;
    else
        return 185;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    
    if(component == 0)
    {
        
        country =[[NSString alloc] initWithFormat:@"%@" , [Countries_List objectAtIndex:row]];
        state =@"";
        [pickerView reloadComponent:1];
        if ([country isEqualToString:@"India"]) {
            
            state = [indian_StatesList objectAtIndex:0];
        }
        if ([country isEqualToString:@"Australia"]) {
            state = [australian_StatesList objectAtIndex:0];
        }
        if ([country isEqualToString:@"Hongkong"]) {
            state = [hongkong_StatesList objectAtIndex:0];
        }
        if ([country isEqualToString:@"New Zealand"]) {
            state = [newZealand_StatesList objectAtIndex:0];
        }
        if ([country isEqualToString:@"Philippines"]) {
            state = [philippines_StatesList objectAtIndex:0];
        }
        [pickerView selectRow:0 inComponent:1 animated:YES];
    }
    if (component == 1) {
        //comp2Row = row;
        if ([country isEqualToString:@"India"]) {
            state = [indian_StatesList objectAtIndex:row];
        }
        if ([country isEqualToString:@"Australia"]) {
            state = [australian_StatesList objectAtIndex:row];
        }
        if ([country isEqualToString:@"Hongkong"]) {
            state = [hongkong_StatesList objectAtIndex:row];
        }
        if ([country isEqualToString:@"New Zealand"]) {
            state = [newZealand_StatesList objectAtIndex:row];
        }
        if ([country isEqualToString:@"Philippines"]) {
            state = [philippines_StatesList objectAtIndex:row];
        }
        
        
        
    }
    NSLog(@"Selected::%@",[[NSString alloc]initWithFormat:@"%@,%@",country,state]);
    if([country isEqualToString:@"Australia"])
    {
        country_str = @"Australia";
        
        
        if ([state isEqualToString:@"Australian Capital Territory"])
            state_str = @"ACT";
        else if ([state isEqualToString:@"New South Wales"])
            state_str = @"NSW";
        else  if ([state isEqualToString:@"Northern Territory"])
            state_str = @"NT";
        else if ([state isEqualToString:@"Queensland"])
            state_str = @"QLD";
        else if ([state isEqualToString:@"South Australia"])
            state_str = @"SA";
        else if ([state isEqualToString:@"Tasmania"])
            state_str = @"TAS";
        else if ([state isEqualToString:@"Victoria"])
            state_str = @"VIC";
        else
            state_str = @"WA";
        
    }
    
    
    else if([country isEqualToString:@"Hongkong"])
    {
        
        country_str = @"HK";
        state_str = @"HK";
    }
    
    
    
    else if([country isEqualToString:@"India"])
    {
        
        
        country_str = @"IN";
        
        if ([state isEqualToString:@"AndhraPradesh"])
            state_str = @"AP";
        else if ([state isEqualToString:@"Delhi"])
            state_str = @"DL";
        else if ([state isEqualToString:@"Gujarat"])
            state_str = @"GJ";
        else if ([state isEqualToString:@"Haryana"])
            state_str = @"HR";
        else if ([state isEqualToString:@"Karnataka"])
            state_str = @"KA";
        else if ([state isEqualToString:@"Maharastra"])
            state_str = @"MH";
        else if ([state isEqualToString:@"TamilNadu"])
            state_str = @"TN";
        else if ([state isEqualToString:@"UttarPradesh"])
            state_str = @"UP";
        else
            state_str = @"WB";
        
        
        
    }
    else if([country isEqualToString:@"New Zealand"])
    {
        
        country_str = @"NZ";
        
        if ([state isEqualToString:@"Auckland"])
            state_str = @"AUK";
        else  if ([state isEqualToString:@"Bay of plenty"])
            state_str = @"BOP";
        else  if ([state isEqualToString:@"Canterbury"])
            state_str = @"CAN";
        else  if ([state isEqualToString:@"Fiordland"])
            state_str = @"FIL";
        else  if ([state isEqualToString:@"Gisborne"])
            state_str = @"GIS";
        else if ([state isEqualToString:@"Hawkes Bay"])
            state_str = @"HKB";
        else  if ([state isEqualToString:@"Marlborough"])
            state_str = @"MBH";
        else  if ([state isEqualToString:@"Manawattu"])
            state_str = @"MWT";
        else  if ([state isEqualToString:@"Nelson & Bays"])
            state_str = @"NAB";
        else  if ([state isEqualToString:@"Northland"])
            state_str = @"NTL";
        else  if ([state isEqualToString:@"otago"])
            state_str = @"OTA";
        else  if ([state isEqualToString:@"Southland"])
            state_str = @"STL";
        else  if ([state isEqualToString:@"Taranaki"])
            state_str = @"TKI";
        else  if ([state isEqualToString:@"Timaru"])
            state_str = @"TMR";
        else  if ([state isEqualToString:@"Tasman"])
            state_str = @"TSM";
        else  if ([state isEqualToString:@"wanganui"])
            state_str = @"WGI";
        else if ([state isEqualToString:@"Willington"])
            state_str = @"WGN";
        else  if ([state isEqualToString:@"Waikato"])
            state_str = @"WKO";
        else  if ([state isEqualToString:@"Wairapapa"])
            state_str = @"WPP";
        
        else
            state_str = @"WTC";
        
        
        
    }
    else
    {                           //Philippines
        country_str = @"PH";
        
        if ([state isEqualToString:@"Luzon"])
            state_str = @"LUZ";
        else  if ([state isEqualToString:@"Mindanao"])
            state_str = @"MIN";
        else if ([state isEqualToString:@"NCR"])
            state_str = @"NCR";
        else
            state_str = @"VIS";
        
    }
    
    NSLog(@"State is : %@ State Code :%@",state,state_str);
    state_TextField.text=[[NSString alloc]initWithFormat:@"%@,%@",country,state];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 0.0f, 290.0f, 60.0f)]; //x and width are mutually correlated
    label.textAlignment = NSTextAlignmentCenter;
    
    
    if (component==0) {
        
        label.text = [Countries_List objectAtIndex:row];
        //return label;
    }
	else
    {
        if ([country isEqualToString:@"India"])
            //return [indian_StatesLabels_List objectAtIndex:row];
            label.text = [indian_StatesList objectAtIndex:row];
        
        if ([country isEqualToString:@"Australia"])
            //return [australian_StatesLabels_List objectAtIndex:row];
            label.text = [australian_StatesList objectAtIndex:row];
        
        if ([country isEqualToString:@"Hongkong"])
            //return [hongkong_StatesLabels_List objectAtIndex:0];
            label.text = [hongkong_StatesList objectAtIndex:row];
        
        if ([country isEqualToString:@"New Zealand"])
            //return [newZealand_StatesLabels_List objectAtIndex:row];
            label.text = [newZealand_StatesList objectAtIndex:row];
        
        if([country isEqualToString:@"Philippines"])
            //return [philippines_StatesLabels_List objectAtIndex:row];
            label.text = [philippines_StatesList objectAtIndex:row];
        
        
        
        
    }
    return label;
}

-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
        return [Countries_List count];
    
    else {
        if ([country isEqualToString:@"India"]) {
            return [indian_StatesList count];
        }
        if ([country isEqualToString:@"Australia"]) {
            return [australian_StatesList count];
        }
        if ([country isEqualToString:@"Hongkong"]) {
            return [hongkong_StatesList count];
        }
        if ([country isEqualToString:@"New Zealand"]) {
            return  [newZealand_StatesList count];
        }
        if ([country isEqualToString:@"Philippines"]) {
            return [philippines_StatesList count];
        }
        
    }
    
    return 0;
}

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}
/*
 -(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
 {
 if (component==0)
 
 return [Countries_List objectAtIndex:row];
 
 else {
 
 if ([country isEqualToString:@"India"]) {
 return [indian_StatesList objectAtIndex:row];
 }
 if ([country isEqualToString:@"Australia"]) {
 return [australian_StatesList objectAtIndex:row];
 }
 if ([country isEqualToString:@"Hongkong"]) {
 return [hongkong_StatesList objectAtIndex:row];
 }
 if ([country isEqualToString:@"New Zealand"]) {
 return [newZealand_StatesList objectAtIndex:row];
 }
 if ([country isEqualToString:@"Philippines"]) {
 return [philippines_StatesList objectAtIndex:row];
 }
 
 }
 return 0;
 }*/

//=========================================//
//===========UserDataXMLPARSER  DELEGATE METHODS =======================//

#pragma mark -- UserDataXMLParser Delegate methods

- (void)parsingUserDetailsFinished:(User *)userDetails
{
    
    [self dismissLoginContainerWithUserDetails:userDetails password:password];
    
}

- (void)userDetailXMLparsingFailed
{
    
}


- (void)parser:(NSXMLParser *)parserLocal parseErrorOccurred:(NSError *)parseError
{
    parser.delegate = nil;
    parser = nil;
}

- (void)parserDidEndDocument:(NSXMLParser *)parserLocal
{
    parser.delegate = nil;
    parser = nil;
}
NSMutableString *parserElementString;
//=========================================//

- (void)dealloc {
    
    
    
    if (logingRequest) {
        [logingRequest cancel];
        logingRequest = nil;
    }
    
    if (parser) {
        parser.delegate = nil;
        parser = nil;
    }
    
    if (parserDelegate) {
        parserDelegate.delegate = nil;
        parserDelegate = nil;
    }
    
    
    Countries_List = nil;
    australian_StatesList=nil;
    indian_StatesList=nil;
    hongkong_StatesList=nil;
    philippines_StatesList = nil;
    newZealand_StatesList=nil;
    
    
}

/*
 <?xml version="1.0" encoding="UTF-8" ?>
 <root>
 <user>
 <id>4315412</id>
 <client_id>24</client_id>
 <domain_id>16</domain_id>
 <type>Member</type>
 <username>sasi</username>
 <email>avenkatsasi@gmail.com</email>
 <first_name>sasi</first_name>
 <last_name>v</last_name>
 <state>VIC</state>
 <country>Australia</country>
 <mobile></mobile>
 <card_ext>jpg</card_ext>
 <client_name>My Rewards</client_name>
 <newsletter>0</newsletter>
 </user>
 <user>
 <id>4315413</id>
 <client_id>716</client_id>
 <domain_id>8</domain_id>
 <type>Member</type>
 <username>sasi</username>
 <email>avenkatsasi@gmail.com</email>
 <first_name>sasi</first_name>
 <last_name>V</last_name>
 <state>VIC</state>
 <country>Australia</country>
 <mobile></mobile>
 <card_ext></card_ext>
 <client_name>@work</client_name>
 <newsletter>0</newsletter>
 </user>
 </root>
 */

- (void)viewDidUnload {
    [self setFirsttimeloginButton:nil];
    bgView = nil;
    [self setBgView:nil];
    [self setReferralno_TextField:nil];
    [super viewDidUnload];
}


@end
