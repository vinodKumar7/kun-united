//
//  MyIssueViewController.h
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>


//@class MyUnionViewController;
@interface MyIssueViewController : UIViewController< UITextFieldDelegate,UITableViewDelegate,UITextViewDelegate,UITableViewDataSource,UIAlertViewDelegate,UIActionSheetDelegate,UIActionSheetDelegate, MFMailComposeViewControllerDelegate,UIPickerViewDelegate,UIPickerViewDataSource >
{
    NSMutableArray *contactsList;
    NSMutableArray *locationArray;
}
//@property(unsafe_unretained) MyUnionViewController *unionViewController;

@property (nonatomic, strong) NSMutableArray *contactsList;

@property(nonatomic,strong)IBOutlet UITextView *myTextView;
@property(nonatomic,strong)IBOutlet UIImageView *textViewBackground;
@property(nonatomic,strong)IBOutlet UIView *accessoryView;

@property(nonatomic,strong)IBOutlet UIView *containerView;
@property(nonatomic,strong)IBOutlet UIImageView *background_ImageView;


@property (strong, nonatomic) IBOutlet UIImageView *topImageView;

@property(nonatomic,strong)IBOutlet UIButton *send_Button;
@property(nonatomic,strong)IBOutlet UIButton *add_Button;
@property(nonatomic,strong)IBOutlet UIButton *delete_Button;

@property (strong, nonatomic) IBOutlet UILabel *vehicleLabel;
@property (strong, nonatomic) IBOutlet UILabel *dateLabel;
@property (strong, nonatomic) IBOutlet UILabel *serviceLabel;
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;
@property (strong, nonatomic) IBOutlet UILabel *commentLabel;
@property (strong, nonatomic) IBOutlet UIImageView *namebackimageView;
@property (strong, nonatomic) IBOutlet UILabel *RegistrationLabel;

@property (strong, nonatomic) IBOutlet UIImageView *memnobackImageView;
@property(nonatomic,strong)IBOutlet UILabel *nameLabel;
@property(nonatomic,strong)IBOutlet UILabel *membershipLabel;

@property (strong, nonatomic) IBOutlet UITextField *registration_TextField;
@property (strong, nonatomic) IBOutlet UITextField *vechicle_TextField;
@property (strong, nonatomic) IBOutlet UITextField *date_TextField;
@property (strong, nonatomic) IBOutlet UITextField *service_TextField;
@property (strong, nonatomic) IBOutlet UITextField *location_Textfield;
@property (strong, nonatomic) IBOutlet UITextField *current_Textfield;

//- (void) hideHeaderView:(BOOL) hide;

-(IBAction)doneButton_Pressed:(id)sender;
-(IBAction)sendButton_Pressed:(id)sender;
-(IBAction)deleteButton_Pressed:(id)sender;
- (IBAction)addDateAction:(id)sender;
- (IBAction)addServiceAction:(id)sender;
- (IBAction)addLocationAction:(id)sender;

@end
