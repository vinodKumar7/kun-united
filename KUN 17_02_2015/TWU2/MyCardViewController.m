//
//  MyCardViewController.m
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//
#import <QuartzCore/QuartzCore.h>
#import "MyCardViewController.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"

@interface MyCardViewController ()
{
    ASIFormDataRequest *imageRequest;
    AppDelegate *appDelegate;
}
- (void) myCardsDisplay;
@end

@implementation MyCardViewController
@synthesize background;
@synthesize containerView;
@synthesize background_ImageView;
@synthesize myCardImageView, userNameLabel, clientIdLabel,userNameLabel1;
@synthesize activityView;
@synthesize activityIndicatorView;
@synthesize loadingView;
//=========================DELEGATE METHODS=============================//

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
        [[self navigationController] setNavigationBarHidden:NO animated:NO];
    
//    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
//    if ([[ver objectAtIndex:0] intValue] >= 7)
//    {
//        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];        self.navigationController.navigationBar.translucent = NO;
//    }
//    else
//    {
//        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
//    }
 /*   if([self.navigationController.navigationBar respondsToSelector:@selector(barTintColor)])
    {
        // iOS7
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
        self.navigationController.navigationBar.translucent = NO;
    }
    else
    {
        // older
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
    }*/
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"My Card";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor whiteColor]];
    self.navigationItem.titleView = TitleLabel;
        
}
-(void)back
{
    
    
    //[activityIndicatorView startAnimating];
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}

- (void)viewDidLoad
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    NSLog(@"=====MY Card view=====");
    [self loadViews];
    [self.background_ImageView setHidden:YES];
   
    [super viewDidLoad];
    

  
   
    [self showActivityView];
    [self performSelector:@selector(myCardsDisplay) withObject:nil afterDelay:0.06];
    
    // Do any additional setup after loading the view from its nib.
}
//===================================================//

//===============DELEGATE METHODS TO LOCK ORIENTATION=================//
-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}
//-(BOOL)s

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

- (BOOL) shouldAutorotate
{
    return NO;
}
//==========================================================================================//
//=======================METHOD TO LOAD VIEWS IN IPHONE  ==================================//

-(void)loadViews
{
       
    
        if ([appDelegate isIphone5])//condition to load in iphone 5
        {
            NSLog(@"====UIDeviceResolution_iPhoneRetina5====");
            
            CGRect frame =self.background_ImageView.frame;// To load background Image for iphone 5
            frame.size.height = 505;
            self.background_ImageView.frame = frame;
     
            CGRect cardframe =self.myCardImageView.frame;// To load card view for iphone 5
           
            cardframe.size.height=200;
           
           
           // cardframe.size.width=200;
            cardframe.origin.y = 80;
            cardframe.origin.x=10;
            //NSLog(@"Card width")
            self.myCardImageView.frame = cardframe;

        }
        else                                           //condition to load view other than iphone 5
        {
            
                        
         
            NSLog(@"====UIDeviceResolution_iPhoneRetina4====");
            NSLog(@"====UIDeviceResolution_iPhoneRetina4====");
            CGRect containerframe = self.background.frame;
            containerframe.origin.y=10;
            containerframe.size.height = 380;
            self.background.frame = containerframe;
            
            
            CGRect cardframe =self.myCardImageView.frame;// To load card view for iphone 5
            cardframe.size.height=280;
            // cardframe.size.width=200;
            cardframe.origin.y = 20;
            cardframe.origin.x=10;
            //NSLog(@"Card width")
            self.myCardImageView.frame = cardframe;


            
            
        }
        
        
        // Do any additional setup after loading the view from its nib.
    
       
}
//=======================================================================//
//============METHOD TO DISPLAY CARD VIEW IN IPHONE 4 & 5 =========//

- (void) myCardsDisplay
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@.%@",MyCard_URL_Prefix,appDelegate.sessionUser.client_id,appDelegate.sessionUser.card_ext];//Retriving Image URL to String
    
    NSLog(@"Image URL: %@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];// Image URL
    
    
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];// Assigning "URL data" to "data"
    UIImage *img = [[UIImage alloc] initWithData:data];       // Retriving image from "data"
    myCardImageView.image = img;
    //Assigning user name
    NSLog(@"%@,%@",appDelegate.sessionUser.first_name, appDelegate.sessionUser.last_name);
    NSString *userNameStr = [NSString stringWithFormat:@"Name: %@ %@",appDelegate.sessionUser.first_name, appDelegate.sessionUser.last_name];
    userNameLabel.text = userNameStr;
    userNameLabel.transform = CGAffineTransformMakeRotation (-3.14/2);
     NSLog(@"%@",appDelegate.sessionUser.client_name); 
                                                             //Assigning client name
    clientIdLabel.text = [NSString stringWithFormat:@"Client: %@", appDelegate.sessionUser.client_name];                                 
    clientIdLabel.transform = CGAffineTransformMakeRotation (-3.14/2);
     NSLog(@"%@",appDelegate.sessionUser.username); 
                                                             //Assigning Membership
    userNameLabel1.text = [NSString stringWithFormat:@"Membership: %@", appDelegate.sessionUser.username];
    userNameLabel1.transform = CGAffineTransformMakeRotation (-3.14/2);
    
                                                            // Update clientIdLabel
    CGRect clientIdLabelFrame = clientIdLabel.frame;
    NSLog(@"myCardImageView.X::%f",myCardImageView.frame.origin.x);
    NSLog(@"clientIdLabelFrame.width::%f",clientIdLabelFrame.size.width);
    clientIdLabelFrame.origin.x = myCardImageView.frame.origin.x - clientIdLabelFrame.size.width+30;
    clientIdLabelFrame.origin.y = self.containerView.frame.origin.y + 100;
    //clientIdLabelFrame.origin.y = 100.0;
    clientIdLabel.frame = clientIdLabelFrame;
                                                              // Update FirstNameLabel                                                                                    
    CGRect userNameLabelFrame = userNameLabel.frame;
    userNameLabelFrame.origin.x = myCardImageView.frame.origin.x + myCardImageView.frame.size.width-24;
    //userNameLabelFrame.origin.y = 150.0;
    userNameLabelFrame.origin.y = self.containerView.frame.origin.y + 117;

    userNameLabel.frame = userNameLabelFrame;
                                                               // Update SecondNameLabel   
    CGRect userNameLabel1Frame = userNameLabel1.frame;
    userNameLabel1Frame.origin.x = userNameLabelFrame.origin.x + userNameLabel.frame.size.width;
    userNameLabel1Frame.origin.y = userNameLabelFrame.origin.y;
    userNameLabel1.frame = userNameLabel1Frame;
    [activityIndicatorView startAnimating];

    [self dismissActivityView];                                //dimiss Activityview
   
}
//=======================================================================//
//=============== ASIHttpRequest DELEGATE METHODS=================//

#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
   
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [failureAlert show];

    NSLog(@"Image Data -- requestFailed:");
    
}
//==================================================================================//

//============================ METHOD TO SHOW ACTIVITY VIEW   =============================//
- (void) showActivityView
{  // [NSObject performSelectorOnMainThread:@selector(SEL) withObject:nil waitUntilDone:YES];

      // [UIView setAnimationDelegate:self];
   // [UIView setAnimationDuration:.1];
   // [UIView setAnimationDelay:.1];

    CGRect frame =activityView.frame;
    self.activityView.frame=frame;
    
    [self.view addSubview:activityView];
    [NSThread detachNewThreadSelector: @selector(function) toTarget: self withObject: nil];

}
-(void)function
{
    [self.activityIndicatorView  performSelectorOnMainThread:@selector(startAnimating) withObject:nil waitUntilDone:NO];

//[self.activityIndicatorView startAnimating];
    NSLog(@"function");

}

- (void) dismissActivityView
{
    [activityView removeFromSuperview];
}
//==================================================================================//


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
   
    [self setActivityIndicatorView:nil];
    [self setLoadingView:nil];
    [super viewDidUnload];
}
@end
