//
//  SendAFriendViewController.h
//  TWU2
//
//  Created by MyRewards on 2/18/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>


@interface SendAFriendViewController : UIViewController<MFMailComposeViewControllerDelegate >
{
    
  //  __unsafe_unretained MyUnionViewController *unionViewController;
}
//@property(unsafe_unretained) MyUnionViewController *unionViewController;
@property (strong, nonatomic) IBOutlet UITextField *friendName;
@property (strong, nonatomic) IBOutlet UITextField *friendMail;
@property (strong, nonatomic) IBOutlet UIImageView *containerImageView;
@property (strong, nonatomic) IBOutlet UIButton *safButton;
@property(nonatomic,strong)IBOutlet UIImageView *background_ImageView;
@property(nonatomic,strong)IBOutlet UIView *containerView;
@property (strong, nonatomic) IBOutlet UIImageView *safimageView;
@property (strong, nonatomic) IBOutlet UIView *subView;

@property (strong, nonatomic) IBOutlet UIScrollView *safScrollview;
- (IBAction)closeWindow:(id)sender;
@property(nonatomic, strong)UITextField *currentTextField;
-(IBAction)sendAFriend_clicked:(id)sender;
- (IBAction)sendfriendDetails:(id)sender;
//- (void) performUpdateWithFriendDetails:(NSString *) fName fMail:(NSString *) lName;
@property (strong, nonatomic) IBOutlet UIImageView *safpageView;
- (IBAction)doneButtonClicked:(id)sender;
@end
