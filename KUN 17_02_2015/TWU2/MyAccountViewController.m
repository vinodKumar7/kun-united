//
//  MyAccountViewController.m
//  TWU2
//
//  Created by MyRewards on 2/22/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//
#import "MyAccountViewController.h"
#import "AppDelegate.h"
#import "ASIFormDataRequest.h"
#import <QuartzCore/QuartzCore.h>
#define kOFFSET_FOR_KEYBOARD 150

@interface MyAccountViewController ()
{
    BOOL stayup;
    AppDelegate *appDelegate;
    
    UIScreen *mainScreen;
    CGFloat scale;
    CGFloat pixelHeight;
    UIActionSheet *asheet;
    NSString *FirstName;
    NSString *LastName;
    
    NSArray *indian_StatesList;
    NSArray *australian_StatesList;
    NSArray *hongkong_StatesList;
    NSArray *philippines_StatesList;
    NSArray *newZealand_StatesList;
    NSArray *Countries_List;
    
   
    
    
    NSString *country;
    NSString *state;
    NSString *country_str;
    NSString *state_str;
    UIView   *pickerContainerView;
    
}
- (BOOL) validateLogin;
@end

@implementation MyAccountViewController
@synthesize firstnameLabel;
@synthesize lastnameLabel;
@synthesize emailLabel;
@synthesize stateLabel;
@synthesize memnoLabel;
@synthesize noteLabel;
@synthesize myScrollView;
@synthesize submit_Button;
@synthesize firstName_TextFeild;
@synthesize lastName_TextFeild;
@synthesize email_TextFeild;
@synthesize state_TextFeild;
@synthesize memberShip_TextFeild;
@synthesize background_ImageView;
@synthesize currentTextFeild;

//=========================DELEGATE METHODS=============================//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    state_str = @"";
    country_str = @"";
    country = @"Australia";
    state = @"";
    
    Countries_List = [[NSArray alloc]initWithObjects:@"Australia",@"Hongkong",@"India",@"New Zealand",@"Philippines", nil];
    
    indian_StatesList = [[NSArray alloc]initWithObjects:@"AndhraPradesh",@"Delhi",@"Gujarat",@"Haryana",@"Karnataka",@"Maharastra",@"TamilNadu",@"UttarPradesh",@"WestBengal",nil];
    
    australian_StatesList = [[NSArray alloc]initWithObjects:@"Australian Capital Territory",@"New South Wales",@"Northern Territory",@"Queensland",@"South Australia",@"Tasmania",@"Victoria",@"Western Australia", nil];
    
    hongkong_StatesList = [[NSArray alloc]initWithObjects:@"Hongkong", nil];
    
    philippines_StatesList = [[NSArray alloc]initWithObjects:@"Luzon",@"Mindanao",@"NCR",@"Visayas", nil];
    
    newZealand_StatesList = [[NSArray alloc]initWithObjects:@"Auckland",@"Bay of plenty",@"Canterbury",@"Fiordland",@"Gisborne",@"Hawkes Bay",@"Marlborough",@"Manawattu",@"Nelson & Bays",@"Northland",@"otago",@"Southland",@"Taranaki",@"Timaru",@"Tasman",@"wanganui",@"Willington",@"Waikato",@"Wairapapa",@"West Coast", nil];
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self loadViews];

}

-(void) textFieldsContent
{
    //=======CONFIGURE TEXTFIELDS
    self.firstName_TextFeild.text = appDelegate.sessionUser.first_name;
    self.lastName_TextFeild.text = appDelegate.sessionUser.last_name;
    self.email_TextFeild.text = appDelegate.sessionUser.email;
    
    NSString *statencountry=[NSString stringWithFormat:@"%@,%@",appDelegate.sessionUser.country,appDelegate.sessionUser.state];
    NSLog(@"country is %@ ",appDelegate.sessionUser.country);
    NSLog(@"country is %@",appDelegate.sessionUser.state);
    NSLog(@"appDelegate.sessionUser.state  %@",statencountry);
    if([appDelegate.sessionUser.country isEqualToString:@"Australia"])
    {
        appDelegate.sessionUser.country = @"Australia";
        
        
        if ([appDelegate.sessionUser.state isEqualToString:@"ACT"])
            appDelegate.sessionUser.state = @"Australian Capital Territory";
        else if ([appDelegate.sessionUser.state isEqualToString:@"NSW"])
            appDelegate.sessionUser.state = @"New South Wales";
        else  if ([state isEqualToString:@"NT"])
            appDelegate.sessionUser.state = @"Northern Territory";
        else if ([appDelegate.sessionUser.state isEqualToString:@"QLD"])
            appDelegate.sessionUser.state = @"Queensland";
        else if ([appDelegate.sessionUser.state isEqualToString:@"SA"])
            appDelegate.sessionUser.state = @"South Australia";
        else if ([appDelegate.sessionUser.state isEqualToString:@"TAS"])
            appDelegate.sessionUser.state = @"Tasmania";
        else if ([appDelegate.sessionUser.state isEqualToString:@"VIC"])
            appDelegate.sessionUser.state = @"Victoria";
        else if ([appDelegate.sessionUser.state isEqualToString:@"WA"])
            appDelegate.sessionUser.state = @"Western Australia";
        
    }
    
    
    else if([appDelegate.sessionUser.country isEqualToString:@"Hongkong"])
    {
        
        country_str = @"Hongkong";
        state_str = @"HK";
    }
    
    
    
    else if([appDelegate.sessionUser.country isEqualToString:@"India"])
    {
        
        
        appDelegate.sessionUser.country = @"India";
        
        if ([appDelegate.sessionUser.state isEqualToString:@"AP"])
            appDelegate.sessionUser.state = @"AndhraPradesh";
        else if ([appDelegate.sessionUser.state isEqualToString:@"DL"])
            appDelegate.sessionUser.state = @"Delhi";
        else if ([appDelegate.sessionUser.state isEqualToString:@"GJ"])
            appDelegate.sessionUser.state = @"Gujarat";
        else if ([appDelegate.sessionUser.state isEqualToString:@"HR"])
            appDelegate.sessionUser.state = @"Haryana";
        else if ([appDelegate.sessionUser.state isEqualToString:@"KA"])
            appDelegate.sessionUser.state = @"Karnataka";
        else if ([appDelegate.sessionUser.state isEqualToString:@"MH"])
            appDelegate.sessionUser.state = @"Maharastra";
        else if ([appDelegate.sessionUser.state isEqualToString:@"TN"])
            appDelegate.sessionUser.state = @"TamilNadu";
        else if ([appDelegate.sessionUser.state isEqualToString:@"UP"])
            appDelegate.sessionUser.state = @"UttarPradesh";
        else if ([appDelegate.sessionUser.state isEqualToString:@"WB"])
            appDelegate.sessionUser.state = @"West Bengal";
        
        
        
        
    }
    else if([appDelegate.sessionUser.country isEqualToString:@"New Zealand"])
    {
        
        country_str = @"New Zealand";
        
        if ([appDelegate.sessionUser.state isEqualToString:@"AUK"])
            appDelegate.sessionUser.state = @"Auckland";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"BOP"])
            appDelegate.sessionUser.state = @"Bay of plenty";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"CAN"])
            appDelegate.sessionUser.state = @"Canterbury";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"FIL"])
            appDelegate.sessionUser.state = @"Fiordland";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"GIS"])
            appDelegate.sessionUser.state = @"Gisborne";
        else if ([appDelegate.sessionUser.state isEqualToString:@"HKB"])
            appDelegate.sessionUser.state = @"Hawkes Bay";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"MBH"])
            appDelegate.sessionUser.state = @"Marlborough";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"MWT"])
            appDelegate.sessionUser.state = @"Manawattu";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"NAB"])
            appDelegate.sessionUser.state = @"Nelson & Bays";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"NTL"])
            appDelegate.sessionUser.state = @"Northland";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"OTA"])
            appDelegate.sessionUser.state = @"otago";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"STL"])
            appDelegate.sessionUser.state = @"Southland";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"TKI"])
            appDelegate.sessionUser.state = @"Taranaki";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"TMR"])
            appDelegate.sessionUser.state = @"Timaru";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"TSM"])
            appDelegate.sessionUser.state = @"Tasman";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"WGI"])
            appDelegate.sessionUser.state = @"wanganui";
        else if ([appDelegate.sessionUser.state isEqualToString:@"WGN"])
            appDelegate.sessionUser.state = @"Willington";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"WKO"])
            appDelegate.sessionUser.state = @"Waikato";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"WPP"])
            appDelegate.sessionUser.state = @"Wairapapa";
        
        else if ([appDelegate.sessionUser.state isEqualToString:@"WTC"])
            appDelegate.sessionUser.state = @"West Coast";
        
        
        
    }
    else if([appDelegate.sessionUser.country isEqualToString:@"Philippines"])
    {                           //Philippines
        country_str = @"Philippines";
        
        if ([appDelegate.sessionUser.state isEqualToString:@"Luzon"])
            state_str = @"LUZ";
        else  if ([appDelegate.sessionUser.state isEqualToString:@"Mindanao"])
            state_str = @"MIN";
        else if([appDelegate.sessionUser.state isEqualToString:@"Visayas"])
            state_str = @"VIS";
        else
            state_str = @"NCR";
        
    }
    NSString *statetext=[[NSString stringWithFormat:@"%@,%@",appDelegate.sessionUser.country,appDelegate.sessionUser.state] stringByReplacingOccurrencesOfString:@",(null)" withString:@""];
    NSLog(@"state textfield is................ %@ ", statetext);
    NSString *statetext1=[statetext stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    self.state_TextFeild.text = statetext1;
    //[NSString stringWithFormat:@"%@,%@",appDelegate.sessionUser.country,appDelegate.sessionUser.state];
    
    NSLog(@"state textfield is %@ ", statetext1);
    //============================
    
    NSLog(@"appDelegate.sessionUser.userId is========>>>>>> : %@",appDelegate.sessionUser.userId);
    
    
    [self configureCountryPicker];
}


-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [self textFieldsContent];  //Displaying data on MyAccount TextFields..........
    
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
    UIImage *myImage2 = [UIImage imageNamed:@"logout-4.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
	myButton2.showsTouchWhenHighlighted = YES;
	myButton2.frame = CGRectMake(110.0, 3.0, 50,30);
	[myButton2 addTarget:self action:@selector(presentLogoutController) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
//    self.navigationController.navigationBar.translucent = NO;
//    [[self navigationController] setNavigationBarHidden:NO animated:NO];
//    
//    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
//    if ([[ver objectAtIndex:0] intValue] >= 7)
//    {
//        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];        self.navigationController.navigationBar.translucent = NO;
//    }
//    else
//    {
//        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
//    }
     /*   NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
        if ([[ver objectAtIndex:0] intValue] >= 7)
        {
            self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
        }
    
    if([self.navigationController.navigationBar respondsToSelector:@selector(barTintColor)])
    {
        // iOS7
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
        self.navigationController.navigationBar.translucent = NO;
    }
    else
    {
        // older
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
    }*/
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"My Account";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor whiteColor]];;
    self.navigationItem.titleView = TitleLabel;
    
    //[self settingPickerViewLabels];
}
/*
-(void)settingPickerViewLabels{
    
    
    [CountriesLabels_List removeAllObjects];
    [indian_StatesLabels_List removeAllObjects];
    [australian_StatesLabels_List removeAllObjects];
    [hongkong_StatesLabels_List removeAllObjects];
    [philippines_StatesLabels_List removeAllObjects];
    [newZealand_StatesLabels_List removeAllObjects];
    
    UILabel *theview;
    for (int i=0;i<=4;i++) {
        theview = [[UILabel alloc] init];
        
		theview.text = [NSString stringWithFormat:@"%@",[Countries_List objectAtIndex:i]];
        //NSLog(@"%@",[NSString stringWithFormat:@"%@",[Countries_List objectAtIndex:i]]);
		theview.textColor = [UIColor blackColor];
		theview.frame = CGRectMake(0,0,200,200);
		theview.backgroundColor = [UIColor clearColor];
		//theview.textAlignment = UITextAlignmentCenter;
		theview.shadowColor = [UIColor whiteColor];
		theview.shadowOffset = CGSizeMake(-1,-1);
		//theview.adjustsFontSizeToFitWidth = YES;
		
		UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:15];
		[theview setFont:myFont];
        
        [CountriesLabels_List addObject:theview];
    }
    
    UILabel *theview1;
    for (int i=0;i<9;i++) {
        theview1 = [[UILabel alloc] init];
        
		theview1.text = [NSString stringWithFormat:@"%@",[indian_StatesList objectAtIndex:i]];
		theview1.textColor = [UIColor blackColor];
		theview1.frame = CGRectMake(0,0,200,200);
		theview1.backgroundColor = [UIColor clearColor];
		//theview1.textAlignment = UITextAlignmentCenter;
		theview1.shadowColor = [UIColor whiteColor];
		theview1.shadowOffset = CGSizeMake(-1,-1);
		//theview.adjustsFontSizeToFitWidth = YES;
		
		UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
		[theview1 setFont:myFont];
        
        [indian_StatesLabels_List addObject:theview1];
    }
    
    UILabel *theview2;
    for (int i=0;i<8;i++) {
        theview2 = [[UILabel alloc] init];
        
		theview2.text = [NSString stringWithFormat:@"%@",[australian_StatesList objectAtIndex:i]];
		theview2.textColor = [UIColor blackColor];
		theview2.frame = CGRectMake(0,0,200,200);
		theview2.backgroundColor = [UIColor clearColor];
		//theview2.textAlignment = UITextAlignmentCenter;
		theview2.shadowColor = [UIColor whiteColor];
		theview2.shadowOffset = CGSizeMake(-1,-1);
		//theview.adjustsFontSizeToFitWidth = YES;
		
		UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
		[theview2 setFont:myFont];
        
        [australian_StatesLabels_List addObject:theview2];
    }
    
    UILabel *theview3;
    for (int i=0;i<4;i++) {
        theview3 = [[UILabel alloc] init];
        
		theview3.text = [NSString stringWithFormat:@"%@",[philippines_StatesList objectAtIndex:i]];
		theview3.textColor = [UIColor blackColor];
		theview3.frame = CGRectMake(0,0,200,200);
		theview3.backgroundColor = [UIColor clearColor];
        //	theview3.textAlignment = UITextAlignmentCenter;
		theview3.shadowColor = [UIColor whiteColor];
		theview3.shadowOffset = CGSizeMake(-1,-1);
		//theview.adjustsFontSizeToFitWidth = YES;
		
		UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
		[theview3 setFont:myFont];
        
        [philippines_StatesLabels_List addObject:theview3];
    }
    
    UILabel *theview4;
    for (int i=0;i<20;i++) {
        theview4 = [[UILabel alloc] init];
        
		theview4.text = [NSString stringWithFormat:@"%@",[newZealand_StatesList objectAtIndex:i]];
		theview4.textColor = [UIColor blackColor];
		theview4.frame = CGRectMake(0,0,200,200);
		theview4.backgroundColor = [UIColor clearColor];
        //	theview4.textAlignment = UITextAlignmentCenter;
		theview4.shadowColor = [UIColor whiteColor];
		theview4.shadowOffset = CGSizeMake(-1,-1);
		//theview.adjustsFontSizeToFitWidth = YES;
		
		UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
		[theview4 setFont:myFont];
        
        [newZealand_StatesLabels_List addObject:theview4];
    }
    
    UILabel *theview5;
    theview5 = [[UILabel alloc] init];
    
    theview5.text = [NSString stringWithFormat:@"%@",[hongkong_StatesList objectAtIndex:0]];
    theview5.textColor = [UIColor blackColor];
    theview5.frame = CGRectMake(0,0,200,200);
    theview5.backgroundColor = [UIColor clearColor];
    // theview5.textAlignment = UITextAlignmentCenter;
    theview5.shadowColor = [UIColor whiteColor];
    theview5.shadowOffset = CGSizeMake(-1,-1);
    //theview.adjustsFontSizeToFitWidth = YES;
    
    UIFont *myFont = [UIFont fontWithName:@"Helvetica-Bold" size:14];
    [theview5 setFont:myFont];
    
    [hongkong_StatesLabels_List addObject:theview5];
    
    
    
}*/
-(void)back
{
    [currentTextFeild resignFirstResponder];
    [self setViewMovedUp:NO TextField:currentTextFeild];
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
    
}

-(void) presentLogoutController
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Logout?" message:@"Do you really want to logout?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    
    alert.tag = 111;
    
    [alert show];
    
}
//=======================================================================//
- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(20.0f, 0.0f, 290.0f, 60.0f)]; //x and width are mutually correlated
    label.textAlignment = NSTextAlignmentCenter;
    
    
    if (component==0) {
        
        label.text = [Countries_List objectAtIndex:row];
        //return label;
    }
	else
    {
        if ([country isEqualToString:@"India"])
            //return [indian_StatesLabels_List objectAtIndex:row];
            label.text = [indian_StatesList objectAtIndex:row];
        
        if ([country isEqualToString:@"Australia"])
            //return [australian_StatesLabels_List objectAtIndex:row];
            label.text = [australian_StatesList objectAtIndex:row];
        
        if ([country isEqualToString:@"Hongkong"])
            //return [hongkong_StatesLabels_List objectAtIndex:0];
            label.text = [hongkong_StatesList objectAtIndex:row];
        
        if ([country isEqualToString:@"New Zealand"])
            //return [newZealand_StatesLabels_List objectAtIndex:row];
            label.text = [newZealand_StatesList objectAtIndex:row];
        
        if([country isEqualToString:@"Philippines"])
            //return [philippines_StatesLabels_List objectAtIndex:row];
            label.text = [philippines_StatesList objectAtIndex:row];
        
        
        
        
    }
    return label;
}

/*//===============DELEGATE METHODS TO LOCK ORIENTATION============//
 -(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
 {
 return UIInterfaceOrientationIsPortrait(interfaceOrientation);
 }
 
 
 -(NSUInteger)supportedInterfaceOrientations
 {
 return  UIInterfaceOrientationMaskPortrait ;
 }
 
 -(void)orientationDetected:(UIEvent *)event
 {
 [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
 [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
 
 }
 
 - (BOOL) shouldAutorotate
 {
 return NO;
 }
 
 //=============================================================//*/

// ================METHOD TO LOAD VIEWS IN IPHONE  =====================//
-(void)loadViews
{
    
    
    
    
    if([appDelegate isIphone5])
    {
        // IPhone 5
        NSLog(@"Iphone5 View Loaded");
        self.view = [[NSBundle mainBundle] loadNibNamed:@"MyAccountViewController" owner:self options:nil][0];
        
        
        
        
    }
    else
    {
        //Iphone 4
        NSLog(@"Iphone4 View Loaded");
        self.view = [[NSBundle mainBundle] loadNibNamed:@"MyAccountViewController" owner:self options:nil][0];
        
        
        CGRect firstnameframe=self.firstnameLabel.frame;
        firstnameframe.origin.y=122;
        self.firstnameLabel.frame=firstnameframe;
        
        CGRect firstfieldframe=self.firstName_TextFeild.frame;
        firstfieldframe.origin.y=122;
        self.firstName_TextFeild.frame=firstfieldframe;
        
        
        CGRect lastnameframe=self.lastnameLabel.frame;
        lastnameframe.origin.y=160;
        self.lastnameLabel.frame=lastnameframe;
        
        CGRect lastfieldframe=self.lastName_TextFeild.frame;
        lastfieldframe.origin.y=160;
        self.lastName_TextFeild.frame=lastfieldframe;
        
        
        CGRect emailframe=self.emailLabel.frame;
        emailframe.origin.y=196;
        self.emailLabel.frame=emailframe;
        
        CGRect emailfieldframe=self.email_TextFeild.frame;
        emailfieldframe.origin.y=196;
        self.email_TextFeild.frame=emailfieldframe;
        
        CGRect stateframe=self.stateLabel.frame;
        stateframe.origin.y=234;
        self.stateLabel.frame=stateframe;
        
        CGRect statefieldframe=self.state_TextFeild.frame;
        statefieldframe.origin.y=234;
        self.state_TextFeild.frame=statefieldframe;
        
        CGRect memnoframe=self.memnoLabel.frame;
        memnoframe.origin.y=275;
        self.memnoLabel.frame=memnoframe;
        
        CGRect memnofieldframe=self.memberShip_TextFeild.frame;
        memnofieldframe.origin.y=273;
        self.memberShip_TextFeild.frame=memnofieldframe;
        
        CGRect noteframe=self.noteLabel.frame;
        noteframe.origin.y=305;
        self.noteLabel.frame=noteframe;
        
        CGRect buttonframe=self.submit_Button.frame;
        buttonframe.origin.y=344;
        self.submit_Button.frame=buttonframe;
        
        
    }
    
}
//===================================================================//

// =======================METHOD TO UPDATE USER DETAILS  ===========//
-(IBAction)submitButton_Pressed:(id)sender
{
    NSLog(@"=====SUBMIT BUTTON PRESSED=======");
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    if (![self validateLogin])
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fields Empty" message:@"All fields are mandatory  \nPlease fill all the fields." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    else if ([emailTest evaluateWithObject:email_TextFeild.text] == NO)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Fields Empty" message:@" Email should be in proper format. " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    else
        [self performUpdateWithUserName:self.firstName_TextFeild.text lastName:self.lastName_TextFeild.text];
    
    
    
}

//===========================================================//

// =================VALIDATING USER SUBMITING DETAILS ================//


- (BOOL) validateLogin
{
    
    BOOL success = YES;
    
    if (self.firstName_TextFeild.text.length == 0 || self.lastName_TextFeild.text.length == 0 || self.email_TextFeild.text.length == 0 || self.state_TextFeild.text.length == 0)
    {
        success = NO;
    }
    
    
    
    return success;
}

//===================================================================//


// ===============   POSTING USER DETAILS     =======================//
// ===============   POSTING USER DETAILS     =======================//
- (void) performUpdateWithUserName:(NSString *) fName lastName:(NSString *) lName
{
    
    FirstName = fName;
    LastName = lName;
    NSLog(@"state is %@ ",self.state_TextFeild.text);
    NSArray *subStrings;
    NSString *firstString;
    NSString *lastString;
    if ([self doesString:self.state_TextFeild.text containCharacter:','])// phNo having character '/'
    {
        subStrings = [self.state_TextFeild.text componentsSeparatedByString:@","]; //or rather @" - "
        firstString = [subStrings objectAtIndex:0];
        lastString=[subStrings objectAtIndex:1];
        
        NSLog(@"country is %@ and state is %@ ",firstString,lastString);
        if([firstString isEqualToString:@"Australia"])
        {
            firstString = @"Australia";
            
            
            if ([lastString  isEqualToString:@"Australian Capital Territory"])
                lastString  = @"ACT";
            else if ([lastString  isEqualToString:@"New South Wales"])
                lastString  = @"NSW";
            else  if ([lastString  isEqualToString:@"Northern Territory"])
                lastString  = @"NT";
            else if ([lastString  isEqualToString:@"Queensland"])
                lastString  = @"QLD";
            else if ([lastString  isEqualToString:@"South Australia"])
                lastString  = @"SA";
            else if ([lastString  isEqualToString:@"Tasmania"])
                lastString  = @"TAS";
            else if ([lastString  isEqualToString:@"Victoria"])
                lastString  = @"VIC";
            else
                lastString  = @"WA";
            
        }
        
        
        else if([firstString isEqualToString:@"Hongkong"]){
            
            firstString = @"Hongkong";
            lastString  = @"HK";
        }
        
        
        
        else if([firstString isEqualToString:@"India"]){
            
            
            firstString = @"India";
            
            if ([lastString  isEqualToString:@"AndhraPradesh"])
                lastString  = @"AP";
            else if ([lastString  isEqualToString:@"Delhi"])
                lastString  = @"DL";
            else if ([lastString  isEqualToString:@"Gujarat"])
                lastString  = @"GJ";
            else if ([lastString  isEqualToString:@"Haryana"])
                lastString  = @"HR";
            else if ([lastString  isEqualToString:@"Karnataka"])
                lastString  = @"KA";
            else if ([lastString  isEqualToString:@"Maharastra"])
                lastString  = @"MH";
            else if ([lastString  isEqualToString:@"TamilNadu"])
                lastString  = @"TN";
            else if ([lastString  isEqualToString:@"UttarPradesh"])
                lastString  = @"UP";
            else
                lastString  = @"WB";
            
            
            
        }
        else if([firstString isEqualToString:@"New Zealand"]){
            
            firstString = @"New Zealand";
            
            if ([lastString  isEqualToString:@"Auckland"])
                lastString  = @"AUK";
            else  if ([lastString  isEqualToString:@"Bay of plenty"])
                lastString  = @"BOP";
            else  if ([lastString  isEqualToString:@"Canterbury"])
                lastString  = @"CAN";
            else  if ([lastString  isEqualToString:@"Fiordland"])
                lastString  = @"FIL";
            else  if ([lastString  isEqualToString:@"Gisborne"])
                lastString  = @"GIS";
            else if ([lastString  isEqualToString:@"Hawkes Bay"])
                lastString  = @"HKB";
            else  if ([lastString  isEqualToString:@"Marlborough"])
                lastString  = @"MBH";
            else  if ([lastString  isEqualToString:@"Manawattu"])
                lastString  = @"MWT";
            else  if ([lastString  isEqualToString:@"Nelson & Bays"])
                lastString  = @"NAB";
            else  if ([lastString  isEqualToString:@"Northland"])
                lastString  = @"NTL";
            else  if ([lastString  isEqualToString:@"otago"])
                lastString  = @"OTA";
            else  if ([lastString  isEqualToString:@"Southland"])
                lastString  = @"STL";
            else  if ([lastString  isEqualToString:@"Taranaki"])
                lastString  = @"TKI";
            else  if ([lastString  isEqualToString:@"Timaru"])
                lastString  = @"TMR";
            else  if ([lastString  isEqualToString:@"Tasman"])
                lastString  = @"TSM";
            else  if ([lastString  isEqualToString:@"wanganui"])
                lastString  = @"WGI";
            else if ([lastString  isEqualToString:@"Willington"])
                lastString  = @"WGN";
            else  if ([lastString  isEqualToString:@"Waikato"])
                lastString  = @"WKO";
            else  if ([lastString  isEqualToString:@"Wairapapa"])
                lastString  = @"WPP";
            
            else
                lastString  = @"WTC";
            
            
            
        }
        else
        {                           //Philippines
            firstString = @"Philippines";
            
            if ([lastString  isEqualToString:@"Luzon"])
                lastString  = @"LUZ";
            else  if ([lastString  isEqualToString:@"Mindanao"])
                lastString  = @"MIN";
            else if ([lastString  isEqualToString:@"NCR"])
                lastString  = @"NCR";
            else
                lastString  = @"VIS";
            
        }
        NSLog(@"country is %@ state is %@ ",firstString,lastString);
        
    }
    else
    {
        
        NSLog(@"state is %@ ",self.state_TextFeild.text);
        
    }
    
    NSLog(@"firstString is %@ laststring %@",firstString,lastString);
     NSString *urlString = [NSString stringWithFormat:@"%@update_user_info.php",URL_Prefix];
     NSURL *url = [NSURL URLWithString:urlString];
     
     ASIFormDataRequest *req = [[ASIFormDataRequest alloc] initWithURL:url];
     [req setPostValue:appDelegate.sessionUser.userId          forKey:@"uname"];
     [req setPostValue:FirstName                               forKey:@"fname"];
     [req setPostValue:LastName                                forKey:@"lname"];
     
     [req setPostValue:email_TextFeild.text                    forKey:@"email"];
     
     
     [req setPostValue:firstString                               forKey:@"country"];
     [req setPostValue:lastString                               forKey:@"state"];
     
     
     
     
     [req setPostValue:appDelegate.sessionUser.newsletter forKey:@"newsletter"];
     NSLog(@"url is========>>>>>> : %@",url);
     [req setDelegate:self];
     [req startAsynchronous];
    
    
}
//=====================================================================//
-(BOOL)doesString:(NSString *)string containCharacter:(char)character
{
    if ([string rangeOfString:[NSString stringWithFormat:@"%c",character]].location != NSNotFound)
    {
        return YES;
    }
    return NO;
}
//=====================================================================//

// ========== ASIHTTPREQEST  DELEGATE METHODS      ======================//

#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSLog(@"REQUEST SUBMITTED:%@",[request responseString]);
    NSString *result = [request responseString];
    if([result isEqualToString:@"success"])
    {
        
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"first_name"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"last_name"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"email"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"state"];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"country"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
        [[NSUserDefaults standardUserDefaults] setValue:firstName_TextFeild.text forKey:@"first_name"];
        [[NSUserDefaults standardUserDefaults] setValue:lastName_TextFeild.text forKey:@"last_name"];
        
        [[NSUserDefaults standardUserDefaults] setValue:email_TextFeild.text forKey:@"email"];
        NSArray *subStrings;
        NSString *firstString;
        NSString *lastString;
        if ([self doesString:self.state_TextFeild.text containCharacter:','])// phNo having character '/'
        {
            subStrings = [self.state_TextFeild.text componentsSeparatedByString:@","]; //or rather @" - "
            firstString = [subStrings objectAtIndex:0];
            lastString=[subStrings objectAtIndex:1];
            
            NSLog(@"country is %@ and state is %@ ",firstString,lastString);
            if([firstString isEqualToString:@"Australia"])
            {
                firstString = @"Australia";
                
                
                if ([lastString  isEqualToString:@"Australian Capital Territory"])
                    lastString  = @"ACT";
                else if ([lastString  isEqualToString:@"New South Wales"])
                    lastString  = @"NSW";
                else  if ([lastString  isEqualToString:@"Northern Territory"])
                    lastString  = @"NT";
                else if ([lastString  isEqualToString:@"Queensland"])
                    lastString  = @"QLD";
                else if ([lastString  isEqualToString:@"South Australia"])
                    lastString  = @"SA";
                else if ([lastString  isEqualToString:@"Tasmania"])
                    lastString  = @"TAS";
                else if ([lastString  isEqualToString:@"Victoria"])
                    lastString  = @"VIC";
                else
                    lastString  = @"WA";
                
            }
            
            
            else if([firstString isEqualToString:@"Hongkong"]){
                
                firstString = @"Hongkong";
                lastString  = @"HK";
            }
            
            
            
            else if([firstString isEqualToString:@"India"]){
                
                
                firstString = @"India";
                
                if ([lastString  isEqualToString:@"AndhraPradesh"])
                    lastString  = @"AP";
                else if ([lastString  isEqualToString:@"Delhi"])
                    lastString  = @"DL";
                else if ([lastString  isEqualToString:@"Gujarat"])
                    lastString  = @"GJ";
                else if ([lastString  isEqualToString:@"Haryana"])
                    lastString  = @"HR";
                else if ([lastString  isEqualToString:@"Karnataka"])
                    lastString  = @"KA";
                else if ([lastString  isEqualToString:@"Maharastra"])
                    lastString  = @"MH";
                else if ([lastString  isEqualToString:@"TamilNadu"])
                    lastString  = @"TN";
                else if ([lastString  isEqualToString:@"UttarPradesh"])
                    lastString  = @"UP";
                else
                    lastString  = @"WB";
                
                
                
            }
            else if([firstString isEqualToString:@"New Zealand"]){
                
                firstString = @"New Zealand";
                
                if ([lastString  isEqualToString:@"Auckland"])
                    lastString  = @"AUK";
                else  if ([lastString  isEqualToString:@"Bay of plenty"])
                    lastString  = @"BOP";
                else  if ([lastString  isEqualToString:@"Canterbury"])
                    lastString  = @"CAN";
                else  if ([lastString  isEqualToString:@"Fiordland"])
                    lastString  = @"FIL";
                else  if ([lastString  isEqualToString:@"Gisborne"])
                    lastString  = @"GIS";
                else if ([lastString  isEqualToString:@"Hawkes Bay"])
                    lastString  = @"HKB";
                else  if ([lastString  isEqualToString:@"Marlborough"])
                    lastString  = @"MBH";
                else  if ([lastString  isEqualToString:@"Manawattu"])
                    lastString  = @"MWT";
                else  if ([lastString  isEqualToString:@"Nelson & Bays"])
                    lastString  = @"NAB";
                else  if ([lastString  isEqualToString:@"Northland"])
                    lastString  = @"NTL";
                else  if ([lastString  isEqualToString:@"otago"])
                    lastString  = @"OTA";
                else  if ([lastString  isEqualToString:@"Southland"])
                    lastString  = @"STL";
                else  if ([lastString  isEqualToString:@"Taranaki"])
                    lastString  = @"TKI";
                else  if ([lastString  isEqualToString:@"Timaru"])
                    lastString  = @"TMR";
                else  if ([lastString  isEqualToString:@"Tasman"])
                    lastString  = @"TSM";
                else  if ([lastString  isEqualToString:@"wanganui"])
                    lastString  = @"WGI";
                else if ([lastString  isEqualToString:@"Willington"])
                    lastString  = @"WGN";
                else  if ([lastString  isEqualToString:@"Waikato"])
                    lastString  = @"WKO";
                else  if ([lastString  isEqualToString:@"Wairapapa"])
                    lastString  = @"WPP";
                
                else
                    lastString  = @"WTC";
                
                
                
            }
            else
            {                           //Philippines
                firstString = @"Philippines";
                
                if ([lastString  isEqualToString:@"Luzon"])
                    lastString  = @"LUZ";
                else  if ([lastString  isEqualToString:@"Mindanao"])
                    lastString  = @"MIN";
                else if ([lastString  isEqualToString:@"NCR"])
                    lastString  = @"NCR";
                else
                    lastString  = @"VIS";
                
            }
            NSLog(@"country is %@ state is %@ ",firstString,lastString);
            [[NSUserDefaults standardUserDefaults] setValue:lastString forKey:@"state"];
            [[NSUserDefaults standardUserDefaults] setValue:firstString forKey:@"country"];
            appDelegate.sessionUser.state = lastString;
            appDelegate.sessionUser.country=firstString;
        }
        else
        {
            [[NSUserDefaults standardUserDefaults] setValue:self.state_TextFeild.text forKey:@"country"];
            NSLog(@"state is %@ ",self.state_TextFeild.text);
            
        }
        
        
        NSLog(@"state is  %@",state_TextFeild.text);
        [[NSUserDefaults standardUserDefaults] synchronize];
        appDelegate.sessionUser.first_name = firstName_TextFeild.text;
        appDelegate.sessionUser.last_name = lastName_TextFeild.text;
        appDelegate.sessionUser.email = email_TextFeild.text;
        appDelegate.sessionUser.state = state_TextFeild.text;
        
        
        
        UIAlertView *updateAlert = [[UIAlertView alloc]initWithTitle:@"Success" message:@"Account Updated Successfully" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [updateAlert show];
        
        
        
    }
    else
    {
        UIAlertView *failedAlert = [[UIAlertView alloc]initWithTitle:@"Failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [failedAlert show];
        
        
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    
    NSLog(@"=====REQUEST FAILED=======");
    UIAlertView *failedAlert = [[UIAlertView alloc]initWithTitle:@"Failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [failedAlert show];
    
    
    
}
//================================================================//
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.tag == 111)
    {
        if (buttonIndex == alertView.cancelButtonIndex)
        {
            [appDelegate logoutUserSession];
        }
    }
    else
    {
    [self back];
    }
    
}
//===================== TEXTFIELD DELEGATE METHOS =======================//


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSLog(@"Textfield Return..");
    [textField resignFirstResponder];
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    return (newLength > 25) ? NO : YES;
}
-(void)setViewMovedUp:(BOOL)movedUp TextField:(UITextField *)TField
{
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.3]; // if you want to slide up the view
    [UIView setAnimationBeginsFromCurrentState:YES];
    
    CGRect rect;
    
    rect = self.view.frame;
    
    
    if (movedUp)
    {
        // 1. move the view's origin up so that the text field that will be hidden come above the keyboard
        // 2. increase the size of the view so that the area behind the keyboard is covered up.
        NSLog(@"INSIDE MOVEDUP AND SCROLL VIEW Y IS::%f",rect.origin.y);
        
        
        rect.origin.y -= kOFFSET_FOR_KEYBOARD;
        
        
    }
    else
    {
        
        rect.origin.y += kOFFSET_FOR_KEYBOARD;
        
        
    }
    self.view.frame = rect;
    [UIView commitAnimations];
}



- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    currentTextFeild = textField;
    if (textField.tag == 1 || textField.tag == 2)
    {
    
    }
    
    else
    {
        if (textField.tag==4) {
            textField.inputView = pickerContainerView;
            
            
        }
        NSLog(@"Textfield remaining..");
        stayup = YES;
        
        [self setViewMovedUp:YES  TextField:textField];
        
        
    }
    
   // [memberShip_TextFeild setEnabled:NO];
    
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (textField.tag == 1 || textField.tag == 2){}
    else {
        NSLog(@"textFieldDidEndEditing Textfield 1&2..");
        stayup = NO;
        
        [self setViewMovedUp:NO TextField:textField];}
}
//====================================================================//
-(void)configureCountryPicker
{
    
    
    UIPickerView  *myPickerView = [[ UIPickerView alloc] initWithFrame:CGRectMake(0, 0, 320, 150)];
    myPickerView.dataSource = self;
    myPickerView.delegate = self;
    myPickerView.showsSelectionIndicator=YES;
    myPickerView.backgroundColor=[UIColor whiteColor];
    
    
   
    [myPickerView setFrame:CGRectMake(0, 50, 320, 250)];
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectMake(250, 10, 50, 30)];
    [button setImage:[UIImage imageNamed:@"done.png"] forState:UIControlStateNormal];
    [button addTarget:self action:@selector(doneButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    
    
    pickerContainerView = [[UIView alloc]initWithFrame:CGRectMake(0, 300, 320, 266)];
    pickerContainerView.backgroundColor = [UIColor whiteColor];
    [pickerContainerView addSubview:myPickerView];
    [pickerContainerView addSubview:button];
    
}

-(void)doneButtonPressed
{
    
    NSLog(@"doneButtonPressed");
    if (state_TextFeild.text.length==0)
    {
       state_TextFeild.text=@"Australia,Australia Capital Territory";
    }
    [currentTextFeild resignFirstResponder];
    [pickerContainerView removeFromSuperview];
}
/*- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    
    NSLog(@"Button index is %d",buttonIndex);
    if(buttonIndex == 0)
    {
        
        //   country_str = country;
        //  state_str = state;
        
        NSLog(@"country::%@",country);
        NSLog(@"state::%@",state);
        self.stateTextField.text = [NSString stringWithFormat:@"%@,%@",country,state];
        
        
        if([country isEqualToString:@"Australia"])
        {
            country_str = @"Australia";
            
            
            if ([state isEqualToString:@"Australian Capital Territory"])
                state_str = @"ACT";
            else if ([state isEqualToString:@"New South Wales"])
                state_str = @"NSW";
            else  if ([state isEqualToString:@"Northern Territory"])
                state_str = @"NT";
            else if ([state isEqualToString:@"Queensland"])
                state_str = @"QLD";
            else if ([state isEqualToString:@"South Australia"])
                state_str = @"SA";
            else if ([state isEqualToString:@"Tasmania"])
                state_str = @"TAS";
            else if ([state isEqualToString:@"Victoria"])
                state_str = @"VIC";
            else
                state_str = @"WA";
            
        }
        
        
        else if([country isEqualToString:@"Hongkong"]){
            
            country_str = @"Hongkong";
            state_str = @"HK";
        }
        
        
        
        else if([country isEqualToString:@"India"]){
            
            
            country_str = @"India";
            
            if ([state isEqualToString:@"AndhraPradesh"])
                state_str = @"AP";
            else if ([state isEqualToString:@"Delhi"])
                state_str = @"DL";
            else if ([state isEqualToString:@"Gujarat"])
                state_str = @"GJ";
            else if ([state isEqualToString:@"Haryana"])
                state_str = @"HR";
            else if ([state isEqualToString:@"Karnataka"])
                state_str = @"KA";
            else if ([state isEqualToString:@"Maharastra"])
                state_str = @"MH";
            else if ([state isEqualToString:@"TamilNadu"])
                state_str = @"TN";
            else if ([state isEqualToString:@"UttarPradesh"])
                state_str = @"UP";
            else
                state_str = @"WB";
            
            
            
        }
        else if([country isEqualToString:@"New Zealand"]){
            
            country_str = @"New Zealand";
            
            if ([state isEqualToString:@"Auckland"])
                state_str = @"AUK";
            else  if ([state isEqualToString:@"Bay of plenty"])
                state_str = @"BOP";
            else  if ([state isEqualToString:@"Canterbury"])
                state_str = @"CAN";
            else  if ([state isEqualToString:@"Fiordland"])
                state_str = @"FIL";
            else  if ([state isEqualToString:@"Gisborne"])
                state_str = @"GIS";
            else if ([state isEqualToString:@"Hawkes Bay"])
                state_str = @"HKB";
            else  if ([state isEqualToString:@"Marlborough"])
                state_str = @"MBH";
            else  if ([state isEqualToString:@"Manawattu"])
                state_str = @"MWT";
            else  if ([state isEqualToString:@"Nelson & Bays"])
                state_str = @"NAB";
            else  if ([state isEqualToString:@"Northland"])
                state_str = @"NTL";
            else  if ([state isEqualToString:@"otago"])
                state_str = @"OTA";
            else  if ([state isEqualToString:@"Southland"])
                state_str = @"STL";
            else  if ([state isEqualToString:@"Taranaki"])
                state_str = @"TKI";
            else  if ([state isEqualToString:@"Timaru"])
                state_str = @"TMR";
            else  if ([state isEqualToString:@"Tasman"])
                state_str = @"TSM";
            else  if ([state isEqualToString:@"wanganui"])
                state_str = @"WGI";
            else if ([state isEqualToString:@"Willington"])
                state_str = @"WGN";
            else  if ([state isEqualToString:@"Waikato"])
                state_str = @"WKO";
            else  if ([state isEqualToString:@"Wairapapa"])
                state_str = @"WPP";
            
            else
                state_str = @"WTC";
            
            
            
        }
        else{                           //Philippines
            country_str = @"Philippines";
            
            if ([state isEqualToString:@"Luzon"])
                state_str = @"LUZ";
            else  if ([state isEqualToString:@"Mindanao"])
                state_str = @"MIN";
            else if ([state isEqualToString:@"NCR"])
                state_str = @"NCR";
            else
                state_str = @"VIS";
            
        }
        
        
        country = nil;
        state = nil;
        [asheet removeFromSuperview];
        //   [self bringTheFirstTimeLoginViewDown];
    }
    
    
    
}*/

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    
    
    if(component == 0)
    {
        
        country =[[NSString alloc] initWithFormat:@"%@" , [Countries_List objectAtIndex:row]];
        state =@"";
        [pickerView reloadComponent:1];
        if ([country isEqualToString:@"India"]) {
            
            state = [indian_StatesList objectAtIndex:0];
        }
        if ([country isEqualToString:@"Australia"]) {
            state = [australian_StatesList objectAtIndex:0];
        }
        if ([country isEqualToString:@"Hongkong"]) {
            state = [hongkong_StatesList objectAtIndex:0];
        }
        if ([country isEqualToString:@"New Zealand"]) {
            state = [newZealand_StatesList objectAtIndex:0];
        }
        if ([country isEqualToString:@"Philippines"]) {
            state = [philippines_StatesList objectAtIndex:0];
        }
        [pickerView selectRow:0 inComponent:1 animated:YES];
    }
    if (component == 1) {
        //comp2Row = row;
        if ([country isEqualToString:@"India"]) {
            state = [indian_StatesList objectAtIndex:row];
        }
        if ([country isEqualToString:@"Australia"]) {
            state = [australian_StatesList objectAtIndex:row];
        }
        if ([country isEqualToString:@"Hongkong"]) {
            state = [hongkong_StatesList objectAtIndex:row];
        }
        if ([country isEqualToString:@"New Zealand"]) {
            state = [newZealand_StatesList objectAtIndex:row];
        }
        if ([country isEqualToString:@"Philippines"]) {
            state = [philippines_StatesList objectAtIndex:row];
        }
        
        
        
    }
    NSLog(@"Selected::%@",[[NSString alloc]initWithFormat:@"%@,%@",country,state]);
    
    state_TextFeild.text=[[NSString alloc]initWithFormat:@"%@,%@",country,state];
}
- (void)willPresentActionSheet:(UIActionSheet *)actionSheet
{
    state = @"Australian Capital Territory";
    country = @"Australia";
    
    
    
    //Configure picker...
    
    
    //Add picker to action sheet
    // [actionSheet addSubview:myPickerView];
    
    
    //Gets an array af all of the subviews of our actionSheet
    NSArray *subviews = [actionSheet subviews];
    
    [[subviews objectAtIndex:0] setFrame:CGRectMake(20, 170, 280, 40)];
    // [[subviews objectAtIndex:1] setFrame:CGRectMake(20, 317, 280, 46)];
}
/*-(NSString *) pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
 {
 if (component==0) {
 
 return [Countries_List objectAtIndex:row];
 }
 else {
 if ([country isEqualToString:@"India"]) {
 return [indian_StatesList objectAtIndex:row];
 }
 if ([country isEqualToString:@"Australia"]) {
 return [australian_StatesList objectAtIndex:row];
 }
 if ([country isEqualToString:@"Hongkong"]) {
 return [hongkong_StatesList objectAtIndex:row];
 }
 if ([country isEqualToString:@"New Zealand"]) {
 return [newZealand_StatesList objectAtIndex:row];
 }
 if ([country isEqualToString:@"Philippines"]) {
 return [philippines_StatesList objectAtIndex:row];
 }
 }
 return 0;
 }*/

-(NSInteger) numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 2;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component
{
    if (component == 0)
        return 105;
    else
        return 185;
}
-(NSInteger) pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    if (component == 0)
        return [Countries_List count];
    
    else {
        if ([country isEqualToString:@"India"]) {
            return [indian_StatesList count];
        }
        if ([country isEqualToString:@"Australia"]) {
            return [australian_StatesList count];
        }
        if ([country isEqualToString:@"Hongkong"]) {
            return [hongkong_StatesList count];
        }
        if ([country isEqualToString:@"New Zealand"]) {
            return  [newZealand_StatesList count];
        }
        if ([country isEqualToString:@"Philippines"]) {
            return [philippines_StatesList count];
        }
        
    }
    
    return 0;
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




- (void)viewDidUnload
{
    
    [self setLogoView:nil];
    [self setFirstnameTextField:nil];
    [self setFirstnameLabel:nil];
    [self setLastnameTextField:nil];
    [self setEmailTextField:nil];
    [self setStateTextField:nil];
    [self setMemnoTextField:nil];
    [self setLastnameLabel:nil];
    [self setEmailLabel:nil];
    [self setStateLabel:nil];
    [self setMemnoLabel:nil];
    [self setNoteLabel:nil];
    [super viewDidUnload];
}
@end
