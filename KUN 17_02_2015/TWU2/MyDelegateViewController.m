//
//  MyDelegateViewController.m
//  TWU2
//
//  Created by MyRewards on 2/15/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "MyDelegateViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface MyDelegateViewController ()
{
    ASIFormDataRequest *SearchRequest;
    MyUnionParser *DisplayXMLParser;
    
    AppDelegate *appDelegate;
}

@end

@implementation MyDelegateViewController

@synthesize myUnionDisplayView;
@synthesize myUnionActivityIndicator;
@synthesize containerView;
@synthesize productsList;
@synthesize containerImageView;
@synthesize background_ImageView;
//=========================DELEGATE METHODS=============================//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    [self loadViews];
    // Do any additional setup after loading the view from its nib.
    [self.background_ImageView setHidden:YES];
    //[self.containerImageView setHidden:YES];
    [super viewDidLoad];
    
    containerView.layer.cornerRadius = 10.0;
    self.containerView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Tbbox.png"]];
    
    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    if ([[ver objectAtIndex:0] intValue] >= 7)
    {
        self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    }
    
   
    [self performSelector:@selector(getmydealsData) withObject:nil afterDelay:0.3];

    
}
- (void)getmydealsData
{
    NSString *urlString;
    //====================
    urlString = @"http://www.myrewards.com.au/newapp/get_page.php?cid=73&pid=2";
    //============
    NSLog(@"URL: %@",urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    SearchRequest = [ASIFormDataRequest requestWithURL:url];
    [SearchRequest setDelegate:self];
    [SearchRequest startAsynchronous];

    [self.myUnionActivityIndicator startAnimating];

}

-(void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    [[self navigationController] setNavigationBarHidden:NO animated:NO];
//    NSArray *ver = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
//    if ([[ver objectAtIndex:0] intValue] >= 7)
//    {
//        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];        self.navigationController.navigationBar.translucent = NO;
//    }
//    else
//    {
//        self.navigationController.navigationBar.tintColor =[UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
//    }
    
   /* if([self.navigationController.navigationBar respondsToSelector:@selector(barTintColor)])
    {
        // iOS7
        self.navigationController.navigationBar.barTintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
        self.navigationController.navigationBar.translucent = NO;
    }
    else
    {
        // older
        self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:0/255.f green:130/255.f blue:214/255.f alpha:1.0f];
    }*/
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    
    
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    UIImage *myImage1 = [UIImage imageNamed:@"new.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    TitleLabel.text = @"Our Team";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    [TitleLabel setTextColor:[UIColor colorWithRed:(255/255.f) green:(255/255.f) blue:(255/255.f) alpha:1.0f]];
    self.navigationItem.titleView = TitleLabel;
    
    
}
-(void)back
{
    
    
    
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
}

//=================================================================//

//===========DELEGATE METHODS TO LOCK ORIENTATION=======================//
-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}
//-(BOOL)s

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

- (BOOL) shouldAutorotate
{
    return NO;
}
//=================================================================//
// ============METHOD TO LOAD VIEWS IN IPHONE===========================//

-(void)loadViews
{
   // UIScreen *mainScreen = [UIScreen mainScreen];
   // CGFloat scale = ([mainScreen respondsToSelector:@selector(scale)] ? mainScreen.scale : 1.0f);
   // CGFloat pixelHeight = (CGRectGetHeight(mainScreen.bounds) * scale);
    if ([appDelegate isIphone5])
        {
            
          NSLog(@"====UIDeviceResolution_iPhoneRetina5====");
            
            CGRect frame = background_ImageView.frame;
            frame.size.height = 435;//self.view.frame.size.height;
            background_ImageView.frame = frame;
            
           
            CGRect containerframe = self.containerImageView.frame;
            containerframe.origin.y=10;
            containerframe.size.height = 310;
            self.containerImageView.frame = containerframe;
            
            CGRect webviewframe = self.myUnionDisplayView.frame;
            webviewframe.origin.x=0;
            webviewframe.origin.y=0;
            webviewframe.size.width=320;
            webviewframe.size.height =435;;
            self.myUnionDisplayView.frame= webviewframe;
            
            //setting Activity Indicator Frame
            CGRect Activityframe = self.myUnionActivityIndicator.frame;
            NSLog(@"X=%f",self.myUnionActivityIndicator.frame.origin.x);
            NSLog(@"Y=%f",self.myUnionActivityIndicator.frame.origin.y);
            Activityframe.origin.y+=40;
            self.myUnionActivityIndicator.frame = Activityframe;
            
        }
        
        
       
}
//==============================================================//

//===========ASIHTTP REQUEST DELEGATE MATHODS=================//

- (void)requestFinished:(ASIHTTPRequest *)request {
    
    NSLog(@"DetailView -- requestFinished:");
    if (request == SearchRequest)
    {
        
        SearchRequest = nil;
        NSLog(@"============>Response is ::: %@",[request responseString]);
        // NSString *responseString = [request responseString];
        // NSLog(@"RESPONSE STRING IS:: %@",responseString);
        
        //[appDelegate addRecord:responseString];
        
        NSXMLParser *myParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        
        DisplayXMLParser = [[MyUnionParser alloc] init];
        DisplayXMLParser.delegate = self;
        myParser.delegate = DisplayXMLParser;
        [myParser parse];
        
    }
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    NSLog(@"DetailView -- requestFailed:");
    UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
    [failureAlert show];

    
    [self.myUnionActivityIndicator stopAnimating];
}

//======================================================================//

//==========USER DATA XML PARSER DELEGATE MATHODS ==============//
#pragma mark -- UserDataXMLParser Delegate methods

- (void) parsingMyUnionDetailsFinished:(Display *) myUnionDisplay
{
    
    NSMutableString *html;
    
    html = [NSMutableString stringWithString:myUnionDisplay.displaymsg];
    
    
    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"saveToCoreData"])
    {
        NSLog(@"===== HTMLPage NOT Available====");
        [appDelegate addRecord:html];
        [[NSUserDefaults standardUserDefaults] setValue:@"HTMLPageAvailable" forKey:@"saveToCoreData"];
    }
    
    
    NSString *myDescriptionHTML=[NSString stringWithFormat:@"<html> \n"
                                 "<head> \n"
                                 "<style type=\"text/css\"> \n"
                                 "body {font-family: \"%@\"; font-size: %@;line-height:1.5%;}\n"
                                 "</style> \n"
                                 "</head> \n"
                                 "</body>%@</body>\n"
                                 "</html>", @"Helvetica", [NSNumber numberWithInt:13], html];
    
    
    [self.myUnionDisplayView setBackgroundColor:[UIColor clearColor]];
    // self.displayView.scalesPageToFit = YES;
    [self.myUnionDisplayView loadHTMLString:myDescriptionHTML  baseURL:nil];
    [self.myUnionActivityIndicator stopAnimating];

    
    
}
-(BOOL)webView:(UIWebView *)descriptionTextView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (UIWebViewNavigationTypeLinkClicked == navigationType) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}
- (void) MyUnionDetailsXMLparsingFailed
{
    
}
//======================================================================//



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
    [self setContainerImageView:nil];
    [super viewDidUnload];
}
@end
