//
//  MyUnionContact.h
//  TWU2
//
//  Created by vairat on 09/04/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface MyUnionContact : NSObject
{
    
}
@property (nonatomic, strong) NSString * name;
@property (nonatomic, strong) NSString * number;
@property (nonatomic, strong) NSString * carRegistration;
@property (nonatomic, strong) NSString * carModel;
@property (nonatomic, strong) NSString * carYear;
@end
