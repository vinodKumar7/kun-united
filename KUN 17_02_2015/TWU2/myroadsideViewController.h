//
//  myroadsideViewController.h
//  TWU2
//
//  Created by vairat on 22/04/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import "NSString+SBJSON.h"
#import "RoadSideMap.h"


@interface myroadsideViewController : UIViewController<CLLocationManagerDelegate, MKMapViewDelegate, MKOverlay, UITableViewDataSource, UITableViewDelegate>
{
    
    CLLocationManager *locManager;
    NSString *strLatitude,*strLongitude;
    CLLocationCoordinate2D currentLocation;
    IBOutlet UILabel *currentlocation_Label;
    
    RoadSideMap *roadSideMapObj;
    
    NSMutableArray * addressArray, * subTitleArray, * phnoArray;
    int tableViewFlag;
    double distanceCompareValue;
    int timeCompareValue;
    NSString *esc_addr;
    NSString *addressTxt, *distanceAndTimeString;
    
    NSDictionary *dictRouteInfo;
    MKAnnotationView *pin;
    
    UIButton *callButton;
}


@property (nonatomic, retain) NSArray *wayPoints;
@property (nonatomic, readwrite) CLLocationCoordinate2D currentLocation,currentLocationCordinates;
@property (strong, nonatomic) IBOutlet UILabel *chooseLabel;
@property (strong, nonatomic) IBOutlet UILabel *locationLabel;

@property (retain, nonatomic) IBOutlet MKMapView *roadMapView;
@property (retain, nonatomic) IBOutlet UILabel *currentlocation_Label;
@property (strong, nonatomic) IBOutlet UILabel *distanceLabel;
@property (strong, nonatomic) IBOutlet UILabel *timeLabel;

@property (nonatomic, strong) UITableView *locationTableView;


@property (strong, nonatomic) IBOutlet UIButton *chooseBtnOutlet;
@property (strong, nonatomic) IBOutlet UIButton *selectBtnOutlet;

- (IBAction)kunServiceLinkButton_Tapped:(id)sender;
- (IBAction)chooseLocationButton_Tapped:(id)sender;



@end
