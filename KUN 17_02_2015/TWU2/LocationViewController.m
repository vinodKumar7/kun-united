//
//  LocationViewController.m
//  TWU
//
//  Created by vairat on 20/06/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//
#import "LocationViewController.h"
#import <MapKit/MapKit.h>
#import "AppDelegate.h"

@interface LocationViewController ()
{
    ParkingTimer *currentRemainder,*currentUserLocation, *carLocation,*carLoc_Details;
    AppDelegate *appDelegate;
}

@end

@implementation LocationViewController

@synthesize when_Label;
@synthesize where_Label;
@synthesize latitude_Label;
@synthesize longitude_Label;
@synthesize whenfixedLabel;
@synthesize wherefixedLabel;
@synthesize latitudefixedLabel;
@synthesize longitudefixedLabel;
@synthesize parkedlocationlabel;
@synthesize getDirectionsButton;
@synthesize bgImageView;
//===================================DELEGATE METHOD============================================//

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil userLoc:(ParkingTimer *) locDetails carLoc:(ParkingTimer *)carDetails
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        currentUserLocation = locDetails;
        carLocation = carDetails;
    }
    return self;
}
//===============================================================================================//
//===============DELEGATE METHODS TO LOCK ORIENTATION===========================================//


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}

- (BOOL) shouldAutorotate
{
    return NO;
}

//==============================================================================================//


// =======================METHOD TO LOAD DIRECTIONS IN IPHONE  ==================================//

- (IBAction)directions_Action:(id)sender
{
    
    NSLog(@"directions_Action");
    
    NSString *str = [NSString stringWithFormat:@"https://maps.google.com/maps?saddr=%@,%@&daddr=%@,%@",currentUserLocation.latitude,currentUserLocation.longitude,carLocation.latitude,carLocation.longitude];
   
    
   // NSString *urlString =  @"https://maps.google.com/maps?%@", str];
    NSURL *myURL = [NSURL URLWithString:str];
    
    [[UIApplication sharedApplication] openURL:myURL];

    
    
    
    
    
    
    
}
//==============================================================================================//


//===================================DELEGATE METHODS============================================//


- (void)viewWillDisappear:(BOOL)animated
{
    NSLog(@"viewWillDisappear");
    
}

- (void)viewDidDisappear:(BOOL)animated
{
    NSLog(@"viewDidDisappear");
}
-(void)viewWillAppear:(BOOL)animated
{
    
    currentRemainder = [appDelegate FetchRemainder];
    if (![currentRemainder isKindOfClass:[NSNull class]])
    {
        NSLog(@"currentRemainder object is available");
        // [self.done_Button setImage:[UIImage imageNamed:@"CancelTimer.png"] forState:UIControlStateNormal];
        // self.location_TextField.text = currentRemainder.location;
        
        
        
        //center
        CLLocationCoordinate2D center;
        center.latitude = [currentRemainder.latitude floatValue];
        center.longitude = [currentRemainder.longitude floatValue];
        
        carLoc_Details = [[ParkingTimer alloc]init];
        carLoc_Details.latitude = currentRemainder.latitude;
        carLoc_Details.longitude = currentRemainder.longitude;
        carLoc_Details.parkedLocation = currentRemainder.parkedLocation;
        carLoc_Details.parkedTime = currentRemainder.parkedTime;
        NSString *stripped = [currentRemainder.parkedLocation stringByReplacingOccurrencesOfString:@",(null)" withString:@""];
        NSString *stripped0 = [stripped stringByReplacingOccurrencesOfString:@"(null)," withString:@""];
        NSString *stripped1 = [stripped0 stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
        self.where_Label.text = stripped1;
        self.when_Label.text =currentRemainder.parkedTime;
        self.latitude_Label.text = currentRemainder.latitude;
        self.longitude_Label.text = currentRemainder.longitude;
        /*   self.where_Label.text = carLocation.parkedLocation;
         self.when_Label.text =carLocation.parkedTime;
         self.latitude_Label.text = carLocation.latitude;
         self.longitude_Label.text = carLocation.longitude;*/
        
        //span
        
        
        
    }
    else
    {
        self.where_Label.text = @"";
        self.when_Label.text = @"";
        self.latitude_Label.text =  @"";
        self.longitude_Label.text =  @"";
    }
    
    
    
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
     appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    
    self.where_Label.text = carLocation.location;
    self.when_Label.text =carLocation.parkedTime;
    self.latitude_Label.text = carLocation.latitude;
    self.longitude_Label.text = carLocation.longitude;
    
   if (![appDelegate isIphone5])// loading view for iphone 4
   {
       NSLog(@"Iphone4");
        CGRect parkedlabelframe=self.parkedlocationlabel.frame; //parkedlabel frame
        parkedlabelframe.origin.y=240;
        parkedlocationlabel.frame=parkedlabelframe;
        
        CGRect imageframe=self.bgImageView.frame;               //image frame
        imageframe.origin.y=parkedlabelframe.origin.y+24;
        bgImageView.frame=imageframe;
        
        CGRect whereframe=self.where_Label.frame;                //where frame
        whereframe.origin.y=imageframe.origin.y+18;
        where_Label.frame=whereframe;
        
        CGRect wherefixedframe=self.wherefixedLabel.frame;       //wherefixed frame
        wherefixedframe.origin.y=imageframe.origin.y+18;
        wherefixedLabel.frame=wherefixedframe;
        
        CGRect whenframe=self.when_Label.frame;                   //when frame
        whenframe.origin.y=whereframe.origin.y+42;
        when_Label.frame=whenframe;
        
        CGRect whenfixedframe=self.whenfixedLabel.frame;           // whenfixed frame
        whenfixedframe.origin.y=whereframe.origin.y+42;
        whenfixedLabel.frame=whenfixedframe;
        
        
        CGRect latitudeframe=self.latitude_Label.frame;             //latitudeframe
        latitudeframe.origin.y=whenframe.origin.y+32;
        latitude_Label.frame=latitudeframe;
        
        CGRect latitudefixedframe=self.latitudefixedLabel.frame;     //latitudefixed frame
        latitudefixedframe.origin.y=whenframe.origin.y+32;
        latitudefixedLabel.frame=latitudefixedframe;
        
        CGRect longitudeframe=self.longitude_Label.frame;             // longitude frame
        longitudeframe.origin.y=latitudeframe.origin.y+22;
        longitude_Label.frame=longitudeframe;
        
        CGRect longitudefixedframe=self.longitudefixedLabel.frame;     //longitudefixed frame
        longitudefixedframe.origin.y=latitudeframe.origin.y+22;
        longitudefixedLabel.frame=longitudefixedframe;
        
        CGRect getDirectionsButtonframe=self.getDirectionsButton.frame; //getDirectionsButton frame
        getDirectionsButtonframe.origin.y=longitudefixedframe.origin.y+40;
        getDirectionsButton.frame=getDirectionsButtonframe;

        
    }    
    
    
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
//==============================================================================================//



- (void)viewDidUnload
{
    //    [self setWhere_Label:nil];
    //    [self setWhen_Label:nil];
    //    [self setLatitude_Label:nil];
    //    [self setLongitude_Label:nil];
    [self setParkedlocationlabel:nil];
    [self setWherefixedLabel:nil];
    [self setWhenfixedLabel:nil];
    [self setLatitudefixedLabel:nil];
    [self setLongitudefixedLabel:nil];
    [self setGetDirectionsButton:nil];
    [self setBgImageView:nil];
    [super viewDidUnload];
}
@end
