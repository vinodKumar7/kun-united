//
//  RoadSideMap.h
//  KUN
//
//  Created by vairat on 08/12/14.
//  Copyright (c) 2014 MyRewards. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MapKit/MKAnnotation.h"

@interface RoadSideMap : NSObject <MKAnnotation>

//subtitle
@property (nonatomic, strong) NSString *phoneNo;
@property (nonatomic, readwrite, copy) NSString *title;
@property (nonatomic, readwrite, copy) NSString *subtitle;
@property (nonatomic, strong) NSString *latitude;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, assign) CLLocationCoordinate2D coordinate;
@end
