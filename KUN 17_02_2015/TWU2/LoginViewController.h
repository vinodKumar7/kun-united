//
//  LoginViewController.h
//  GrabItNow
//
//  Created by MyRewards on 11/24/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MenuViewController.h"
#import "ASIHTTPRequest.h"
#import "UserDataXMLParser.h"
#import "CustomIOS7AlertView.h"

@interface LoginViewController : UIViewController <UITextFieldDelegate,ASIHTTPRequestDelegate, NSXMLParserDelegate, UserDataXMLParser, UIActionSheetDelegate, UIPickerViewDataSource,UIPickerViewDelegate,CustomIOS7AlertViewDelegate>
{
    BOOL processRenwalOfToken;
    MenuViewController  *menuViewController;
    IBOutlet UIImageView *bgView;
    
    IBOutlet UIImageView *bottom_Banner;
}
@property (strong, nonatomic) IBOutlet UIImageView *bgView;

@property (nonatomic, readwrite) BOOL processRenwalOfToken;

@property (strong, nonatomic) IBOutlet UITextField *usernameTextField;
@property (strong, nonatomic) IBOutlet UITextField *passwordTextField;
@property (strong, nonatomic) IBOutlet UITextField *subDomainTextField;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) IBOutlet UIView *loginContainerView;
@property (strong, nonatomic) IBOutlet UIView *loginContainer_Container;
@property (strong, nonatomic) IBOutlet  IBOutlet UIImageView *bottom_Banner;
@property (strong, nonatomic) IBOutlet UIButton *firsttimeloginButton;
@property (strong, nonatomic) IBOutlet UITextField *password_TextField;
@property (strong, nonatomic) IBOutlet UITextField *confirmpwd_TextField;
@property (strong, nonatomic) IBOutlet UITextField *referralno_TextField;
@property (strong, nonatomic) IBOutlet UITextField *email_TextField;
@property (strong, nonatomic) IBOutlet UIView *processing_View;
@property (strong, nonatomic) IBOutlet UIButton *firsttimeLogin_submitButton;
@property (strong, nonatomic) IBOutlet UITextView *tc_TextView;
@property (strong, nonatomic)          UITextField *membership_TextField;
@property (strong, nonatomic)          UITextField *alertTextField;
- (IBAction)firsttimeLoginDetailsSubmit_ButtonTapped:(id)sender;
@property (strong, nonatomic)          UITextField *webAddress_TextField;
@property (strong, nonatomic) IBOutlet UIView *firsttimelogin_ContainerView;
- (IBAction)tandcButton_Tapped:(id)sender;

@property (strong, nonatomic) IBOutlet UITextField *state_TextField;
@property (strong, nonatomic) IBOutlet UIButton *checkButton;
@property (strong, nonatomic) IBOutlet UIButton *newsLetter_Button;
@property (strong, nonatomic) IBOutlet UITextField *current_TextField;
@property (strong, nonatomic) IBOutlet UITextField *lastname_TextField;
@property (strong, nonatomic) IBOutlet UITextField *firstname_TextField;

- (IBAction)firsttimeLogin_ButtonTapped:(id)sender;

- (IBAction)closeButton_Tapped:(id)sender;

- (IBAction)loginButtonTapped:(id)sender;

@end
