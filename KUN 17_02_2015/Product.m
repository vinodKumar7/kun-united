//
//  Product.m
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "Product.h"

@implementation Product

@synthesize productImgLink;
@synthesize productName;
@synthesize productId;
@synthesize productDesciption;
@synthesize productOffer;
@synthesize productText;
@synthesize productImage;
@synthesize pin_type;
@synthesize termsAndConditions;
@synthesize contactMerchantName,contactSuburb,contactState,contactPostcode,contactlastName,contactFirstName,contactCountry;

@synthesize imageDoesntExist;

@synthesize displayImagePrefix;
@synthesize imageExtension;

@synthesize phone;
@synthesize mobile;
@synthesize websiteLink;

@synthesize productDetailedDesciption;
@synthesize productTermsAndConditions;

@synthesize coordinate;
@synthesize hotoffer_extension;
@synthesize quantity;


@synthesize merchantId;
@synthesize merchantLogoExtention;
@synthesize mobilecoupon;

- (id)init {
    self = [super init];
    
    if (self) {
        imageDoesntExist = NO;
    }
    
    return self;
}

@end


