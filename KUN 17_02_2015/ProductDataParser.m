//
//  ProductDataParser.m
//  GrabItNow
//
//  Created by MyRewards on 12/25/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "ProductDataParser.h"

@interface ProductDataParser()
{
    NSMutableString *charString;
    Product *currProduct;
}

@end


@implementation ProductDataParser
@synthesize delegate;


- (void)parserDidStartDocument:(NSXMLParser *)parser {
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    charString = nil;
    
    if ([elementName isEqualToString:@"product"]) {
        currProduct = [[Product alloc] init];
    }
    
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"details"]) {
        currProduct.productDesciption = finalString;
    }
    else if ([elementName isEqualToString:@"text"]) {
        currProduct.productText = finalString;
    }
    else if ([elementName isEqualToString:@"terms_and_conditions"]) {
        currProduct.productTermsAndConditions = finalString;
    }
    else if ([elementName isEqualToString:@"image_extension"]) {
        currProduct.imageExtension = finalString;
    }
    else if ([elementName isEqualToString:@"logo_extension"]) {
        currProduct.merchantLogoExtention = finalString;
    }
    else if ([elementName isEqualToString:@"display_image"]) {
        if ([finalString isEqualToString:@"Merchant Logo"]) {
            currProduct.displayImagePrefix = Image_URL_Prefix;
        }
        else {
            currProduct.displayImagePrefix = Product_Image_URL_Prefix;
        }
    }
    else if ([elementName isEqualToString:@"id"]) {
        currProduct.productId = finalString;
        
        // construct image link
        NSString *imgLink = [NSString stringWithFormat:@"%@",finalString];
        currProduct.productImgLink = imgLink;
        
    }
    else if ([elementName isEqualToString:@"merchant_id"]) {
        currProduct.merchantId = finalString;
    }
    else if ([elementName isEqualToString:@"hotoffer_extension"]) {
        currProduct.hotoffer_extension = finalString;
    }
    else if ([elementName isEqualToString:@"mobile_reward"])
    {
        currProduct.mobilecoupon = finalString;
        
        NSLog(@"........%@ ......%@ .....%@", currProduct.mobilecoupon,finalString,elementName);
    }
    else if ([elementName isEqualToString:@"name"])
    {
        currProduct.productName = finalString;
    }
    else if ([elementName isEqualToString:@"highlight"]) {
        currProduct.productOffer = finalString;
    }
    else if ([elementName isEqualToString:@"mname"]) {
        currProduct.contactMerchantName = finalString;
    }
    else if ([elementName isEqualToString:@"contact_first_name"]) {
        currProduct.contactFirstName = finalString;
    }
    else if ([elementName isEqualToString:@"contact_last_name"]) {
        currProduct.contactlastName = finalString;
    }
    else if ([elementName isEqualToString:@"mail_suburb"]) {
        currProduct.contactSuburb = finalString;
    }
    else if ([elementName isEqualToString:@"mail_state"]) {
        currProduct.contactState = finalString;
    }
    else if ([elementName isEqualToString:@"mail_postcode"]) {
        currProduct.contactPostcode = finalString;
    }
    else if ([elementName isEqualToString:@"mail_country"]) {
        currProduct.contactCountry = finalString;
    }
    else if ([elementName isEqualToString:@"phone"]) {
        currProduct.phone = finalString;
    }
    else if ([elementName isEqualToString:@"mobile"]) {
        currProduct.mobile = finalString;
    }
    else if ([elementName isEqualToString:@"link1"]) {
        currProduct.websiteLink= finalString;
    }
    else if ([elementName isEqualToString:@"product"]) {
        
        // ** Calculating image name
        if ([currProduct.displayImagePrefix isEqualToString:Image_URL_Prefix]) {
            // construct merchant image link
            NSString *imgLink = [NSString stringWithFormat:@"%@",currProduct.merchantId];
            currProduct.productImgLink = imgLink;
            
            // ** Update Extension param
            currProduct.imageExtension = currProduct.merchantLogoExtention;
        }
        else {
            // construct product image link
            NSString *imgLink = [NSString stringWithFormat:@"%@",currProduct.productId];
            currProduct.productImgLink = imgLink;
        }
        
        // Update image link (prefix+image_name+.+extention )
        
        if (currProduct.displayImagePrefix && currProduct.productImgLink) {
            currProduct.productImgLink = [NSString stringWithFormat:@"%@%@.%@",currProduct.displayImagePrefix,currProduct.productImgLink,currProduct.imageExtension];
        }
        
        
        NSLog(@"Product image link: %@",currProduct.productImgLink);
        
        [parser abortParsing];
        [delegate parsingProductDataFinished:currProduct];
        
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    
    [delegate parsingProductDataXMLFailed];
}

@end

/*
 
 <mname>Metung Holiday Villas </mname>
 <contact_first_name>Garry</contact_first_name>
 <contact_last_name>Avage</contact_last_name>
 <contact_title>Mr</contact_title>
 <contact_position>Manager</contact_position>
 <mail_address1>Cnr Mairburn &amp; Stirling Road</mail_address1>
 <mail_address2></mail_address2>
 <mail_suburb>Metung</mail_suburb>
 <mail_state>VIC</mail_state>
 <mail_postcode>3904</mail_postcode>
 <mail_country>Australia</mail_country>
 <email></email>
 <phone>03 5156 2306</phone>
 <mobile></mobile>
 <fax></fax>
 <latitude>-37.8833330</latitude>
 <longitude>147.8500000</longitude>
 
 */


