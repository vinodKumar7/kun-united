//
//  HelpViewController.h
//  GrabItNow
//
//  Created by Venkat Sasi Allamraju on 05/03/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "iCarousel.h"
#import "GrabItBaseViewController.h"
#import <MessageUI/MessageUI.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface HelpViewController : UIViewController<MFMailComposeViewControllerDelegate> {
    
}
@property (unsafe_unretained, nonatomic) IBOutlet UIPageControl *coarouselPage_Control;

@property (strong, nonatomic) GrabItBaseViewController *baseVC;
@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (nonatomic, strong) IBOutlet UIImageView *screens_ImageView;
- (UIViewController *) controllerAtIndex:(NSInteger) index ;
@end
