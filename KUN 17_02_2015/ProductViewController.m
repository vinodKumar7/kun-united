//
//  ProductViewController.m
//  GrabItNow
//
//  Created by MyRewards on 12/25/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "ProductViewController.h"
#import "ASIFormDataRequest.h"
#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "Merchant.h"
#import "ProductCell.h"
#import "MerchantAddressCell.h"

#import "NSString_stripHtml.h"
#import "ProductListViewController.h"
#import "SwitchTabBarController.h"
#define ProductCellHeight 64.0;



@interface ProductViewController ()
{
    UILabel *loadingLabel;
    
    ProductListViewController *product;
    UIView *offerView;
    UIView *contactsView;
    UIView *addressView;
    ASIFormDataRequest *imageRequest;
    ASIFormDataRequest * merchantFetchRequest;
    ASIFormDataRequest *redeemRequest;
    Product *thisProduct;
    Merchant *merchant;
    MerchantListParser *merchantListParser;
    AppDelegate *appDelegate;
    BOOL addressDetailsLoaded;
    
    int contactLabelOrigin_Y;
    NSString *callNumber;

    BOOL requestCancel;
    
}

@property (nonatomic, strong) UIView *offerView;
@property (nonatomic, strong) UIView *contactsView;
@property (nonatomic, strong) UIView *addressView;
@property (nonatomic, strong) Product *thisProduct;

@end

@implementation ProductViewController

@synthesize Location;
@synthesize userLocation;
@synthesize locationManager;
@synthesize couponMerchantImage;
@synthesize couponImageView;
@synthesize TandCButton;
@synthesize TandCView;
@synthesize TandCLabel;
@synthesize couponTandCTextView;
@synthesize TandCBackButton;
@synthesize couponFlipView;
@synthesize coupounbackgrndImageView;
@synthesize couponFrontView;
@synthesize couponBackView;
@synthesize redeemActivityIndicator;
@synthesize containerView;
@synthesize fromNearbyMe;
@synthesize couponCardBackBg_ImageView;
@synthesize offerView;
@synthesize contactsView;
@synthesize addressView;
@synthesize couponBgImageView;
@synthesize thisProduct;
@synthesize couponCardBg_ImageView;
@synthesize membership_TextField;
@synthesize scroller;
@synthesize imageActivityIndicator;
@synthesize frontredeemButton;
@synthesize imgContainerView;
@synthesize imageView;
@synthesize favButton;
@synthesize redeemButton;
@synthesize couponButton;
@synthesize productNameLabel;
@synthesize productOfferLabel;
@synthesize couponbackImageView;
@synthesize mainContentHolder;
@synthesize offerButton;
@synthesize contactButton;
@synthesize addressButton;
@synthesize noImageLabel;
@synthesize couponView;
@synthesize discountLabel;
@synthesize memNameLabel,memNumberLabel,clientLabel;
@synthesize delegate;
@synthesize merchantTableView,mailComposeView;
@synthesize myCouponImage;
@synthesize redeemnow,fromDailyDeals;

- (void)dealloc {
    
    if (imageRequest) {
        imageRequest.delegate = nil;
        [imageRequest cancel];
        imageRequest = nil;
    }
    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil andProduct:(Product *) pro
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        thisProduct = pro;
       // fromNearbyMe = NO;
        addressDetailsLoaded = NO;
        //NSLog(@"\n #################### Product ID inside init method of ProductViewController = %@",thisProduct.productId);
        //NSLog(@"\n #################### Product offer inside init method of ProductViewController = %@",thisProduct.productOffer);
    }
    
    return self;
}

- (void) updateFavoriteStatus {
    if ([appDelegate productExistsInFavorites:thisProduct]) {
        //[favButton setTitle:@"Remove favorite" forState:UIControlStateNormal];
        [favButton setImage:[UIImage imageNamed:@"heart_full.png"] forState:UIControlStateNormal];
    }
    else {
        //[favButton setTitle:@"Add to favorite" forState:UIControlStateNormal];
        [favButton setImage:[UIImage imageNamed:@"heart_empty.png"] forState:UIControlStateNormal];
    }
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    phnoCount=0;
    
    couponMerchantImage.layer.cornerRadius = 5.0;
    couponMerchantImage.layer.borderColor = [UIColor colorWithRed:39/255.0 green:74/255.0 blue:107/255.0 alpha:1.0].CGColor;
    couponMerchantImage.layer.borderWidth = 2.0;
    
    imageView.layer.borderWidth = 2.0;
    imageView.layer.borderColor = [UIColor colorWithRed:39/255.0 green:74/255.0 blue:107/255.0 alpha:1.0].CGColor;
    imageView.layer.cornerRadius = 4.0;
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    [self updateFavoriteStatus];
    
    if (thisProduct.productImage) {
        [self.imageView setImage:thisProduct.productImage];
    }
    NSLog(@"viewdidload");
    //    else {
    //        imageRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:thisProduct.productImgLink]];
    //        imageRequest.delegate = self;
    //        [imageRequest startAsynchronous];
    //    }
    
    //offerButton.layer.cornerRadius = addressButton.layer.cornerRadius = contactButton.layer.cornerRadius = 5.0;
    
    //offerButton.layer.shadowColor = addressButton.layer.shadowColor = contactButton.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
    //offerButton.layer.shadowOpacity = addressButton.layer.shadowOpacity = contactButton.layer.shadowOpacity = 1.0;
    //offerButton.layer.shadowOffset = addressButton.layer.shadowOffset = contactButton.layer.shadowOffset = CGSizeMake(2.0, -2.0);
    //offerButton.layer.shadowRadius = addressButton.layer.shadowRadius = contactButton.layer.shadowRadius = 5.0;
    
    offerButton.layer.borderColor = contactButton.layer.borderColor = addressButton.layer.borderColor = [UIColor colorWithRed:39/255.0 green:74/255.0 blue:107/255.0 alpha:1.0].CGColor;
    offerButton.layer.borderWidth = contactButton.layer.borderWidth = addressButton.layer.borderWidth = 2.0;
    couponButton.layer.borderColor = [UIColor colorWithRed:39/255.0 green:74/255.0 blue:107/255.0 alpha:1.0].CGColor;
    couponButton.layer.borderWidth = 2.0;
    
    imgContainerView.layer.cornerRadius = 5.0;
    imgContainerView.layer.borderColor = [UIColor colorWithRed:39/255.0 green:74/255.0 blue:107/255.0 alpha:1.0].CGColor;
    imgContainerView.layer.borderWidth = 2.0;
    
    
    // Intially show favorite screen
    [self offerButtonPressed:offerButton];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (appDelegate.reader.view.tag == 182) {
        self.switchTabBarController.tabBar.hidden = NO;
        appDelegate.reader.view.tag = nil;
    }
    
    couponView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.9];
    if(fromNearbyMe) {
        
       // fromNearbyMe = NO;
        
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
        [self.navigationController.navigationBar setTranslucent:NO];
        [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
        
        
        UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
        UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
        [myButton1 setImage:myImage1 forState:UIControlStateNormal];
        myButton1.showsTouchWhenHighlighted = YES;
        myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
        [myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
        UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
        self.navigationItem.leftBarButtonItem = leftButton;
        
        UIImage *myImage2 = [UIImage imageNamed:@"scan1.png"];
        UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
        [myButton2 setImage:myImage2 forState:UIControlStateNormal];
        [myButton2 addTarget:self action:@selector(scaningBarCode) forControlEvents:UIControlEventTouchUpInside];
        myButton2.showsTouchWhenHighlighted = YES;
        myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
        UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
        self.navigationItem.rightBarButtonItem = rightButton;
        
        CGRect frame = CGRectMake(0, 0, 200, 44);
        UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
        TitleLabel.backgroundColor = [UIColor clearColor];
        //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
        //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
        [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
        
        TitleLabel.text = @"Nearby Rewards";
        if (fromDailyDeals) {
             TitleLabel.text = @"Daily Deals";
        }
        TitleLabel.textAlignment = UITextAlignmentCenter;
        TitleLabel.textColor=[UIColor  whiteColor];
        
        self.navigationItem.titleView = TitleLabel;
        
        // Create cell view
        ProductCell *cell = nil;
        NSArray* views = [[NSBundle mainBundle] loadNibNamed:@"ProductCell" owner:nil options:nil];
        
        for (UIView *cellview in views) {
            if([cellview isKindOfClass:[UITableViewCell class]]) {
                cell = (ProductCell*)cellview;
            }
        }
        
        // apply shadow
        cell.headerContainerView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
        cell.headerContainerView.layer.shadowOpacity = 1.0;
        cell.headerContainerView.layer.shadowOffset = CGSizeMake(0.0, 5.0);
        cell.headerContainerView.layer.shadowRadius = 5.0;
       // cell.headerContainerView.backgroundColor = [UIColor colorWithRed:39/255.0 green:74/255.0 blue:107/255.0 alpha:1.0];
        
        cell.productNameLabel.text = thisProduct.productName;
        //cell.productNameLabel.textColor=[UIColor colorWithRed:214/255.0 green:219/255.0 blue:224/255.0 alpha:1.0];
        
        cell.productOfferLabel.text = thisProduct.productOffer;
       // cell.productOfferLabel.textColor=[UIColor colorWithRed:214/255.0 green:219/255.0 blue:224/255.0 alpha:1.0];
        cell.headerContainerView.backgroundColor = [UIColor colorWithRed:0/255.0 green:130/255.0 blue:214/255.0 alpha:1.0];
        cell.productNameLabel.textColor=[UIColor whiteColor];
        
        // Update cell height
        //        CGRect cellFrame = cell.frame;
        //        cellFrame.origin.y = -20.0;
        //        cell.frame = cellFrame;
        [self.view addSubview:cell];
        
        //============== Adding button to cell to enable back action
        
        UIButton *myButton = [UIButton buttonWithType:UIButtonTypeCustom];
        myButton.frame = CGRectMake(0,0, 320.0, 64.0);
        [myButton addTarget:self
                     action:@selector(cellpressed:)
           forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:myButton];
        [self.view bringSubviewToFront:myButton];
        
        //==================
        
        // Update view size
        CGRect viewFrame = self.containerView.frame;
        viewFrame.origin.y = cell.frame.origin.y + cell.frame.size.height;
        viewFrame.size.height = viewFrame.size.height - cell.frame.size.height;
        self.containerView.frame = viewFrame;
        
        if (!couponView.hidden) {
            [self.view bringSubviewToFront:couponView];
        }
    }
    
}

-(void)back
{
    //    [appDelegate showhomeScreen];
    
    if([merchantFetchRequest isExecuting])
    {
        requestCancel = YES;
        [merchantFetchRequest cancel];
    }
    else if ( [imageRequest isExecuting])
    {
        requestCancel = YES;
        [imageRequest cancel];
    }
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)scaningBarCode
{
    
    [appDelegate scanQRcode];
    // present and release the controller
    [self presentViewController:appDelegate.reader animated:YES completion:nil];
    self.switchTabBarController.tabBar.hidden = YES;
    
}

-(void) cellpressed:(id)sender
{
    [self dismissViewControllerAnimated:NULL completion:^{}];
    
    //[self.navigationController popToRootViewControllerAnimated:YES];
    [UIView
     transitionWithView:self.navigationController.view
     duration:0.15f
     options:UIViewAnimationOptionTransitionCrossDissolve
     animations:^{
         [self.navigationController popViewControllerAnimated:NO];}
     completion:NULL];
    
    
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    scroller.frame = self.containerView.bounds;
    NSLog(@"mobile coupon is one is %@ ",thisProduct.mobilecoupon);
}

/*
 -(void)getCurrentLocation
 {
 //userlocation tracking
 locationManager = [[CLLocationManager alloc] init];
 //locationManager.delegate = self;
 locationManager.desiredAccuracy = kCLLocationAccuracyBest;
 locationManager.distanceFilter = kCLDistanceFilterNone;
 [locationManager startUpdatingLocation];
 
 }
 
 */


//- (void)viewWillLayoutSubviews {
//
//
//}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) bringForwardTab:(UIButton *) tabBtn {
    
    if (tabBtn == offerButton) {
        
        //[scroller insertSubview:addressButton belowSubview:offerButton];
        //[scroller insertSubview:contactButton belowSubview:offerButton];
        
        offerButton.backgroundColor = [UIColor whiteColor];
        contactButton.backgroundColor = [UIColor whiteColor];
        addressButton.backgroundColor = [UIColor whiteColor];
        
        [offerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [contactButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [addressButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [offerButton setBackgroundImage:[UIImage imageNamed:@"sale1.png"] forState:UIControlStateNormal];
        [contactButton setBackgroundImage:[UIImage imageNamed:@"phone.png"] forState:UIControlStateNormal];
        [addressButton setBackgroundImage:[UIImage imageNamed:@"drop.png"] forState:UIControlStateNormal];
        
    }
    else if (tabBtn == contactButton) {
        //[scroller insertSubview:offerButton belowSubview:contactButton];
        //[scroller insertSubview:addressButton belowSubview:contactButton];
        
        offerButton.backgroundColor = [UIColor whiteColor];
        contactButton.backgroundColor = [UIColor whiteColor];
        addressButton.backgroundColor = [UIColor whiteColor];
        
        [offerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [contactButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [addressButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [offerButton setBackgroundImage:[UIImage imageNamed:@"sale.png"] forState:UIControlStateNormal];
        [contactButton setBackgroundImage:[UIImage imageNamed:@"phone1.png"] forState:UIControlStateNormal];
        [addressButton setBackgroundImage:[UIImage imageNamed:@"drop.png"] forState:UIControlStateNormal];
    }
    else if (tabBtn == addressButton) {
        //[scroller insertSubview:offerButton belowSubview:addressButton];
        //[scroller insertSubview:contactButton belowSubview:addressButton];
        
        offerButton.backgroundColor = [UIColor whiteColor];
        contactButton.backgroundColor = [UIColor whiteColor];
        addressButton.backgroundColor = [UIColor whiteColor];
        
        [offerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [contactButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [addressButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [offerButton setBackgroundImage:[UIImage imageNamed:@"sale.png"] forState:UIControlStateNormal];
        [contactButton setBackgroundImage:[UIImage imageNamed:@"phone.png"] forState:UIControlStateNormal];
        [addressButton setBackgroundImage:[UIImage imageNamed:@"drop1.png"] forState:UIControlStateNormal];
    }
    
}

- (void) cleanMainController {
    
    for (UIView *subView in mainContentHolder.subviews) {
        [subView removeFromSuperview];
    }
    
}

- (void) updateScrollerContentSize {
    
    scroller.frame = self.containerView.bounds;
    
    float contentHeight = mainContentHolder.frame.origin.y + mainContentHolder.frame.size.height +64.0;
    
    scroller.contentSize = CGSizeMake(scroller.contentSize.width, contentHeight);
    
    NSLog(@"** scroller contentSize: %@",NSStringFromCGSize(scroller.contentSize));
    NSLog(@"** scroller frame: %@",NSStringFromCGRect(scroller.frame));
    
    scroller.scrollEnabled = YES;
    
}

- (IBAction) favoritesButtonPressed:(id) sender {
    
    
    if (![appDelegate productExistsInFavorites:thisProduct]) {
        //[appDelegate removeProductFromfavorites:thisProduct];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Add to favorites?" message:[NSString stringWithFormat:@"Do you want to add %@ to favorites",thisProduct.productName] delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag = 21;
        [alert show];
    }
    else {
        //[appDelegate addProductToFavorites:thisProduct];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Remove favorite?" message:[NSString stringWithFormat:@"Do you want to remove %@ from favorites",thisProduct.productName] delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
        alert.tag = 22;
        [alert show];
    }
    
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    
    if (alertView.cancelButtonIndex == buttonIndex)
    {
        if (alertView.tag == 21)
        {
            // Add to fav case
            BOOL success = [appDelegate addProductToFavorites:thisProduct];
            
            [self updateFavoriteStatus];
            
            
            if (success && delegate && [delegate respondsToSelector:@selector(productAddedToFavorites:)]) {
                [delegate productAddedToFavorites:thisProduct];
            }
            
        }
        else if (alertView.tag == 22)
        {
            // Remove from fav case
            BOOL success = [appDelegate removeProductFromfavorites:thisProduct];
            
            [self updateFavoriteStatus];
            
            if (success && delegate && [delegate respondsToSelector:@selector(productremovedFromFavorites:)]) {
                [delegate productremovedFromFavorites:thisProduct];
            }
            
        }
        
        if(alertView.tag == 111)
        {
            NSLog(@"call alert view and button index::%d",buttonIndex);
            
            NSString *phoneURLString = [NSString stringWithFormat:@"tel:%@",callNumber];
//            NSLog(@"phone no. is %@",phoneURLString);
            NSURL *phoneURL = [NSURL URLWithString:phoneURLString];
            [[UIApplication sharedApplication] openURL:phoneURL];
            
        }
        
    }
    else
    {
        if(alertView.tag == 222)
        {
            NSLog(@"Redeem alert view and button index::%d",buttonIndex);
            if(buttonIndex == 1)
            {
                
                NSString *lattitude = @"0.00";
                //[NSString stringWithFormat:@"%f",self.userLocation.latitude];
                NSString *longitude = @"0.00";
                //[NSString stringWithFormat:@"%f",self.userLocation.longitude];
                // NSLog(@"%@",lattitude);
                // NSLog(@"%@",longitude);
                
                NSString *urlString = [NSString stringWithFormat:@"%@redeemed.php",URL_Prefix];
                NSURL *url = [NSURL URLWithString:urlString];
                NSLog(@"url is:==> %@",url);
                
                
                redeemRequest = [[ASIFormDataRequest alloc] initWithURL:url];
                [redeemRequest setPostValue:appDelegate.sessionUser.userId forKey:@"user_id"];
                [redeemRequest setPostValue:thisProduct.productId forKey:@"pid"];
                [redeemRequest setPostValue:appDelegate.sessionUser.client_id forKey:@"cid"];
                [redeemRequest setPostValue:lattitude forKey:@"lat"];
                [redeemRequest setPostValue:longitude forKey:@"lon"];
                
                [redeemRequest setDelegate:self];
                [redeemRequest startAsynchronous];
                [self.redeemActivityIndicator startAnimating];
                
                
                
            }
        }
        if(alertView.tag == 333)
        {
            NSLog(@"ThankYou alert view and button index::%d",buttonIndex);
            if(buttonIndex == 0)
            {
                
                [self handleCouponTap:nil];
                
            }
        }
    }//else
}

- (IBAction) redeemButtonPressed:(id) sender {
    
    //[delegate showMailComposer:nil withBody:nil];
    //[appDelegate.homeViewController showMailComposer:nil withBody:nil];
    
    
    UIAlertView *redemAlert = [[UIAlertView alloc]initWithTitle:@"The redeem button is for merchant use only." message:@"A merchant  will press this to record your redemption. Some offers/vouchers do not permit multiple use so don't waste a voucher.\n\n" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
    
    
    UILabel *txtField = [[UILabel alloc] initWithFrame:CGRectMake(12.0, 160.0, 260.0, 45.0)];
    [txtField setFont:[UIFont fontWithName:@"Helvetica-Bold" size:(15.0)]];
    txtField.textAlignment = UITextAlignmentCenter;
    txtField.numberOfLines = 2;
    txtField.textColor = [UIColor whiteColor];
    txtField.text = @"If you are not a merchant, Press No now to go back.";
    txtField.backgroundColor = [UIColor clearColor];
    [redemAlert addSubview:txtField];
    redemAlert.tag = 222;
    [redemAlert show];
}





- (IBAction) couponButtonPressed:(id) sender
{
    
if ([thisProduct.mobilecoupon isEqualToString:@"0"])
{
    UIAlertView *CouponAlert=[[UIAlertView alloc]initWithTitle:@"This is NOT a coupon offer" message:@"  Refer to offer terms for redemption instructions" delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil];
    CouponAlert.tag=123;
    [CouponAlert show];
    
}
else
{

    if (!fromNearbyMe)
    {
        // Alternate procedure
        NSLog(@"couponButtonPressed");
        [delegate showProductCouponForProduct:thisProduct couponImage:self.myCouponImage];
        return;
    }
    
    
    UITapGestureRecognizer * recognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(couponGestureSelector:)];
    recognizer.delegate = self;
    [couponView addGestureRecognizer:recognizer];
    
    couponView.frame = self.view.bounds;/*containerView.bounds*/;
    
    NSLog(@"prashanth..............");
    couponView.hidden = NO;
    
    couponView.backgroundColor = [UIColor colorWithWhite:0.1 alpha:0.9];
    [self.view addSubview:couponView];
    
    if (thisProduct.productOffer.length == 0)  //If ProductOffer is Null.....
    {
        thisProduct.productOffer = @"";
    }
    
    discountLabel.text = thisProduct.productOffer;
    productNameLabel.text = thisProduct.productName;
    
    couponMerchantImage.image = self.myCouponImage;
    memNameLabel.text =[NSString stringWithFormat:@"Name: %@ %@",appDelegate.sessionUser.first_name, appDelegate.sessionUser.last_name];
    clientLabel.text = [NSString stringWithFormat:@"Client: %@", appDelegate.sessionUser.client_name];
    memNumberLabel.text = [NSString stringWithFormat:@"Membership: %@", appDelegate.sessionUser.username];
    
    
    
    
    
    CGRect Cframe = couponFrontView.frame;
    Cframe.origin.y= 0;
    if([appDelegate isIphone5])
        Cframe.size.height = 470;
    else
        Cframe.size.height = 400;
    couponFrontView.frame = Cframe;
    
    CGRect Cbgframe = couponCardBg_ImageView.frame;
    Cbgframe.origin.y= 0;
    if([appDelegate isIphone5])
        Cbgframe.size.height = 510;
    else
        Cbgframe.size.height = 420;
    couponCardBg_ImageView.frame = Cbgframe;
    
    CGRect Cbframe = couponBackView.frame;
    Cbframe.origin.y= 0;
    if([appDelegate isIphone5])
        Cbframe.size.height = 470;
    else
        Cbframe.size.height = 400;
    couponBackView.frame = Cbframe;
    
    CGRect Cbbgframe = couponCardBackBg_ImageView.frame;
    Cbbgframe.origin.y= 0;
    if([appDelegate isIphone5])
        Cbbgframe.size.height = 510;
    else
        Cbbgframe.size.height = 420;
    couponCardBackBg_ImageView.frame = Cbbgframe;
    
    
    //buttons
    CGRect TandCButtonFrame = TandCButton.frame;
    TandCButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - TandCButtonFrame.size.width-22;
    if([appDelegate isIphone5])
        TandCButtonFrame.origin.y = couponFrontView.frame.size.height-70;
    else
        TandCButtonFrame.origin.y = couponFrontView.frame.size.height-90;
    TandCButton.frame = TandCButtonFrame;
    
    
    CGRect TandCBackButtonFrame = TandCBackButton.frame;
    TandCBackButtonFrame.origin.x = 40.0;
    if ([appDelegate isIphone5])
        TandCBackButtonFrame.origin.y = couponBackView.frame.size.height-70;
    else
        TandCBackButtonFrame.origin.y = couponBackView.frame.size.height-90;
    
    TandCBackButton.frame = TandCBackButtonFrame;
    
    CGRect couponFrame = couponImageView.frame;
    if ([appDelegate isIphone5])
    {}
    else
        couponFrame.size.height=360;
    couponImageView.frame= couponFrame;
    
    CGRect backcouponFrame = couponbackImageView.frame;
    if ([appDelegate isIphone5])
    {}
    else
        backcouponFrame.size.height=360;
    couponbackImageView.frame= backcouponFrame;
    
    
    CGRect fredeemButtonFrame = frontredeemButton.frame;
    fredeemButtonFrame.origin.x = 40.0;
    if ([appDelegate isIphone5])
        fredeemButtonFrame.origin.y = couponBackView.frame.size.height-70;
    else
        fredeemButtonFrame.origin.y = couponBackView.frame.size.height-90;
    frontredeemButton.frame = fredeemButtonFrame;
    
    
    CGRect redeemButtonFrame = redeemButton.frame;
    redeemButtonFrame.origin.x = couponImageView.frame.origin.x + couponImageView.frame.size.width - redeemButtonFrame.size.width - 23.0;
    if ([appDelegate isIphone5])
        redeemButtonFrame.origin.y = couponBackView.frame.size.height-70;
    else
        redeemButtonFrame.origin.y = couponBackView.frame.size.height-90;
    
    redeemButton.frame = redeemButtonFrame;
    
    
    //labels
    
    CGRect discountLabelFrame = discountLabel.frame;
    discountLabelFrame.origin.y = self.productNameLabel.frame.origin.y + self.productNameLabel.frame.size.height+20;
    //discountLabelFrame.size.height = 100;
    discountLabel.frame = discountLabelFrame;
    
    
    
    CGRect nameLabelFrame = memNameLabel.frame;
    
    if([appDelegate isIphone5])
        nameLabelFrame.origin.y = discountLabel.frame.origin.y+discountLabel.frame.size.height+20;
    else
        nameLabelFrame.origin.y = discountLabel.frame.origin.y+discountLabel.frame.size.height+10;
    memNameLabel.frame = nameLabelFrame;
    
    CGRect memnumLabelFrame = memNumberLabel.frame;
    memnumLabelFrame.origin.y = self.memNameLabel.frame.origin.y + self.memNameLabel.frame.size.height+2;
    memNumberLabel.frame = memnumLabelFrame;
    
    CGRect clientLabelFrame = clientLabel.frame;
    clientLabelFrame.origin.y = self.memNumberLabel.frame.origin.y + self.memNumberLabel.frame.size.height+2;
    clientLabel.frame = clientLabelFrame;
    
    CGRect couponmerchantimageframe=self.couponMerchantImage.frame;
    couponmerchantimageframe.origin.y=40;
    couponMerchantImage.frame=couponmerchantimageframe;
    
    CGRect TandCLabelFrame=self.TandCLabel.frame;
    // if([appDelegate isIphone5])
    TandCLabelFrame.origin.y=30;
    TandCLabel.frame=TandCLabelFrame;
    
    CGRect couponTandCTextviewframe=self.couponTandCTextView.frame;
    if([appDelegate isIphone5])
        couponTandCTextviewframe.size.height=338;
    
    else
        couponTandCTextviewframe.size.height=258;
    
    couponTandCTextviewframe.origin.y=50;
    couponTandCTextView.frame=couponTandCTextviewframe;
    
    
    CGRect couponBgFrame = couponBgImageView.frame;
    
    couponBgFrame.origin.y=183;
    if([appDelegate isIphone5])
        couponBgFrame.size.height = 150;
    else
        couponBgFrame.size.height = 112;
    couponBgImageView.frame = couponBgFrame;
    
    //===========
    
    
    //===========
    [couponFlipView addSubview:couponBackView];
    [couponFrontView addSubview:redeemnow];
    [self.couponFlipView bringSubviewToFront:redeemnow];
    
    [couponFlipView addSubview:couponFrontView];
    
    couponBackView.alpha = 0.0;
    couponFrontView.alpha = 1.0;
    
    
    }
}





- (IBAction) termsAndCondButtonPressed:(id)sender {
    
    NSLog(@"termsAndCondButtonPressed");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:couponFlipView cache:NO];
    
    NSLog(@"%@",thisProduct.productTermsAndConditions);
    
    NSString *htmlString = thisProduct.productTermsAndConditions;
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"&"
                                                       withString:@"and"];
    
    couponTandCTextView.text = [htmlString stripHtml];
//    couponTandCTextView.text = [thisProduct.productTermsAndConditions stripHtml];
    
    //couponMerchantImage.image = thisProduct.productImage;
    
    NSLog(@"::thisProduct.termsAndConditions=%@",thisProduct.productTermsAndConditions);
    couponFrontView.alpha = 0.0;
    couponBackView.alpha = 1.0;
    [UIView commitAnimations];
    
}

- (IBAction) termsAndCondBackButtonPressed:(id)sender {
    NSLog(@"termsAndCondBackButtonPressed");
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:0.5];
    [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:couponFlipView cache:NO];
    
    couponFrontView.alpha = 1.0;
    couponBackView.alpha = 0.0;
    [UIView commitAnimations];
}

- (void) couponGestureSelector:(UIGestureRecognizer *) gesRec {
    
    CGPoint touchPoint = [gesRec locationOfTouch:0 inView:couponView];
    
    if (couponFrontView.alpha == 0.0) {
        // i.e Front view is not visible, but backView is
        if( CGRectContainsPoint(TandCBackButton.frame, touchPoint) ) {
            [self termsAndCondBackButtonPressed:TandCBackButton];
        }
        else if( CGRectContainsPoint(redeemButton.frame, touchPoint) ) {
            [self redeemButtonPressed:redeemButton];
        }
        else {
            [self handleCouponTap:nil];
        }
    }
    else
    {
        // i.e Back view is not visible, but frontView is
        if( CGRectContainsPoint(TandCButton.frame, touchPoint) )
        {
            [self termsAndCondButtonPressed:TandCButton];
        }
        else if( CGRectContainsPoint(frontredeemButton.frame, touchPoint) )
        {
            [self redeemButtonPressed:redeemButton];
        }
        else
        {
            [self handleCouponTap:nil];
        }
    }
}


-(IBAction)handleCouponTap:(id)sender
{
    NSLog(@"handleCouponTap:");
    couponView.hidden = YES;
    
    // [self redeemButtonPressed:<#(id)#>];
    //[self.containerView sendSubviewToBack:couponView];
}

- (void) addOfferViewAsSubview {
    
    CGRect mainFrame = mainContentHolder.frame;
    mainFrame.size = offerView.frame.size;
    mainContentHolder.frame = mainFrame;
    
    [mainContentHolder addSubview:offerView];
    
    [self updateScrollerContentSize];
    
}

- (void) addContactsViewAsSubview {
    
    contactsView.frame = mainContentHolder.bounds;
    
    [mainContentHolder addSubview:contactsView];
    
    [self updateScrollerContentSize];
    
}

- (void) addAddressViewAsSubview {
    
    addressView.frame = mainContentHolder.bounds;
    
    [mainContentHolder addSubview:addressView];
    
    [self updateScrollerContentSize];
    
}


- (void) updateOfferView {
    
    if (!offerView) {
        return;
    }
    
    // Check if webview already exists
    
    UIWebView *offerWebView;
    //  offerWebView.scalesPageToFit = YES;
    
    if ((offerView.subviews.count != 0) ) {
        offerWebView = (UIWebView *)[offerView viewWithTag:2012];
        
    }
    else {
        offerWebView = [[UIWebView alloc] initWithFrame:offerView.bounds];
        
        NSLog(@"ELSE WEBVIEW");
    }
    
    NSString *webViewtext;
    if ([thisProduct.productDesciption length] <= 0 && [thisProduct.productText length] <= 0)
        webViewtext = @"";
    else if ([thisProduct.productDesciption length] > 0&& [thisProduct.productText length] > 0)
    {
        webViewtext = [NSString stringWithFormat:@"%@%@",thisProduct.productDesciption,thisProduct.productText];
    }else
    {
        if ([thisProduct.productDesciption length] > 0) {
            webViewtext = [NSString stringWithFormat:@"%@",thisProduct.productDesciption];
        }
        
        if ([thisProduct.productText length] > 0) {
            webViewtext = [NSString stringWithFormat:@"%@",thisProduct.productText];
        }
        
    }
    
    
    NSLog(@" webtext is..... %@ ",webViewtext);
    NSLog(@" product offers is..... %@ ",thisProduct.productOffer);
    NSString *myDescriptionHTML = @"";
    NSLog(@"QUANTITY:%@",thisProduct.quantity);
    
    if ([thisProduct.quantity isEqualToString:@"0"]||
        [thisProduct.quantity length]<=0)
        myDescriptionHTML =  [NSString stringWithFormat:@"<html> \n"
                              "<head> \n"
                              "<style type=\"text/css\"> \n"
                              "body {font-family: \"%@\"; font-size: %@;line-height:1.5%;}\n"
                              "H4{ color: rgb(198,38,21) }\n"
                              "</style> \n"
                              "<style type='text/css'>body { max-width: 290%; width: auto; height: auto; }</style>"
                              "<style type='text/css'>img { max-width: 300%; width: auto; height: auto; }</style>"
                              "</head> \n"
                              "<body><H4>%@</H4>%@</body> \n"
                              "</html>", @"Helvetica", [NSNumber numberWithInt:13],thisProduct.productOffer, webViewtext];
    
    else{
        NSString *shopNowURL = [NSString stringWithFormat:@"http://www.perksatwork.com.au/?user=%@&pageid=%@",appDelegate.sessionUser.username,thisProduct.quantity];
        
        myDescriptionHTML =  [NSString stringWithFormat:@"<html> \n"
                              "<head> \n"
                              "<style type=\"text/css\"> \n"
                              "body {font-family: \"%@\"; font-size: %@;line-height:1.5%;}\n"
                              "H4{ color: rgb(198,38,21) }\n"
                              "</style> \n"
                              "<style type='text/css'>body { max-width: 290%; width: auto; height: auto; }</style>"
                              "<style type='text/css'>img { max-width: 300%; width: auto; height: auto; }</style>"
                              "</head> \n"
                              "<body><H4>%@</H4><a href=%@  style=color:rgb(198,38,21)>Shop Now</a>%@</body> \n"
                              "</html>", @"Helvetica", [NSNumber numberWithInt:13],thisProduct.productOffer,shopNowURL,webViewtext];
        
        
    }
    
    
    NSString *stripped = [myDescriptionHTML stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    
    
    NSLog(@"html is final  %@",stripped);
    NSLog(@"html is %@",myDescriptionHTML);
    [offerWebView loadHTMLString:stripped baseURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] bundlePath]]];
    offerWebView.tag = 2012;
    //   offerWebView.scalesPageToFit = YES;
    //   offerWebView.multipleTouchEnabled= YES;
    offerWebView.delegate = self;
    [offerView addSubview:offerWebView];
    
    // NSLog(@"phone number:;%@",thisProduct.phone);
    // Add DETAILS here
    
    /*
     
     float commonYGap = 15.0;
     float xMargin = 10.0;
     float commonWidth = offerView.frame.size.width - 2*xMargin;
     
     */
    /*
     
     UILabel *productDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(xMargin, commonYGap, commonWidth, 30.0)];
     productDescLabel.numberOfLines = 0;
     productDescLabel.backgroundColor = [UIColor clearColor];
     productDescLabel.textColor = [UIColor darkGrayColor];
     
     UIFont *commonFont = [UIFont systemFontOfSize:17.0];
     
     CGSize proDescConstraintSize = [thisProduct.productDesciption sizeWithFont:commonFont constrainedToSize:CGSizeMake(commonWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
     
     productDescLabel.text = thisProduct.productDesciption;
     
     CGRect finalProductDescFrame = productDescLabel.frame;
     finalProductDescFrame.size = proDescConstraintSize;
     productDescLabel.frame = finalProductDescFrame;
     
     [offerView addSubview:productDescLabel];
     */
    // Add Product detail description here
    /*
     float proDetDescLblY = productDescLabel.frame.size.height + productDescLabel.frame.origin.y + commonYGap;
     
     UILabel *productDetailedDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(xMargin, proDetDescLblY, commonWidth, 30.0)];
     productDetailedDescLabel.backgroundColor = [UIColor clearColor];
     productDetailedDescLabel.textColor = [UIColor darkGrayColor];
     
     CGSize proDetailedDescConstraintSize = [thisProduct.productDetailedDesciption sizeWithFont:commonFont constrainedToSize:CGSizeMake(commonWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
     
     productDetailedDescLabel.text = thisProduct.productDetailedDesciption;
     
     CGRect finalProductDetailedDescFrame = productDetailedDescLabel.frame;
     finalProductDetailedDescFrame.size = proDetailedDescConstraintSize;
     productDetailedDescLabel.frame = finalProductDetailedDescFrame;
     
     [offerView addSubview:productDetailedDescLabel];
     */
    // Add terms and conditions here
    /*
     float proTACLblY = productDetailedDescLabel.frame.size.height + productDetailedDescLabel.frame.origin.y + commonYGap;
     
     UILabel *TACDescLabel = [[UILabel alloc] initWithFrame:CGRectMake(xMargin, proTACLblY, commonWidth, 30.0)];
     TACDescLabel.backgroundColor = [UIColor clearColor];
     TACDescLabel.textColor = [UIColor darkGrayColor];
     
     CGSize proTACConstraintSize = [thisProduct.productTermsAndConditions sizeWithFont:commonFont constrainedToSize:CGSizeMake(commonWidth, 9999) lineBreakMode:NSLineBreakByWordWrapping];
     
     TACDescLabel.text = thisProduct.productTermsAndConditions;
     
     CGRect finalTACFrame = TACDescLabel.frame;
     finalTACFrame.size = proTACConstraintSize;
     TACDescLabel.frame = finalTACFrame;
     
     [offerView addSubview:TACDescLabel];
     */
}

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [loadingLabel setHidden:NO];
}


//=======
-(BOOL)doesString:(NSString *)string containCharacter:(char)character
{
    if ([string rangeOfString:[NSString stringWithFormat:@"%c",character]].location != NSNotFound)
    {
        return YES;
    }
    return NO;
}


//=======


- (void) updateContactsView
{
    NSLog(@"\n merchant name = %@", thisProduct.contactMerchantName);
    if (!contactsView)
    {
        return;
    }
    
    NSLog(@"phno::%@",thisProduct.phone);
    NSLog(@"website link::%@",thisProduct.websiteLink);
    
    NSString* phnoString = [thisProduct.phone stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    phArray = [[NSArray alloc]init];
    phMutableArray = [[NSMutableArray alloc]init];
    
//    phArray = [phnoString componentsSeparatedByString:@"/,"];
    phArray = [phnoString componentsSeparatedByCharactersInSet:
               [NSCharacterSet characterSetWithCharactersInString:@"/,"]
               ];
    NSLog(@"strings %@",phArray);
    
    if([phArray count] !=  0)
    {
        contactLabelOrigin_Y = 5;
        for (int i = 0; i < [phArray count]; i++) {
            
            UILabel *contactlabel = [[UILabel alloc]initWithFrame:CGRectMake(60, contactLabelOrigin_Y, 150, 30)];
            [contactsView addSubview:contactlabel];
            
            if ([[phArray objectAtIndex:i] length] <= 4) {
                
                NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[[phArray objectAtIndex:i-1] length] - [[phArray objectAtIndex:i] length]];
                
                for (int j=0; j < [[phArray objectAtIndex:i-1] length] - [[phArray objectAtIndex:i] length]; j++) {
                    NSString *ichar  = [NSString stringWithFormat:@"%c", [[phArray objectAtIndex:i-1] characterAtIndex:j]];
                    [characters addObject:ichar];
                }
                
                NSString * newString = [[characters valueForKey:@"description"] componentsJoinedByString:@""];
                contactlabel.text = [newString stringByAppendingString:[phArray objectAtIndex:i]];
                
                [phMutableArray addObject:contactlabel.text];
            }
            else
            {
                contactlabel.text = [phArray objectAtIndex:i];
                [phMutableArray addObject:[phArray objectAtIndex:i]];
            }
            
            UIButton *contactlabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(30, contactLabelOrigin_Y+3, 25, 25)];
            contactlabel_Button.tag = i;
            [contactlabel_Button addTarget:self action:@selector(callButtonpressed:)forControlEvents:UIControlEventTouchUpInside];
            [contactlabel_Button setImage:[UIImage imageNamed:@"phones.png"] forState:UIControlStateNormal];
            
            [contactsView addSubview:contactlabel_Button];
            
            contactLabelOrigin_Y = contactLabelOrigin_Y + 30;
        }
        
    }
    else
    {
        UILabel *contactsLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60, 5, 150, 50)];
        
        contactsLabel.text = @"No contact Number Available";
        
        
        contactsLabel.numberOfLines = 0;
        contactsLabel.font = [UIFont systemFontOfSize:15.0];
        [contactsView addSubview:contactsLabel];
        
    }

    // website button and label
    if([thisProduct.websiteLink length] > 0)
    {
        UILabel *websiteLabel;
        UIButton *websiteLabel_Button;
        
        if([phArray count] ==1){
            websiteLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60, 35, 200, 30)];
            websiteLabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(60, 35, 200, 30)];}
        else{
            websiteLabel  = [[UILabel alloc]initWithFrame:CGRectMake(60,65, 200, 30)];
            websiteLabel_Button = [[UIButton alloc]initWithFrame:CGRectMake(60, 65, 200, 30)];                                                                          }
        [websiteLabel_Button addTarget:self
                                action:@selector(websiteButtonpressed:)
                      forControlEvents:UIControlEventTouchUpInside];
        
        websiteLabel.text = thisProduct.websiteLink;
        
        websiteLabel.numberOfLines = 0;
        websiteLabel.font = [UIFont systemFontOfSize:15.0];
        [contactsView addSubview:websiteLabel];
        [contactsView addSubview:websiteLabel_Button];
        
        UIButton *websiteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [websiteButton setBackgroundImage:[UIImage imageNamed:@"globe.png"] forState:UIControlStateNormal];
        [websiteButton addTarget:self
                          action:@selector(websiteButtonpressed:)
                forControlEvents:UIControlEventTouchUpInside];
        if([phArray count] ==1)
            websiteButton.frame = CGRectMake(30.0,40.0, 25.0, 25.0);
        
        else
            websiteButton.frame = CGRectMake(30.0,70.0, 25.0, 25.0);
        [contactsView addSubview:websiteButton];
        
        
    }
    
    
}

- (void) updateAddressView {
    
    [self fetchMerchantWithProductID:thisProduct.productId];
    
}

- (void) fetchMerchantWithProductID:(NSString *) productId {
    
    if (merchantFetchRequest) {
        // Already a request is in progress.
        return;
    }
    
    if(addressDetailsLoaded) {
        // Already address is processed.
        return;
    }
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product_addresses.php?pid=%@",URL_Prefix,productId];
    
    NSLog(@"Fetch Merchants URL: %@",urlString);
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    merchantFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [merchantFetchRequest setDelegate:self];
    [merchantFetchRequest startAsynchronous];
    
    
}


-(NSString *)appendStringWithNewline:(NSString *)str
{
    NSString *appendStr = [NSString stringWithFormat:@"\n%@",str];
    return appendStr;
}

- (IBAction) offerButtonPressed:(id) sender {
    
    [self bringForwardTab:sender];
    
    if (!offerView) {
        offerView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
        
        offerView.backgroundColor = [UIColor whiteColor];
        
        //offerView.layer.cornerRadius = 5.0;
        offerView.layer.borderColor = [UIColor colorWithRed:39/255.0 green:74/255.0 blue:107/255.0 alpha:1.0].CGColor;
        offerView.layer.borderWidth = 2.0;
    }
    
    [self cleanMainController];
    
    [self updateOfferView];
    
    [self addOfferViewAsSubview];
    [self updateUIWithProductDetails:thisProduct];
    
}

- (IBAction) contactButtonPressed:(id) sender {
    
    [self bringForwardTab:sender];
    
    if (!contactsView) {
        contactsView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
        contactsView.backgroundColor = [UIColor whiteColor];
        
        //contactsView.layer.cornerRadius = 5.0;
        contactsView.layer.borderColor = [UIColor colorWithRed:39/255.0 green:74/255.0 blue:107/255.0 alpha:1.0].CGColor;
        contactsView.layer.borderWidth = 2.0;
        
        
        
    }
    
    [self cleanMainController];
    [self updateContactsView];
    [self addContactsViewAsSubview];
    
}

-(void)callButtonpressed:(id)sender
{
    NSLog(@"call button pressed");
    
    
    callNumber =@"";
    NSLog(@"callButtonPressed %d",[sender tag]);
    
    callNumber = [phMutableArray objectAtIndex:[sender tag]];
    
    UIAlertView *call_Alert = [[UIAlertView alloc]initWithTitle:@"Call" message:callNumber delegate:self cancelButtonTitle:@"Call" otherButtonTitles:@"Cancel" , nil];
    call_Alert.tag = 111;
    [call_Alert show];
    
}
-(void)websiteButtonpressed:(id)sender
{
    NSLog(@"==website button pressed");
    NSURL *websiteUrl = [NSURL URLWithString:thisProduct.websiteLink];
    
    [[UIApplication sharedApplication] openURL:websiteUrl];
}
- (IBAction) addressButtonPressed:(id) sender {
    
    [self bringForwardTab:sender];
    
    if (!addressView) {
        addressView = [[UIView alloc] initWithFrame:mainContentHolder.bounds];
        addressView.backgroundColor = [UIColor whiteColor];
        
        //addressView.layer.cornerRadius = 5.0;
        addressView.layer.borderColor = [UIColor colorWithRed:39/255.0 green:74/255.0 blue:107/255.0 alpha:1.0].CGColor;
        addressView.layer.borderWidth = 2.0;
    }
    if(!merchantTableView)
    {
        merchantTableView = [[UITableView alloc]initWithFrame:mainContentHolder.bounds style:UITableViewStylePlain];
        merchantTableView.delegate = self;
        merchantTableView.dataSource = self;
        [addressView addSubview:merchantTableView];
    }
    
    [self cleanMainController];
    [self updateAddressView];
    [self addAddressViewAsSubview];
    
}


- (IBAction) mapButtonPressed:(id) sender
{
    NSLog(@"=====MAPBUTTON PRESSED=====");
    
    //  NSString *googleMapsURLString = [NSString stringWithFormat:@"http://maps.google.com/?saddr=newyork"];
    
    NSLog(@"Tag value is..... %d",[sender tag]);
    Merchant *aMerchant = [merchantList objectAtIndex:[sender tag]];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    NSLog(@"latitude and longitude is.. %f,%f",Location.latitude,Location.longitude);
    
    
    
    NSURL *addressUrl = [NSURL URLWithString:[NSString stringWithFormat:@"http://maps.apple.com?q=%f,%f",Location.latitude, Location.longitude, nil]];
    
    [[UIApplication sharedApplication] openURL:addressUrl];
}

//get_product.php

- (void) fetchProductDetails:(id) sender {
    
}


- (void) updateUIWithProductDetails:(Product *) aProduct {
    
    thisProduct = aProduct;
    
    if (thisProduct.productImage) {
        [self.imageView setImage:thisProduct.productImage];
    }
    else {
        NSLog(@"thisProduct.productImgLink: %@",thisProduct.productImgLink);
        imageRequest = [ASIFormDataRequest requestWithURL:[NSURL URLWithString:thisProduct.productImgLink]];
        imageRequest.delegate = self;
        [self.imageActivityIndicator startAnimating];
        [imageRequest startAsynchronous];
    }
    
    
    [self updateOfferView];
    
    NSLog(@"** PRODUCT ** updateUIWithProductDetails");
    
}



//#pragma WebViewDelegate methods

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    
    if (webView.tag = 2012) {
        
        UIWebView *offerWebView = webView;
        
        CGRect modifiedFrame = offerWebView.frame;
        modifiedFrame.size = offerWebView.scrollView.contentSize;
        offerWebView.frame = modifiedFrame;
        
        NSLog(@"webview frame = %@, webview content size = %@",NSStringFromCGRect(offerWebView.frame),NSStringFromCGSize(offerWebView.scrollView.contentSize));
        
        CGRect modifiedOfferViewFrame = offerView.frame;
        modifiedOfferViewFrame.size = offerWebView.frame.size;
        offerView.frame = modifiedOfferViewFrame;
        
        if ([offerView superview]) {
            [self addOfferViewAsSubview];
        }
        
    }
    
}

-(BOOL)webView:(UIWebView *)descriptionTextView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    if (UIWebViewNavigationTypeLinkClicked == navigationType) {
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}


#pragma mark -- ASIHttpRequestDelegate methods

- (void) showNoImage {
    imageView.layer.borderWidth = 2.0;
    imageView.layer.borderColor = [UIColor colorWithRed:39/255.0 green:74/255.0 blue:107/255.0 alpha:1.0].CGColor;
    imageView.layer.cornerRadius = 4.0;
    
    self.noImageLabel.textColor = [UIColor darkGrayColor];
    self.noImageLabel.hidden = NO;
}


- (void)requestFailed:(ASIHTTPRequest *)request {
    
    if (requestCancel == YES) {
        requestCancel = NO;
    }
    else
    {
        if (request == imageRequest) {
            imageRequest = nil;
            [self.imageActivityIndicator stopAnimating];
            //UIImage *image = [UIImage imageWithData:[request responseData]];
            //[self.imageView setImage:image];
            // [self showNoImage];
            [self performSelector:@selector(showNoImage) withObject:nil afterDelay:2];
        }
        else if(request == merchantFetchRequest) {
            merchantFetchRequest = nil;
            NSLog(@"Product ** RES: %@",[request responseString]);
            
            NSXMLParser *merchantParser = [[NSXMLParser alloc] initWithData:[request responseData]];
            merchantListParser = [[MerchantListParser alloc] init];
            merchantListParser.delegate = self;
            merchantParser.delegate = merchantListParser;
            [merchantParser parse];
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
        }
        
        else if(request == redeemRequest)
        {
            redeemRequest = nil;
            [self.redeemActivityIndicator stopAnimating];
            NSLog(@"REDEEM RESPONSE::: %@",[request responseString]);
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
        }

    }
    
    
}

- (void)requestFinished:(ASIHTTPRequest *)request {
    if (request == imageRequest) {
        imageRequest = nil;
        [self.imageActivityIndicator stopAnimating];
        UIImage *image = [UIImage imageWithData:[request responseData]];
        
        if (image.size.width == 0 && image.size.height == 0) {
            //NSLog(@"requestFinished -- :( There was NO image");
           // [self showNoImage];
            [self performSelector:@selector(showNoImage) withObject:nil afterDelay:2];
            
        }
        else {
            
            [self.imageView setImage:image];
            
            
            self.myCouponImage = image;
            
            
        }
    }
    else if(request == merchantFetchRequest) {
        NSLog(@"Merchant ** RES: %@",[request responseString]);
        
        NSXMLParser *merchantParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        merchantListParser = [[MerchantListParser alloc] init];
        merchantListParser.delegate = self;
        merchantParser.delegate = merchantListParser;
        [merchantParser parse];
        
        merchantFetchRequest = nil;
        addressDetailsLoaded = YES;
    }
    
    
    else if(request == redeemRequest)
    {
        redeemRequest = nil;
        [self.redeemActivityIndicator stopAnimating];
        NSLog(@"REDEEM RESPONSE::: %@",[request responseString]);
        
        NSString *msg = [NSString stringWithFormat:@"For redeeming this offer %@",thisProduct.productName];
        if([[request responseString] isEqualToString:@"success"])
        {
            
            UIAlertView *thanksAlert = [[UIAlertView alloc]initWithTitle:@"Thank You!" message:msg delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK",nil, nil];
            thanksAlert.tag = 333;
            [thanksAlert show];
            
        }
    }
    
}



- (void)parsingMerchantListFinished:(NSArray *)merchantsListLocal {
    NSLog(@"\n Merchants lIst in  parsingDataFinished metnod = %d",[merchantsListLocal count]);
    
    if (!merchantList) {
        merchantList = [[NSMutableArray alloc] initWithArray:merchantsListLocal];
    }
    else {
        [merchantList addObjectsFromArray:merchantsListLocal];
    }
    
    [merchantTableView reloadData];
}

- (void)parsingMerchantListXMLFailed {
    
}


#pragma mark -- UITableView Datasource & Delegate methods

/** Datasource methods **/
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    NSLog(@"\n MerchantsList count = %d",[merchantList count]);
    return [merchantList count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    static NSString *loadingCellIdentifier = @"loadingCell";
    
    if (indexPath.row == merchantList.count) {
        
        UITableViewCell *loadingCell = [tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
        
        if (!loadingCell) {
            loadingCell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:loadingCellIdentifier];
        }
        
        
        loadingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        return loadingCell;
        
    }
    
    
    MerchantAddressCell *cell ;
    
    
    cell= (MerchantAddressCell *)[tableView dequeueReusableCellWithIdentifier:loadingCellIdentifier];
    if(cell==nil)
    {
        
        cell =[[[NSBundle mainBundle] loadNibNamed:@"MerchantAddressCell" owner:self options:nil]objectAtIndex:0];
        
        
    }
    
    
    
    Merchant *aMerchant = [merchantList objectAtIndex:indexPath.row];
    
    NSLog(@"Address: %@",aMerchant.mMailAddress1);
    NSLog(@"Suburb:%@",aMerchant.mMailSuburb);
    NSLog(@"State:%@",aMerchant.mState);
    NSLog(@"Phone:%@",aMerchant.mPhone);
    
    if([aMerchant.mMailAddress1 length]<=0 && [merchantList count] == 1){
        NSLog(@"null");
        cell.merchantAddress.text = @"No Addresses Available";
        cell.mapButton.hidden = YES;
        cell.maplabel.hidden = YES;
    }
    else{
        NSString * str=aMerchant.mMailAddress1;
        
        if( [aMerchant.mMailSuburb isEqualToString:@"(null)"] )
        {// NSLog(@"Address: %@",aMerchant.mMailAddress1);
            // str=@"trimmed";
            
        }
        else
        {
            NSLog(@"null: %@",[NSNull null]);
            
            str = [str stringByAppendingString:[self appendStringWithNewline:aMerchant.mMailSuburb]];
        }
        if([aMerchant.mState length]>0 && [aMerchant.mPostCode length]>0)
            str = [NSString stringWithFormat:@"%@,%@, %@-%@",str,aMerchant.mState,aMerchant.mCity,aMerchant.mPostCode];
        else if([aMerchant.mState length]>0)
            str = [NSString stringWithFormat:@"%@, %@",str,aMerchant.mState];
        if([aMerchant.mPhone length]>0 ){
            NSString *phno = [NSString stringWithFormat:@"Ph: %@",aMerchant.mPhone];
            str = [str stringByAppendingString:[self appendStringWithNewline:phno]];
        }
        // NSString *deviceName = @"Kenny's iPhone";
       // NSLog(@"non--stripped1 is...%@ ",str  );
        NSString *stripped = [str stringByReplacingOccurrencesOfString:@"(null)," withString:@""];
        NSString *stripped1 = [stripped stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
        //[stripped stringByReplacingOccurrencesOfString:@"," withString:@""];
        
        
        
        //  NSLog(@"stripped1 is...%@ ",stripped  );
        
        
        //NSString* result = [[stripped  componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]] componentsJoinedByString:@" "];
        // NSLog(@"stripped fnl is...%@ ",result );
        cell.merchantAddress.text = stripped1;
    }
    cell.merchantAddress.textColor = [UIColor blackColor];
    Location.latitude = aMerchant.coordinate.latitude;
    Location.longitude = aMerchant.coordinate.longitude;
    
    
    NSLog(@"%f,%f",Location.latitude,Location.longitude);
    if ( (Location.latitude == 0.000000)|| (Location.longitude == 0.000000) || [cell.merchantAddress.text isEqualToString:@"No Addresses Available"])
    {
        cell.mapButton.hidden = YES;
        cell.maplabel.hidden = YES;
        
        
    }
    else
    {
        cell.mapButton.hidden = NO;
        cell.maplabel.hidden = NO;
        
    }
    cell.mapButton.tag = indexPath.row;
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return ProductCellHeight;
}







- (void)viewDidUnload {
    [self setCouponBgImageView:nil];
    [self setCouponbackImageView:nil];
    //  [self setFredeemButton:nil];
    [self setFrontredeemButton:nil];
    [super viewDidUnload];
}
@end
