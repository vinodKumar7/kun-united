//
//  DailyDealsViewController.m
//  GrabItNow
//
//  Created by MyRewards on 3/22/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "DailyDealsViewController.h"
#import "SwitchTabBarController.h"
#import "AppDelegate.h"


@interface DailyDealsViewController ()
{
    AppDelegate *appDelegate;
    ASIFormDataRequest *getImageNPidRequest;
    ASIFormDataRequest * productFetchRequest;
    ProductDataParser *productDataXMLParser;
    NSString *currentProID;
    
    BOOL requestCancel;
    int i;
}
- (void) showActivityView;
- (void) dismissActivityView;
-(void)fetchImageAndProductID;
@end

@implementation DailyDealsViewController

@synthesize activityView;
@synthesize productController;
@synthesize prodDetail;
@synthesize myImageView;
@synthesize loadingView;
//============================ DELEGATE METHODS  ============================//

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
        self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
        return self;
}


- (void)viewDidLoad
{
        i=0;
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
        NSLog(@" client id:::%@",appDelegate.sessionUser.client_id);
        NSLog(@"%@",appDelegate.sessionUser.country);
    
        CGRect myImageFrame = self.myImageView.frame;
    if([appDelegate isIphone5])
        myImageFrame.size.height = 470;
    else
        
        myImageFrame.size.height = 375;
        self.myImageView.frame = myImageFrame;
    
   
    
    
        [self showActivityView];
        [self fetchImageAndProductID];
        [super viewDidLoad];
    
   
}

-(void)viewWillAppear:(BOOL)animated
{
    if (appDelegate.reader.view.tag == 182) {
        self.switchTabBarController.tabBar.hidden = NO;
        appDelegate.reader.view.tag = nil;
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    
    UIImage *myImage2 = [UIImage imageNamed:@"scan1.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(scaningBarCode) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    
    TitleLabel.text = @"Daily Deals";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    TitleLabel.textColor=[UIColor  whiteColor];
    
    self.navigationItem.titleView = TitleLabel;

       
}

-(void)back
{
    if([getImageNPidRequest isExecuting])
    {
        requestCancel = YES;
        [getImageNPidRequest cancel];
    }
    
    [appDelegate showhomeScreen];
    //    [self.navigationController popViewControllerAnimated:NO];
}

-(void)scaningBarCode
{
    
    [appDelegate scanQRcode];
    // present and release the controller
    //    [self presentViewController:appDelegate.reader animated:YES completion:nil];
    [self.view.window.rootViewController presentViewController:appDelegate.reader animated:YES completion:nil];
    self.switchTabBarController.tabBar.hidden = YES;
}

//===============================================================//

//============================ SHOW ALERTVIEW METHODS  ============================//
- (void) showActivityView
{
        activityView.frame = self.view.bounds;
        UIView *loadingLabelView = [activityView viewWithTag:10];
        loadingLabelView.center = activityView.center;
        [self.view addSubview:activityView];
}
- (void) dismissActivityView
{
        [activityView removeFromSuperview];
}
//===============================================================//

//============================ SHOW ALERTVIEW METHODS  ============================//
-(IBAction)Image_Clicked:(id)sender;
{
        [self showActivityView];
        [self fetchProductWithProductID:currentProID];
    
    
}

//=================== PRODUCT IMAGE AND ID FETCHING =====================//
-(void)fetchImageAndProductID {
    
   
    
    
    NSString *urlString = [[NSString stringWithFormat:@"http://www.myrewards.com.au/newapp/get_daily_deal.php?cid=%@&country=%@",appDelegate.sessionUser.client_id,appDelegate.sessionUser.country]stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding];
    
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    
    NSLog(@"url is:==> %@",url);
        getImageNPidRequest = [[ASIFormDataRequest alloc] initWithURL:url];
   

    
        [getImageNPidRequest setDelegate:self];
        [getImageNPidRequest startAsynchronous];
    
    
    
}
//============================ PRODUCT PRODUTS ===================================//
- (void) fetchProductWithProductID:(NSString *) productId
{
    
        // Make server call for more products.
        NSString *urlString = [NSString stringWithFormat:@"%@get_product.php?id=%@",URL_Prefix,productId];
    
        NSLog(@"Search URL: %@",urlString);
    
    
        NSURL *url = [NSURL URLWithString:urlString];
        productFetchRequest = [ASIFormDataRequest requestWithURL:url];
        [productFetchRequest setDelegate:self];
        [productFetchRequest startAsynchronous];
    
    
}





//============================ ASIHTTP REQUEST DELEGATE METHODS ====================//
#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
        NSLog(@"DailyDealsVC -- requestFinished:");
    
    if (request == getImageNPidRequest)
    {
       
        NSLog(@"**getImageNPidRequest  RES: %@",[request responseString]);        
        getImageNPidRequest = nil;
        i=1;
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
        
        
    }
    if (request == productFetchRequest)
    {
        [self dismissActivityView];
        productFetchRequest = nil;
        i=2;
        NSLog(@"Product ** RES: %@",[request responseString]);
        NSLog(@"ProductDataParser");
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
    }
    
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
        NSLog(@"DailyDealVC -- requestFailed:");
    
    if (requestCancel == YES) {
        
    }
    else
    {
        if (request == getImageNPidRequest)
        {
            [self dismissActivityView];
            NSLog(@"**getImageNPidRequest  req failed: %@",[request responseString]);
            getImageNPidRequest = nil;
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
        }
        else
            
        {
            [self dismissActivityView];
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
            productFetchRequest = nil;
        }

    }
}



//============================ ***********************   ==========================//

#pragma mark -- Product Data Parser Delegate methods

- (void)parsingProductDataFinished:(Product *)product
{
    
        prodDetail = product;
        // NSLog(@"\n product offer = %@", prodDetail.productOffer);
    
    if(i==1)
    {
        NSLog(@"%@",prodDetail.hotoffer_extension);
        NSLog(@"product id %@",prodDetail.productId);
        currentProID = prodDetail.productId;
        
        NSString *str = [NSString stringWithFormat:@"%@%@.%@",DailyDeal_Image_URL_Prefix ,prodDetail.productId,prodDetail.hotoffer_extension];
        NSLog(@"String is:::%@",str);
        
                                 
        UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:str]]];
        self.myImageView.image = image;
        [self dismissActivityView];
    }
    
    else if (i==2)
    {
    
    if (productController)
    {
        if ([productController.view superview])
        {
            [productController.view removeFromSuperview];
        };
             productController = nil;
    }
    
        ProductViewController *prVC = [[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil andProduct:prodDetail];
        prVC.fromNearbyMe = YES;
        prVC.fromDailyDeals = YES;
        prVC.delegate = self;
        productController = prVC;
        prVC.routeCheck=1;
        [self.navigationController pushViewController:prVC animated:YES];
    
        [productController updateUIWithProductDetails:product];
    }
}

-(Product *)returnProduct:(Product *)productDet
{
        return Nil;
}

- (void)parsingProductDataXMLFailed
{
    
}

- (void) productAddedToFavorites:(Product *) pro{}
- (void) productremovedFromFavorites:(Product *) pro{}
- (void) showProductCouponForProduct:(Product *) pro couponImage:image{}



//===================================================================================================//

- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidUnload {
        [self setLoadingView:nil];
        [super viewDidUnload];
}
@end
