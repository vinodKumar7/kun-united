//
//  NoticeBoardDataParser.m
//  GrabItNow
//
//  Created by MyRewards on 3/25/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "NoticeBoardDataParser.h"
#import "NoticeBoard.h"
#import "AppDelegate.h"

@class AppDelegate, NoticeBoard;
@interface NoticeBoardDataParser()
{
    AppDelegate *appdelegate;
    
    
        NSMutableString *charString;
    NoticeBoard *currNotice;
    NSMutableArray *noticesList;
}

@end

@implementation NoticeBoardDataParser
@synthesize delegate;



- (void)parserDidStartDocument:(NSXMLParser *)parser {
    noticesList = [[NSMutableArray alloc] init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    charString = nil;
    
    if ([elementName isEqualToString:@"product"]) {
        currNotice = [[NoticeBoard alloc] init];
    }
    if ([elementName isEqualToString:@"notice"]) {
        currNotice = [[NoticeBoard alloc] init];
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string {
    
    if (!charString) {
        charString = [[NSMutableString alloc] initWithString:string];
    }
    else {
        [charString appendString:string];
    }
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName {
    
    NSString *finalString = [charString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    
    if ([elementName isEqualToString:@"id"]) {
        //NSLog(@"didEndElement");
       currNotice.notice_Id = finalString;
        
        [noticesList addObject:currNotice];
        }
    
    
      else if ([elementName isEqualToString:@"user_id"]) {
           currNotice.user_Id = finalString;
           }
           else if ([elementName isEqualToString:@"client_id"]) {
           currNotice.client_Id = finalString;
           }
           else if ([elementName isEqualToString:@"subject"]) {
           currNotice.subject = finalString;
          }
           else if ([elementName isEqualToString:@"details"]) {
           currNotice.details = finalString;
           }
           else if ([elementName isEqualToString:@"created"]) {
               currNotice.created_Date = finalString;
           }

           else if ([elementName isEqualToString:@"product"]) {
           
          // NSLog(@"current notid %@",currNotice.notice_Id);
           
           //[noticesList addObject:currNotice];
           
          // NSLog(@"notiList is %@",noticesList);
           currNotice = nil;
           
           }
    
           else if([elementName isEqualToString:@"root"]) {
               [parser abortParsing];
               
               [delegate parsingNoticeBoardDataFinished:noticesList];
           }
}
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    
    
    //[delegate parsingNoticeBoardDataXMLFailed];
}

@end




