//
//  MyCardViewController.m
//  GrabItNow
//
//  Created by Monish Kumar on 06/01/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "MyCardViewControllerGrab.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"

@interface MyCardViewControllerGrab ()
{
    ASIFormDataRequest *imageRequest;
    AppDelegate *appDelegate;
}
- (void) myCardsDisplay;
@end

@implementation MyCardViewControllerGrab
@synthesize background;
@synthesize myCardImageView, userNameLabel, clientIdLabel,userNameLabel1;
//=========================DELEGATE METHODS=============================//

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void) viewWillAppear:(BOOL)animated
{
       [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidLoad
{
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [self myCardsDisplay];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

//===================================================//
//===============DELEGATE METHODS TO LOCK ORIENTATION=================//
-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}
//-(BOOL)s

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

- (BOOL) shouldAutorotate
{
    return NO;
}
//===================================================//

//============METHOD TO DISPLAY CARD VIEW IN IPHONE 4 & 5 =========//

- (void) myCardsDisplay
{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@.%@",MyCard_URL_Prefix,appDelegate.sessionUser.client_id,appDelegate.sessionUser.card_ext];
    
    NSLog(@"Image URL: %@",urlString);
    NSURL *url = [NSURL URLWithString:urlString];
    
  
    NSData *data = [[NSData alloc] initWithContentsOfURL:url];
    UIImage *img = [[UIImage alloc] initWithData:data];
    myCardImageView.image = img;
    
    NSString *userNameStr = [NSString stringWithFormat:@"Name: %@ %@",appDelegate.sessionUser.first_name, appDelegate.sessionUser.last_name];
    userNameLabel.text = userNameStr;
    userNameLabel.transform = CGAffineTransformMakeRotation (-3.14/2);
    
    clientIdLabel.text = [NSString stringWithFormat:@"Client: %@", appDelegate.sessionUser.client_name];
    clientIdLabel.transform = CGAffineTransformMakeRotation (-3.14/2);
    
    userNameLabel1.text = [NSString stringWithFormat:@"M No: %@", appDelegate.sessionUser.username];
    userNameLabel1.transform = CGAffineTransformMakeRotation (-3.14/2);

    // Update label frames
    CGRect clientIdLabelFrame = clientIdLabel.frame;
    clientIdLabelFrame.origin.x = myCardImageView.frame.origin.x - clientIdLabelFrame.size.width;
    clientIdLabel.frame = clientIdLabelFrame;
    
    CGRect userNameLabelFrame = userNameLabel.frame;
    userNameLabelFrame.origin.x = myCardImageView.frame.origin.x + myCardImageView.frame.size.width;
    userNameLabel.frame = userNameLabelFrame;
    
    CGRect userNameLabel1Frame = userNameLabel1.frame;
    userNameLabel1Frame.origin.x = userNameLabelFrame.origin.x + userNameLabel.frame.size.width;
    userNameLabel1Frame.origin.y = userNameLabelFrame.origin.y;
    userNameLabel1.frame = userNameLabel1Frame;
}

//===================================================//
//=============== ASIHttpRequest DELEGATE METHODS=================//

#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
}

- (void)requestFailed:(ASIHTTPRequest *)request
{    
    NSLog(@"Image Data -- requestFailed:");
}

//===================================================//


- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


@end
