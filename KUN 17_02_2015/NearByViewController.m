//
//  NearByViewController.m
//  GrabItNow
//
//  Created by Monish Kumar on 06/01/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import "NearByViewController.h"
#import "ASIFormDataRequest.h"
#import "AppDelegate.h"
#import "ASIHTTPRequest.h"
#import "ProductDataParser.h"
#import "ProductListParser.h"
#import "ProductAnnotation.h"
#import "ProductListViewController.h"
#import "ZSPinAnnotation.h"
#import "SwitchTabBarController.h"
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <MapKit/MapKit.h>

@interface NearByViewController ()
{
    ASIFormDataRequest *categoryNearByRequest;
    ASIFormDataRequest * productFetchRequest;
    
    AppDelegate *appDelegate;
    ProductListParser *productsXMLParser;
    ProductDataParser *productDataXMLParser;
    BOOL isSearchCalled;
    NSMutableArray *productsList;
    NSMutableArray *repAnnonatastions;
    UIButton *myButton1;
    
    BOOL requestCancel;
}
@property(nonatomic,strong) NSMutableArray *productsList;
@property(nonatomic,strong)NSMutableArray *repAnnonatastions;
-(void)getCurrentLocation;
- (void) fetchProductWithProductID:(NSString *) productId;
- (void) showActivityView;
- (void) dismissActivityView;
- (void)mutateCoordinatesOfClashingAnnotations:(NSArray *)annotations;
@end

@implementation NearByViewController
@synthesize mkView;
@synthesize locationManager;
@synthesize currentLocation;
@synthesize productsList;
@synthesize productController;
@synthesize prodDetail;
@synthesize activityView;
@synthesize repAnnonatastions;
//=========================DELEGATE METHODS=============================//
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    isSearchCalled = NO;
    [mkView setMapType:MKMapTypeStandard];
    [mkView setZoomEnabled:YES];
    [mkView setScrollEnabled:YES];
    [self getCurrentLocation];
    
    [mkView setDelegate:self];
    
    repAnnonatastions=[[NSMutableArray alloc]init];
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void) viewWillAppear:(BOOL)animated
{
    NSLog(@"===viewWillAppear====");
    [super viewWillAppear:animated];
    
    if (appDelegate.reader.view.tag == 182) {
        self.switchTabBarController.tabBar.hidden = NO;
        appDelegate.reader.view.tag = nil;
    }
   
// [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    if (![CLLocationManager locationServicesEnabled])
    {
        
        NSLog(@"===location Services not Enabled ====");
        UIAlertView *locatioAlert = [[UIAlertView alloc] initWithTitle:@"ALERT"
                                                               message:@"Your location sevices are not enabled."
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [locatioAlert show];
    }
    
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    
    UIImage *myImage2 = [UIImage imageNamed:@"scan1.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(scaningBarCode) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    
    TitleLabel.text = @"Nearby Rewards";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    TitleLabel.textColor=[UIColor  whiteColor];
    
    self.navigationItem.titleView = TitleLabel;

}

-(void)back
{

    if([categoryNearByRequest isExecuting])
    {
        requestCancel = YES;
        [categoryNearByRequest cancel];
    }
    
    [appDelegate.navController popViewControllerAnimated:NO];
}

-(void)scaningBarCode
{
    
    [appDelegate scanQRcode];
    // present and release the controller
    //    [self presentViewController:appDelegate.reader animated:YES completion:nil];
    [self.view.window.rootViewController presentViewController:appDelegate.reader animated:YES completion:nil];
    self.switchTabBarController.tabBar.hidden = YES;
}

- (void)updateCurrentLocation {
    
    if ([CLLocationManager locationServicesEnabled]) {
        if ([self.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [self.locationManager performSelector:@selector(requestWhenInUseAuthorization)];
        }
        // .. do something here ..//
    }
    
    [self.locationManager startUpdatingLocation];
}

-(void)getCurrentLocation
{
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
//    [locationManager startUpdatingLocation];
    
    //Vinod Added
    [self updateCurrentLocation];
  }

- (void)viewDidUnload
{
    
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}
//===================================================//

//===============DELEGATE METHODS TO LOCK ORIENTATION=================//
-(void)orientationDetected:(UIEvent *)event
{
    [[UIApplication sharedApplication] setStatusBarOrientation: [UIDevice currentDevice].orientation animated: NO ];
    [[UIApplication sharedApplication] setStatusBarOrientation:UIInterfaceOrientationMaskPortrait animated:NO];
    
}
//-(BOOL)s

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsPortrait(interfaceOrientation);
}


-(NSUInteger)supportedInterfaceOrientations
{
    return  UIInterfaceOrientationMaskPortrait ;
}

- (BOOL) shouldAutorotate
{
    return NO;
}
//====================================================//

//===============METHODS TO LOAD PINS AROUND USER=================//

- (void) searchProductsOnKeywordBasis
{
    
      
    // Make server call for more products.
    NSLog(@"\n Lat = %f, long = %f in searchProductsOnKeywordBasis",self.currentLocation.latitude,self.currentLocation.longitude);
    NSString *urlString = [NSString stringWithFormat:@"%@get_products_by_loc.php?lat=%f&lng=%f&cid=%@&b=%f&c=%@",URL_Prefix,self.currentLocation.latitude,self.currentLocation.longitude,appDelegate.sessionUser.client_id,0.05,appDelegate.sessionUser.country];
    NSLog(@"Search URL: %@",urlString);
    
    // NSURL *url = [NSURL URLWithString:urlString];
    NSURL *url = [NSURL URLWithString:[urlString stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    
    categoryNearByRequest = [ASIFormDataRequest requestWithURL:url];
    [categoryNearByRequest setDelegate:self];
    [categoryNearByRequest startAsynchronous];

    
}
//===================================================//
//===============METHODS TO ADD PINS AROUND USER=================//

- (void) addNewAnnotations
{
   // NSLog(@"==========addNewAnnotations===========");
    NSLog(@"PRODUCTS COUNT: %d",[productsList count]);
    //int i=0;
    for(Product *prod in productsList)
    {
       // NSLog(@"\n Product offers in addNewAnnotations Method = %@",prod.productOffer);
        ProductAnnotation *ann = [[ProductAnnotation alloc]init];
        ann.coordinate = prod.coordinate;
        ann.title = prod.productName;
        NSLog(@"Coo :: %f",ann.coordinate);
        //ann.subtitle=prod.pin_type;
        ann.prod = prod;
        ann.pinColor = prod.pin_type;
        //ann.prodAnnTag = i++;
        [mkView addAnnotation:ann];
        [repAnnonatastions addObject:ann];
        
            }
    NSLog(@"================================================");
    for (ProductAnnotation *ann  in repAnnonatastions ) {
        
        NSLog(@"title::%@",ann.title);
        NSLog(@"pinType::%@",ann.subtitle);
        NSLog(@"latitude::%f",ann.coordinate.latitude);
        NSLog(@"longitude::%f",ann.coordinate.longitude);
        NSLog(@"-----------------------------------------------");
    }
    NSLog(@"================================================");
    
    [self mutateCoordinatesOfClashingAnnotations:repAnnonatastions];
  

}
//===================================================//

//=========================DELEGATE METHODS=============================//

#pragma mark CoreLocation Delegate Methods.........

- (void)locationManager:(CLLocationManager *)manager
	didUpdateToLocation:(CLLocation *)newLocation
           fromLocation:(CLLocation *)oldLocation
{
    NSLog(@"==========locationManager===========");
    CLLocationCoordinate2D coordinate = [newLocation coordinate];
    NSString *latitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    NSString *longitude = [NSString stringWithFormat:@"%f", coordinate.longitude];

    
    NSLog(@"latitude : %@", latitude);
    NSLog(@"longitude : %@",longitude);
    self.currentLocation = coordinate;
    MKCoordinateRegion region = mkView.region; 
        
    region.center.latitude = self.currentLocation.latitude;
    region.center.longitude = self.currentLocation.longitude;
    region.span.longitudeDelta = 0.05f;
    region.span.latitudeDelta = 0.0f;
    [mkView setRegion:region animated:YES];
    [mkView showsUserLocation];
    if(isSearchCalled == NO)
    {
        [self searchProductsOnKeywordBasis];
        isSearchCalled = YES;
        [locationManager stopUpdatingLocation];
    }
    
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    if (error.code ==  kCLErrorDenied)
    {
        NSLog(@"Location manager denied access - kCLErrorDenied");
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location issue"
                                                        message:@"Your location cannot be determined."
                                                       delegate:nil
                                              cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
    }
}


- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation
{
     NSLog(@"==========viewForAnnotation===========");
    

    if ([[(ProductAnnotation*)annotation title] isEqualToString:@"Current Location"])
    {
        
        return nil;
    }
    static NSString *annotationIdentifier = @"AnnotationIdentifier";
    MKAnnotationView *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:annotationIdentifier];
    
    
   
    if (annotationView == nil)
        
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:annotationIdentifier];
    
    
    NSLog(@"%@ %@",[(ProductAnnotation*)annotation title],[(ProductAnnotation*)annotation pinColor]);
        
        if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"1"])
        {
            NSLog(@"==>AUTOMOBILE:: %@",[(ProductAnnotation*)annotation title]);
            annotationView.image = [UIImage imageNamed:@"automobile_map.png"];
            annotationView.annotation = annotation;
        }
       else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"2"])
       {
            NSLog(@"==>DINNING:: %@",[(ProductAnnotation*)annotation title]);
            annotationView.image = [UIImage imageNamed:@"dining_map.png"];
            annotationView.annotation = annotation;
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"3"])
        {
            NSLog(@"==>HEALTH&BEAUTY:: %@",[(ProductAnnotation*)annotation title]);
            annotationView.image = [UIImage imageNamed:@"healthAndBeauty_map.png"];
            annotationView.annotation = annotation;
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"4"])
        {
            NSLog(@"==>HOME:: %@",[(ProductAnnotation*)annotation title]);
            annotationView.image = [UIImage imageNamed:@"home_map.png"];
            annotationView.annotation = annotation;
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"5"])
        {
            NSLog(@"==>SHOPPING:: %@",[(ProductAnnotation*)annotation title]);
            annotationView.image = [UIImage imageNamed:@"Shopping_map.png"];
            annotationView.annotation = annotation;
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"6"])
        {
            NSLog(@"==>TRAVEL:: %@",[(ProductAnnotation*)annotation title]);
            annotationView.image = [UIImage imageNamed:@"travel_map.png"];
            annotationView.annotation = annotation;
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"7"])
        {
            NSLog(@"==>LEISSURE & ENTERTAINMENT:: %@",[(ProductAnnotation*)annotation title]);
            annotationView.image = [UIImage imageNamed:@"leisure_map.png"];
            annotationView.annotation = annotation;
        }
        else if ([[(ProductAnnotation*)annotation pinColor] isEqualToString:@"0"])
        {
            NSLog(@"==>GOLF:: %@",[(ProductAnnotation*)annotation title]);
            annotationView.image = [UIImage imageNamed:@"golf_map.png"];
            annotationView.annotation = annotation;
        }
        else
        {
            annotationView.image = [UIImage imageNamed:@"empty_map.png"];
            annotationView.annotation = annotation;
        }
        annotationView.canShowCallout = YES;
        annotationView.rightCalloutAccessoryView = [UIButton buttonWithType:UIButtonTypeDetailDisclosure];
        annotationView.userInteractionEnabled = YES;
        
   // }
   // else
  //  {
     //   NSLog(@"ELSE");
       // annotationView.annotation = annotation;
   // }
    
    
    return annotationView;
}

- (void)mutateCoordinatesOfClashingAnnotations:(NSArray *)annotations
{
    
    NSDictionary *coordinateValuesToAnnotations = [self groupAnnotationsByLocationValue:annotations];
    NSLog(@"coordinateValuesToAnnotations::%d",[coordinateValuesToAnnotations count]);
    
    for (NSValue *coordinateValue in coordinateValuesToAnnotations.allKeys) {
        NSMutableArray *outletsAtLocation = coordinateValuesToAnnotations[coordinateValue];
        if (outletsAtLocation.count > 1) {
            CLLocationCoordinate2D coordinate;
            [coordinateValue getValue:&coordinate];
            [self repositionAnnotations:outletsAtLocation toAvoidClashAtCoordination:coordinate];
        }
    }
}

- (NSDictionary *)groupAnnotationsByLocationValue:(NSArray *)annotations
{

    
    NSMutableDictionary *result = [NSMutableDictionary dictionary];
    
    for (id<MKAnnotation> pin in annotations)
    {
        
        CLLocationCoordinate2D coordinate = pin.coordinate;
        NSValue *coordinateValue = [NSValue valueWithBytes:&coordinate objCType:@encode(CLLocationCoordinate2D)];
        
        NSMutableArray *annotationsAtLocation = result[coordinateValue];
        if (!annotationsAtLocation) {
            annotationsAtLocation = [NSMutableArray array];
            result[coordinateValue] = annotationsAtLocation;
        }
        
        [annotationsAtLocation addObject:pin];
        
        //NSLog(@"annotationsAtLocation count is %d ",annotationsAtLocation.count);
        
    }
    return result;
}

- (void)repositionAnnotations:(NSMutableArray *)annotations toAvoidClashAtCoordination:(CLLocationCoordinate2D)coordinate
{
  
    double distance = 3 * annotations.count / 2.0;
    //NSLog(@"distance::%f",distance);
    double radiansBetweenAnnotations = (M_PI * 2) / annotations.count;
    
    for (int i = 0; i < annotations.count; i++) {
        
        double heading = radiansBetweenAnnotations * i;
        CLLocationCoordinate2D newCoordinate = [self calculateCoordinateFrom:coordinate onBearing:heading atDistance:distance];
        
        id <MKAnnotation> annotation = annotations[i];
        annotation.coordinate = newCoordinate;
        
        [mkView addAnnotation:annotations[i]];
    }
    //NSLog(@"annotations.count %d ",annotations.count );
    //[mkView reloadInputViews];
}

- (CLLocationCoordinate2D)calculateCoordinateFrom:(CLLocationCoordinate2D)coordinate  onBearing:(double)bearingInRadians atDistance:(double)distanceInMetres
{
     
    
    double coordinateLatitudeInRadians = coordinate.latitude * M_PI / 180;
    double coordinateLongitudeInRadians = coordinate.longitude * M_PI / 180;
    
    double distanceComparedToEarth = distanceInMetres / 6378100;
    
    double resultLatitudeInRadians = asin(sin(coordinateLatitudeInRadians) * cos(distanceComparedToEarth) + cos(coordinateLatitudeInRadians) * sin(distanceComparedToEarth) * cos(bearingInRadians));
    double resultLongitudeInRadians = coordinateLongitudeInRadians + atan2(sin(bearingInRadians) * sin(distanceComparedToEarth) * cos(coordinateLatitudeInRadians), cos(distanceComparedToEarth) - sin(coordinateLatitudeInRadians) * sin(resultLatitudeInRadians));
    
    CLLocationCoordinate2D result;
    result.latitude = resultLatitudeInRadians * 180 / M_PI;
    result.longitude = resultLongitudeInRadians * 180 / M_PI;
    return result;
}

- (void)mapView:(MKMapView *)mapView annotationView:(MKAnnotationView *)view calloutAccessoryControlTapped:(UIControl *)control
{
    ProductAnnotation *ann =(ProductAnnotation *)view.annotation;
    [self showActivityView];
    [self fetchProductWithProductID:ann.prod.productId];
}

- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated
{
    
    
   // NSLog(@"##map region: %f,%f",mapView.region.center.latitude,mapView.region.center.longitude);
    
}

- (void)mapView:(MKMapView *)aMapView didUpdateUserLocation:(MKUserLocation *)userLocation
{
  /*  CLLocationCoordinate2D loc = [userLocation.location coordinate];
    MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(loc, 1000.0f, 1000.0f);
    [mkView setRegion:region animated:YES]; */
}

//===================================================//
//=========== METHOD TO SHOW ACTIVITY VIEW   ========================//

- (void) showActivityView
{
    activityView.frame = self.view.bounds;
    UIView *loadingLabelView = [activityView viewWithTag:10];
    loadingLabelView.center = activityView.center;
    [self.view addSubview:activityView];
}
//===================================================//
//=============== METHOD TO DISMISS ACTIVITY VIEW   ====================//

- (void) dismissActivityView
{
    [activityView removeFromSuperview];
}
//===================================================//

//===========ASIHttpRequest DELEGATE METHODS========================//

#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    NSLog(@"ProdcutsListVC -- requestFinished:");
    
    if (request == categoryNearByRequest)
    {
        
        categoryNearByRequest = nil;
        
        //NSLog(@"RES in NEARBY: %@",[request responseString]);
        
        NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productsXMLParser = [[ProductListParser alloc] init];
        productsXMLParser.delegate = self;
        productsParser.delegate = productsXMLParser;
        [productsParser parse];
        
    }
    if (request == productFetchRequest)
    {
        [self dismissActivityView];
        productFetchRequest = nil;
        NSLog(@"Product ** RES: %@",[request responseString]);
        NSLog(@"ProductDataParser");
        NSXMLParser *productParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productDataXMLParser = [[ProductDataParser alloc] init];
        productDataXMLParser.delegate = self;
        productParser.delegate = productDataXMLParser;
        [productParser parse];
    }
    
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    NSLog(@"ProdcutsListVC -- requestFailed:");
    
    if (requestCancel == YES) {
        
    }
    else
    {
        if (request == categoryNearByRequest)
        {
            categoryNearByRequest = nil;
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
        }
        else
        {
            [self dismissActivityView];
            UIAlertView *failureAlert = [[UIAlertView alloc] initWithTitle:@"Network Connection failed" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil];
            [failureAlert show];
            
            productFetchRequest = nil;
        }
    }
    
    myButton1.userInteractionEnabled = YES;
        
}

//===================================================//
//===========Product Parser DELEGATE METHODS========================//

#pragma mark -- Product Parser Delegate methods

- (void)parsingProductListFinished:(NSArray *)prodcutsListLocal
{
    
    NSLog(@".......... prodcutsListLocal: %d",[prodcutsListLocal count]);
    
    // Here update products list
    if (!productsList)
    {
        productsList = [[NSMutableArray alloc] initWithArray:prodcutsListLocal];
    }
    else
    {
        [productsList addObjectsFromArray:prodcutsListLocal];
        NSLog(@"pins Count::%d",[productsList count]);
    }
    [self addNewAnnotations];
}

- (void)parsingProductListXMLFailed
{
    
}
//===================================================//
//===========Product Data  Parser DELEGATE METHODS========================//

#pragma mark -- Product Data Parser Delegate methods

- (void)parsingProductDataFinished:(Product *)product
{
    
    prodDetail = product;
   // NSLog(@"\n product offer = %@", prodDetail.productOffer);
    
    if (productController)
    {
        if ([productController.view superview])
        {
            [productController.view removeFromSuperview];
        };
        productController = nil;
    }
    
    ProductViewController *prVC = [[ProductViewController alloc] initWithNibName:@"ProductViewController" bundle:nil andProduct:prodDetail];
     productController = prVC;
    prVC.fromNearbyMe = YES;
    prVC.delegate = self;
    prVC.routeCheck=0;
    
   
    [self.navigationController pushViewController:prVC animated:YES];
    /*[self presentViewController:prVC animated:NO completion:^{
        CGRect frame=prVC.view.frame;
        frame.origin.y=0;
        prVC.view.frame=frame;}];*/

    
    [productController updateUIWithProductDetails:product];
    
}

-(Product *)returnProduct:(Product *)productDet
{
    return  Nil;
}

- (void)parsingProductDataXMLFailed
{
    
}
//===================================================//

//=========== METHOD TO FETCH USER SELECTED PIN DETAILS   =================//


- (void) fetchProductWithProductID:(NSString *) productId
{
    
    // Make server call for more products.
    NSString *urlString = [NSString stringWithFormat:@"%@get_product.php?id=%@",URL_Prefix,productId];
    
    NSLog(@"URL: %@",urlString);
    
    
    NSURL *url = [NSURL URLWithString:urlString];
    productFetchRequest = [ASIFormDataRequest requestWithURL:url];
    [productFetchRequest setDelegate:self];
    [productFetchRequest startAsynchronous];
    
    
}
//===================================================//




- (void) productAddedToFavorites:(Product *) pro{}
- (void) productremovedFromFavorites:(Product *) pro{}
- (void) showProductCouponForProduct:(Product *) pro couponImage:image{}

@end
