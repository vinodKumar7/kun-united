//
//  DailyDealsViewController.h
//  GrabItNow
//
//  Created by MyRewards on 3/22/13.
//  Copyright (c) 2013 MyRewards. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ProductDataParser.h"
#import "Product.h"
#import "ProductViewController.h"
#import "ASIHTTPRequest.h"
#import "ASIFormDataRequest.h"

@interface DailyDealsViewController : UIViewController<ASIHTTPRequestDelegate,ProductViewDelegate,ProductXMLParserDelegate>
{
    ProductViewController *productController;
}

@property (nonatomic, strong) IBOutlet UIView *activityView;
@property (nonatomic, strong) ProductViewController *productController;
@property (nonatomic, strong) Product *prodDetail;
@property (nonatomic, strong) IBOutlet UIImageView *myImageView;
@property (strong, nonatomic) IBOutlet UIView *loadingView;

-(IBAction)Image_Clicked:(id)sender;
@end
