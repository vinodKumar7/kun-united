//
//  ProductCell.m
//  GrabItNow
//
//  Created by MyRewards on 12/23/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "ProductCell.h"

@implementation ProductCell

@synthesize productImg;
@synthesize productNameLabel;
@synthesize productOfferLabel;
@synthesize imgLoadingIndicator;
@synthesize noImageLabel;
@synthesize headerContainerView;
@synthesize removeFavoritebutton;
@synthesize cell_backgroundImage;
@synthesize accesoryImageView;


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
        imgLoadingIndicator.hidesWhenStopped = YES;
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
