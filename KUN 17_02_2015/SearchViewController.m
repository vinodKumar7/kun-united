//
//  SearchViewController.m
//  GrabItNow
//
//  Created by MyRewards on 11/25/12.
//  Copyright (c) 2012 MyRewards. All rights reserved.
//

#import "SearchViewController.h"

#import <QuartzCore/QuartzCore.h>
#import "ASIFormDataRequest.h"
#import "OffersViewController.h"
#import "AppDelegate.h"
#import "Category.h"
#import "ProductListViewController.h"
#import "GrabItBaseViewController.h"
#import "MenuViewController.h"
#import "SwitchTabBarController.h"

@interface SearchViewController ()
{
    AppDelegate *appDelegate;
    
    BOOL showingSettingsView;
    
    NSString *locString;
    NSString *keyString;
    
    NSMutableArray *categoryArray;
    
    ASIFormDataRequest *categoryFetchRequest;
    ASIFormDataRequest *keywordSearchrequest;
    ASIFormDataRequest *categorySearchRequest;
    
    CategoryXMLParser *categoryParserDelegate;
    ProductListParser *productsXMLParser;
    
    ProductListViewController *productsListController;
    
    int selectedCategory;
    
    int currentPageNo;
    NSString *helpText;
    
    BOOL requestCancel;

}

@property (nonatomic, strong) ProductListViewController *productsListController;
@property (nonatomic, strong) IBOutlet UIWebView *helpText_webView;

- (void) fetchCategoryList;
- (void) searchProductsOnKeywordBasis;
-(void)slideInView:(UIView*)newView toView:(UIView*)oldView ;

@end

@implementation SearchViewController

@synthesize productsListController;

@synthesize tblView;
@synthesize toolBar;
@synthesize settingsView;
@synthesize mainController;
@synthesize helpText_webView;
@synthesize currentTextfield;


@synthesize locationLabel;
@synthesize keywordLabel;
@synthesize locationEditTextField;
@synthesize keywordEditTextField;
@synthesize infoView;
@synthesize activityView;

@synthesize infoButton;
@synthesize resetButton;
//=========================DELEGATE METHODS=============================//

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        showingSettingsView = NO;
        
        locString = @"";
        keyString = @"";
        
        categoryArray = [[NSMutableArray alloc] init];
        
        selectedCategory = -1;
        
        currentPageNo = 0;
        
        helpText =@"<h3>  &nbsp &nbsp &nbsp &nbsp Searching made easy!</h3> This function allows you to search by category ,location and keywords<br><br> 1.  Select the category in the menu .<br> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp      EG: dining and fast food  <br><br>2. Enter a location such as a city, town or suburb.  <br>&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp           EG: Carlton or Auckland  <br><br> 3. Enter a key word  <br> &nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp&nbsp  EG : Pizza  <br><br> The search will look for a dining offers that are located  in Carlton and has pizza in the key words.  <br><br>If your search is unsuccessful we recommend that you broaden the search parameters.   <br><br>To broaden your  search simply select the category and then enter a location and exclude any keywords.  <br><br>All the offers that match your category and location will be displayed ";
        
    }
        return self;
} 

- (void)dealloc
{
    
    if (categoryFetchRequest)
    {
        [categoryFetchRequest cancel];
         categoryFetchRequest = nil;
    }
    
    if (categorySearchRequest)
    {
        [categorySearchRequest cancel];
         categorySearchRequest = nil;
    }
}


- (void)viewDidLoad
{
        [super viewDidLoad];
        // Do any additional setup after loading the view from its nib.
        selectedCategory = -1;
        appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

        // ** Apply round corners to the Container
        UIView *infoLabelBgView = [infoView viewWithTag:10];
        infoLabelBgView.layer.borderColor = [UIColor colorWithWhite:1.0 alpha:1.0].CGColor;
        infoLabelBgView.layer.borderWidth = 2.0;
        infoLabelBgView.layer.cornerRadius = 4.0;
    
        settingsView.layer.shadowColor = [UIColor colorWithWhite:0.2 alpha:0.8].CGColor;
        settingsView.layer.shadowOpacity = 1.0;
        settingsView.layer.shadowOffset = CGSizeMake(0.0, -3.0);
        settingsView.layer.shadowRadius = 3.0;
    
        helpText = [NSString stringWithFormat:@"<html> \n"
                "<head> \n"
                "<style type=\"text/css\"> \n"
                "body {font-family: \"%@\"; font-size: %@; font-color: \"%@\"}\n"
                "</style> \n"
                "</head> \n"
                "<body>%@</body> \n"
                "</html>", @"Helvetica", [NSNumber numberWithInt:15],@"#FF00000", helpText];
    
        [self.helpText_webView setOpaque:NO];
        // [self.helpText_webView loadHTMLString:helpText baseURL:nil];
        [self.helpText_webView loadHTMLString:[NSString stringWithFormat:@"<html><body p style='color:white' text=\"#FFFFFF\" face=\"Bookman Old Style, Book Antiqua, Garamond\" size=\"5\">%@</body></html>", helpText] baseURL: nil];
    
        [self fetchCategoryList];
    
}

- (void)viewWillAppear:(BOOL)animated
{
        [super viewWillAppear:animated];
//        [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    
    if (appDelegate.reader.view.tag == 182) {
        self.switchTabBarController.tabBar.hidden = NO;
        appDelegate.reader.view.tag = nil;
    }
    
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    self.navigationController.navigationBar.barStyle = UIStatusBarStyleLightContent;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBackgroundImage:[UIImage imageNamed:@"navBar.png"] forBarMetrics:UIBarMetricsDefault];
    
    
    UIImage *myImage1 = [UIImage imageNamed:@"Back1.png"];
	UIButton *myButton1 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton1 setImage:myImage1 forState:UIControlStateNormal];
	myButton1.showsTouchWhenHighlighted = YES;
	myButton1.frame = CGRectMake(0.0, 3.0, 50,30);
	[myButton1 addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
	UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:myButton1];
	self.navigationItem.leftBarButtonItem = leftButton;
    
    
    UIImage *myImage2 = [UIImage imageNamed:@"scan1.png"];
	UIButton *myButton2 = [UIButton buttonWithType:UIButtonTypeCustom];
	[myButton2 setImage:myImage2 forState:UIControlStateNormal];
    [myButton2 addTarget:self action:@selector(scaningBarCode) forControlEvents:UIControlEventTouchUpInside];
	myButton2.showsTouchWhenHighlighted = YES;
    myButton2.frame = CGRectMake(0.0, 30.0, 50,40);
    UIBarButtonItem *rightButton = [[UIBarButtonItem alloc] initWithCustomView:myButton2];
	self.navigationItem.rightBarButtonItem = rightButton;
    
    CGRect frame = CGRectMake(0, 0, 200, 44);
    UILabel    *TitleLabel = [[UILabel alloc] initWithFrame:frame];
    TitleLabel.backgroundColor = [UIColor clearColor];
    //TitleLabel.font = [UIFont boldSystemFontOfSize:16.0];
    //TitleLabel.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5];
    [TitleLabel setFont:[UIFont fontWithName:@"Helvetica-Bold" size:17]];
    
    TitleLabel.text = @"My Rewards";
    TitleLabel.textAlignment = UITextAlignmentCenter;
    TitleLabel.textColor=[UIColor  whiteColor];
    
    self.navigationItem.titleView = TitleLabel;

    
    
    if (IOS_VERSION <= 7)
    {
       // self.tblView = [[UITableViewController alloc] initWithStyle:UITableViewStylePlain];
    }

}

-(void)back
{
    
    if([categoryFetchRequest isExecuting])
    {
        requestCancel = YES;
        [categorySearchRequest cancel];
    }
    else if([categorySearchRequest isExecuting])
    {
        requestCancel = YES;
        [categoryFetchRequest cancel];
    }
    
    
    [appDelegate.navController popViewControllerAnimated:NO];
}

-(void)scaningBarCode
{
    
    [appDelegate scanQRcode];
    // present and release the controller
    //    [self presentViewController:appDelegate.reader animated:YES completion:nil];
    [self.view.window.rootViewController presentViewController:appDelegate.reader animated:YES completion:nil];
    self.switchTabBarController.tabBar.hidden = YES;
}


- (void)didReceiveMemoryWarning
{
        [super didReceiveMemoryWarning];
        // Dispose of any resources that can be recreated.
}
//===================================================//
//========SETTING FRAMES TO SEARCH VIEW  ====================//
- (void)viewWillLayoutSubviews
{
        /*
    CGRect toolbarFrame = self.toolBar.frame;
    toolbarFrame.origin.y = self.view.frame.size.height - toolbarFrame.size.height;
    self.toolBar.frame = toolbarFrame;
    */
    
        CGRect settingsViewFrame = settingsView.frame;
        settingsViewFrame.origin.y = self.view.frame.size.height - settingsViewFrame.size.height;
        settingsView.frame = settingsViewFrame;
    
        CGRect tableFrame = tblView.frame;
        tableFrame.size.height = self.view.frame.size.height - settingsViewFrame.size.height;
        tblView.frame = tableFrame;
    
        CGRect infoButtonFrame = infoButton.frame;
        infoButtonFrame.origin.y = self.view.frame.size.height - 175.0;
        infoButton.frame = infoButtonFrame;
    
}
//===================================================//
//========SHOWS ACTIVITY VIEW  ====================//
- (void) showActivityView {
    
        CGRect activityframe=self.activityView.frame;
    if([appDelegate isIphone5])
        activityframe.origin.y=-20;
    else
        activityframe.origin.y=-20;
        activityframe.size.height=550;
    
        activityView.frame=activityframe;
        UIView *loadingLabelView = [activityView viewWithTag:10];
        loadingLabelView.center = activityView.center;
        [self.view addSubview:activityView];
}
//===================================================//
//========DISMISS ACTIVITY VIEW  ====================//
- (void) dismissActivityView {
        [activityView removeFromSuperview];
}
//===================================================//
//============FETCHS PRODUCT LIST ON CATEGORY SELECTED  ====================//
- (void) fetchCategoryList {
    
        NSLog(@"... fetchCategoryList ..., cid=%@ && country=%@",appDelegate.sessionUser.client_id,appDelegate.sessionUser.country);
        [self showActivityView];
    
    NSURL *url = [NSURL URLWithString:[[NSString stringWithFormat:@"%@get_cat.php?cid=%@&country=%@",URL_Prefix,appDelegate.sessionUser.client_id,appDelegate.sessionUser.country ]stringByAddingPercentEscapesUsingEncoding: NSUTF8StringEncoding]];
    NSLog(@"url is:%@",url);
        categoryFetchRequest = [ASIFormDataRequest requestWithURL:url];
        [categoryFetchRequest setDelegate:self];
        [categoryFetchRequest startAsynchronous];
    
}
//===================================================//
//====FETCHS PRODUCT LIST ON "LOCATION" OR "KEYWORD" ENTERED BY USER  =====//

- (void) searchProductsOnKeywordBasis
{

        [self showActivityView];
    
        Category *cat;
        NSString *categoryID = @"-1";
    if(selectedCategory != -1)
    {
        cat = [categoryArray objectAtIndex:selectedCategory];
        categoryID = cat.catId;
    }
        
        NSString *urlString = [NSString stringWithFormat:@"%@search.php?",URL_Prefix];
    
    if(selectedCategory != -1)
    {
        urlString = [NSString stringWithFormat:@"%@cat_id=%@",urlString,categoryID];
        
    }
    if([locationEditTextField.text length]>0)
    {
        
        if(selectedCategory != -1)
        {
            
            urlString = [NSString stringWithFormat:@"%@&",urlString];

        }
            urlString = [NSString stringWithFormat:@"%@p=%@",urlString,locationEditTextField.text];
    }
    if([keywordEditTextField.text length]>0)
    {
        
        if(selectedCategory != -1 || ([locationEditTextField.text length]!= 0))
        {
            
            urlString = [NSString stringWithFormat:@"%@&",urlString];
            
        }
            urlString = [NSString stringWithFormat:@"%@q=%@",urlString,keywordEditTextField.text];
    }
    
    urlString = [NSString stringWithFormat:@"%@&cid=%@&country=%@&start=0&limit=30",urlString,appDelegate.sessionUser.client_id,appDelegate.sessionUser.country];
    
    NSString *searchURL = [urlString stringByTrimmingCharactersInSet:
                           [NSCharacterSet whitespaceCharacterSet]];
    
    NSLog(@"trimmed URL: %@",searchURL);
    NSURL *url = [NSURL URLWithString:[searchURL stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    categorySearchRequest = [ASIFormDataRequest requestWithURL:url];
    [categorySearchRequest setDelegate:self];
    [categorySearchRequest startAsynchronous];
    
}
//===================================================//
//===================================================//

- (IBAction)searchButtonTapped:(id)sender
{
       // [sender resignFirstResponder];
    
    if ((selectedCategory == -1 && locationEditTextField.text.length == 0) && keywordEditTextField.text.length == 0)
    {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select a search criteria" message:@"Please select a category or enter a keyword or a location" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        
        
        return;
    }
    
    if (selectedCategory == -1 && keywordEditTextField.text.length != 0)
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Select a search criteria" message:@"Please select a category " delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
         return;

    }
    
    
        [self searchProductsOnKeywordBasis];
    [currentTextfield resignFirstResponder];
}
//===================================================//

- (IBAction)settingsButtonTapped:(id)sender {
    
    if (showingSettingsView)
    {
        // Remove the menuView
        showingSettingsView = NO;
        
        [self settingsViewClosed];
        
        [UIView animateWithDuration:0.5 animations:^{
            CGRect settingsViewFrame = settingsView.frame;
            settingsViewFrame.origin.y = toolBar.frame.origin.y;
            settingsView.frame = settingsViewFrame;
        } completion:^(BOOL finished) {
            [settingsView removeFromSuperview];
        }];
        
    }
    else
    {
        
        if ([settingsView superview])
        {
            // This means menu closing/opening animation is happening.
            // Do nothing here
            return;
        }
        
        CGRect settingsViewFrame = settingsView.frame;
        settingsViewFrame.origin.y = toolBar.frame.origin.y;
        settingsView.frame = settingsViewFrame;
        
        [self.view insertSubview:settingsView belowSubview:toolBar];

        
        settingsViewFrame.origin.y = toolBar.frame.origin.y - settingsViewFrame.size.height;
        
        [UIView animateWithDuration:0.5 animations:^{
            settingsView.frame = settingsViewFrame;
        } completion:^(BOOL finished) {
            showingSettingsView = YES;
        }];
    }
}
//===================================================//
+ (void)transitionFromView:(UIView *)fromView toView:(UIView *)toView duration:(NSTimeInterval)duration options:(UIViewAnimationOptions)options completion:(void (^)(BOOL finished))completion
{

}

- (void) settingsViewClosed {
    
        // Finally update labels
        locationLabel.text = locString;
        keywordLabel.text = keyString;
    
}
//===================================================//

- (void)showInfoView {
    
        infoView.frame = self.view.bounds;
        [self.view addSubview:infoView];
        [self.view bringSubviewToFront:infoButton];
    
}
//===================================================//

- (void)transitionToViewController:(UIViewController *)viewController
                    withTransition:(UIViewAnimationOptions)transition
{
    NSLog(@"push,.,.,.,");
    
        [UIView transitionFromView:self.view
                        toView:viewController.view
                      duration:0.15f
                       options:transition
                    completion:^(BOOL finished){
                    } ];
}
//===================================================//



- (void) dismissInfoView
{
        [self.infoView removeFromSuperview];
}

//===================================================//

- (IBAction)infoButtonTapped:(id)sender
{
    
        UIButton *infoButtonLocal = (UIButton *)sender;
        infoButtonLocal.selected = !infoButtonLocal.selected;
    
    
    if (infoButtonLocal.selected)
    {
        [self showInfoView];
        
    }
    else
    {
        [self dismissInfoView];
    }
    [currentTextfield resignFirstResponder];

}
//===================================================//

- (IBAction)resetButtonTapped:(id)sender
{
        selectedCategory = -1;
        [tblView reloadData];
    
        locationEditTextField.text = @"";
        keywordEditTextField.text = @"";
    
    
}
//===================================================//

#pragma mark - UItableview datasource methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
        return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return [categoryArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    
    if (!cell)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"cell"];
    }
    
        Category *catObj = [categoryArray objectAtIndex:indexPath.row];
    
        cell.textLabel.text = catObj.catName;
        cell.textLabel.font = [UIFont fontWithName:@"Helvetica-Bold" size:16]; //[UIFont boldSystemFontOfSize:17];
        //  cell.textLabel.font = [UIFont ]
    cell.textLabel.textColor = [UIColor colorWithRed:118/255.0 green:123/255.0 blue:127/255.0 alpha:1.0];

       
        UIImageView *radioButton = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
        radioButton.image = (selectedCategory == indexPath.row) ? [UIImage imageNamed:@"radio-button_on.png"] : [UIImage imageNamed:@"radio-button_off.png"] ;
        cell.accessoryView = radioButton;
  
    if ([catObj.catName isEqualToString:@"Automotive"])
    {
       cell.imageView.image = [UIImage imageNamed:@"automobile_map.png"];
    }
    else if ([catObj.catName isEqualToString:@"Dining & Fast Food"])
    {
        cell.imageView.image = [UIImage imageNamed:@"dining_map.png"];
    }
    else if  ([catObj.catName isEqualToString:@"Golf Courses"])
    {
       cell.imageView.image = [UIImage imageNamed:@"golf_map.png"];
    }
    else if  ([catObj.catName isEqualToString:@"Handicaps"]||[catObj.catName isEqualToString:@"Golf Handicaps"])
    {
        cell.imageView.image = [UIImage imageNamed:@"golf_map.png"];
    }
    else if  ([catObj.catName isEqualToString:@"Health & Beauty"])
    {
        cell.imageView.image = [UIImage imageNamed:@"healthAndBeauty_map.png"];
    }
    else if  ([catObj.catName isEqualToString:@"Home & Lifestyle"])
    {
         cell.imageView.image = [UIImage imageNamed:@"home_map.png"];
    }

    else if  ([catObj.catName isEqualToString:@"Insurance"]||[catObj.catName isEqualToString:@"Golf Insurance"])
    {
         cell.imageView.image = [UIImage imageNamed:@"insurance_map.png"];
    }
   else if ([catObj.catName isEqualToString:@"Leisure & Entertainment"])
    {
         cell.imageView.image = [UIImage imageNamed:@"leisure_map.png"];
    }
    else if ([catObj.catName isEqualToString:@"Shopping & Vouchers"])
    {
         cell.imageView.image = [UIImage imageNamed:@"Shopping_map.png"];
    }
    else if ([catObj.catName isEqualToString:@"Travel & Accommodation"])
    {
         cell.imageView.image = [UIImage imageNamed:@"travel_map.png"];
    }
    else
    {
        cell.imageView.image = [UIImage imageNamed:@"empty_map.png"];
    }
   // NSLog(@"catObj.catName %@ COUNT IS %d",catObj.catName,categoryArray.count);
    
  /*  switch (indexPath.row) {
        case 0:
            cell.imageView.image = [UIImage imageNamed:@"automobile_map.png"];
            break;
        case 1:
            cell.imageView.image = [UIImage imageNamed:@"dining_map.png"];
            break;
        case 2:
            cell.imageView.image = [UIImage imageNamed:@"golf_map.png"];  //Golf
            break;
        case 3:
            cell.imageView.image = [UIImage imageNamed:@"golf_map.png"];   //Handicafs
            break;
        case 4:
            cell.imageView.image = [UIImage imageNamed:@"healthAndBeauty_map.png"];
            break;
        case 5:
            cell.imageView.image = [UIImage imageNamed:@"home_map.png"];
            break;
        case 6:
            cell.imageView.image = [UIImage imageNamed:@"insurance_map.png"];   //Insurance
            break;
        case 7:
            cell.imageView.image = [UIImage imageNamed:@"leisure_map.png"];
            break;
        case 8:
            cell.imageView.image = [UIImage imageNamed:@"Shopping_map.png"];
            break;
        case 9:
            cell.imageView.image = [UIImage imageNamed:@"travel_map.png"];
            break;
            
        default:
            break;
    }*/
    
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
        return cell;
}



- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
        int prevSelection = selectedCategory;
        selectedCategory = indexPath.row;
    
    if (prevSelection != -1)
    {
        [tblView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:[NSIndexPath indexPathForRow:prevSelection inSection:0], nil] withRowAnimation:UITableViewRowAnimationFade];
    }
    
        [tblView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:indexPath, nil] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark - TextField Delegate methods

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
        [textField resignFirstResponder];
    
    
    
        [UIView animateWithDuration:0.28 animations:^{
        CGRect settingsViewFrame = settingsView.frame;
        settingsViewFrame.origin.y = self.view.frame.size.height - settingsViewFrame.size.height;
        settingsView.frame = settingsViewFrame;
        CGRect infoframe=infoButton.frame;
        infoframe.origin.y=self.view.frame.size.height - settingsViewFrame.size.height+35;
        infoButton.frame=infoframe;
    } completion:^(BOOL finished) {
        
    }];

    
        return YES;
}

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
        self.currentTextfield = textField;
        [UIView animateWithDuration:0.28 animations:^{
        CGRect settingsFrame = settingsView.frame;
        settingsFrame.origin.y = self.view.frame.size.height - Default_keyboard_height_iPhone - settingsFrame.size.height;
        settingsView.frame = settingsFrame;
        CGRect infoframe=infoButton.frame;
        infoframe.origin.y=self.view.frame.size.height - Default_keyboard_height_iPhone - settingsFrame.size.height+35;
        infoButton.frame=infoframe;
        
    }  completion:^(BOOL finished) {
        
    }];
    
    
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (textField == locationEditTextField)
    {
        locString = locationEditTextField.text;
    }
    else if (textField == keywordEditTextField)
    {
        keyString = keywordEditTextField.text;
    }
        [textField resignFirstResponder];
}


#pragma mark -- ASIHttpRequest Delegate methods

- (void)requestFinished:(ASIHTTPRequest *)request
{
    
    if (request == categoryFetchRequest)
    {
        categoryFetchRequest = nil;
        
        NSXMLParser *categoryParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        categoryParserDelegate = [[CategoryXMLParser alloc] init];
        categoryParserDelegate.delegate = self;
        categoryParser.delegate = categoryParserDelegate;
        [categoryParser parse];
    }
    else if (request == categorySearchRequest)
    {
    
        categorySearchRequest = nil;
        [currentTextfield resignFirstResponder];
        NSLog(@"ProductsList response: %@",[request responseString]);
        if([[request responseString] length] == 0)
        {
            UIAlertView *responseAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"No Results Found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [responseAlert show];
            
        }
        else
        {
        NSXMLParser *productsParser = [[NSXMLParser alloc] initWithData:[request responseData]];
        productsXMLParser = [[ProductListParser alloc] init];
        productsXMLParser.delegate = self;
        productsParser.delegate = productsXMLParser;
        [productsParser parse];
        }
        
    }
    
        [self dismissActivityView];
}

- (void)requestFailed:(ASIHTTPRequest *)request
{
    
    [currentTextfield resignFirstResponder];
    
    if (requestCancel == YES) {
        
    }
    else
    {
        if (request == categoryFetchRequest)
        {
            categoryFetchRequest = nil;
            UIAlertView *responseAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [responseAlert show];
        }
        else if (request == categorySearchRequest)
        {
            categorySearchRequest = nil;
            UIAlertView *responseAlert = [[UIAlertView alloc]initWithTitle:@"Alert" message:@"Please check network connection and try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [responseAlert show];
        }
        NSLog(@"Request Failed");
        [self dismissActivityView];
    }
    
}

#pragma mark -- Category Parser Delegate methods

- (void)parsingCategoriesFinished:(NSArray *)categoryList
{
    
    for (Category *cat in categoryList)
    {
        [categoryArray addObject:cat];
    }
    
    [tblView reloadData];
    
}

- (void)categoryXMLparsingFailed
{
    
}

#pragma mark -- Product Parser Delegate methods

- (void)parsingProductListFinished:(NSArray *)prodcutsList
{
    
    NSLog(@"parsingProductListFinished");
    
    // Here pass control to Products List view controller
    ProductListViewController *viewController = [[ProductListViewController alloc] initWithNibName:@"ProductListViewController" bundle:nil productListType:ProductListTypeSearch];
    /*  GrabItBaseViewController *base=[[GrabItBaseViewController alloc]initWithNibName:@"GrabItBaseViewController" bundle:nil title:@"Search"];
     MenuViewController *menu=[[MenuViewController alloc]initWithNibName:@"MenuViewController" bundle:nil ];
     viewController.productsList = [[NSMutableArray alloc] initWithArray:prodcutsList];
     [self transitionFromViewController:menu toViewController:viewController duration:0.03 options:UIViewAnimationOptionCurveEaseIn animations:nil completion:^(BOOL finished) {
     
     }];
     
     
     [UIView transitionFromView:self.view
     toView:viewController.view
     duration:0.15f
     options:UIViewAnimationOptionCurveLinear
     completion:^(BOOL finished){
     } ];*/
    
    
    //[self slideInView:self.view toView:viewController];
    //[self replaceView:self.view withView:viewController];
    //  [self presentViewController:viewController animated:YES completion:^(BOOL finished){
    // }];
    
/* [self presentViewController:viewController animated:NO completion:^{
        CGRect frame=viewController.view.frame;
        frame.origin.y=0;
        viewController.view.frame=frame;
        
        
    }];*/
    productsListController = viewController;
    [self.navigationController pushViewController:viewController animated:YES  ];
    [productsListController setInitialProductsList:prodcutsList];
    
    CGRect frame=viewController.view.frame;
    frame.origin.y=200;
    viewController.view.frame=frame;
    
    Category *cat = [categoryArray objectAtIndex:selectedCategory];
    [productsListController setSearchCategoryID:cat.catId keyword:keyString location:locString];
    
}

- (void)parsingProductListXMLFailed
{
    
}
-(void)replaceView:(UIView*)oldSubView withView:(UIView*)newSubView{



}

-(void)slideInView:(UIView*)newView toView:(UIView*)oldView {
    /* Sets the view outside the right edge and adds it to mainView */
    newView.bounds = self.view.bounds;
    newView.frame = CGRectMake(newView.frame.origin.x + 784, newView.frame.origin.y, newView.frame.size.width, newView.frame.size.height);
    [self.view addSubview:newView];
    
    /* Begin transition */
   // [UIView beginAnimations:@"Slide Left" context:self.view];
    [UIView setAnimationDelegate:self];
    [UIView setAnimationDelay:0.0f];
    [UIView setAnimationDuration:0.5f];
    [UIView setAnimationDidStopSelector:@selector(slideInCompleted:finished:context:)];
    [UIView setAnimationCurve:UIViewAnimationCurveEaseOut];
    newView.frame = CGRectMake(0,0, newView.frame.size.width, newView.frame.size.height);
    oldView.frame = CGRectMake(oldView.frame.origin.x - 784, oldView.frame.origin.y, oldView.frame.size.width, oldView.frame.size.height);
    [UIView commitAnimations];
}

-(void)slideInCompleted:(NSString *)animationID finished:(NSNumber *)finished context:(void *)context {
   // UIView *view = (UIView*)context;
    [self.view removeFromSuperview];
}
@end
